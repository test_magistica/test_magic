<?php

error_reporting(E_ALL);
ini_set('display_errors', 'on');

$yii = '../../framework/yii.php';

require_once($yii);

$conf = include('../../protected/config/main.php');

try {
    $conn = new PDO($conf['components']['db']['connectionString'], $conf['components']['db']['username'], $conf['components']['db']['password']);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $data = $conn->query('DROP TABLE `users`');

    foreach ($data as $row) {
        print_r($row);
    }
} catch (PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
}
?>