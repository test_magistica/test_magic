<?php
return array(
    'More' => "Узнать больше ...",
    "People have long sought to reveal the secret veil of time , and look into the future . At first this was only public affairs : shamans and sorcerers danced around the campfire and went into the woods to listen to the spirits in order to know when the rains come , there will be the next successful hunt . Passion for divination became increasingly popular , and later every person has wished himself to understand whether he chose the second half, not whether it zagryzet jaguar in the jungle, if the snake did not bite the child. Why do people want to understand the future? This is an opportunity to prepare , and thus completely avoid any problems in life , it is an opportunity in advance to make the right choice. It is a way to adjust itself under upcoming events . We hope that our online divination will help you sort out your thoughts and calculate possible scenarios . Predictions of this section, you can use every day. As a rule, they give a small forecast night , at least - for a longer period . You are granted a great way to learn what to expect today that the fate in store for you and will come true if what you want in the near future . But do not apply to these predictions lightly , they are as useful as the more complex divination." =>
    "Люди издавна стремились приоткрыть секретную завесу времени, и заглянуть в будущее. "
    . "Поначалу это касалось только общественных дел: шаманы и колдуны плясали у костра и ходили в леса слушать духов, для того, "
    . "чтобы узнать, когда же пойдут дожди, удачной ли будет следующая охота. "
    . "Увлечение гаданием становилось все более востребованным, и позже уже каждый человек желал сам"
    . " понимать, правильно ли он выбрал вторую половинку, не загрызет ли его ягуар в джунглях,"
    . " не укусит ли змея ребенка. Зачем человек хотел понимать свое будущее? Это возможность "
    . "подготовиться, а значит и полностью избежать каких-либо жизненных проблем, это возможность "
    . "заранее сделать правильный выбор. Это способ подстроить себя под грядущие события."
    . " Надеемся, что наши гадания онлайн помогут Вам разобраться в своих мыслях и просчитать "
    . "возможные варианты развития событий. Предсказания из данного раздела вы можете использовать "
    . "каждый день. Как правило, они дают небольшой прогноз на сутки, реже – на больший период. "
    . "Вам предоставляется отличный способ узнать, что ожидает Вас сегодня, что готовит Вам судьба,"
    . " и сбудется ли то, о чем Вы мечтаете, в ближайшее время. Но не стоит относится к данным "
    . "предсказания несерьезно, они столь же полезны, как и более сложные прорицания."
);