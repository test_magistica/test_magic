<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    
    /**
     *
     * @var type string for seo title default for pages
     */
    public $pageTitle='Гадание онлайн по телефону. Экстрасенсы онлайн консультации, ясновидящих с мобильного 0913 доб 99. magistika.com';
    
    /**
     *
     * @var type string for seo description default for pages
     */
    public $pageDescription='Гадание по телефону онлайн. Консультации Экстрасенсов, Астрологов, Тарологов, Гадания на ТАРО. Сайт Магистика. Позвонить гадалке 8-809-505-6747';

    /**
     *
     * @var type string for seo keywords default for pages
     */
    public $pageKeywords = array( 'гадание по телефону онлайн', 
                              'консультация',
                              'экстрасенсы', 
                              'ясновидящие', 
                              'карты таро',
                              'узнать будущее', 
                              'гороскоп на сегодня онлайн', 
                              'гадание по фото' );
    

    public function sendMailUser($id, $title, $msg) {
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer();
        $mail->SetFrom('no-reply@magistika.com', 'magistika.com');//
        $mail->Subject = $title;
        $mail->AltBody = '';
        $mail->CharSet = 'UTF-8';
        $mail->MsgHTML($msg);
        $mail->AddAddress( intval($id) ? $this->getUser($id, 'email') : $id, intval($id) ? $this->getUser($id, 'name') : '');
        if($mail->Send())
            return TRUE;
	    else
		    return FALSE;
    }

    public function getCHPU($string) {
        return Article::model()->translit($string);
    }

    public function getDate($date) {
        $ar = Yii::app()->locale->getMonthNames();
        $result = date('d', strtotime($date)) . ' ' . $ar[date('n', strtotime($date))];
        return $result;
    }

    public function getMonthYear($m = 4) {
        $curDate = date('Y-m-d');
        $toDate = date("Y-m-d", mktime(0, 0, 0, date("m") - $m, date("d"), date("Y")));
        $datesArray = $this->dateArray($toDate, $curDate, true);

        return $datesArray;
    }

    function dateArray($from, $to, $value = NULL) {
        $begin = new DateTime($from);
        $end = new DateTime($to);
        $interval = DateInterval::createFromDateString('1 month');
        $days = new DatePeriod($begin, $interval, $end);

        $baseArray = array();
        foreach ($days as $day) {
            $dateKey = $day->format("Y-m-d");
            $baseArray[$dateKey] = $this->getNumDate($dateKey);
        }
        $baseArray[date('Y-m-d')] = $this->getNumDate(date('Y-m-d'));

        return $baseArray;
    }

    public function getNumDate($date) {
        $ar = Yii::app()->locale->getMonthNames($width = 'wide', $standAlone = true);
        $result = $ar[date('n', strtotime($date))] . ' ' . date('Y', strtotime($date));
        return $result;
    }

    public function getYear() {
        $curDate = date('Y');
        $date1 = $curDate - 18;
        $date2 = $curDate - 85;
        for ($a = $date2; $a <= $date1; $a++) {
            $result[$a] = $a;
        }
        krsort($result);
        return $result;
    }

    public function getDay() {
        $result[''] = '';
        for ($a = 1; $a <= 31; $a++) {
            $result[$a] = $a;
        }
        return $result;
    }

    public function getMonth() {
        return Yii::app()->locale->getMonthNames($width = 'wide', $standAlone = TRUE);
    }

    public static function getCountry() {
        return array('+7' => 'Россия','+380' => 'Украина', '+49' => 'Германия', '+972' => 'Израиль', '+1' => 'США, Канада','+39' => 'Италия','+48' => 'Польша','+371' => 'Латвия','+44' => 'UK','+34' => 'Испания','+370' => 'Литва');
    }

    public static function getUser($id, $attr) {
        $model = Users::model()->findByPk($id);
        return $model[$attr];
    }

    public function getLastTopic($toID) {
        $rows = Yii::app()->db->createCommand('SELECT topic FROM `message` WHERE `toID` = ' . $toID . ' ORDER BY `id` DESC LIMIT 1')->queryAll();
        foreach ($rows as $row)
            return $row['topic'];
    }

    public function getUserImage($userID, $forWhat = FALSE) {
        $image = Userphoto::model()->find("userID = '" . $userID . "' AND main=1");
        if($forWhat === FALSE){
            $image = !empty($image['image']) ? $image['image'] : 'nophoto.jpg';
        }
        elseif ($forWhat == "question") {
            $image = !empty($image['image']) ? $image['image'] : 'nophoto_question.jpg';
        }
        return $image;
    }

    public static function getMessageCount() {
        $rows = Yii::app()->db->createCommand("SELECT id FROM `message`  WHERE `toID` = '" . (int) Yii::app()->user->id . "' AND `userID` != '" . (int) Yii::app()->user->id . "' AND status = 0")->queryAll();
        return count($rows);
    }

    public static function getBookingCount() {
        $rows = Yii::app()->db->createCommand("SELECT id FROM `servicemessage`  WHERE `fromID` = '" . (int) Yii::app()->user->id . "' AND `userID` != '" . (int) Yii::app()->user->id . "' AND status = 0")->queryAll();
        return count($rows);
    }

    public static function getBookingCountExpert() {
        $rows = Yii::app()->db->createCommand("SELECT id FROM `servicemessage`  WHERE `toID` = '" . (int) Yii::app()->user->id . "' AND `userID` != '" . (int) Yii::app()->user->id . "' AND status = 0")->queryAll();
        return count($rows);
    }

    public static function checkBooking($id) {
        $rows = Yii::app()->db->createCommand("SELECT id FROM `servicemessage`  WHERE `fromID` = '" . (int) Yii::app()->user->id . "' AND `userID` != '" . (int) Yii::app()->user->id . "' AND bookingID = '" . $id . "' AND status = 0")->queryAll();
        return count($rows);
    }

    public static function checkUserBill($userID, $amount) {
        $user = Users::model()->findByPk((int) $userID);
        return $user->bill >= $amount ? TRUE : FALSE;
    }

	public static function takeUserBill($userID, $amount) {
		$user = Users::model()->findByPk((int) $userID);
		$amount = $user->bill - $amount;
		return Yii::app()->db->createCommand("UPDATE users SET bill='{$amount}' WHERE id='{$userID}'")->query();
	}

	public static function updateUserBill($userID, $amount) {
		$user = Users::model()->findByPk($userID);
		$amount = $user->bill + $amount;
		return Yii::app()->db->createCommand("UPDATE users SET bill='{$amount}' WHERE id='{$userID}'")->query();
	}

    public function BuildUrlParamsWithSignature($params, $password) {
        ksort($params);

        $url = '';

        if (!is_array($params))
            return $url;

        foreach ($params as $key => $value) {
            $url .= $key . "=" . urlencode($value) . "&";
        }

        $signature = md5($url . "&password=" . urlencode($password));

        $url .= "signature=" . $signature;

        return $url;
    }

    public function getUserAge($id){
        $user = Users::model()->findByPk((int) $id);
        if ($user) { 
            $birthday = explode('.', $user['birthday']);
            //echo $user['birthday'];
            //echo date('Y').' - '.$birthday[2];
            $age = date('Y') - $birthday[2];
        }else{
            $age=null;
        }    
        return $age;
    }

    public function getLastExpertReview($id) {
        $review = Expertreviews::model()->find("expertID = '$id' and status = 1 order by id desc");
        $user = $this->getUser($review['userID'], 'name');
        return '<b>' . $user . ' ' . $this->getUserAge(/*$id*/$review['userID']) . ' лет пишет:&nbsp;</b>' . '"' . $review['description'] . '"';
    }

    public function getExpertReviewsCount($id) {
        return Expertreviews::model()->count("expertID = '$id' and status = 1");
    }

    public function addCallBack($data) {
        $query = 'INSERT INTO `call_back` (';
        $q = '';
        
        $expert = Users::model()->findByPk($data['user_id']);
	    if ($expert->sip_tel == 'sip')
	        if ($expert->sip)
	    	    $data['from_num'] = $expert->sip;
		else 
		    $data['from_num'] = $expert->tele;
	    else 
		$data['from_num'] = $expert->tele; 
        
        
        foreach ($data as $key => $value) {
            $query .= "`$key`,";
            $q .= "'$value',";
        }

        $l1 = strlen($query);
        $query = substr($query, 0, $l1 - 1);

        $l2 = strlen($q);
        $q = substr($q, 0, $l2 - 1);

        $query .= ")VALUES ($q)";

        Yii::app()->db->createCommand($query)->query();
        return TRUE;
    }

    public function getAlerts($alert) {
        switch ($alert) {
            case 2:
                $alert = "Спасибо! Ваш отзыв добавлен!";
                break;
            case 3:
                $alert = "Спасибо! Ваша фотография отправлена эксперту!";
                break;
            case 4:
                $alert = "Ожидайте вызов от эксперта, через несколько секунд... Пожалуйста оставьте свой отзыв эксперту, после консультации.";
                break;
            case 5:
                $alert = "Ожидайте вызов от эксперта в назначенное время (время московское)";
                break;
            case 6:
                $alert = "Пожалуйста введите комментарий к фото и задайте вопрос эксперту!";
                break;
            case 7:
                $alert = "В заданное вами время этот эксперт не работает!";
                break;
            case 8:
                $alert = "Невозможно заказать звонок более чем на неделю вперед!";
                break;
            case 9:
                $alert = "Сейчас эксперт не доступен!";
                break;
            case 10:
                $alert = "Сейчас эксперт занят!";
                break;
        }
        return $alert;
    }

    public function getMonthForConsultation() {
        $month = array();
        for ($m = 0; $m <= 1; $m++) {
            $month[date('m', mktime(0, 0, 0, date("m") + $m, date("d"), date("Y")))] = $this->getNumDate(date('Y-m-d', mktime(0, 0, 0, date("m") + $m, date("d"), date("Y"))));
        }
        return $month;
    }

    public function getDayForConsultation() {
        $day = array();
        for ($d = 1; $d <= 31; $d++) {
            $day[sprintf("%02s", $d)] = sprintf("%02s", $d);
        }
        return $day;
    }

    public function getHourForConsultation() {
        $hour = array();
        for ($h = 1; $h <= 24; $h++) {
            $hour[sprintf("%02s", $h)] = sprintf("%02s", $h);
        }
        return $hour;
    }

    public function getMinuteForConsultation() {
        $minute = array();
        for ($m = 0; $m <= 59; $m++) {
            $minute[sprintf("%02s", $m)] = sprintf("%02s", $m);
        }
        return $minute;
    }
        public function getNameStatus($status)
    {
	switch($status)
        {
            case '1':
        	break;
            case 'BREAK':
                echo "Звонок сорвался.<br/>";
                break;
            case '3':
        	echo "Удалено экспертом";
            case 'ANSWER':
    	        echo "Отвечено<br/>";
    	        break;
    	    case 'BEGIN':
    	        echo "В процессе<br/>";
    	        break;
    	    case 'CANCEL':
    		echo "Отменен";
    		break;
    	    case 'BUSY':
    		echo "Занят/Отклонен";
    		break;
    	    case 'NOANSWER':
        	echo "Нет ответа";
        	break;
	    default:
	        echo "Неизвестный статус<br/>";
	        break;
        };
    }
    /**
     * Если эксперт не занят, то возвращается URL на чат иначе FALSE
     * @param \Users $expert expert id
     * @return mixed URL - string или false
     */
    public function getUrlToExpertChat(Users $expert) {
        $isBusy = (int)Yii::app()->db->createCommand("select count(1) from chats where expert_id=".$expert->id." and end_date IS NULL AND countdown_date IS NOT NULL")->queryScalar();
        if($isBusy == 0){
            return Yii::app()->createUrl("/chat/Chat/startChat", array('id' => $expert->id));
        }else{
            return FALSE;
        }
    }

}