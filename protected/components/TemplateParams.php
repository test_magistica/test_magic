<?php

class TemplateParams extends CComponent
{
	private $_params = array('subject', 'body');


	public function getSubjectParams()
	{
		return isset($this->_params['subject']) ? $this->_params['subject'] : array();
	}

	public function getBodyParams()
	{
		return isset($this->_params['body']) ? $this->_params['body'] : array();
	}


	public function setSubjectParam($name, $value)
	{
		$this->_params['subject'][$name] = $value;
	}

	public function setSubjectParams($params)
	{
		foreach ($params as $key=>$value)
			$this->_params['subject'][$key] = $value;
	}


	public function setBodyParam($name, $value)
	{
		$this->_params['body'][$name] = $value;
	}

	public function setBodyParams($params)
	{
		foreach ($params as $key=>$value)
			$this->_params['body'][$key] = $value;
	}
}
