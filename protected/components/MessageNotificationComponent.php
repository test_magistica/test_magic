<?php
/**
 * Компонент для сбора сообщений, которые ноебходимо показать пользователю.
 */
class MessageNotificationComponent extends CComponent {

    private $messages = array();
    private $sessionKey = "mn_widget";

    public function __construct() {
        if (isset(Yii::app()->session[$this->sessionKey])) {
            $this->messages = Yii::app()->session[$this->sessionKey];
        } else {
            Yii::app()->session[$this->sessionKey] = $this->messages;
        }
    }

    public function getSessionKey() {
        return $this->sessionKey;
    }

    /**
     * Добавляет сообщение в коллекцию сообщений.
     * @param string $msgId Ключ, который идентифицирует данное сообщение в коллекции.
     * @param string $msg Сообщение
     */
    public function addMessage($msgId, $msg) {
        if (isset($msgId) && !empty($msg)) {
            $this->messages[$msgId] = $msg;
            $this->rewriteSession();
        }
    }

    public function removeMessage($msgId) {
        if (array_key_exists($msgId, $this->messages)) {
            unset($this->messages[$msgId]);
            $this->rewriteSession();
        } else {
            throw new CException("Can not remove message.");
        }
    }

    public function getMessage($msgId) {
        if (array_key_exists($msgId, $this->messages)) {
            return $this->messages[$msgId];
        }
    }
    /**
     * Возвращает javascript объект с сообщениями.
     * @return string
     */
    public function getJSObjectData() {
        $data = "var " . $this->sessionKey . " = {";
        if (count($this->messages) > 0) {
            
            foreach ($this->messages as $msgId => $msg) {
                $data .= $msgId.":\"".$msg."\",";
            }
        }
        $data .= "};";
        $this->clearMessages();
        return $data;
    }

    private function rewriteSession() {
        Yii::app()->session[$this->sessionKey] = $this->messages;
    }
    
    private function clearMessages(){
        $this->messages = array();
        Yii::app()->session->remove($this->sessionKey);
    }

}
