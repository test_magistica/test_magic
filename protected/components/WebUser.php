<?php

class WebUser extends CWebUser {
    private $_model = null;
 
    function getRole() {
        if($user = $this->getModel()){
            // в таблице User есть поле role
            return $user->role;
        }
    }
    public function getChat_available() {
        if($user = $this->getModel()){
            // в таблице User есть поле chat_available
            return $user->chat_available;
        }
    }
    /**
     * Бесплатные минуты для чата
     * @return int Количество бесплатных минут
     */
    public function getFreeChatMinutes() {
        if($user = $this->getModel()){
            // в таблице User есть поле free_chat_minutes
            return $user->free_chat_minutes;
        }
    }
 
    public function getModel(){
        if (!$this->isGuest && $this->_model === null){
            $this->_model = Users::model()->findByPk($this->id);//, array('select' => 'role'));
        }
        return $this->_model;
    }
}
?>
