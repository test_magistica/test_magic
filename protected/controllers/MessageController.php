<?php

class MessageController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'index', 'view', 'check'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionCreate() {
        $model = new Message();
        $model->scenario = 'create';
        $this->performAjaxValidation($model);
        if (isset($_POST['Message'])) {
            $model->attributes = $_POST['Message'];
//            if ()
//            {
//
//            }
            if ($model->save())
                $this->redirect(array('/message/'));
        }
        $this->render('create', array('model' => $model));
    }

    public function actionIndex() {

        $criteria = new CDbCriteria();
        $criteria->order = 'status desc';
        $criteria->group = 'toID asc';
        $criteria->compare('fromID', Yii::app()->user->id);
        $count = Message::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 15;
        $pages->applyLimit($criteria);
        $models = Message::model()->findAll($criteria);

        $this->render('index', array('models' => $models,
            'pages' => $pages));
    }

    public function actionView($id) {
        $model = new Message();
        $model->scenario = 'expert';
        $this->performAjaxValidation2($model);
        if (isset($_POST['Message'])) {
            $model->attributes = $_POST['Message'];
            if ($model->save())
                $this->refresh();
        }
        $criteria = new CDbCriteria();
        $criteria->order = 'id desc';
        $criteria->compare('fromID', (int) Yii::app()->user->id);
        $criteria->compare('toID', (int) $id);
        $count = Message::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 15;
        $pages->applyLimit($criteria);
        $models = Message::model()->findAll($criteria);
        Yii::app()->db->createCommand("UPDATE `message` SET `status` = '1' WHERE `fromID` ='" . (int) Yii::app()->user->id . "' AND `toID` ='" . (int) $id . "' AND `userID` = '" . (int) $id . "'")->query();
        $this->render('view', array(
            'models' => $models, 'pages' => $pages, 'model' => $model, 'count' => $count
        ));
    }

    public function loadModel($id) {
        $model = Message::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation2($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'register-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'register-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionCheck() {
        if (isset($_POST['check'])) {
            $rows = Yii::app()->db->createCommand("SELECT id FROM `message`  WHERE `toID` = '" . (int) Yii::app()->user->id . "' AND `userID` != '" . (int) Yii::app()->user->id . "' AND status = 0")->queryAll();
            echo count($rows);
        }

    }

    public function actionCheck2() {
        if (isset($_POST['check'])) {
            $rows = Yii::app()->db->createCommand("SELECT id FROM `message`  WHERE `fromID` = '" . (int) Yii::app()->user->id . "' AND `userID` != '" . (int) Yii::app()->user->id . "' AND status = 0")->queryAll();
            echo count($rows);
        }

    }

}