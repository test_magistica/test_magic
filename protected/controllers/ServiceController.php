<?php

class ServiceController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations            
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('view', 'index'),
                'users' => array('*'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('view', 'index'),
                'users' => array('@'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('booking', 'upload'),
                'users' => array('user'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
                'message' => "это функция предусмотрена только для клиентов",
            ),
        );
    }

    public function actions() {
        return array(
            'upload' => array(
                'class' => 'ext.xupload_1.actions.XUploadAction',
                'path' => Yii::app()->getBasePath() . "/../uploads",
                'publicPath' => Yii::app()->getBaseUrl() . "/uploads",
            ),
        );
    }

    public function actionView($id) {
        $model = $this->loadModel($id);
        if (isset($_GET['booking']) && is_numeric($_GET['booking'])) {
            $this->redirect(array('/service/booking/', 'booking' => $id));
        }
        $c = new CDbCriteria();
        $c->order = 'id desc';
        $c->compare('serviceID', $model->id);

        $reviews = Servicereviews::model()->findAll($c);

        $r = new Servicereviews();

        if (isset($_POST['Servicereviews'])) {
            if (Yii::app()->user->isGuest)
                Yii::app()->user->loginRequired();
            $r->attributes = $_POST['Servicereviews'];
            if ($r->validate()) {
                $r->save();
                $this->refresh();
            }
        }

        $this->render('view', array(
            'model' => $model, 'reviews' => $reviews, 'r' => $r
        ));
    }

	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'register-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionBooking($id) {
        $booking = new Booking;
        $service = Service::model()->findByPk($id);
        $message = new Servicemessage();
	    $this->performAjaxValidation($message);
        if (isset($_POST['Servicemessage'])) {
            $modelS = $this->loadModel($id);
            if ($this->checkUserBill(Yii::app()->user->id, $modelS->honorarium)) {
                $booking->userID = Yii::app()->user->id;
                $booking->expertID = $service->expertID;
                $booking->serviceID = (int) $id;
                $booking->type = 'Услуга';
                $booking->createTime = new CDbExpression('NOW()');
                if ($booking->save(false)) {
                    $message->message = $_POST['Servicemessage']['message'];
                    $message->fromID = Yii::app()->user->id;
                    $message->toID = $service->expertID;
                    $message->userID = Yii::app()->user->id;
                    $message->createTime = new CDbExpression('NOW()');
                    $message->status = 0;
                    $message->bookingID = $booking->id;
                    if ($message->save()) {
                        $this->takeUserBill(Yii::app()->user->id, $modelS->honorarium);
	                    $expImg = Controller::getUserImage($service->expertID);
	                    $params = array(
		                    'userName'          => $this->getUser(Yii::app()->user->id, 'name') . ' ' . $this->getUser(Yii::app()->user->id, 'surname'),
		                    'orderNum'          => $service->id,
		                    'service'           => $service->notes,
		                    'expertName'        => $this->getUser($service->expertID, 'name'),
		                    'expertSurname'     => $this->getUser($service->expertID, 'surname'),
		                    'expertProfession'  => $this->getUser($service->expertID, 'profession'),
		                    'expertHref'        => "http://{$_SERVER['HTTP_HOST']}/extrasens/view/{$service->expertID}",
		                    'expertImage'       => "http://{$_SERVER['HTTP_HOST']}/uploads/user/{$expImg}",
	                    );

	                    $tp = new TemplateParams();
	                    $tp->setSubjectParams($params);
	                    $tp->setBodyParams($params);
	                    $mt = EmailTemplates::parseTemplate(Yii::app()->params['paymentServices']['photoGuess']['keyPayment'], $tp);

	                    Controller::sendMailUser($this->getUser(Yii::app()->user->id, 'email'), $mt->parsedSubject, $mt->parsedBody);

                        Yii::app()->user->setFlash('booking', 'Ваш заказ принят.');
                        $this->redirect(array('/service/booking/', 'id' => $id));
                    }
                }
            } else {
                $this->redirect(array('/payment'));
            }
        }
        $model = new XUploadForm;


        $this->render('booking', array('model' => $model, 'booking' => $booking, 'id' => $id, 'servive' => $service, 'message' => $message));
    }

    public function loadModel($id) {
        $model = Service::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionIndex() {

        $criteria = new CDbCriteria();
        $criteria->order = 'id desc';
        $criteria->compare('status', 1);
        if (isset($_GET['categoryID']))
            $criteria->compare('categoryID', $_GET['categoryID']);
        $count = (int) Service::model()->count($criteria);
        if ($count > 0) {
            $pages = new CPagination($count);
            $pages->pageSize = 15;
            $pages->applyLimit($criteria);
            $models = Service::model()->findAll($criteria);

            $this->render('index', array('models' => $models,
                'pages' => $pages));
        } else {
            throw new CHttpException(404, Yii::t("app", "Category of the service not found"));
        }
    }

}
