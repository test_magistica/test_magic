<?php

class SuccessController extends Controller {

	public function actionIndex() {
		if ((isset($_GET['InvId'])) AND (isset($_GET['SignatureValue'])) AND (isset($_GET['OutSum']))) {
			$criteria = new CDbCriteria();
			$criteria->compare('id', intval($_GET['InvId']));
			$criteria->compare('status', 'expectations');

			$model = Payments::model()->find($criteria);

			$pay_log = new Payments_log;

			if ($model['id'])
			{
				$out_sum = $_GET['OutSum'];
				$id = $model['id'];
				$shp_Item = $model['shpItem'];
				$mrh_pass1 = 'bohPaej0th'; //пароль1
				$model_crc = md5 ($out_sum.':'.$id.':'.$mrh_pass1.':shpItem='.$shp_Item);

				if ($model_crc == $_GET['SignatureValue']){

					Yii::app()->db->createCommand("UPDATE `payments` SET `status` = 'success' WHERE `id` = '" . (int) $model['id'] . "'")->query();

					if ($model['InvId'] > 0){
						// Controller::updateUserBill((int) $model['InvId'], $model['OutSum']);
						Yii::app()->user->setFlash('success', 'Платеж направлен, ожидайте, в ближайшее время баланс будет пополнен.<br/>'.
							'Если баланс не пополнен, обратитесь к администратору.');
					}
					else {
						//гость
						$guest = Guest::model()->findByPk($shp_Item);
						$criteria = new CDbCriteria();
						$criteria->compare('id', $guest->consultation);
						$criteria->compare('status', 'nopaid');
						if ($consult = Consultation::model()->find($criteria)){
							$consult->status = 'paid';
							$consult->save();

							Yii::app()->user->setFlash('success', 'Ваша консультация номер: '.$consult['id'].' будет активирована, как поступит платеж.<br/>'.
								'Если в назначенное время эксперт с вами не связался, обратитесь к администратору.');

							$img_name = trim($consult['image']) != '' ? $consult['image'] : 'nofoto.gif';
							$params = array(
								'imgSrc'            => "http://{$_SERVER['HTTP_HOST']}/uploads/consultation/{$img_name}",
								'countMin'          => $consult['duration']/60,
								'reservedDateTime'  => date('d.m.Y h:i',  strtotime($consult['date'])),
								'guestName'         => $consult['guest_name']
							);
							$tp = new TemplateParams();
							$tp->setBodyParams($params);
							$mt = EmailTemplates::parseTemplate('toExpertMsgAboutConsultGuest', $tp);
						}
						else {
							$msg = 'Error pay-consult.No search consult with status=nopaid, id='.$guest->consultation;
							$pay_log->id_pay = $_GET['InvId'];
							$pay_log->message = $msg;
							$pay_log->save();

							Yii::app()->user->setFlash('success', 'Ваш платеж направлен, но консультации не обнаружено.<br/>'.
								'Oбратитесь к администратору. Код сообщения: '.$pay_log->id);
						}
					}
				}
				else {
					$msg = 'Error success-pay.Not equal CRC.'.$model_crc.'get:'.$_GET['SignatureValue'];
					$pay_log->id_pay = $_GET['InvId'];
					$pay_log->message = $msg;
					$pay_log->save();
				}
			}
			else {
				Yii::app()->user->setFlash('success', "Запрос не выполнен обратитесь к администратору код ошибки #4");
				$msg = 'Error success-pay.Not search `expectations`-status where payments.id='.$_GET['InvId'];
				$pay_log->id_pay = $_GET['InvId'];
				$pay_log->message = $msg;
				$pay_log->save();
			}
			$this->redirect(array('/success'));
		}
		$this->render('index');
	}
}