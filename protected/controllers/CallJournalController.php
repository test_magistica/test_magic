<?php
/*
    Для гет запросов
    звонок начался
     statcall = on или off
        id_line - id связки в call_back
        begin_call
        t_client
        t_expert

    закончился
     statcall = on или off
     id_line - id связки в call_back
     dur
     dialstatus

*/
class CallJournalController extends Controller {

    public function actionIndex()
    {
    if (isset($_GET['statcall']))// статус звонка
    {
        switch($_GET['statcall'])
        {
        case 'on': //звонок начался
        {

            $call_back = CallBack::model()->findByPk($_GET['id_line']);
            $expert = Users::model()->findByPk($call_back->user_id);

            // Создаем запись в журнале
            $cj = new CallJournal;
            Yii::app()->db->createCommand("UPDATE `call_back` SET `check` = 1 WHERE `id` = '" . (int) $_GET['id_line'] . "'")->query();
            if (!empty($call_back->cbk_id))
            {
                // обновляем статус консультации
                $consult = Consultation::model()->findByPk($call_back->cbk_id);
                $consult->status = 'BEGIN';
                $consult->save();

                //Yii::app()->db->createCommand("UPDATE `consultation` SET `status` = 'BEGIN' WHERE `id` = '" . (int) $call_back->cbk_id . "'")->query();

                $cj->caller_id = $call_back->user_id ;
                if ($consult->userID > 0)
                $cj->called_id = $consult->userID;
                else
                //$guest = Guest::model()->find('consultation=:consultation',
                //    array(':consultation',$consult->id));
                $cj->called_id = 0;//$guest->id;

            }
            else
            {
                $cj->caller_id = $call_back->client_id ;
                $cj->called_id = $call_back->user_id;
            };

            $cj->status = 'BEGIN';
            $cj->tel_client = $_GET['t_client'];
            $cj->tel_expert = $_GET['t_expert'];
            $date = $_GET['begin_call'];
            $cj->call_date = new CDbExpression('FROM_UNIXTIME('.$date.')'); // дата звонка
            $cj->id_callback = $_GET['id_line'];
            $cj->id_call = $_GET['id_call'];
            $cj->save();

            if($expert->online != 'autonomous')
            {
                Yii::app()->db->createCommand("UPDATE `users` SET `online` = 'busy' WHERE `id` = '" . (int) $expert->id . "'")->query();
                print 'Статус у пользователя '.$expert->name.' обновился. ';
            }
            else
            {
                print 'Пользователь '.$expert->name.' оффлайн. ';
            };
            break;
        }
        case 'off': //звонок закончился
        {
            $id_call = $_GET['id_call'];

            // Обновляем запись в журнале
            $criteria = new CDbCriteria;
            $criteria->compare('id_call',$id_call);

            $cj = CallJournal::model()->find($criteria);

            $call_back = CallBack::model()->findByPk($cj->id_callback);

            $expert = Users::model()->findByPk($call_back->user_id);

            if ($expert->online != 'autonomous')
            {
                Yii::app()->db->createCommand("UPDATE `users` SET `online` = 'online' WHERE `id` = '" . (int) $expert->id . "'")->query();
                print 'Статус у '.$expert->name.'обновился';
            }
            else
            {
                print 'Пользователь '.$expert->name.' оффлайн.';
            };


            $dur = $_GET['dur'];
            $dialstatus = $_GET['dialstatus'];

            if ($dur > 3)// тарификация с 3 секунды
            $res_min = ceil($dur/60);
            else
            {
            $res_min = 0;
            if ($dialstatus != 'ANSWER')
            {
                $dur = 0;
            }
            else
            {
                $dialstatus = 'BREAK'; // сорвался
            };
            };
            $cj->duration = $dur;

            if ( ! empty($call_back->cbk_id) )// значит консультация
            {
                $consult = Consultation::model()->findByPk($call_back->cbk_id);

                //$client = Users::model()->findByPk($consult->userID);

                //возвращение средств клиенту
                // $returned = $consult->cost - ($res_min * $expert->tariff);
                //$bill = $client->bill + $returned;

                // обновляем консультацию
                Yii::app()->db->createCommand("UPDATE `consultation` SET `status` = '".$dialstatus."' WHERE `id` = '" . (int) $call_back->cbk_id . "'")->query();
            //$consult->status = $dialstatus;
            //$consult->save();
            }
            else
            {
                $client = Users::model()->findByPk($call_back->client_id);
                $bill = $client->bill - ($res_min * $expert->tariff);

                // обновляем баланс клиента
                Yii::app()->db->createCommand("UPDATE `users` SET `bill` = '$bill' WHERE `id` = '" . (int) $client->id . "'")->query();
            };

            $cj->cost = $res_min * $expert->tariff;

            if (! empty($expert->ratio))
            $cj->profit_expert = $expert->ratio * ($res_min * $expert->tariff);
            else
            $cj->profit_expert = $res_min * $expert->tariff;

            $cj->status = $dialstatus;

            $cj->save();
            break;
        }
        }

    }
    else
    {
        print 'Неизвестный запрос';
    };
    }

}