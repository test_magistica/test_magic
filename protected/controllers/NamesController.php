<?php

class NamesController extends Controller {

    public $_characters = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У',
        'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ь', 'Ю', 'Я');

    public function actionIndex() {

        $this->render('index', array('characters' => $this->_characters));
    }

    public function actionMan() {
        if (isset($_GET['character']) && in_array($_GET['character'], $this->_characters)) {
            $models = ManNames::model()->findAll("`name` LIKE '" . $_GET['character'] . "%'");
            $this->render('index', array('characters' => $this->_characters, 'models' => $models));
        } else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionWoman() {
        if (isset($_GET['character']) && in_array($_GET['character'], $this->_characters)) {
            $models = WomanNames::model()->findAll("`name` LIKE '" . $_GET['character'] . "%'");
            $this->render('index', array('characters' => $this->_characters, 'models' => $models));
        } else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionView($id) {
        if (isset($_GET['t']) && in_array($_GET['t'], array('woman', 'man'))) {
            $m = ucfirst($_GET['t']) . 'Names';
            $model = $m::model()->findByPk($id);
            $this->render('view', array(
                'model' => $model, 'characters' => $this->_characters
            ));
        } else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionSearch() {
        if (isset($_POST['q'])) {
            if (empty($_POST['q']))
                $this->redirect(array('names/index'));
            $m = isset($_POST['man']) ? 'ManNames' : 'WomanNames';
            $m2 = isset($_POST['man']) ? 'man' : 'woman';
            $models = $m::model()->findAll("`name` LIKE '%" . $_POST['q'] . "%' ORDER BY `name` ASC LIMIT 100");
            $this->render('index', array('characters' => $this->_characters, 'models' => $models, 't' => $m2));
        } else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
    }

}
