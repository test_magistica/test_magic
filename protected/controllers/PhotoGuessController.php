<?php

class PhotoGuessController extends Controller
{
	public function actions() {
		return array(

		);
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * Site register consultation
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionExample() {
		$this->layout='//layouts/main_payment';
		$this->render('example');
	}
	/**
	 * Site register consultation
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionRegister() {
		$this->layout='//layouts/main_payment';
		if ( ! Yii::app()->user->isGuest){
			// проверка пользователь или эксперт
			if (Yii::app()->user->role !== Users::ROLE_USER){
				throw new CHttpException(404, 'Эта операция возможна только клиенту.');
			}
		}

		$expList = ExpertStaticServices::model()->findAll();
		$list = CHtml::listData($expList, 'id', 'expertID');

		$id = reset(array_values($list));
		$expert = Users::model()->findByPk($id);
		$criteria = new CDbCriteria();
		$criteria->compare('role', 'expert');
		$criteria->addInCondition('id', array_values($list));
		$expertList = Users::model()->findAll($criteria);

		if ($expert === NULL)
			throw new CHttpException(404, 'The requested page does not exist.');

		$model = new PhotoGuess();
		$this->performAjaxValidation($model);

		if (isset($_POST['PhotoGuess'])) {

			unset($_POST['PhotoGuess']['therms']);
			unset($_POST['PhotoGuess']['forget']);

			$model->attributes  = $_POST['PhotoGuess'];

			$amount    = $_POST['PhotoGuess']['cost'];

			$model->cost        = $amount;
			$model->expertID    = $_POST['PhotoGuess']['expertID'];
			$model->image       = $_POST['PhotoGuess']['image'];

			if ( Yii::app()->user->isGuest) { // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

				$model->status  = 'nopaid';

				if ($model->save(false)){

					$ident = md5(uniqid(rand(0, 10), 1));
					$guest              = new Guest;
					$guest->email       = $model->email;
					$guest->id_guest    = $ident;
					$guest->consultation = $model->id;
					if ($guest->save()) {
						if ($guest->id_guest != 'error') {
							$this->redirect("http://{$_SERVER['HTTP_HOST']}/payment?guest_id={$guest->id_guest}&photoGuess_id={$model->id}&service=photoGuess");
						}
						else {
							Yii::app()->user->setFlash('error', 'Невозможно завазать сервис "Гадание по фото".Обратитесь к администратору. ');
						} // если id != error
					} //если модель гостя сохранена\
					$this->refresh();
				} // если модель сохранена
				else{
					Yii::app()->user->setFlash('error', 'Невозможно обработать запрос для гостя. Обратитесь к администратору. ');
				}

			}
			else {                            // -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
				$user = Users::model()->findByPk(Yii::app()->user->id);

				$model->userID  = Yii::app()->user->id;
				$model->status  = 'nopaid';
				$model->email   = $user->email;

				// проверка баланса пользователя
				if ($user['bill'] < $amount){
					$model->save(false);
					$this->redirect(array("/payment?other_amount={$amount}&photoGuess_id={$model->id}&service=photoGuess"));
				}

				//Берет оплату с пользователя
				if (Controller::takeUserBill(Yii::app()->user->id, $amount)) {
					$model->status = 'paid';

					if($model->save(false)) {
						ShopOrder::model()->sendMailSuccessPaid('photoGuess', $model->id);

						Yii::app()->user->setFlash('photoGuess', 'Ожидайте ответ от эксперта.');
						$this->refresh();
					}
					else {
						Yii::app()->user->setFlash('error', 'Невозможно обработать запрос для пользователя. Обратитесь к администратору. ');
					}
				}
			}
		}
		$this->render(
			'index',
			array(
				'expert' => $expert,
				'model' => $model,
				'expertList' => $expertList,
				'infoBlock' => $this->actionExpertInfo($id)
			)
		);
	}

	public function actionExpertInfo($idx) {

		$expert = Users::model()->findByPk($idx);
		$image = $this->getUserImage($idx);
		$arr = array('online' => 'онлайн', 'busy' => 'занят', 'autonomous' => 'нет на линии');

		$a = '
			<div class="img"><img src="'.Yii::app()->request->baseUrl .'/uploads/user/'.$image.'" alt="" /></div>
			<div class="name">'.CHtml::link($expert['name'], array('/extrasens/view/', 'id' => $expert['id'], 'title' => $this->getCHPU($expert['name'] . '-' . $expert['profession']))).'</div>
			<div class="type">'.$expert['profession']." &nbsp;&nbsp; " .'</div>';
		if(isset($_GET['idx'])){
			echo json_encode(
				array(
					'info' => $a,
					'reviews' => $this->renderPartial('/extrasens/_review', array('model'=>$expert), true),
					'schedule' => $this->renderPartial('/extrasens/_schedule', array('model'=>$expert), true))
			);
		}
		else
			return $a;
	}

	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'photo_guess-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}