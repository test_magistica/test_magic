<?php

class FailController extends Controller {

	public function actionIndex() {
		if (isset($_GET['InvId'])) {
			$criteria = new CDbCriteria();
			$criteria->compare('id', intval($_GET['InvId']));

			$criteria->compare('status', 'expectations');

			$model = Payments::model()->find($criteria);

			if ($model['id']) {
				$model->status = 'fail';
				$model->save();
				Yii::app()->db->createCommand("UPDATE `payments` SET `status` = 'fail' WHERE `id` = '" . (int) $model['id'] . "'")->query();
				Yii::app()->user->setFlash('fail', 'Вы отменили пополнение счета.<br/>Если нет, то обратитесь к администратору.');
			}
			else {
				Yii::app()->user->setFlash('fail', "Запрос не выполнен обратитесь к администратору код ошибки #3");
				$msg = 'Error cancel-pay.Not search `expectations`-status where payments.id='.$_GET['InvId'];
				$pay_log = new Payments_log;
				$pay_log->id_pay = $_GET['InvId'];
				$pay_log->message = $msg;
				$pay_log->save();
			};
			$this->redirect(array('/fail'));
		}
		$this->render('index');
	}
}