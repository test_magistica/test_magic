<?php

class HoroscopeController extends Controller {

	public function actionIndex() {
		$this->render('index');
	}

	public function actionSubscription() {
		Yii::import('ext.Horoscope.Horoscope');
		$horo = new Horoscope();

		$model = new Subscription();
		if ( ! Yii::app()->user->isGuest) {
			$userEmail = $this->getUser(Yii::app()->user->id,'email');
			if($subscript = Subscription::model()->find('email="'.$userEmail.'"')) {
				$model = $subscript;
				$model->zodiak = explode(',', $model->zodiak);
				$model->category = explode(',', $model->category);
				$model->email = $userEmail;
			}
			else{
				$model->email = $userEmail;
			}
		}

		if (isset($_POST['Subscription'])) {
			$model->zodiak      = $_POST['Subscription']['zodiak'] ? implode(',',$_POST['Subscription']['zodiak']) : '';
			$model->category    = $_POST['Subscription']['category'] ? implode(',',$_POST['Subscription']['category']) : '';
			$model->email       = $_POST['Subscription']['email'] ? $_POST['Subscription']['email'] : '';
			$model->key         = uniqid();
			if ($model->save()){
				Yii::app()->user->setFlash('subscription', 'Подписка успешно сохранена!');
			}
			else{
				$model->zodiak = explode(',', $model->zodiak);
				$model->category = explode(',', $model->category);
			}
		}

		$this->render('subscription', array(
			'model'     => $model,
			'zodiaks'   => $horo->_zodiacs,
		));
	}

    public function actionSubscriptionDay() {
        Yii::import('ext.Horoscope.Horoscope');
        $horo = new Horoscope();
        $horo->addToCache();
        $Subject = 'Подписка на ежедневную рассылку гороскопов';
        $GeneralHoroscopeOnline = Yii::app()->cache->get('GeneralHoroscopeOnline');
        $models = Subscription::model()->findAll("category LIKE '%day%'");
        foreach ($models as $model) {
	        $msg = '';
            $z = explode(',', $model['zodiak']);
            foreach ($z as $key => $value) {
                $msg .= '<div style="font-family: Verdana;font-size: 13px;padding: 4px">
                            <strong style="color: #ff6600">Прогноз для знака зодиака</strong>
                            <strong style="color: #004ca0">"' . $horo->_zodiacs[$value] . '"</strong>
                     </div>
                     <div style="font-family: Verdana;font-size: 13px;background-color: #004ca0;color: #ffffff;padding: 4px">                        
                        <strong>Прогноз на сегодня, ' . $GeneralHoroscopeOnline['date']['@attributes']['today'] . ':</strong>
                     </div>
                     <div style="font-family: Verdana;font-size: 13px;color: #004ca1;padding: 4px;padding-left: 26px">
                        ' . $GeneralHoroscopeOnline[$value]['today'] . '
                     </div>';
            }

            $msg .= '<div style="font-family: Verdana;font-size: 11px;padding: 4px;background-color: #d6d6d6;color: #666666">
                    Для отказа от этой подписки перейдите по следующей ссылке:<br>
                    <a target="_blank" style="color: #999999" href="' . Yii::app()->createAbsoluteUrl('/horoscope/subscriptionRefuse/' . $model['id']) . '?key=' . $model['key'] . '">' . Yii::app()->createAbsoluteUrl('/horoscope/refuse/' . $model['id']) . '</a>
                 </div>';
	        Controller::sendMailUser($model['email'], $Subject, $msg);
        }
        Yii::app()->end();
    }

    public function actionSubscriptionWeek() {
        Yii::import('ext.Horoscope.Horoscope');
        $horo = new Horoscope();
        $horo->addToCache();
        $Subject = 'Подписка на еженедельную рассылку гороскопов';
        $HoroscopeForTheWeek = Yii::app()->cache->get('HoroscopeForTheWeek');
        $models = Subscription::model()->findAll("category LIKE '%week%'");
        foreach ($models as $model) {
	        $msg = '';
            $z = explode(',', $model['zodiak']);
            foreach ($z as $key => $value) {
                $msg .= '<div style="font-family: Verdana;font-size: 13px;padding: 4px">
                            <strong style="color: #ff6600">Прогноз для знака зодиака</strong>
                            <strong style="color: #004ca0">"' . $horo->_zodiacs[$value] . '"</strong>
                     </div>
                     <div style="font-family: Verdana;font-size: 13px;background-color: #004ca0;color: #ffffff;padding: 4px">                        
                        <strong>Прогноз на неделю, ' . $HoroscopeForTheWeek['date']['@attributes']['weekly'] . ':</strong>
                     </div>
                     <div style="font-family: Verdana;font-size: 13px;color: #004ca1;padding: 4px;padding-left: 26px">
                        ' . $HoroscopeForTheWeek[$value]['common'] . '
                     </div>';
            }
            $msg .= '<div style="font-family: Verdana;font-size: 11px;padding: 4px;background-color: #d6d6d6;color: #666666">
                    Для отказа от этой подписки перейдите по следующей ссылке:<br>
                    <a target="_blank" style="color: #999999" href="' . Yii::app()->createAbsoluteUrl('/horoscope/subscriptionRefuse/' . $model['id']) . '?key=' . $model['key'] . '">' . Yii::app()->createAbsoluteUrl('/horoscope/refuse/' . $model['id']) . '</a>
                 </div>';
	        Controller::sendMailUser($model['email'], $Subject, $msg);
        }
        Yii::app()->end();
    }

    public function actionSubscriptionRefuse($id) {
        $model = Subscription::model()->findByPk($id);
        if ($model === NULL)
            Yii::app()->user->setFlash('success', 'Ваша подписка удалена!');
        else {
            if ($model->key === $_GET['key']) {
                $model->delete();
                Yii::app()->user->setFlash('success', 'Ваша подписка удалена!');
            } else {
                throw new CHttpException(404, 'The requested page does not exist.');
            }
        }
        $this->render('refuse');
    }

    public function actionAjax() {
        if (Yii::app()->request->isAjaxRequest && isset($_POST['zodiak']) && isset($_POST['category'])) {

            Yii::import('ext.Horoscope.Horoscope');
            $horo = new Horoscope();
            $horo->addToCache();

            $cache = Yii::app()->cache->get($_POST['category']);
            $result['text'] = $cache[$_POST['zodiak']]['today'];
            $result['date'] = $cache['date']['@attributes']['today'];
            $result['zodiak'] = $horo->_zodiacs[$_POST['zodiak']];
            $result['image'] = Yii::app()->request->baseUrl . '/images/hrcp-' . ucfirst($_POST['zodiak']) . '.jpg';
            echo json_encode($result);
        } else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
    }

	protected function performAjaxValidation($model) {
		if(isset($_POST['ajax']) && $_POST['ajax'] === 'subscription-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}