<?php

class ArticleController extends Controller {

    public function actionView($id) {
        $model = $this->loadModel($id);
        $this->clips['articleAuthorId'] = $model->authort->id;
        $this->clips['articleExqlude'] = array($id);
        $this->render('view', array(
            'model' => $model
        ));
    }

    public function loadModel($id) {
        $model = Article::model()->findByPk($id);
        if ($model === null || $model->status == 0)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionIndex() {

        $criteria = new CDbCriteria();
        $criteria->compare('status', 1);
        $criteria->order = 'createTime desc';
        $count = Article::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 15;
        $pages->applyLimit($criteria);
        $models = Article::model()->findAll($criteria);

        $this->render('index', array('models' => $models,
            'pages' => $pages));
    }

}