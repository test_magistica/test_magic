<?php

class FaqController extends Controller {

    public function actionIndex() {
        $this->layout = "main_faq";
        $groups = FaqGroups::model()->with("nested", "faqs")->findAll(array(
            'condition' => "t.parent_group=".FaqGroups::ROOT_GROUP." and t.status=".FaqGroups::STATUS_SHOW,
        ));
        $this->render('index', array("fgroups" => $groups));
    }
    
    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
}
