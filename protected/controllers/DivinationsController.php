<?php

class DivinationsController extends Controller {

    public function actionIndex() {

	    $this->layout='//layouts/main_payment';
        $criteria = new CDbCriteria();        
        $criteria->order = 'id desc';
        $count = Divination::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 15;
        $pages->applyLimit($criteria);
        $models = Divination::model()->findAll($criteria);

        $this->render('index', array('models' => $models,
            'pages' => $pages));
    }

    public function actionView($id,$title=null) {
        
        $model = $this->loadModel($id);
        $tr_title = $model->translit($model->title);
        if ( $title<>$tr_title) $this->redirect(array('view','id'=>$model->id, 'title' => $tr_title));
        
        $this->render('view', array(
            'model' => $model
        ));
    }

    public function loadModel($id) {
        $model = Divination::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    
}