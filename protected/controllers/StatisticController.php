<?php

class StatisticController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations            
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('view', 'index'),
                'users' => array('user'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id) {
        $model = $this->loadModel($id);
        $this->render('view', array(
            'model' => $model
        ));
    }

    public function loadModel($id) {
        $model = CallBack::model()->findByPk($id);
        if ($model === null || $model->check == 0)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionIndex() {

        $criteria = new CDbCriteria();
        $criteria->order = 'id desc';
        //$criteria->compare('`status`', 2);
        //$criteria->select = array('*','sec_to_time(duration) as duration');
        $user_id = Yii::app()->user->id; 
        $criteria->addCondition('`caller_id`='.$user_id.' OR `called_id`='.$user_id);
        
        $criteria->select = array('*','DATE_FORMAT(`call_date`,\'%d.%m.%Y %H:%i:%s\') as call_date');
        
        if (isset($_GET['date'])) 
        {
            if (preg_match("/[0-9]{4}-[0-9]{2}-[0-9]{2}/", $_GET['date'])) {
                $ar = explode('-', $_GET['date']);
                $criteria->addCondition("YEAR(`call_date`) = {$ar[0]} AND MONTH(`call_date`) = {$ar[1]}");
            }
        }
        
        $count = CallJournal::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 15;
        $pages->applyLimit($criteria);
        $models = CallJournal::model()->findAll($criteria);

        $this->render('index', array('models' => $models,
            'pages' => $pages));
            
    }

}