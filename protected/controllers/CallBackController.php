<?php

class CallBackController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations            
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('cron', 'test', 'check'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view'),
                'users' => array('user'),
                //'deniedCallback' => $this->redirect('/site/error')
            )
            //array('deny', // deny all users
            //    'users' => array('*'),
            //    'message' => "это функция предусмотрена только для клиентов",

            //),
        );
    }

    public function actionCron() {
         $output = '';
        $url = Yii::app()->createAbsoluteUrl('/callBack/check');
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('date', date('Y-m-d', mktime(0, 0, 0, date('n'), date('j') - 3)), date('Y-m-d', mktime(0, 0, 0, date('n'), date('j') + 1)), 'AND');
        $criteria->addCondition('`check` = 0');
        $models = CallBack::model()->findAll($criteria);
        foreach ($models as $model)
            $output = Yii::app()->curl->post($url, array('check' => $model['cbk_id']));
        print_r($output);
    }
    
    public function actionView($id) {

        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->createAbsoluteUrl("site/register/".$id));
        }
        //$call = CallBack::model()->find("`user_id` = '" . Yii::app()->user->id . "' order by id desc");
        //$date1 = 'now';
        //$date2 = $call['date'];

        // этот результат будет всегда разным
        $expert_info = Users::model()->findByPk($id);
        //$d = $this->date_diff($date1, $date2)/(60);
	// если меньше 7 минут
	$user_info = Users::model()->findByPk(Yii::app()->user->id);
	if ($user_info['role'] != 'user')
        {
    	    Yii::app()->user->setFlash('callK', 'Только клиентам доступна данная услуга.. ');
//            $this->redirect(array('/extrasens/view/', 'id' => $id));
            $this->redirect(array('/extrasens/view/','id'=>$expert_info->id, 'title' => $this->getCHPU($expert_info->name . '-' . $expert_info->profession)), TRUE, 301);
        };
	$criter = new CDbCriteria;
	$criter->addCondition('`caller_id`='.(int)Yii::app()->user->id.' OR `called_id`='.(int)Yii::app()->user->id);
	$criter->compare('status',"BEGIN");
	//$criter2 = new CDbCriteria;
	//$criter2->compare('client_id',(int)Yii::app()->user->id);
	//$criter2->compare('check', 0);
	
	
	$i = CallBack::model()->countBySql(
	    'SELECT * FROM `call_back` WHERE `client_id`='.(int)Yii::app()->user->id.' AND `check`=0');
        //$j = Consultation::model()->countBySql('
    	//    SELECT * FROM `consultation` WHERE `userID`='.(int)Yii::app()->user->id.' AND `status`=3');
    	//if ((CallJournal::model()->count($criter)>0) OR ($i>0) )
        if ((CallJournal::model()->count($criter)>0) OR ($i>0) )
        {
            Yii::app()->user->setFlash('callK', 'В настоящий момент вызов недоступен.<br/>Попробуйте позвонить позже..');
            $this->redirect(array('/extrasens/view/', 'id' => $id, 'title' => $this->getCHPU($expert_info->name . '-' . $expert_info->profession)));
        };
                
        
        
        $expert_id = $id;
        if ($user_info['bill'] < $expert_info['tariff']) {
            $this->redirect(array('/payment'));
        }
        if ($expert_info['online'] !== 'online') {
            $this->redirect(array('/extrasens/view/', 'id' => $expert_id,'title' => $this->getCHPU($expert_info->name .'-'.$expert_info->profession),
        	'a' => '10'));
        }
        
        $maxdur = floor($user_info['bill'] / $expert_info['tariff'])*60; //рублей = 1 минута

        $tele = substr($user_info['tele'], 1);
        if ($maxdur > 0) {
            //если больше чем положено, делаем сколько положенно - 30 минут 
            if ($maxdur > 1800){
                $maxdur = 1800;
	    };
	    
            // В этом файле находится функция для подписи параметров

            //$baseurl = "http://api.comtube.com/scripts/api/callback.php";
	    /*
            $params = array();
            //$params["username"] = "parissema";
            $params["action"] = "call";
            //$params['uid'] = "adc3c80f-04d3-48ae-b338-fafb808b349";
            $params["number1"] = str_replace('+', '', $tele);
            $params["number2"] = str_replace('+', '', $expert_info['tele']);
            $params["maxdur"] = $maxdur;
            $params['type'] = 'xml';
            
            // Создаем подпись к параметрам
            $urlparams = $this->BuildUrlParamsWithSignature($params, "20071981");
            // Формируем полный URL для обращения к серверу
            $url = $baseurl . "?" . $urlparams;
            // �?нициализируем curl и отправляем запрос
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $data = curl_exec($ch);
            curl_close($ch);
            // Анализируем результат

            $matches = array();
            preg_match("/<uid>([a-z0-9\-]+)<\/uid>/", $data, $uid);
            preg_match("/<code>([a-z0-9\-]+)<\/code>/", $data, $status);
            preg_match("/<desc>([a-z0-9\-]+)<\/desc>/", $data, $description);
            if (count($uid) > 0) {
                $code = $status[1];
                $desc = $description;
                $cbk_id = $uid[1];
                
                if ($code != 200) {
                    // При обработке запроса произошла ошибка
                    throw new CHttpException($code,"Не удалось сделать запрос, причина: " . $desc . " (" . $code . ")");                    
                } else {
                */
                    // Успешное выполнение запроса
                    //echo "Wait call, please";
                    $data = array(
                	'user_id' => $expert_id,
                        'client_id' => Yii::app()->user->id,
                        'cbk_id' => '', //$cbk_id,
                        'from_num' => str_replace('+', '', $expert_info['tele']),
                        'to_num' => str_replace('+', '', $tele),
                        'maxdur' => $maxdur
                    );

                    //Yii::app()->session['cbk_id'] = $cbk_id;
                    $this->addCallBack($data);
                    //Yii::app()->db->createCommand("UPDATE `users` SET `online` = 'busy' WHERE `id` = '" . (int) $expert_id . "'")->query();
		    Yii::app()->user->setFlash('call', 'Success');
		    //Controller::takeUserBill($user['id'], $expert['tariff'] * ceil($xml->callback->calllegs[1]->duration / 60));
                    
                    $this->redirect(array('/extrasens/view/', 'id' => $id, 'title' => $this->getCHPU($expert_info->name . '-' . $expert_info->profession ),
                	'a'=>'4'));
                    
                    //$this->redirect(array('/extrasens/view/', 'id' => $expert_id, 'a' => '4'));
                
            /*}
            } else {
                // Получен неправильный ответ от сервера
                throw new CHttpException(404,"Неверный результат");  
                
            }*/
        }
    }
/*    public function actionCheck_Consultation($id_expert) // id experta 
    {
	$cons = Consultation::model()->findAllBySql('SELECT * FROM `consultation` WHERE `status`=1 AND `date` < CURRENT_TIMESTAMP()');
	//$now = time();//strtotime(date('d.m.y H:i:s'));
	//print $now;
	foreach ($cons as $consult)
	{
	    $expert_info = Users::model()->findByPk($consult->expertID);
	    $data = array(
                'user_id' => $consult->userID,
                'cbk_id' => 'asd', //$cbk_id,
                'from_num' => str_replace('+', '', $consult->phone),
                'to_num' => str_replace('+', '', $expert_info->tele),
                'maxdur' => $consult->duration
            );
    	    $this->addCallBack($data);
    	    $command = Yii::app()->db->createCommand();
    	    $command->update('consultation',array('status'=>2),'id=:id',array(':id'=>$consult->id));
	    //$consult->status = 2;
	    //$consult->update(array('`status`'));
    		
        }
    }*/
    public function actionCheck() {

	$sql = CallBack::model()->findAllBySQL('SELECT * FROM call_back WHERE `check` = 0 AND `date` BETWEEN DATE_SUB(NOW(), INTERVAL 75 SECOND) AND NOW()');
	//print_r($result);
	$calls = array();
	foreach ($sql as $call)
	{
	    $curr['from']=$call->from_num;
	    $curr['to']=$call->to_num;
	    $curr['id']=$call->id;
	    //$curr['user_id'] = $call->user_id;
	    $curr['max_dur'] = $call->maxdur;
	    $calls[] = $curr;
	    
	};
	print @json_encode($calls);

	// Обновляем колбэки
        Yii::app()->db->createCommand('UPDATE `call_back` SET `check` = 3 WHERE `date` < DATE_SUB(NOW(), INTERVAL 75 SECOND)')->query();

	//print_r($calls);
        /*if (isset($_POST['check'])):
            $c = CallBack::model()->find("cbk_id = '" . $_POST['check'] . "'");
            if ($c['check'] == 0) {
                $baseurl = "http://api.comtube.com/scripts/api/callback.php";
                $params = array();
                $params["username"] = "parissema";
                $params["action"] = "statistics";
                $params['uid'] = $_POST['check'];
                $params['type'] = 'xml';
                // Создаем подпись к параметрам
                $urlparams = $this->BuildUrlParamsWithSignature($params, "20071981");
                // Формируем полный URL для обращения к серверу
                $url = $baseurl . "?" . $urlparams;
                // �?нициализируем curl и отправляем запрос
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $data = curl_exec($ch);
                curl_close($ch);

                // Анализируем результат
                $xml = simplexml_load_string($data);
//print_r($xml);exit;
                if ($xml->callback->calllegs[1]->call == "SUCCEEDED") {
                    $expert = Users::model()->find("`tele`='" . '+' . $c['to_num'] . "'");
                    $user = Users::model()->find("`tele`='" . '+' . $c['from_num'] . "'");
                    Controller::takeUserBill($user['id'], $expert['tariff'] * ceil($xml->callback->calllegs[1]->duration / 60));
   Yii::app()->db->createCommand("UPDATE `users` SET `online` = 'online' WHERE `id` = '" . (int) $expert['id'] . "'")->query();
   Yii::app()->db->createCommand("UPDATE `call_back` SET `check` = '1', `maxdur`='".$xml->callback->calllegs[1]->duration."' WHERE `cbk_id` = '" . $_POST['check'] . "'")->query();
                    unset(Yii::app()->session['cbk_id']);
                    $user = Users::model()->find("`tele`='" . '+' . $c['from_num'] . "'");
                    echo $user['bill'];
                }
            }
        endif;*/
    }

}
