<?php

class UserController extends Controller {

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            'upload' => array(
                'class' => 'xupload.actions.XUploadAction',
                'path' => Yii::app()->getBasePath() . "/../uploads",
                'publicPath' => Yii::app()->getBaseUrl() . "/uploads",
            ),
        );
    }

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations            
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'create', 'captcha'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('update', 'photo', 'upload', 'setphoto', 'password', 'updateSettings', 'notification'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionCreate() {
        $model = new Users();
        $model->scenario = 'create';
        $this->performAjaxValidation($model);
        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            $model->role = 'user';
//            $model->free_chat_minutes = isset(Yii::app()->params['freeChatMinutes']) ? (int)  Yii::app()->params['freeChatMinutes'] : 0;
            if ($model->save()) {

                $msg = '<p>
                        <style type="text/css">
                P { margin-bottom: 0.08in; direction: ltr; color: rgb(0, 0, 0); widows: 2; orphans: 2; }A:link { color: rgb(0, 0, 255); }	</style>
                </p>
                <p style="margin-bottom: 0.14in">
                        Здравствуйте, ' . $_POST['Users']['name'] . ' ' . $_POST['Users']['surname'] . '!</p>
                <p style="margin-bottom: 0.14in">
                        Поздравляем Вас с регистрацией</p>
                <p style="margin-bottom: 0.14in">
                        Ваш логин: <span lang="en-US"><a href="mailto:' . $_POST['Users']['email'] . '">' . $_POST['Users']['email'] . '</a> </span></p>
                <p lang="en-US" style="margin-bottom: 0.14in">
                        Ваш пароль: <span lang="en-US">' . $_POST['Users']['pwd'] . '</span><br />
                        &nbsp;</p>
                <p style="margin-bottom: 0.14in">
                        Мы обязательно будем Вам помогать во всех Ваших вопросах.</p>
                <p style="margin-bottom: 0.14in">
                        Лучшие Тарологи, Астрологи, Экстрасенсы готовы ответить на все интересующие Вас вопросы и помочь в любой ситуации.</p>
                <p style="margin-bottom: 0.14in">
                        Получить консультацию Вы можете по многоканальному платному телефону из России 8-809-505-6747</p>
                <p style="margin-bottom: 0.14in">
                        Или заказать обратный звонок по предоплате</p>
                <p style="margin-bottom: 0.14in">
                        8-800-555-3317<br />
                        &nbsp;</p>
                <p style="margin-bottom: 0.14in">
                        Со всей душой к Вам,</p>
                <p style="margin-bottom: 0.14in">
                        Команда Эзотерической службы &laquo;МАГИСТИКА&raquo;</p>
                <p style="margin-bottom: 0.14in">
                        Наш сайт <a href="http://magistika.com"><span lang="en-US">http://magistika.com</span></a></p>
                ';

                Yii::import('application.extensions.phpmailer.JPhpMailer');
                $mail = new JPhpMailer();
                $mail->SetFrom('no-reply@magistika.com', 'magistika.com');
                $mail->Subject = 'Поздравляем Вас с регистрацией';
                $mail->AltBody = '';
                $mail->CharSet = 'UTF-8';
                $mail->MsgHTML($msg);
                $mail->AddAddress($_POST['Users']['email'], $_POST['Users']['name']);
                $mail->Send();

                $identity = new UserIdentity($_POST['Users']['email'], $_POST['Users']['pwd']);
                if ($identity->authenticate())
                    Yii::app()->user->login($identity);
                else {
                    echo $identity->errorMessage;
                    exit;
                }
                $this->redirect(array('site/index'));
            }
        }
        $this->render('create', array('model' => $model));
    }

    public function actionUpdateSettings() {
        if (Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['uid']) && isset($_POST['attr']) && isset($_POST['v'])) {
                $uid = (int) $_POST['uid'];
                $attr = $_POST['attr'];
                $v = $_POST['v'];
                if (!empty($uid) && !empty($attr)) {
                    switch ($attr) {
                        case "chat_available":
                            Users::model()->updateByPk($uid, array("chat_available" => $v));
                            if($v == "0"){
                                Yii::import("chat.components.mController", TRUE);
                                Yii::import("chat.controllers.ChatController", TRUE);
                                ChatController::closeAllExpertChats($uid);
                                }
                            break;
                    }
                }
            }
        }
        Yii::app()->end();
    }

    public function actionUpdate($id) {
        if (Yii::app()->user->role == 'expert')
            $this->layout = '//../modules/expert/views/layouts/column2';

        $model = $this->loadModel($id);
        $model->scenario = 'update';
        $d = explode('.', $model->birthday);
        $model->day = $d[0];
        $model->month = $d[1];
        $model->birthday = $d[2];
        $this->performAjaxValidation($model);
        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
            if ($model->save())
                $this->redirect(array('/user/update/', 'id' => $model->id), TRUE, 301);
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionPhoto() {
        if (Yii::app()->user->role == 'expert')
            $this->layout = '//../modules/expert/views/layouts/column2';
        //Yii::import("xupload.models.XUploadForm");
        $model = new XUploadForm;
        $this->render('photo', array('model' => $model));
    }

    public function actionSetphoto() {
        if (Yii::app()->user->role == 'expert')
            $this->layout = '//../modules/expert/views/layouts/column2';
        if (isset($_GET['file']) && !empty($_GET['file'])) {
            Yii::app()->db->createCommand("UPDATE `userphoto` SET `main` = '0' WHERE `userID` = '" . Yii::app()->user->id . "' AND `main`=1")->query();
            Yii::app()->db->createCommand("UPDATE `userphoto` SET `main` = '1' WHERE `userID` = '" . Yii::app()->user->id . "' AND `image` = '" . CHtml::encode($_GET['file']) . "'")->query();
            $this->redirect(array('/user/photo'), TRUE, 301);
        }
    }

    public function actionPassword() {
        if (Yii::app()->user->role == 'expert')
            $this->layout = '//../modules/expert/views/layouts/column2';
        $model = new PassswordForm();
        $password = Users::model()->find("`id` = '" . Yii::app()->user->id . "'");
        if (isset($_POST['PassswordForm']))
            if ($password->pwd !== md5($_POST['PassswordForm']['curPassword'])) {
                $model->addError('curPassword', 'Вы заполнили не правильный пароль');
            } else {
                $password->pwd = md5($_POST['PassswordForm']['password']);
                if ($password->save(false))
                    Yii::app()->user->setFlash('password', 'Ваш пароль успешно изменен');
            }
        $this->render('password', array('model' => $model));
    }

    public function loadModel($id) {
        $model = Users::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'register-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Проверяет поступили ли для эксперта заказы на услуги и пр.
     * @param int $uid ExpertID
     */
    public function actionNotification(){
        $uid = Yii::app()->request->getParam("uid", 0);
        if(Yii::app()->request->isAjaxRequest && $uid > 0){
            $notificationSessionKey = "notificationKey";
            $sesss= array();
//            unset(Yii::app()->session[$notificationSessionKey]);
            if(!isset(Yii::app()->session[$notificationSessionKey])){
                Yii::app()->session[$notificationSessionKey] = array();
            }else{
                $sesss = Yii::app()->session[$notificationSessionKey];
            }
            $newBookings = Booking::model()->findAll("expertID=".$uid." AND status=".Booking::STATUS_WAIT);
            $newConsultations = Consultation::model()->findAll("expertID=".$uid." AND status='".Consultation::STATUS_WAIT."'");
            $newPaidQuestions = Question::model()->findAll("status=".Question::STATUS_PUBLISHED." AND pay=".Question::QUESTION_PAY." AND is_paid=".Question::QUESTION_PAID." AND create_time > date_sub(NOW(), INTERVAL 1 DAY)");
            
            $news = array();
            foreach ($newBookings as $booking){
                if(isset($sesss['book'])){
                    if(in_array($booking->id, $sesss['book'])){
                    continue;
}
                }else{
                   $sesss['book'] = array();
                }
                $sesss['book'][] = $booking->id;
                $news[] = array(
                    "title" => "Поступил новый заказ на услугу.",
                    "url" => Yii::app()->createUrl("/expert/booking/admin"),
                );
            }
            foreach ($newConsultations as $consultation){
                if(isset($sesss['cons'])){
                    if(in_array($consultation->id,$sesss['cons'])){
                    continue;
                }
                }else{
                    $sesss['cons'] = array();
                }
                $sesss['cons'][] = $consultation->id;
                $news[] = array(
                    "title" => "Поступил новый заказ на консультацию.",
                    "url" => Yii::app()->createUrl("/expert/booking/admin"),
                );
            }
            foreach ($newPaidQuestions as $question){
                if(isset($sesss['quest'])){
                    if(in_array($question->id, $sesss['quest'])){
                    continue;
                }
                }else{
                    $sesss['quest'] = array();
                }
                $sesss['quest'][] = $question->id;
                $news[] = array(
                    "title" => "Поступил новый платный вопрос.",
                    "url" => Yii::app()->createUrl("/expert/question/admin"),
                );
            }
            if(count($news) > 0){
                Yii::app()->session[$notificationSessionKey] = $sesss;
                echo json_encode($news);
            }else{
                echo json_encode(array("empty" => 1));
            }
            Yii::app()->end();
                    
        }
        //throw new CHttpException(404, "Requested page does not exists.");
    }
}
