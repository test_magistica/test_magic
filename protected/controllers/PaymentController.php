<?php

class PaymentController extends Controller {

	public function actionIndex() {
		$shopOrder = new ShopOrder();
		if (isset($_POST['action'])) {

			//print_r($_POST);
			$amount =  $_POST['amount'] ? $_POST['amount'] : $_POST['other_amount'];

			$guest_id = isset($_POST['guest_id']) ? $_POST['guest_id'] : 0;

			$entity = isset($_GET['service']) ? $_GET['service'] : 'bill';

			$entity_id = null;

			$description = 'Пополнение личного счета Magistika.com';

			$user_id = ! Yii::app()->user->isGuest ? Yii::app()->user->id : 0;
			if(isset($_GET['service'])) {

				if($_GET['service'] == 'consultation')
					$description = 'Оплата консультации, на сайте Magistika.com';

				if($_GET['service'] == 'photoGuess')
					$description = 'Оплата услуги "Гадание по фотографии", на сайте Magistika.com';

				$entity_id = $_GET[$_GET['service'].'_id'];

				if($entity != 'bill'){
					$mod = ucfirst($entity);
					$service = $mod::model()->findByPk($entity_id);
					$user_id = $service->userID;
				}
			}

			$payment_system = $_POST['payment_system'] ? $_POST['payment_system'] : '';

			$language = 'ru';

			$currency = 'RUR';



			$shopOrder->userID      = $user_id;
			$shopOrder->system      = $_POST['action'];
			$shopOrder->lang        = $language;
			$shopOrder->entity      = $entity;
			$shopOrder->entity_id   = $entity_id;

			if ($_POST['action'] == "PLATRON") {
				$shopOrder->amount = $amount;

				// гости
				if (Yii::app()->user->isGuest){
					if (($guest_id != 'error') AND (! empty ($guest_id))) {
						$criteria = new CDbCriteria();
						$criteria->compare('id_guest', $guest_id);
						$guest = Guest::model()->find($criteria);
						if (Guest::model()->count($criteria) == 1) {
							$shopOrder->guest = (int) $guest->id;
							$consult = Consultation::model()->findByPk($guest->consultation);
							$user_phone = str_replace('+','',$_POST['user_phone']);
						}
						else{
							Yii::app()->user->setFlash('not_enough_money',"Ошибка идентификатора, обратитесь к администратору.");
							$this->render('index');
						}
					}
					else {
						Yii::app()->user->setFlash('not_enough_money',"Не задан верный идентификатор.");
						$this->render('index');
					}
				}
				else {
					$user_phone = $_POST['user_phone'];
				}
				$shopOrder->save();

				$result = Yii::app()->platron->getUrlForPayment($shopOrder->id, $shopOrder->amount, $description, $currency, $language, $user_phone, $payment_system);

				$this->redirect($result);
			}
			if ($_POST['action'] == 'ROBOKASSA') {

				$mrh_login = "magistika-com"; // идентификатор магазина
				$mrh_pass1 = 'bohPaej0th'; //пароль1

				if (isset($_POST['guest_id'])) {

					if (($guest_id != 'error') AND (! empty ($guest_id))) {
						$criteria = new CDbCriteria();
						$criteria->compare('id_guest', $guest_id);

						if ($guest = Guest::model()->find($criteria)) {
							$shp_item = (int) $guest->id;
						}
						else {
							Yii::app()->user->setFlash('not_enough_money',"Ошибка идентификатора, обратитесь к администратору.");
							$this->render('index');
						}
					}
					else {
						Yii::app()->user->setFlash('not_enough_money',"Ошибка переданных данных.Пустой идентификатор. ");
						$this->render('index');
					}
				}
				else {
					if (! Yii::app()->user->isGuest) {
						$shp_item = 0;
					}
					else{
						Yii::app()->user->setFlash('not_enough_money',"Ошибка переданных данных.Зарегистрируйтесь.");
						$this->render('index');
					}
				}

				$shopOrder->amount      = $amount;
				$shopOrder->hash        = '';
				$shopOrder->guest       = $shp_item;

				if ($shopOrder->save()) {
					$inv_id = $shopOrder->id;

					$crc = md5("$mrh_login:$amount:$inv_id:$mrh_pass1:shpItem=$shp_item");

					Yii::app()->db->createCommand("UPDATE ShopOrder SET hash = '{$crc}' WHERE id = {$inv_id}")->query();

					$url = 'https://auth.robokassa.ru/Merchant/Index.aspx'.
							'?MrchLogin='   .$mrh_login.
							'&OutSum='      .$amount.
							'&InvId='       .$inv_id.
							'&Desc='        .$description.
							'&SignatureValue='.$crc.
							'&IncCurrLabel='.$payment_system.
							'&shpItem='     .$shp_item;
					$this->redirect($url);
				}
				else {
					Yii::app()->user->setFlash('success', "Запрос не выполнен обратитесь к администратору код ошибки #1");
				}
			}
		}
		$this->render('index');
	}

	public function actionCheckConsultPaid(){

		$criteria           = new CDbCriteria();
		$criteria->condition = 'created <> :created AND remind = :remind AND status = :status';
		$criteria->order    = 'created desc';
		$criteria->params   = array(':created' => "0000-00-00 00:00:00", ':remind' =>'0' ,':status' =>'nopaid' );

		$services = Yii::app()->params['paymentServices'];

		foreach($services as $model => $keys) {
			$unicName = $model;
			$model = ucfirst($model);
			//echo $keys['keyRemind'];
			if($service = $model::model()->findAll($criteria)){
				foreach($service as $row){
					$d = new DateTime($row->created);
					$diffM = $d->diff( new DateTime(date("Y-m-d H:i:s")) )->format("%i");

					if($diffM > 10){
						Yii::app()->db->createCommand("UPDATE {$keys['table']} SET remind = 1 WHERE id = {$row->id}")->query();

						if($row->userID == 0){
							$criteria2 = new CDbCriteria();
							$criteria2->compare('consultation', $row->id);

							$guest = Guest::model()->find($criteria2);
							$partPath = "?guest_id={$guest->id_guest}";
							$userName = (isset($row->guest_name) ? $row->guest_name : $row->username);
						}
						else{
							$amount = isset($row->amount) ? $row->amount : $row->cost;
							$user = Users::model()->findByPk($row->userID);
							$partPath = "?other_amount={$amount}";
							$userName = $user->name . " " . $user->surname;
						}

						$url = "payment{$partPath}";
						$params = array(
							'userName'          => $userName,
							'orderNum'          => $row->id,
							'countMin'          => isset($row->duration) ? $row->duration/60 : '' ,
							'reservedDateTime'  => isset($row->date) ? $row->date : '',
							'expertName'        => $this->getUser($row->expertID, 'name'),
							'expertSurname'     => $this->getUser($row->expertID, 'surname'),
							'email'             => isset($row->email)? $row->email : '',
							'contactPhone'      => isset($row->phone) ? $row->phone : '',
							'paymentFullLink'   => "http://{$_SERVER['HTTP_HOST']}/{$url}&{$unicName}_id={$row->id}&service={$unicName}",
						);

						$tp = new TemplateParams();
						$tp->setBodyParams($params);
						$tp->setSubjectParams($params);
						$mt = EmailTemplates::parseTemplate($keys['keyRemind'], $tp);

						Controller::sendMailUser($row->email, $mt->parsedSubject, $mt->parsedBody);
					}
				}
			}
		}
	}
}