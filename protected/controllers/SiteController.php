<?php

class SiteController extends Controller {
	public $layout='//layouts/main';
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
			'upload' => array(
				'class' => 'xupload.actions.XUploadActionConsult',
				'path' => Yii::app()->getBasePath() . "/../uploads",
				'publicPath' => Yii::app()->getBaseUrl() . "/uploads",
			),
			'sitemapxml' => array(
				'class' => 'ext.bestsitemap.ESitemapXMLAction',
				//'bypassLogs'=>true, // if using yii debug toolbar enable this line
				'importListMethod' => 'getBaseSitePageList',
				'classConfig' => array(
					// Експерты
					array('baseModel' => 'Expertreviews',
						'route' => '/extrasens/view',
						'params' => array('id' => 'id'),
						//'scopeName'=>'sitemap',
						'frequency' => 'weekly'
					),
					// Услуги
					array('baseModel' => 'Service',
						'route' => '/service/view',
						'params' => array('id' => 'id'),
						//'scopeName'=>'sitemap',
						'frequency' => 'weekly'
					),
					// Именатор Женский
					array('baseModel' => 'WomanNames',
						'route' => '/names/man',
						'params' => array('id' => 'id'),
						//'scopeName'=>'sitemap',
						'frequency' => 'weekly'
					),
					// Именатор Мужской
					array('baseModel' => 'ManNames',
						'route' => '/names/man',
						'params' => array('id' => 'id'),
						//'scopeName'=>'sitemap',
						'frequency' => 'weekly'
					),
					// Сонник
					array('baseModel' => 'Dreams',
						'route' => '/dreams/view',
						'params' => array('id' => 'id'),
						//'scopeName'=>'sitemap',
						'frequency' => 'weekly'
					),
					// Статьи
					array('baseModel' => 'Article',
						'route' => '/article/view',
						'params' => array('id' => 'id'),
						//'scopeName'=>'sitemap',
						'frequency' => 'weekly'
					),
					// Онлайн гадания
					array('baseModel' => 'Divinations',
						'route' => '/divinations/view',
						'params' => array('id' => 'id'),
						//'scopeName'=>'sitemap',
						'frequency' => 'weekly'
					),
				)
			)
		);
	}

	/**
	 * Provide the static site pages which are not database generated
	 *
	 * Each array element represents a page and should be an array of
	 * 'loc', 'frequency' and 'priority' keys
	 *
	 * @return array[]
	 */
	public function getBaseSitePageList() {

		$list = array(
			// Главная страница
			array(
				'loc' => Yii::app()->createAbsoluteUrl('/'),
				'frequency' => 'weekly',
				'priority' => '1',
				'attr' => array('title' => Yii::t('menu', 'Главная'), 'grid' => null),
				'lid' => '01'
			),
			// Страница - Контакт
			array(
				'loc' => Yii::app()->createAbsoluteUrl('/site/contact'),
				'frequency' => 'yearly',
				'priority' => '0.8',
				'attr' => array('title' => Yii::t('menu', 'Контакт'), 'grid' => null),
				'lid' => '05'
			),
		);
		return $list;
	}

	public function actionCheckExpertStatus() {
		$check = array();
		$experts = Users::model()->findAll("role = 'expert' and status = 1");
		foreach ($experts as $expert) {
			$check[$expert['id']] = $expert['online'];
		}
		echo json_encode($check);
	}

	public function actionCancel($id) {
		$consultation = Consultation::model()->findByPk($id);
		switch ($consultation['status']) {
			case '0':
			case '3':
			case 'BEGIN':
				Yii::app()->user->setFlash('info-message', 'Невозможно отменить');

				break;
			default:

				$userID = $consultation['userID'];
				//$expertID = $consultation['expertID'];
				//$expert = Users::model()->findByPk($expertID);
				$user = Users::model()->findByPk($userID);

				$criteria = new CDbCriteria();
				$criteria->compare('`cbk_id`', $id);
				$criteria->compare('`check`', (int) 0);

				if (CallBack::model()->count($criteria)):

					Yii::app()->user->setFlash('info-message', 'Невозможно отменить');
				else:

					$command = Yii::app()->db->createCommand();
					$sum_cj = $command->select('SUM(`cost`) as `cost`, SUM(CEILING(`duration` / 60)) as `duration`')
						->from('calljournal')
						->where('call_back.cbk_id=:cbk_id', array(':cbk_id' => $id))
						->join('call_back', 'call_back.id = calljournal.id_callback')
						->queryRow();

					if ($sum_cj['cost'] < $consultation['cost']):

						$returned = $consultation['cost'] - $sum_cj['cost'];
						$bill = $user['bill'] + $returned;
						$cost = $consultation['cost'] - $returned;
						$duration = $sum_cj['duration'] * 60;

						Yii::app()->db->createCommand("UPDATE `users` SET `bill` = '$bill' WHERE `id` = '" . (int) $userID . "'")->query();
						Yii::app()->db->createCommand("UPDATE `consultation` SET `duration` = '$duration',`cost`= '$cost' WHERE `id` = '" . (int) $id . "'")->query();
					endif;

					if ($consultation['status'] == '1'):
						if (isset($_GET['act'])) {
							if ($_GET['act'] == 'expert')
								Yii::app()->db->createCommand("UPDATE `consultation` SET `status` = 3 WHERE `id` = '" . (int) $consultation['id'] . "'")->query();
						} else
							Yii::app()->db->createCommand("UPDATE `consultation` SET `status` = 0 WHERE `id` = '" . (int) $consultation['id'] . "'")->query();
					endif;
				endif;
				break;
		}

		if (Yii::app()->user->role !== 'expert')
			$this->redirect(array('/myBooking/'));
		else
			$this->redirect(array('/expert/booking/admin'));
	}

	/**
	 * Site register consultation
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionRegister($id = '') {
$this->layout='//layouts/main_payment';
		if ( ! Yii::app()->user->isGuest){
			// проверка пользователь или эксперт
			if (Yii::app()->user->role !== Users::ROLE_USER){
				throw new CHttpException(404, 'Эта операция возможна только клиенту.');
			}
		}

		$expert = Users::model()->findByPk((int) $id);
		$criteria = new CDbCriteria();
		$criteria->compare('role', 'expert');
		$expertList = Users::model()->findAll($criteria);

		if ($expert === NULL)
			throw new CHttpException(404, 'The requested page does not exist.');

		$model = new Consultation();
		$this->performAjaxValidation($model);

		if (isset($_POST['Consultation'])) {

			$model->attributes = $_POST['Consultation'];

			$amount = $expert->tariff * $model->duration;

			// проверка занятости
			$criteria = new CDbCriteria();
			$criteria->compare('expertID', (int) $_POST['Consultation']['expertID']);
			$criteria->addCondition("`date` BETWEEN STR_TO_DATE('" . $model->date_call . " " . $model->time_call .
				"','%d.%m.%Y %H:%i') AND " .
				" DATE_ADD(STR_TO_DATE('" . $model->date_call . " " . $model->time_call .
				"','%d.%m.%Y %H:%i'),INTERVAL " . ((int)$model->duration) . " MINUTE)");

			$time = Consultation::model()->count($criteria);

			if ($time > 0) {
				Yii::app()->user->setFlash('error', 'В заданное вами время этот эксперт занят!');
				$this->refresh();
			}

			$model->cost    = $amount;
			$model->expertID = $_POST['Consultation']['expertID'];
			$model->image   = $_POST['Consultation']['image'];
			$model->duration = $model->duration * 60;


			if ( ! Yii::app()->user->isGuest) {
				$user = Users::model()->findByPk(Yii::app()->user->id);
				$model->userID = Yii::app()->user->id;
				$model->status = 'nopaid';
				$model->email = $user->email;

				// проверка баланса пользователя
				if ($user['bill'] < $amount){
					$model->save();
					$this->redirect(array("/payment?other_amount={$amount}&consultation_id={$model->id}&service=consultation"));
				}

				//Берет оплату с пользователя
				if (Controller::takeUserBill(Yii::app()->user->id, $amount)) {
					$model->status = '1';

					if($model->validate()) {
						$model->save();

						ShopOrder::model()->sendMailSuccessPaid('consultation', $model->id);

						Yii::app()->user->setFlash('сonsultation', 'Ожидайте вызов от эксперта в назначенное время (время московское)! <br/> Зарезервировано на консультацию ' . $amount . 'руб.');
					}
				} else {
					Yii::app()->user->setFlash('error', 'Невозможно списать средства в настоящее время. Попробуйте позже');
				}
			}
			else {

				$model->status = 'nopaid';

				if ($model->save()){

					$ident = md5(uniqid(rand(0, 10), 1));
					$guest = new Guest;
					$guest->email = $model->email;
					$guest->id_guest = $ident;
					$guest->consultation = $model->id;
					if ($guest->save()) {
						if ($guest->id_guest != 'error') {
							$this->redirect("http://{$_SERVER['HTTP_HOST']}/payment?guest_id={$guest->id_guest}&consultation_id={$model->id}&service=consultation");
						}
						else {
							Yii::app()->user->setFlash('error', 'Невозможно забронировать консультацию.Обратитесь к администратору. ');
						} // если id != error
					} //если модель гостя сохранена
				} // если модель сохранена
			} // если пользователь или гость
			$this->refresh();
		} // если заданы post Consult
		$this->render(
			'register',
			array(
				'expert' => $expert,
				'model' => $model,
				'expertList' => $expertList,
				'infoBlock' => $this->actionExpertInfo($id)
			)
		);
	}

	public function actionExpertInfo($idx) {

		$expert = Users::model()->findByPk($idx);
		$image = $this->getUserImage($idx);
		$arr = array('online' => 'онлайн', 'busy' => 'занят', 'autonomous' => 'нет на линии');

		$a = '
			<div class="img"><img src="'.Yii::app()->request->baseUrl .'/uploads/user/'.$image.'" alt="" /></div>
			<div class="name">'.CHtml::link($expert['name'], array('/extrasens/view/', 'id' => $expert['id'], 'title' => $this->getCHPU($expert['name'] . '-' . $expert['profession']))).'</div>
			<div class="type">'.$expert['profession']." &nbsp;&nbsp; " .'<span class="'.$expert['online'] .'">'.$arr[$expert['online']].'</span></div>';
		if(isset($_GET['idx'])){
			echo json_encode(
				array(
					'info' => $a,
					'reviews' => $this->renderPartial('/extrasens/_review', array('model'=>$expert), true),
					'schedule' => $this->renderPartial('/extrasens/_schedule', array('model'=>$expert), true))
			);
		}
		else
			return $a;
	}

	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'site-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionIndex() {

		$criteria = new CDbCriteria();
		$criteria->order = 'online desc';
		$criteria->compare('role', 'expert');
		// setrais
		$criteria->select = '*, IF( online = "online", 1, IF ( online ="busy", 2 , 3)) as sts, (SELECT count(*) FROM expertreviews ev WHERE (ev.expertID = t.id and ev.status = 1)) as cntrev';
		$criteria->order = 'sts asc, cntrev desc, id desc';
		// end:setrais
		//        ini_set("xdebug.profiler_enable", 1);
		$count = Users::model()->count($criteria);
		//        ini_set("xdebug.profiler_enable", 0);
		$pages = new CPagination($count);
		$pages->pageSize = 14;
		$pages->applyLimit($criteria);

		$experts = Users::model()->findAll($criteria);
		$this->clips['newAnswers'] = Answer::newAnswers(0, 7);
		$this->clips['newArticles'] = Article::newArticles();
		$this->render('index', array('experts' => $experts, 'pages' => $pages));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else {
				$pathToErrorView = "";
				$this->layout = "/error";
				switch ($error['code']) {
					case "404":
						$pathToErrorView = "/../views/system";
						break;
				}
				if (!empty($pathToErrorView)) {
					$this->layout = "/error";
					$this->render($pathToErrorView . "/error" . $error['code'], $error);
				} else {
					$this->render('error', $error);
				}
			}
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact() {
		$model = new ContactForm;
		if (isset($_POST['ContactForm'])) {
			$model->attributes = $_POST['ContactForm'];
			if ($model->validate()) {
				$name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
				$subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
				$headers = "From: $name <{$model->email}>\r\n" .
					"Reply-To: {$model->email}\r\n" .
					"MIME-Version: 1.0\r\n" .
					"Content-type: text/plain; charset=UTF-8";

				Yii::import('application.extensions.phpmailer.JPhpMailer');
				$mail = new JPhpMailer();
				$mail->SetFrom($model->email, $model->name);
				$mail->Subject = $model->subject;
				$mail->AltBody = '';
				$mail->CharSet = 'UTF-8';
				$mail->MsgHTML('Имя: ' . $model->name . '<br />E-mail: ' . $model->email . '<br /> Тема: ' . $model->subject . '<br />Сообщение: ' . $model->body);
				$mail->AddAddress(Yii::app()->params['adminEmail'], $model->name);
				$mail->Send();
				Yii::app()->user->setFlash('contact', 'Благодарим Вас за обращение к нам. Мы ответим вам как можно скорее.');
				$this->refresh();
			}
		}
		$this->render('contact', array('model' => $model));
	}

	public function actionAgreement() {
		$model = new ContactForm;

		$this->render('agreement', array('model' => $model));
	}

	public function actionShowStaticPage($curl) {
		$this->layout = "main_static";
		$page = Page::model()->findByAttributes(array('clean_url' => $curl));
		if ($page != NULL) {
			$this->render("showStaticPage", array(
				"page" => $page,
			));
		} else {
			throw new CHttpException(404, Yii::t('app', "Page not found"));
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin() {

		$service = Yii::app()->request->getQuery('service');
		if (isset($service)) {
			$authIdentity = Yii::app()->eauth->getIdentity($service);
			$authIdentity->redirectUrl = Yii::app()->user->returnUrl;
			$authIdentity->cancelUrl = $this->createAbsoluteUrl('site/login');

			if ($authIdentity->authenticate()) {
				$identity = new ServiceUserIdentity($authIdentity);
				// Successful login
				if ($identity->authenticate()) {
					Yii::app()->user->login($identity);

					// Special redirect with closing popup window
					$authIdentity->redirect();
				} else {
					// Close popup window and redirect to cancelUrl
					$authIdentity->cancel();
				}
			}

			// Something went wrong, redirect to login page
			$this->redirect(array('site/login'));
		}

		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				Yii::app()->db->createCommand("UPDATE `users` SET `online` = 'online' WHERE `id` = '" . (int) Yii::app()->user->id . "'")->query();
				if (Yii::app()->user->checkAccess('expert')) {
					$this->redirect(array('/expert'));
				} else {
					$this->redirect(Yii::app()->user->returnUrl);
				}
			}
		}
		// display the login form
		$this->render('login', array('model' => $model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app()->db->createCommand("UPDATE `users` SET `online` = 'autonomous' WHERE `id` = '" . (int) Yii::app()->user->id . "'")->query();
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

}
