<?php

class DreamsController extends Controller {

    public function actionIndex() {
	    $this->layout='//layouts/main_payment';
        $model = new DreamsForm();

        if (isset($_GET['DreamsForm'])) {
            $model->attributes = $_GET['DreamsForm'];
            if ($model->validate()) {

                $criteria = new CDbCriteria();
                if (!empty($_GET['DreamsForm']['category']))
                    $criteria->compare('category_id', $_GET['DreamsForm']['category']);
                $criteria->addSearchCondition('name', $_GET['DreamsForm']['name'], TRUE);

                $count = Dreams::model()->count($criteria);
                $pages = new CPagination($count);
                $pages->pageSize = 25;
                $pages->applyLimit($criteria);

                $models = Dreams::model()->findAll($criteria);
                $this->render('index', array('models' => $models,
                    'pages' => $pages, 'model' => $model));
            }else{
               $this->render('index', array('model' => $model));
            }
        }
        else
            $this->render('index', array('model' => $model));
    }

    public function actionSearch() {

        if (Yii::app()->request->isAjaxRequest) {
            if (!empty($_GET['DreamsForm']['name'])) {
                $criteria = new CDbCriteria();
                if (!empty($_GET['DreamsForm']['category']))
                    $criteria->compare('category_id', $_GET['DreamsForm']['category']);
                $criteria->addSearchCondition('name', $_GET['DreamsForm']['name'], TRUE);
                $criteria->limit = 10;
                $models = Dreams::model()->findAll($criteria);
                $result = '';
                foreach ($models as $model) {
                    $result .= CHtml::link(str_replace($_GET['DreamsForm']['name'], '<b>' . $_GET['DreamsForm']['name'] . '</b>', $model['name']) . ' - ' . DreamsCategory::model()->findByPk($model['category_id'])->name . '<br />', array('dreams/view/', 'id' => $model['id'],'title'=>$this->getCHPU($model['name'] . '-' . DreamsCategory::model()->findByPk($model['category_id'])->name)));
                }
                echo $result;
            }
        } else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionView($id) {
$model2 = new DreamsForm();
        $model = $this->loadModel($id);
        $this->render('view', array(
            'model' => $model,'model2'=>$model2
        ));
    }

    public function loadModel($id) {
        $model = Dreams::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

}
