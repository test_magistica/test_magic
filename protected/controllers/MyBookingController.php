<?php

class MyBookingController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations            
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'create', 'captcha', 'download'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        $criteria = new CDbCriteria();
        $criteria->order = 'id desc';
        $criteria->compare('userID', Yii::app()->user->id);
        if (isset($_GET['status']) && is_numeric($_GET['status']))
            $criteria->compare('status', $_GET['status']);
        $count = Booking::model()->count($criteria);
        $pages = new CPagination($count);

        if (isset($_GET['size']) && is_numeric($_GET['size']))
            $pages->pageSize = $_GET['size'];
        else
            $pages->pageSize = 5;

        $pages->applyLimit($criteria);
        $models = Booking::model()->findAll($criteria);

	
	$criter = new CDbCriteria();
	$criter->compare('userID',(int)Yii::app()->user->id);
	$criter->addCondition('`status` <> "0"');
	$criter->addCondition('`status` <> "3"');
        
        $criter->order = "Field(`status`,'1','BREAK') desc,`date` desc"; 

	//$criter->select = array('*','DATE_FORMAT(`date`,\'%d.%m.%Y %H:%i:%s\') as date');
        
        //$consulations = Consultation::model()->findAll("userID = '" . Yii::app()->user->id . "'");
        
        //$count2 = Consultation::model()->count("`userID` = '" . Yii::app()->user->id . "'");
        
        $criter2 = new CDbCriteria();
	$criter2->compare('userID',(int)Yii::app()->user->id);
	$criter2->compare('status',array('1'));
	
        $count2 = Consultation::model()->count($criter2);
        
        $consulations = Consultation::model()->findAll($criter);

        $this->render('index', array('models' => $models,
            'pages' => $pages, 'count' => $count, 'count2' => $count2, 'consulations' => $consulations));
    }

    public function actionView($id) {
        $message = new Servicemessage();
        $servive = Booking::model()->findByPk($id);
        if (isset($_POST['Servicemessage'])) {
            $message->message = $_POST['Servicemessage']['message'];
            $message->fromID = Yii::app()->user->id;
            $message->toID = $servive->expertID;
            $message->userID = Yii::app()->user->id;
            $message->createTime = new CDbExpression('NOW()');
            $message->status = 0;
            $message->bookingID = $id;

            if ($message->save()){
	            if (isset($_FILES['files'])) {
	            foreach ($_FILES['files']['name'] as $key => $filename):
		            move_uploaded_file($_FILES['files']['tmp_name'][$key], Yii::app()->params['uploadDir'] . $filename);
		            Yii::app()->db->createCommand("INSERT INTO `bookingfiles` (`id`, `bookingID`, `file`, `expertID`, `messageID`) VALUES (NULL, '$id', '$filename', '" . (int) $message->toID . "', '{$message->id}')")->query();
	            endforeach;
            }
                $this->refresh();
            }
        }
        $criteria = new CDbCriteria();
        $criteria->order = 'id desc';
        $criteria->compare('fromID', (int) Yii::app()->user->id);
        $criteria->compare('bookingID', (int) $id);
        $count = Servicemessage::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);
        $models = Servicemessage::model()->findAll($criteria);

        Yii::app()->db->createCommand("UPDATE `servicemessage` SET `status` = '1' WHERE `userID` != '" . (int) Yii::app()->user->id . "' AND bookingID = '" . $id . "' AND status = 0")->query();
        $this->render('view', array(
            'models' => $models, 'pages' => $pages, 'model' => $message, 'count' => $count
        ));
    }

    public function actionDownload($id) {
        $model = Bookingfiles::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        $filename = 'upload2/' . $model->file;
        Yii::app()->getRequest()->sendFile(basename($filename), @file_get_contents($filename));
    }

}
