<?php

class ResultController extends Controller {
	public function actionIndex() {

		if (isset($_GET['InvId'])) {
			$criteria = new CDbCriteria();
			$criteria->compare('id', intval($_GET['InvId']));
			$criteria->compare('pay_success', '0');

			$pay_log = new Payments_log;

			if ($model = ShopOrder::model()->find($criteria)) {
				$mrh_pass2  = "ahXe1aecak"; // второй пароль
				$OutSum     = $_GET['OutSum'];
				$InvId      = $_GET['InvId'];
				$shpItem    = $_GET['shpItem'];
				$crc_model  = strtolower(md5("$OutSum:$InvId:$mrh_pass2:shpItem=$shpItem"));
				$crc_get    = strtolower($_GET['SignatureValue']);

				if ($crc_model == $crc_get) {
					$model->pay_success = 1;
					$model->date_pay = date("Y-m-d H:i:s");
					$model->save();

					$serviceParams = Yii::app()->params['paymentServices'];
					$serviceParams = $model->entity != 'bill' ? $serviceParams[$model->entity] : $serviceParams;

					if ($model->userID > 0 ){
						if($model->entity == 'bill') {
							if (Controller::updateUserBill($model->userID, $model->amount)) {
								Yii::app()->user->setFlash('success', 'Ваш счет пополнен!');
							}
							else {
								$pl = new Payments_log;
								$pl->id_pay = $model->id;
								$pl->message = "ROBOKASSA. failed query user update bill.";
								$pl->save();
							}
						}
						else{
							Yii::app()->db->createCommand("UPDATE {$serviceParams['table']} SET status = '1' WHERE id = {$model->entity_id} ")->query();
							ShopOrder::model()->sendMailSuccessPaid($model->entity, $model->entity_id);
						}
					}
					else {//гость
						Yii::app()->db->createCommand("UPDATE {$serviceParams['table']} SET status = 'paid' WHERE id = {$model->entity_id} ")->query();
						ShopOrder::model()->sendMailSuccessPaid($model->entity, $model->entity_id);
					}
				}
				else {
					$pay_log->id_pay = $_GET['InvId'];
					$pay_log->message = 'Error result-pay.CRC are not equal.CRC='.$crc_model.',get:'.json_encode($_GET);
					$pay_log->save();
				}

			}
			else {
				$pay_log->id_pay = $_GET['InvId'];
				$pay_log->message = 'Error result-pay.Not search with status<>paid.'.json_encode($_GET);
				$pay_log->save();
			}
		}
		$this->redirect(array('/site/index'));
	}
}