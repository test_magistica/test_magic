<?php
//
class PlatronController extends Controller {

	public function actionIndex() {
		$pg_order_id = ! empty($_REQUEST["pg_order_id"]) ? intval($_REQUEST["pg_order_id"]) : 0;
		$oOrder = ShopOrder::model()->findByPk($pg_order_id);


		if (Yii::app()->platron->checkPayment($oOrder)) {
			// payment success
			$pay_log = new Payments_log;

			if ($oOrder->pay_success != 1 && intval($_REQUEST["pg_result"]) == 1) {
				$oOrder->pay_success = 1;
				$oOrder->date_pay = date("Y-m-d H:i:s");
				$oOrder->save();

				$serviceParams = Yii::app()->params['paymentServices'];
				$serviceParams = $oOrder->entity != 'bill' ? $serviceParams[$oOrder->entity] : $serviceParams;

				if ($oOrder->userID > 0){
					if($oOrder->entity == 'bill') {
						if (Controller::updateUserBill($oOrder->userID, $oOrder->amount)){
							Yii::app()->user->setFlash('success', 'Ваш счет пополнен!');
							$this->redirect(array('platron/success'));
						}
						else {
							$pay_log->id_pay = $oOrder->id;
							$pay_log->message = "PLATRON. failed query user update bill.";
							$pay_log->save();
						}
					}
					else{
						Yii::app()->db->createCommand("UPDATE {$serviceParams['table']} SET status = '1' WHERE id = {$oOrder->entity_id} ")->query();
						ShopOrder::model()->sendMailSuccessPaid($oOrder->entity, $oOrder->entity_id);
						$this->redirect(array('platron/success'));
					}
				}
				else{
					Yii::app()->db->createCommand("UPDATE {$serviceParams['table']} SET status = 'paid' WHERE id = {$oOrder->entity_id} ")->query();
					ShopOrder::model()->sendMailSuccessPaid($oOrder->entity, $oOrder->entity_id);
					$this->redirect(array('platron/success'));
				}
			}
			else {
				$pay_log->id_pay = $oOrder->id;
				$pay_log->message = "Error PLATRON payment";
				$pay_log->save();
			}
		}
		else {
			$this->redirect(array('platron/failed'));
		}
	}

	public function actionSuccess() {
		$this->render('success');
	}

	public function actionFailure() {
		$this->render('failed');
	}



}