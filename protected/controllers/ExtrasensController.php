<?php

class ExtrasensController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations            
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'viewtab', 'create', 'captcha'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('update', 'photo', 'upload', 'setphoto', 'password'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /*public function actionView($id) {
        
        $model = $this->loadModel($id);
        
        if ( (!isset($_GET['title']) || empty($_GET['title'])) && $id<>'' ) {
            $this->redirect(array('view','id'=>$model->id, 'title' => $this->getCHPU($model->name . '-' . $model->profession)));              
        }
                
        $review = new Expertreviews();
        $count = Expertreviews::model()->count("expertID = '" . $id . "' and status = 1");
        if (isset($_POST['Expertreviews'])) {
             if (Yii::app()->user->isGuest)
                Yii::app()->user->loginRequired();      
            $review->attributes = $_POST['Expertreviews'];
            if ($review->validate()) {
                $review->save();
                Yii::app()->user->setFlash('rewiew', 'Спасибо, Ваш отзыв будет размещен, в ближайшее время!');
                $this->refresh();
            }
        }
        
        $this->render('view_old', array(
            'model' => $model, 'review' => $review, 'count' => $count
        ));
    }*/

    public function actionView($id, $tab = null, $title=null) {


	    $this->layout='//layouts/main_expert_view';
	    // Устанавливаются в htaccess
	    //        var_dump($_ENV);die();
	    if(isset($_ENV['tab']) && isset($_ENV['tab'])){
		    $tab = $_ENV["tab"];
		    $title = $_ENV["title"];
		    unset($_ENV['tab']);
		    unset($_ENV['title']);
	    }
	    /*if(isset($_GET['tab'])){
			$tab = $_GET["tab"];
			unset($_GET['tab']);
			unset($_GET['title']);
		}*/
	    $model = $this->loadModel($id);

	    $review = new Expertreviews();
	    $count = Expertreviews::model()->count("expertID = '" . $id . "' and status = 1");
	    if (isset($_POST['Expertreviews'])) {
		    if (Yii::app()->user->isGuest)
			    Yii::app()->user->loginRequired();
		    $review->attributes = $_POST['Expertreviews'];
		    if ($review->validate()) {
			    $review->save();
			    Yii::app()->user->setFlash('rewiew', 'Спасибо, Ваш отзыв будет размещен, в ближайшее время!');
			    $this->refresh();
		    }
	    }

	    $extabs = array('otzyvy','uslugi',"stat'i",'chat','raspisanie');
	    $tabs = array('review','service','article','chat','schedule');
	    $routabs = array_combine($extabs,$tabs);


	    $this->clips['newAnswers'] = Answer::lastExpertAnswers((int)$model->id);
	    if( Yii::app()->request->isAjaxRequest ) {

		    if ( $tab )
		    {
			    if ( !array_key_exists($tab,$routabs))
				    throw new CHttpException(404, 'The requested page does not exist.');

			    $this->renderPartial('_'.$routabs[$tab],array(
				    'model' => $model, 'review' => $review, 'count' => $count,
				    'tab'=>( $tab===null ? $tab : $routabs[$tab]), 'routabs'=>array_flip($routabs)
			    ));

			    Yii::app()->end();
		    }


	    }  else {

		    $this->render('view', array(
			    'model' => $model, 'review' => $review, 'count' => $count, "expertID" =>  $id ,
			    'tab'=>( $tab===null || !in_array($tab,$extabs) ? null : $routabs[$tab] ), 'routabs'=>array_flip($routabs)
		    ));
	    }

    }
    
    public function loadModel($id) {
        $model = Users::model()->findByPk($id);
        if ($model === null || $model->role !== 'expert')
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'register-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}