<?php

class AnswerController extends Controller
{
	public $layout='column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		if(isset($_POST['ajax']) && $_POST['ajax']==='answer-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if(isset($_POST['Answer']))
		{
			$model->attributes=$_POST['Answer'];
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel()->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_POST['ajax']))
				$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Answer', array(
			'criteria'=>array(
				'with'=>'question',
				'order'=>'t.status, t.create_time DESC',
			),
		));

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Approves a particular answer.
	 * If approval is successful, the browser will be redirected to the answer index page.
	 */
	public function actionApprove()
	{
		if(Yii::app()->request->isPostRequest)
		{
			$answer=$this->loadModel();
			$answer->approve();
			$this->redirect(array('index'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
        
        /**
         * Устанавливает рейтинг ответу
         * @param int $id Идентификатор ответа
         * @param int $reit Величина рейтинга указанная пользователем
         * @return json
         */
        public function actionaddRaiting($id, $reit){
            if(isset($id) && isset($reit)){
                //Проверяем рейтинг присланный пользователем на допустимые значения
                if(in_array($reit, array("1","2","3","4","5"))){
                    $answer = Answer::model()->with('a_author', 'a_author.countVotes')->findByPk($id);
                    if($answer !== NULL){
                        $newRating = ((int)$answer->a_author->expert_raiting + (int)$reit);
//                        $maxRating = (int)Yii::app()->params['maxExpertRaiting'];
//                        $newRating = ((int)$answer->a_author->expert_raiting <  $maxRating && $newRating > $maxRating) ? $maxRating : $newRating;
//                        if($newRating <= $maxRating){
                            $answer->a_author->expert_raiting = $newRating;
                            $answer->is_voted = (int)$reit;
                            if($answer->a_author->save(FALSE, 'expert_raiting') && $answer->save(FALSE, 'is_voted')){
                              $answer->a_author->refresh();
                                
                            } 
//                        }  
                        echo json_encode(
                                  array(
                                      'result' => 'Ваша оценка',
                                      'usermark' => $reit, 
                                      'exp_reit' => $answer->a_author->expert_raiting,
                                      'answerid' => $id,
                                      'author' => $answer->a_author->id,
                                      'countVotes' => $answer->a_author->countVotes,
                                      'maxRating' => (int)Yii::app()->params['maxExpertRaiting'],
                                  )
                              );
                          Yii::app()->end();
                          return;
                            
                       
                    }
                }
            }
            echo json_encode(array('error' => 'addRaiting'));
            Yii::app()->end();
        }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=Answer::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
}
