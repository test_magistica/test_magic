<?php

class QuestionController extends Controller {

    public $layout = 'colquest';
    public $pageTitle = '';
    public $metaDescription = '';
    public $metaKeywords = '';
    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;

    /**
     * @return array action filters
     */
    public function _filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * You should override this method to provide stronger access control 
     * to specifc restfull actions via AJAX
     */
    public function validateAjaxUser($action) {
        return true;
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function _accessRules() {
        return array(
            array('allow', // allow all users to access 'index' and 'view' actions.
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated users to access all actions
                'actions' => array('create'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated users to access all actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * @return array action filters
     *
      public function filters()
      {
      return array(
      'accessControl', // perform access control for CRUD operations
      );
      }

      /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     *
      public function accessRules()
      {
      return array(
      array('allow',  // allow all users to access 'index' and 'view' actions.
      'actions'=>array('index','view'),
      'users'=>array('*'),
      ),
      array('allow', // allow authenticated users to access all actions
      'users'=>array('@'),
      ),
      array('deny',  // deny all users
      'users'=>array('*'),
      ),
      );
      }

     */

    /**
     * Displays a particular model.
     */
    public function actionView($id) {

//        if (Yii::app()->user->isGuest) {
//            Yii::app()->user->returnUrl = Yii::app()->request->requestUri;
//            Yii::app()->request->redirect(Yii::app()->createAbsoluteUrl("site/login"));
//        }
        $this->layout = 'colquest2';

        $criteria = new CDbCriteria(); // для спецификации критериев запроса
//        $criteria->order = 't.create_time DESC'; // соритруем по дате в обратном порядке
        $criteria->with = array('author', 'author.countQuestions', 'area_expertise');
        $criteria->condition = "t.id=" . $id;

        $question = Question::model()->find($criteria);
        // Set up title, description and keywords of meta tags of the page
//        $this->pageTitle = $question->title." | ".AreaExpertise::model()->findByPk($question->area_expertise_id)->name." - гадание онлайн";
//        $this->metaDescription = $question->metaDesc;
//        $this->metaKeywords = $question->metaKey;

        $this->pageTitle = $question->title;//str_replace($del_symbols, "", $question->title);
        if(!empty($question->metaDesc)){
            $this->pageDescription = $question->metaDesc;
        }else{
            $this->pageDescription = mb_substr($question->content, 0, 255);
            $words = explode(" ", $question->content);
            $sum = 0;
            $descArr = array();
            foreach ($words as $word){
                $sum += mb_strlen($word);
                $descArr[] = $word;
                if($sum > 255) break;
            }
            $this->pageDescription = implode(" ", $descArr);
        }
        if(!empty($question->metaKey)){
            $this->pageKeywords = explode(",", $question->metaKey);
        }else{
            $this->pageKeywords = explode(" ", $question->title);
        }
        // End of --- Set up title, description and keywords of meta tags of the page
        
        $criteria = new CDbCriteria();
        $criteria->order = "create_time DESC";
        $criteria->condition = "post_id = " . $id;
//                var_dump($question);die();
//        Yii::endProfile('blockId');
//        $user = Users::model()->findByPk($question->author_id);
        //Формируем пейджинг для ответов
        $answers = Answer::model()->findAll($criteria);
        $pages = new CPagination(count($answers)); //передаём число элементов
        $pages->pageSize = 2; // элементов на страницу
        $pages->applyLimit($criteria);

//        // Catalog of AreaExpertise
//        $areas = AreaExpertise::model()->with('countQuestion')->findAll('status=1');
//        $this->clips['areas'] = $areas;
        
        $dataProvider = new CActiveDataProvider('Answer', array(
            'criteria' => array(
                'condition' => "post_id = " . $id,
                'order' => 't.create_time DESC',
                'with' => array(
                    'a_author' => array(
                        'with' => 'countVotes',
                    ), 
                    'question'),
            ),
//            'countCriteria'=>array(
//                'condition'=>'status='.Answer::STATUS_APPROVED,
//                // 'order' and 'with' clauses have no meaning for the count query
//            ),
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
//            var_dump($question->area_expertise->id);
//            var_dump(Users::bestExperts($question->area_expertise->id));die();
        $this->clips['bestExperts'] = Users::bestExperts($question->area_expertise->id);
        $this->clips['newAnswers'] = Answer::newAnswers($id);

        $this->render('view', array(
            'question' => $question,
            'pages' => $pages,
            'a_dataprovider' => $dataProvider,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
//        $this->layout = 'colquest2';
        //Отсекаем незарегистрированных пользователей
        if (Yii::app()->user->isGuest) {
            Yii::app()->user->returnUrl = Yii::app()->request->requestUri;
            Yii::app()->request->redirect(Yii::app()->createAbsoluteUrl("site/login"));
        } else if (Yii::app()->user->role == Users::ROLE_EXPERT) { // У эксперта нет возможности задавать вопросы
            Yii::app()->request->redirect(Yii::app()->createAbsoluteUrl("question"));
        }
        $model = new Question;
//                var_dump(Yii::app()->session->isStarted);die();
        // st - шаг создания фопроса
        //st = 1  - заполняется форма
        //st = 2  - выбирается форма размещения вопроса (платно | бесплатно)
        $step = isset($_GET['st']) ? $_GET['st'] : NULL;

        if ($step !== NULL && isset($_POST['Question'])) {
            if ($step == "1") {
                $model->attributes = $_POST['Question'];
                // Проверяем указал ли вопрос пользователь
                if (!$model->validate(array('title'))) {
                    Yii::app()->user->setFlash("error", "Укажите пожалуйста вопрос.");
                    $this->redirect(Yii::app()->createAbsoluteUrl("question"));
                }
            } else if ($step == "2") {
                // Метод размещения вопроса
                $methodOfPlacingTheQuestion = isset($_POST['Question'] ["pay"]) ? $_POST['Question'] ["pay"] : FALSE;
//                        var_dump( (CUploadedFile::getInstance($model,'image')));die();
                if ($methodOfPlacingTheQuestion === FALSE && isset($_POST['Question'])) {
                    $model->attributes = $_POST['Question'];
                    $model->image = CUploadedFile::getInstance($model, 'image');
                    if ($model->save()) {
//                        if ($model->image !== NULL)
//                            $model->image->saveAs(Yii::app()->params['uploadDir'] . "question_img/" . time().$model->image->getName());
                        // Сохраняем первичный ключ вопроса для дальнейших шагов
                        $id = $model->id;
                        Yii::app()->session->writeSession("qid", $id);
//                        if(!$this->checkUserBill((int)  Yii::app()->user->id, Question::PAY_AMOUNT)){
//                            Yii::app()->user->setFlash('not_enough_money', Yii::t('app', 'Для отправки платного вопроса Вам необходимо пополнить баланс.'));
//                        }
                        // $this->redirect(array('view','id'=>$model->id));
                    }
                    else {
                        $step = 1;
                    }
                } elseif (in_array($methodOfPlacingTheQuestion, array("0", "1"))) {
                    $question = NULL;
                    switch ($methodOfPlacingTheQuestion) {
                        // Разместить вопрос платно
                        case "0":
                            if ($this->checkUserBill(Yii::app()->user->id, Question::PAY_AMOUNT)) {
                                $user = Users::model()->findByPk((int)Yii::app()->user->id);
                                if($user !== NULL){
                                    $user->bill -= Question::PAY_AMOUNT;
                                    $user->free_question_date = NULL;
                                    if($user->save(FALSE, array('bill', 'free_question_date'))){
                                        $question = $this->placeTheQuestionAsPaid(Question::STATUS_PUBLISHED);
                                        $question->is_paid = 1;
                                        $question->save(FALSE, array('is_paid'));
                                        // Рассылаем экспертам
                                        $this->sendEmailToExperts('emailTemplates/email_question_pay', array('question' => $question), $question);
                                        // Пишем пользователю
                                        $msg = $this->renderPartial("email_question_pay", array('question' => $question), TRUE);
                                        $this->sendMailUser($question->author->id, $question->title, $msg);
                                    }
                                }
                            } else { // Денег на счету не достаточно. Отправляем на платежку и мылим письмо
                                $question = $this->placeTheQuestionAsPaid(Question::STATUS_DRAFT);
                                $msg = $this->renderPartial("email_question_pay", array('question' => $question), TRUE);
                                $this->sendMailUser($question->author->id, $question->title, $msg);
//                                Yii::app()->user->setFlash('not_enough_money', Yii::t('app', 'После пополнения баланса, Вы можете отправить сообщение экспертам из личного кабинета\n пункт "Мои вопросы".'));
                                $this->redirect(Yii::app()->createAbsoluteUrl("payment"));
                            }
                            break;
                        // Разместить вопрос бесплатно 
                        case "1":
                            $userNotif = new MessageNotificationComponent();
                            //Check, can the user send a free question.
                            $user = Users::model()->findByPk(Yii::app()->user->id);
                            // Может ли пользователь отправлять бесплатные вопросы
                            $can_send_free_question = TRUE;
                            if($user->free_question_date !== NULL){
                                /*$udt = new DateTime($user->free_question_date);
                                $currDt = new DateTime();
                                $diff = $currDt->diff($udt);
                                // Если разница превысила интервал между отправками бесплатных вопросов - TRUE иначе  - FALSE
                                $can_send_free_question = ($diff->d > Question::DELAY_FOR_FREE_QUESTION);*/

	                            $can_send_free_question = FALSE;
                            }
                            
                            if($can_send_free_question){
                                $question = $this->placeTheQuestionAsFree();
                                $dt = new DateTime();
                                $user->free_question_date = $dt->format(Question::DATE_TIME_FORMAT);
                                $user->update(array('free_question_date'));
                                if($question !== NULL){
                                    // Рассылаем экспертам
    //                                $this->sendEmailToExperts('emailTemplates/email_question_free', array('question' => $question), $question);

                                    $msg = $this->renderPartial("email_question_free", array('question' => $question), TRUE);
                                    $this->sendMailUser($question->author->id, $question->title, $msg);
                                }
                            }else{
                                $qid = Yii::app()->session->readSession("qid");
                                $question = Question::model()->findByPk($qid);
//                                if($question !== NULL){
//                                    if($question->image != NULL){
//                                        $img = Yii::app()->params['uploadDir'].'question_img/' . $question->image;
//                                        if(file_exists($img)){
//                                            unlink($img);
//                                        }nopa
//                                    }
//                                    $question->delete();
//                                }
                                $step = 2;
                                
                                $userNotif->addMessage("notice", "Внимание! Вы уже задавали бесплатный вопрос. Чтоб Ваш вопрос был опубликован, задайте платный вопрос.");
                                //$userNotif->addMessage("notice", "Внимание! Задавать бесплатно вопросы экспертам Вы можете не более 1 раза в неделю.");
//                                Yii::app()->user->setFlash("notice", "Внимание! Задавать бесплатно вопросы экспертам Вы можете не более 1 раза в неделю.");
                               $this->render('create', array(
                                    'model' => $question,
                                    'step' => $step,
                                ));
                               return;
                            }
                            break;
                    }
                    if($question != NULL){
                        Yii::app()->user->setFlash('question_placed', "Ваше фото успешно отправлено экспертам. В самое ближайшее время, вам ответят.  
                                  Ответ с сылкой на Ваш вопрос поступит на электронную почту");
//                        ob_get_clean();
//                        var_dump($question);
                        $this->redirect(Yii::app()->createAbsoluteUrl("question/view", array('id' => $question->id, 'title' => $question->url)));
                    }
                    else {
                        $this->redirect("/question");
                    }
                    
                    
                }
            }
        }
        
        $this->render('create', array(
            'model' => $model,
            'step' => $step,
        ));
    }

    /**
     * Отображает вопросы для указанной AreaExpertise
     * @param int $id Идентификатор AreaExpertise в базе
     */
    public function actionViewArea($id) {

        $criteria = NULL;
        if(Yii::app()->user->isGuest || Yii::app()->user->role == Users::ROLE_BANNED){
        $criteria = new CDbCriteria(array(
            'condition' => 'area_expertise_id=' . $id . ' AND status=' . Question::STATUS_PUBLISHED . " AND  pay=0",
            'order' => 'update_time DESC',
            'with' => 'answerCount',
        ));
        }
        else if(Yii::app()->user->role == Users::ROLE_USER){
        $criteria = new CDbCriteria(array(
            'condition' => 'area_expertise_id=' . $id . ' AND status=' . Question::STATUS_PUBLISHED . " AND if(t.author_id=". Yii::app()->user->id .", pay in (0,1) and 1, pay=0 and 1) = 1",
            'order' => 'update_time DESC',
            'with' => 'answerCount',
        ));
        }
        else if(Yii::app()->user->role == Users::ROLE_EXPERT || Yii::app()->user->role == Users::ROLE_ADMIN){
            $criteria = new CDbCriteria(array(
            'condition' => 'area_expertise_id=' . $id . ' AND status=' . Question::STATUS_PUBLISHED . " AND pay IN (0,1)",
            'order' => 'update_time DESC',
            'with' => 'answerCount',
        ));
        }
        if (isset($_GET['tag']))
            $criteria->addSearchCondition('tags', $_GET['tag']);

        $dataProvider = new CActiveDataProvider('Question', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['questionsPerPage'],
            ),
            'criteria' => $criteria,
        ));
//        $area = AreaExpertise::model()->findByPk($id);
//        // Set up title, description and keywords of meta tags of the page
//        $this->pageTitle = $area->metaTitle;
//        $this->metaDescription = $area->metaDescription;
//        $words = explode(' ', $question->content);
//        $key = $tmp = array();
//        if(count($words) > 10){
//            $keys = array_rand($words, 10);
//        }
//        else{
//            $keys = array_rand($words, count($words));
//        }
//        foreach ($keys  as $key) {
//            $tmp[] = $words[$key];
//        }
//        $words = $tmp;
//        $this->metaKeywords = implode(',', $words);
//        // End of --- Set up title, description and keywords of meta tags of the page
        
        $areas = AreaExpertise::model()->with('countQuestion')->findAll('status=1');
        $this->clips['areas'] = $areas;
//        echo '<pre>';var_dump($areas);        die();

        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'model' => new Question(),
            'area' => AreaExpertise::model()->findByPk((int)$id),
//            'areas' => $areas,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpdate() {
        $model = $this->loadModel();
        if (isset($_POST['Question'])) {
            $model->attributes = $_POST['Question'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     */
    public function actionDelete() {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel()->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(array('index'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $criteria = NULL;
        $emptyTitle = Question::model()->findAll("url=NULL");
        $q = new Question();
        foreach ($emptyTitle as $question) {
            $question->url = $q->translit(trim($question->title," ?!.\t\n\r\0\x0B"));
            $question->seo_url = $q->translit(trim($question->title," ?!.\t\n\r\0\x0B"));
            $question->save();
        }
        unset($emptyTitle);
        unset($q);
        if(Yii::app()->user->id != NULL && Yii::app()->user->role != Users::ROLE_EXPERT){
            $criteria = new CDbCriteria(array(
                'condition' => 'status=' . Question::STATUS_PUBLISHED . " AND if(t.author_id = ".Yii::app()->user->id.", pay=0 OR pay=1, pay=0)",
                'order' => 'update_time DESC',
                'with' => 'answerCount',
            ));
        }
        elseif (Yii::app()->user->role == Users::ROLE_EXPERT) {
            $criteria = new CDbCriteria(array(
                'condition' => 'status=' . Question::STATUS_PUBLISHED . " AND  pay IN(0,1)",
                'order' => 'update_time DESC',
                'with' => 'answerCount',
            ));
        }
        else {
            $criteria = new CDbCriteria(array(
                'condition' => 'status=' . Question::STATUS_PUBLISHED . " AND pay=0",
                'order' => 'update_time DESC',
                'with' => 'answerCount',
            ));
        }
        if (isset($_GET['tag']))
            $criteria->addSearchCondition('tags', $_GET['tag']);

        $dataProvider = new CActiveDataProvider('Question', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['questionsPerPage'],
            ),
            'criteria' => $criteria,
        ));

        $areas = AreaExpertise::model()->with('countQuestion')->findAll('status=1');
        $this->clips['areas'] = $areas;
//        echo '<pre>';var_dump($areas);        die();

        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'model' => new Question(),
//            'areas' => $areas,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Question('search');
        if (isset($_GET['Question']))
            $model->attributes = $_GET['Question'];
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Suggests tags based on the current user input.
     * This is called via AJAX when the user is entering the tags input.
     */
    public function actionSuggestTags() {
        if (isset($_GET['q']) && ($keyword = trim($_GET['q'])) !== '') {
            $tags = Tag::model()->suggestTags($keyword);
            if ($tags !== array())
                echo implode("\n", $tags);
        }
    }

    public function actionStepOne() {
        $this->render('_stepOne', array(
            'model' => new Question(),
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel($id) {
        if ($this->_model === null) {
            if (isset($id)) {
                if (Yii::app()->user->isGuest)
                    $condition = 'status=' . Question::STATUS_PUBLISHED . ' OR status=' . Question::STATUS_ARCHIVED;
                else
                    $condition = '';
                $this->_model = Question::model()->findByPk($id, $condition);
            }
            if ($this->_model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $this->_model;
    }

    /**
     * Creates a new answer.
     * This method attempts to create a new answer based on the user input.
     * If the answer is successfully created, the browser will be redirected
     * to show the created answer.
     * @param Question the question that the new answer belongs to
     * @return Answer the answer instance
     */
    public function actionNewAnswer() {
        $answer = new Answer;
//                var_dump($_POST['Answer']); die();
        $qid = 0;
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'answer-form') {
            echo CActiveForm::validate($answer);
            Yii::app()->end();
        }
        if (isset($_POST['Answer'])) {
            $answer->attributes = $_POST['Answer'];
            $answer->post_id = $_POST['Answer']['post_id'];
            $answer->status = $_POST['Answer']['status'];
            $question = Question::model()->findByPk($answer->post_id);
//                        var_dump($answer->author);
//                        var_dump($question->id);
//                        die();
            if ($question->addAnswer($answer)) {
                if ($answer->status == Answer::STATUS_PENDING) {
                    Yii::app()->user->setFlash('answerSubmitted', 'Thank you for your answer. Your answer will be questioned once it is approved.');
//				$this->refresh();
                }
                $msg = $this->renderPartial("email_answer", array('answer' => $answer), TRUE);
                $this->sendMailUser($question->author->id, $question->title, $msg);
            }
            $qid = $question->id;
        }
        $this->redirect(Yii::app()->createAbsoluteUrl("question/view", array('id' => $qid)));
    }

    public function actionMyProfile() {
        if(Yii::app()->user->isGuest){
            Yii::app()->user->returnUrl = Yii::app()->request->requestUri;
            Yii::app()->request->redirect(Yii::app()->createAbsoluteUrl("site/login"));
//            $this->redirect(Yii::app()->createAbsoluteUrl("site/login"));
        }
        // *** Это что бы отобразиться в админке пользователя
        $p = Yii::app()->params['controllers'];
        array_push($p, 'question');
        Yii::app()->params['controllers'] = $p;
        $this->layout = "//layouts/main";
//        $this->layout = '//../modules/expert/views/layouts/column2';
        // *** End ***
        
//        $myFreeQuestions = Question::getUserQuestions(Yii::app()->user->id, 0);
//        $myPaidQuestions = Question::getUserQuestions(Yii::app()->user->id, 0);
        
        $criteria = new CDbCriteria();
        $criteria->order = "create_time DESC";
        $criteria->with = array(
            'answerCount'
        );
        $criteria->condition = "author_id=" . Yii::app()->user->id . " AND status=".Question::STATUS_PUBLISHED;
        
        $myFreeQuestions = new CActiveDataProvider('Question', array(
            'id' => 'qfree',
            'pagination' => array(
                'pageSize' => Yii::app()->params['questionsPerPage'],
//                'pageSize' => 5,
            ),
            'criteria' => $criteria,
        ));
        $criteria = new CDbCriteria();
        $criteria->order = "create_time DESC";
        $criteria->with = array(
            'answerCount'
        );
        $criteria->condition = "author_id=" . Yii::app()->user->id . " AND pay=1";
        $myPaidQuestions = new CActiveDataProvider('Question', array(
            'id' => "qpaid",
            'pagination' => array(
//                'pageSize' => Yii::app()->params['questionsPerPage'],
                'pageSize' => 10,
            ),
            'criteria' => $criteria,
        ));
        $this->render('myProfile', 
                array(
//                    'freeQuestions' => $myFreeQuestions,
                    'paidQuestions' => $myPaidQuestions,
                )
            );
//        $this->render("myProfile");
    }

    /**
     * Отказ от публикации и отправки вопроса экспертам
     * @param int $id Identification of question in DB
     */
    public function actionRefuseFromQuestion($id){
        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->createAbsoluteUrl("site/login"));
        }
        // *** Это что бы отобразиться в админке пользователя
//        $p = Yii::app()->params['controllers'];
//        array_push($p, 'question');
//        Yii::app()->params['controllers'] = $p;
//        $this->layout = "//layouts/main";
        $question = Question::model()->findByPk($id);
        if($question !== NULL){
            $image = $question->image;
            $image = Yii::app()->params['uploadDir'] . "question_img/" . $image;
            if(is_file($image) && file_exists($image)){
                unlink($image);
            }
            $question->deleteByPk($id);
        }
        
        $this->actionMyProfile();
    }

    public function actionConfirmQuestion($id){
        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->createAbsoluteUrl("site/login"));
        }
        $msgCategory = "error";
        $msg = "Error";
        $question = Question::model()->findByPk((int)$id);
        // Если вопрос еще не полачен. Это для обхода F5.
        if($question->is_paid == "0"){
            if($this->checkUserBill(Yii::app()->user->id, Question::PAY_AMOUNT)){
                $user = Users::model()->findByPk((int)Yii::app()->user->id);
                $user->bill = $user->bill - Question::PAY_AMOUNT;
                if($user->save(FALSE, array('bill'))){
                    $question = Question::model()->findByPk((int)$id);
                    $question->is_paid = 1;
                    $question->status = Question::STATUS_PUBLISHED;
                    if($question->save(FALSE, array('is_paid', 'status'))){
                        // Рассылаем экспертам
                        $this->sendEmailToExperts('emailTemplates/email_question_pay', array('question' => $question), $question);
                        $msgCategory = "success";
                        $msg = " Ваше фото успешно отправлено экспертам.  
                                 В самое ближайшее время, вам ответят.  Ответ с сылкой на Ваш вопрос поступит на электронную почту.
                                 Всего самого наилучшего!";
                    }
                }
                else{
                    $msg = "Возникли технические трудности при списании средств с баланса. Обратитесь пожалуйста к администрации сайта!";

                }
            }
            else{
                $msg = "У Вас недостаточно средств для заказа услуги! Поплните пожалуйста баланс!";
            }
            Yii::app()->user->setFlash($msgCategory, $msg);
        }
         $this->actionMyProfile();
    }
    public function actionUserQuestions($id){
        if(isset($id) && !empty($id)){
            $criteria = new CDbCriteria();
            $criteria->order = "create_time DESC";
            $criteria->condition = "author_id=".$id." AND status=".Question::STATUS_PUBLISHED;
        }
        if (isset($_GET['tag']))
            $criteria->addSearchCondition('tags', $_GET['tag']);

        $dataProvider = new CActiveDataProvider('Question', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['questionsPerPage'],
            ),
            'criteria' => $criteria,
        ));

        $areas = AreaExpertise::model()->with('countQuestion')->findAll('status=1');
        $this->clips['areas'] = $areas;
//        echo '<pre>';var_dump($areas);  
         $this->render('user_questions', array(
            'dataProvider' => $dataProvider,
            'model' => new Question(),
//            'areas' => $areas,
        ));
    }
    
    public function sendEmailToExperts($msgTemplate, $dataForTemplate, Question $question){
        if(isset($msgTemplate) && is_string($msgTemplate) && !empty($msgTemplate)){
            $experts = Users::model()->findAll("status=1 AND role='".Users::ROLE_EXPERT."'");
            if($experts !== NULL && count($experts) > 0){
                Yii::import('application.extensions.phpmailer.JPhpMailer');
                foreach ($experts as $expert) {
                    $mail = new JPhpMailer();
                    $mail->SetFrom('no-reply@magistika.com', 'magistika.com');
                    $mail->Subject = "Поступил новый вопрос!";
                    $mail->AltBody = '';
                    $mail->CharSet = 'UTF-8';
                    $msg = $this->renderPartial($msgTemplate, $dataForTemplate, TRUE);
                    $mail->MsgHTML($msg);
                    $mail->AddAddress($expert->email, $expert->name); 
                    if($question->image !== NULL){
                        $mail->AddAttachment(
                                Yii::app()->params['uploadDir']."question_img/".$question->image);
                    }
                    $mail->Send();
                }
            }
        }
    }
    
    public function paimentSuccess($uid){
        if(isset($uid)){
            $user = Users::model()->findByPk((int)$uid);
            if($user != NULL){
                if($user->bill >= Question::PAY_AMOUNT){
                    $questions = Question::model()->findAll('t.author_id='.$user->id.'pay=1 AND is_paid=0');
                    if(count($questions) > 0){
                        // Оплачиваем все платные вопросы пользователя.
                        foreach ($questions as $question) {
                            if($user->bill >= Question::PAY_AMOUNT){
                                $question->status = Question::STATUS_PUBLISHED;
                                $question->is_paid = 1;
                                $question->update(array('status','is_paid'));
                                $user->bill -= Question::PAY_AMOUNT;
                                $user->save(FALSE, array('bill'));
                                $user->refresh();
                                $this->sendEmailToExperts('//../views/question/emailTemplates/email_question_pay', array('question' => $question), $question);
                            }
                        }
                    }
                }
            }
        }
    }
    private function placeTheQuestionAsFree() {
        // 1. Отправить на модерацию !!!
        // 2. Опубликовать (статус Question::STATUS_PUBLISHED)
        // 3. Разослать экспертам (через 20 часов)
        $qid = Yii::app()->session->readSession("qid");
        if($qid != ""){
            $model = Question::model()->with('author')->findByPk($qid);
            $model->status = Question::STATUS_PUBLISHED;
    //        var_dump($model);die();
            $model->update(array('status'));
            Yii::app()->session->destroySession("qid");
            return $model;
        }
        return NULL;
    }

    private function placeTheQuestionAsPaid($status) {
        $qid = Yii::app()->session->readSession("qid");
        if($qid != ""){
            $model = Question::model()->with('author')->findByPk($qid);
            $model->status = $status;
            $model->pay = 1;
            $model->update(array('status', 'pay', 'is_paid'));
            $model->refresh();
            Yii::app()->session->destroySession("qid");
            return $model;
        }
        return NULL;
    }

}
