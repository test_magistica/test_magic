<?php

class ForgotController extends Controller {

    public function actionPassword() {

        $model = new ForgotPassword();

        if (isset($_POST['ForgotPassword'])) {
            $user = Users::model()->find("email = '" . $_POST['ForgotPassword']['username'] . "'");
            if ($user === NULL)
                $model->addError('username', 'E-mail не найден в базе, будьте внимательны.');
            else {
                $newPassword = rand(1, 1000000);
                Yii::app()->db->createCommand("UPDATE `users` SET `pwd` = '" . md5($newPassword) . "' WHERE `id` = '" . (int) $user['id'] . "'")->query();
                $msg = '<h1 style="font-weight:normal;font-size:26px;color:#ce3f7c;font-family:Georgia;margin:0;margin-bottom:15px;">
                        <span style="color:#000000;">Дорогая ' . $user['name'] . '!</span></h1>
                <p style="color:#3d3f3f;font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:19px;margin:0;margin-bottom:19px;padding-left:1px;">
                        Добро пожаловать на сайт <a href="http://magistika.com"><span style="color:#000000;">Magistika.com</span></a><br />
                        Напоминаем Вам данные для входа в <a class="daria-goto-anchor" href="' . Yii::app()->createAbsoluteUrl('/user/password') . '" style="color:#1079c5;" target="_blank">личный кабинет</a>:</p>
                <p style="color:#3d3f3f;font-family:Arial,Helvetica,sans-serif;font-size:15px;line-height:19px;margin:0;margin-bottom:19px;padding-left:1px;">
                        Ваш e-mail: 
                        <a class="daria-action" href="mailto:' . $user['email'] . '">' . $user['email'] . '<wbr></wbr></a><br />
                        Ваш пароль: ' . $newPassword . '</p>
                ';                
                Yii::import('application.extensions.phpmailer.JPhpMailer');
                $mail = new JPhpMailer();
                $mail->SetFrom('no-reply@magistika.com', 'magistika.com');
                $mail->Subject = 'Magistika.com - напоминание пароля';
                $mail->AltBody = '';
                $mail->CharSet = 'UTF-8';
                $mail->MsgHTML($msg);
                $mail->AddAddress($_POST['ForgotPassword']['username'], $user['name']);
                $mail->Send();
                Yii::app()->user->setFlash('forgot', 'Ваш новый пароль отправлен на вашу почту!');
            }
        }

        $this->render('password', array('model' => $model));
    }

}
