<?php

class ChatModule extends CWebModule {

    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'chat.models.*',
            'chat.components.*',
        ));
        if (!Yii::app()->request->isAjaxRequest) {
            Yii::app()->clientScript->registerScriptFile(
                    Yii::app()->assetManager->publish(
                            $this->getBasePath() . "/js/magistika_chat.js"
                    )
                    , CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(
                    Yii::app()->assetManager->publish(
                            $this->getBasePath() . "/js/ajaxfileupload.js"
                    )
                    , CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(
                    Yii::app()->assetManager->publish(
                            $this->getBasePath() . "/js/jquery.scrollTo-min.js"
                    )
                    , CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(
                    Yii::app()->assetManager->publish(
                            $this->getBasePath() . "/js/jquery.cookie.js"
                    )
                    , CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(
                    Yii::app()->assetManager->publish(
                            $this->getBasePath() . "/js/jquery-ui.js"
                    )
                    , CClientScript::POS_HEAD);
            Yii::app()->clientScript->registerScriptFile(
                    Yii::app()->assetManager->publish(
                            $this->getBasePath() . "/js/audiojs/audio.min.js"
                    )
                    , CClientScript::POS_HEAD);
            Yii::app()->assetManager->publish(
                    $this->getBasePath() . "/mp3/test.mp3"
            );
            Yii::app()->clientScript->registerScript("audio", "
                            var as = null;
                            audiojs.events.ready(function() {
                              window.as = audiojs.createAll();
                            });
                          "
            );
            
            $uipath = Yii::app()->assetManager->publish(
                            $this->getBasePath() . "/css"
                    );
//                    var_dump($uipath);
            $e = Yii::app()->clientScript->registerCssFile(
//                    Yii::app()->assetManager->publish(
                            $uipath . "/jquery-ui.css"
//                    )
            );
            $r = Yii::app()->clientScript->registerCssFile(
//                    Yii::app()->assetManager->publish(
                            $uipath . "/chat.css"
//                    )
            );
//            var_dump($e);
//            var_dump($r);
        }
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else
            return false;
    }

}
