<?php
return array(
    "booked minutes:" => "доступно минут: ",
    "left minutes: " => "осталось минут: ",
    "spent minutes: " => "длительность чата: ",
    "Writing" => "Пишет",
    "Your message" => "Введите текст сообщения",
    "Left a chat" => "Закончить чат",
    "Send" => "Отправить",
    "Show history:" => "Показать историю:",
    "Photo of the {role}" => "Картинка {role}",
    "by week" => "за неделю",
    "by month" => "за месяц",
    "half-year" => "за полгода",
    "since the beginning" => "с начала", 
);

?>