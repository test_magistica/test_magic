function MagistikaChat() {
    var self = this;
    // chatId
    var chid = 0;

    // Left time to the end of the chat
    var leftTime = "chat-left-minutes";

    // Container when to be append new messages
    var msgCanvas = "chat-msg-canvas";

    // Images which show animation when user is writing
    var liveDots = "msg-container last";

    // Textarea with new message
    var txtArea = "chat-new-msg";
    // 
    var countDownInterval = 0;
    var btnFinishChat = "chat-button-stop a";
    var btnSendPhoto = "chat-button-load-photo input";
    var btnSendMessage = "chat-button-send button";

    // 0 - expert, 1 - user
    var userRole = null;
    self.userFindPhoto = false;
    self.userTypingText = false;
    // Запущен ли обратный отсчет
    var isTimerRun = false;

    var oneMinuteNotifWasShowed = false;
    
    var dialogTypes = {
        CHAT_CLOSE: 1,
        CHAT_CLOSE_SYSTEM_NO_MONEY: 2,
        CHAT_CLOSE_USER: 3
    }
    
    // Мониторит последние сообщения
    var refreshHistory = function() {
        var lastMsgId = $("div[class='msg-container'][class!='system']:last").find("div[id^='msg-']");
        if (lastMsgId.length > 0) {
            // Send to the server last message id
            lastMsgId = $(lastMsgId).attr('id').split('-')[1];
        } else {
            // need to select from server ALL messages
            lastMsgId = 0;
        }
        var data = {
            url: "/chat/refresh",
            params: {
                id: lastMsgId,
                cid: chid
            },
            success: function(resp) {
                messageCreator(resp);
                if (resp != null && resp.length > 0) { // chat has a new messages
                    $('#chat-msg-canvas').scrollTo("max");

                } else { //chat has no messages

                }
//                console.log(resp);
            }
        };
        sendAjax(data);

    }

    self.sendMessage = function() {
        var msg = $("#" + txtArea).val().trim();
        $("#" + txtArea).val("");
        if (msg != "") {
            var data = {
                url: "/chat/sendMessage",
                params: {
                    cnm: msg,
                    cid: chid
                },
                success: function(resp) {
                    if (typeof resp.error != "undefined") {

                    } else {
//                        console.log(resp);
                    }
                    self.userTypingText = false;
                }
            };
            sendAjax(data);
        }
    };

    /**
     * Обработчик на событие чата - опонент печатает.
     * @returns {undefined}
     */
    self.UserTyping = function(e) {
        self.userTypingText = e.type == "keydown" ? true : false;
        if (e.keyCode == 13) { // if was press enter
            if (e.type == "keydown") {
                self.sendMessage();
            }
        } else {
            var data = {
                url: "/chat/opponentTyping",
                params: {
                    cid: chid,
                    flag: self.userTypingText
                },
                success: function(resp) {

                }
            };
            sendAjax(data);
        }
    }

    /**
     * Анимация пишущего карандаша (как в скайпе)
     * @returns {undefined}
     */
    var IsOpponentTyping = function() {
        var data = {
            url: "/chat/isOpponentTyping",
            params: {
                cid: chid
            },
            success: function(resp) {
                if (resp != null && resp.r == "1") {
                    // Показываем анимашку что оппонент пишет
                    $(".msg-container-last").show();
//                    console.log(resp);
                } else {
                    // Прячем анимашку что оппонент пишет
                    $(".msg-container-last").hide();
//                    console.log(resp);
                }
            }
        };
        sendAjax(data);
    }

    // Finish chat session
    self.finishChat = function(needConfirm, msg) {
        needConfirm = needConfirm || true;
        msg = msg || "Прекратить чат?";
        if (needConfirm) {
            if (confirm(msg)) {
                window.location.pathname = "/chat/finishChat/" + chid + "";
            }
        } else {
            window.location.pathname = "/chat/finishChat/" + chid + "";
        }
//        return "Чат закрыт";
    }

    self.uploadFile = function() {
        var l = $(".msg-container [id^='msg-']:last");
        self.userFindPhoto = true;
        if (l.length > 0) {
            // message wich to belong a attachment
            l = $(l).attr("id").split('-')[1];
        }
        else {
            // If messages not send yet, 
            // create empty message and attach file to it
            l = 0;
        }
//        alert( $("#chat-button-load-photo-btn").val());
        if ($("#chat-button-load-photo-btn").val() == "") {
            setTimeout(self.uploadFile, 1000);
            return;
        }
        else {
//            alert($("#chat-button-load-photo-btn").val());
            $.ajaxFileUpload({
                type: "POST",
                secureuri: false,
                fileElementId: 'chat-button-load-photo-btn',
                dataType: 'json',
                url: "/chat/uploadFile",
                data: {cid: chid, mid: l},
                success: function(data, status) {
//                    console.log(data);
                    $(".chat-button-load-photo form").trigger('reset');
                    $("#" + btnSendPhoto).off("click");
                    $("#" + btnSendPhoto).on("click", self.uploadFile);
                    self.userFindPhoto = false;
                }
            });
        }
    }
    
    self.showDialog = function(type) {
        switch (type) {
            case dialogTypes.CHAT_CLOSE:
                $("#chat-end-form").dialog("open");
                break;
            case dialogTypes.CHAT_CLOSE_SYSTEM_NO_MONEY:
                $("#chat-end-form-money").dialog("open");
                break;
            case dialogTypes.CHAT_CLOSE_USER:
                $("#chat-end-form-user").dialog("open");
                break;
        }
    }
    
    var countDown = function(time) {
        $("#chat-left-minutes").text(time);
    }

    var watchDog = function() {
        var data = {
            url: "/chat/watchDog",
            params: {
                cnm: 0
            },
            success: function(resp) {
                if (resp.chat_live == 0) {
                    self.showDialog(dialogTypes.CHAT_CLOSE_SYSTEM_NO_MONEY);
                    clearInterval(countDownInterval);
                    return;
                }
                if (resp.chat_live == 2) {
                    self.showDialog(dialogTypes.CHAT_CLOSE_USER);
                    clearInterval(countDownInterval);
                    return;
                }
                if (resp.chat_live == 3) {
//                if (resp.chat_live == 3 && !self.userTypingText && !self.userFindPhoto) {                    
                    self.showDialog(dialogTypes.CHAT_CLOSE);
                    clearInterval(countDownInterval);
                    return;
                }
                
                if (typeof resp.need_wait != "undefined") {
                    messageCreator(resp.need_wait); 
                }
                
                if (resp.chat_live == 1) {
//                    countDown(resp.time);
                }

            }
        };
        sendAjax(data);
    }

    var timer = function() {
        var lTime = $("#chat-left-minutes").text();
        var times = lTime.split(":");
        var h = parseInt(times[0]);
        var i = parseInt(times[1]);
        var s = parseInt(times[2]);
        if (s == 0) {
            if (i > 0) {
                i--;
                s = 59;
            }
            else {
                if (h > 0) {
                    h--;
                    i = 59;
                    s = 59;
                } else {
                    // Время вышло! Завершить чат
                    watchDog();
                    return;
                }
            }
        } else {
            s--;
        }
        times[0] = h <= 9 ? "0" + h : h;
        times[1] = i <= 9 ? ":0" + i : ":" + i;
        times[2] = s <= 9 ? ":0" + s : ":" + s;
        // Информируем пользователей чата, что осталось менее 15 сек
        if (h == 0 && i == 0 && s < 15 && !oneMinuteNotifWasShowed) {
            oneMinuteLeftNotification();
        }
        $("#chat-left-minutes").text(times.join(''));
    }

    var newChatAvailable = function(chats) {
        for (var chat in chats) {
            $(".available-chat-tpl .actpl-name").text(chat.name + " " + chat.surname);
            $(".available-chat-tpl .actpl-time").text(chat.availableTime);
            $(".available-chat-tpl").css("display", "inline-block");
            break;
        }
    }

    var checkNewChats = function() {
        var data = {
            url: "/chat/monitorNewChat",
            params: {
                cid: chid
            },
            success: function(resp) {
                if (resp[0] !== "empty") {
//                    console.log(resp);
                    newChatAvailable(resp);
                } else {
//                    console.log(resp);
                }
            }
        };
        sendAjax(data);
    }

    var getHistory = function(e) {
        var data = {
            url: "/chat/getHistory",
            params: {
                hp: e.data,
                cid: chid
            },
            success: function(resp) {

                if (resp.length > 0) { // chat has a messages
                    $(".msg-container, .msg-date").remove();
                    messageCreator(resp);
                    $('#chat-msg-canvas').scrollTo("max");

                } else { //chat has no messages

                }
//                console.log(resp);
            }
        };
        sendAjax(data);
    }

    var init = function() {
        var cfg = window.magistika_chat_config;
        var chatBlock = $('div[id^="chat-"][class="chat-module"]').attr("id");
        userRole = $("#u-type").val();
        if (typeof chatBlock != "undefined") {
            chid = parseInt(chatBlock.split("-")[1]);
            bindButtonsHandlers();
            bindFormsHandlers();
            // Если доступное время и оставшееся время отличаются - запустить обратный отсчет
            // Если времена одинаковые - эксперт ещё не ответил (чат не начался)
            // Отсчет времени начнется после ответа эксперта
            var bookedMinutes = $("#chat-booked-minutes").text().trim();
            var leftMinutes = $("#chat-left-minutes").text().trim();
            if (bookedMinutes != "" && bookedMinutes != leftMinutes) {
                countDownInterval = setInterval(timer, 1000);
                isTimerRun = true;
            }
            
            if (typeof cfg == "undefined" || cfg.refreshHistory != 0)
                setInterval(refreshHistory, 2000);
            if (typeof cfg == "undefined" || cfg.watchDog != 0)
                setInterval(watchDog, 60 * 600);
            if (typeof cfg == "undefined" || cfg.isOpponentTyping != 0)
                setInterval(IsOpponentTyping, 800);
            jQuery('#chat-msg-canvas').scrollTo("max");
//            window.onbeforeunload = (function(){ return self.finishChat(false, "");});
            if (userRole == "0") {// Эксперт проверяет новые чаты
                if (typeof cfg == "undefined" || cfg.checkNewChats != 0)
                    setInterval(checkNewChats, 10000);
            }
        }
        if ($(".chat-history").length == 1) {
            bindHistoryHandlers();
        }
    }

    var bindHistoryHandlers = function() {
        $(".chat-history span a").each(function(index) {
            $(this).on("click", index + 1, getHistory);
        });
    }

    var bindButtonsHandlers = function() {
        $("#" + btnFinishChat).on("click", self.finishChat);
        $("#" + btnSendMessage).on("click", self.sendMessage);
        $("#" + txtArea).on("keyup, keydown", self.UserTyping);
        $("#" + btnSendPhoto).on("click", self.uploadFile);
    }

    var bindFormsHandlers = function() {
        //if (userRole == 1) {
            $("#chat-end-form").dialog({
                modal: true,
                draggable: false,
                autoOpen: false,
                resizable: false,
                dialogClass: "chat-notification",
                close: function(event, ui) {
                    window.location.pathname = "/chat/finishChat/" + chid + "";
                }
            });
            
            $("#chat-end-form-money").dialog({
                modal: true,
                draggable: false,
                autoOpen: false,
                resizable: false,
                dialogClass: "chat-notification",
                close: function(event, ui) {
                    window.location.pathname = "/chat/finishChat/" + chid + "";
                }
            });
            
            $("#one-minute-notification").dialog({
                modal: true,
                draggable: false,
                autoOpen: false,
                resizable: false,
                dialogClass: "chat-notification"
            });
            
            $("#chat-end-form-user").dialog({
                modal: true,
                draggable: false,
                autoOpen: false,
                resizable: false,
                dialogClass: "chat-notification",
                close: function(event, ui) {
                    window.location.pathname = "/chat/finishChat/" + chid + "";
                }
            });
//        } else {
//
//        }
    }

    var messageCreator = function(data) {
        if (data != null && data.length > 0) {
            var prevDate = null;
            for (var msg in data) {
                if (typeof data[msg].countdown != "undefined") {
                    if (!isTimerRun) {
                        // бесплатное время задано в системе
                        if (typeof data[msg].free_time != "undefined") {
                            if (data[msg].free_time >= 0) {
                        countDownInterval = setInterval(timer, 1000);
                        isTimerRun = true;
                    }
                }
                    }
                }
                var date = null;
                if (typeof data[msg].msg_date != "undefined") {
                    if (prevDate == null || prevDate != data[msg].msg_date) {
                        date = $('<div class="msg-date">').text(data[msg].msg_date);
                        prevDate = data[msg].msg_date;
                    }
                }

                var msg_name = $('<div class="msg-name">');
                var l = $(".msg-container [id^='msg-']:last");
                if (l.length > 0) {
                    //id of the author of message
                    l = $(l).attr("id").split('-')[2];
                }
                else {
                    l = 0;
                }
                if (parseInt(l) != parseInt(data[msg].author_id)) {
                    $(msg_name).text(data[msg].author_name);
                    if (data[msg].author_role == "0") {
                        $(msg_name).addClass("color-expert");
                    }
                }
                else {
                    $(msg_name).html('&nbsp;');
                }

                var msg_ = $('<div class="msg-">');
                $(msg_).attr("id", "msg-" + data[msg].msg_id + "-" + data[msg].author_id);

                var msg_body = $('<div class="msg-body">');
                $(msg_body).text(data[msg].msg_text);
                if (typeof data[msg].msg_attach != 'undefined' && data[msg].msg_attach.length > 0) {
                    for (var a in data[msg].msg_attach) {
                        var att = $("<a>").attr("href", data[msg].msg_attach[a]);
                        $(att).text("Скачать");
                        $("<br>").appendTo(msg_body);
                        $(att).appendTo(msg_body);
                        $("<br>").appendTo(msg_body);
                    }
                }

                var msg_time = $('<div class="msg-time">');
                $(msg_time).text(data[msg].msg_time);

                var msg_row = $('<div class="msg-row">')
                        .append(msg_body)
                        .append(msg_time)
                        .append($('<div style="clear: both;">'));
                $(msg_).append(msg_row);
                var msg_container = $('<div class="msg-container">')
                        .append(msg_name)
                        .append(msg_);
                if (data[msg].author_name == "Системное") {
                    $(msg_container).addClass("system");
                }
                var place = $(".chat-msg-canvas .msg-container:last");
                if (place.length > 0) {
                    $(place).after(msg_container);

                } else {
                    if ($(".chat-msg-canvas").has(".chat-history").length == 1) {
                        $(".chat-history").after(msg_container);
                    } else {
                        $(".chat-msg-canvas").prepend(msg_container);
                    }
                }
                if (date != null) {
                    $(msg_container).prepend(date);
                    $(msg_container).prepend($('<div style="clear: both;">'));
                }
            }
        }
//        console.log(data.length);
    }
    
    var oneMinuteLeftNotification = function() {
        $("#one-minute-notification").dialog("open");
    }
    
    var sendAjax = function(data) {
        $.ajax({
            type: "POST",
            url: data.url,
            data: (data.params),
            dataType: "json",
            success: data.success,
            error: function(err) {
                console.log("error");
            }
        })
    }
    init();
}
$(document).ready(function() {
    $.magistiksChatManager = new MagistikaChat();
});
