<?php if (isset($this->clips['msg']) && !empty($this->clips['msg'])) : ?>
    <?php 
        $msg = $this->clips['msg']; 
        $msgDate = Yii::app()->dateFormatter->format("dd MMMM yyyyг.", $msg->send_time);
    ?>
    <?php if ($this->clips['prevDate'] == 0 || $this->clips['prevDate'] != $msgDate): ?>
        <div class="msg-date"><?php echo $msgDate; ?></div>
        <?php $this->clips['prevDate'] = $msgDate; ?>
    <?php endif; ?>
    <?php $color = $msg->author->role == Users::ROLE_EXPERT ? " color-expert" : "";?>
    <div class="msg-container">
        <?php if ($this->clips['prevAuthor'] == 0 || $this->clips['prevAuthor'] != $msg->author->id): ?>
            <div class="msg-name<?php echo $color; ?>"><?php echo $msg->author->name . " " . $msg->author->surname; ?></div>
            <?php $this->clips['prevAuthor'] = $msg->author->id; ?>
        <?php else: ?>
            <div class="msg-name">&nbsp;</div>
        <?php endif; ?>
        <div class="msg-" id="msg-<?php echo $msg->id; ?>-<?php echo $msg->author->id; ?>">
            <div class="msg-row">
                <div class="msg-body">
                    <?php echo $msg->message; ?>
                    <?php if (count($msg->chatAttachments) > 0): ?>
                        <?php foreach ($msg->chatAttachments as $attachment): ?>
                            <br />
                            <a href="<?php echo $this->createUrl("/chat/Chat/downloadFile", array('id' => $attachment->id)); ?>">Скачать</a>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="msg-time"><?php echo Yii::app()->dateFormatter->format("HH:mm", $msg->send_time); ?></div>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
<?php endif; ?>