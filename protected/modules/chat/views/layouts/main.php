<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <!-- blueprint CSS framework -->
<!--        <link rel="stylesheet" type="text/css" href="<?php // echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />-->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />

         <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap_expert.css">
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style_expert.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

        <link href="<?php echo Yii::app()->request->baseUrl; ?>/css/magistika_select.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/magistika_slider.css"  />
        <link rel="stylesheet"  media="all" href="<?php echo Yii::app()->request->baseUrl; ?>/css/magistika_checked.css"  />
        <link rel="stylesheet"  media="all" href="<?php echo Yii::app()->request->baseUrl; ?>/css/magistika_style.css"  />

        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.7.1.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.coda-slider-3.0.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.10.2.custom.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/select.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.ezmark.min.js" type="text/javascript"></script>
        <!--<script src="<?php // echo Yii::app()->request->baseUrl; ?>/js/radio.js" type="text/javascript"></script>-->
        <!--<script src="<?php // echo Yii::app()->request->baseUrl; ?>/js/main.js" type="text/javascript"></script>-->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.coda-slider-3.0.min.js" type="text/javascript"></script>
        <!--<script src="<?php // echo Yii::app()->request->baseUrl; ?>/ckeditor/ckeditor.js" type="text/javascript"></script>-->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/AjexFileManager/ajex.js" type="text/javascript"></script>
        <?php Yii::app()->clientScript->registerCoreScript('yiiactiveform'); ?>

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body class="profile_page">
        <?php if (Yii::app()->user->role == Users::ROLE_EXPERT): ?>
            <?php $this->widget('application.modules.chat.components.ChatExpertNotification'); ?>
            <?php $this->widget('ext.ExpertNotification.ExpertNotification'); ?>
        <?php endif; ?>
        <div id="wrapper">
            <div class="middlewrap">



                <!-- Шапка сайта -->
                <header>
                    <a href="<?php echo Yii::app()->homeUrl; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" alt="" class="logo" /></a>

                    <div class="socialbox">
                        <span>рассказать друзьям:</span>
                        <a href="#" class="ok"></a>
                        <a href="#" class="tw"></a>
                        <a href="#" class="fb"></a>
                        <a href="#" class="vk"></a>
                        <a href="#" class="gl"></a>
                    </div>

                    <a href="#" class="consult">консультации лучших<br /> экспертов 24 часа</a>

                    <!-- Авторизация -->
                    <?php $this->widget('ext.loginWidget.loginWidget'); ?>
                    <!-- #Авторизация -->





                    <!-- Topmenu -->
                    <?php $this->widget('ext.topMenu.topMenu'); ?>
                    <!-- #Topmenu -->

                </header>
                <!-- #Шапка сайта -->

                <!-- Центральный блок -->

                <?php echo $content; ?>


                <!-- #Центральный блок -->

                <!-- Sidebar -->







                <!-- Подвал -->
                <footer>
                    <div class="fmenu">
                        <a href="#">гадание онлайн по телефону</a>
                        <a href="#">сонник онлайн</a>
                        <a href="#">толкование снов</a>
                        <a href="#">значение сна</a>
                        <a href="#">гадание по фото</a>
                        <a href="#">гадание по смс</a><br />
                        <a href="#">гороскоп на сегодня онлайн</a>
                        <a href="#">любовный гороскоп</a>
                        <a href="#">консультация экстрасенса онлайн</a>
                        <a href="<?php echo Yii::app()->createUrl("site/showStaticPage", array('curl' => 'karta-sajjta'));?>">Карта сайта</a>
                    </div>

                    <a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo2.png" alt="" class="logo2" /></a>

                    <div class="copyright">
                        &copy; 2007-2013 Экстрасенсы онлайн.<br /> Гадание онлайн по телефону. Значение сна
                    </div>

                    <div class="phone">
                        <span>наш телефон:</span>
                        <div class="clr"></div>
                        8 800 555 33 17
                    </div>

                    <div class="developer">Разработано в студии <a href="#">MagicMedia</a></div>

                </footer>
                <!-- #Подвал -->
            </div>

        </div><!-- #middlewrap -->



        <script>
            function updateShouts() {

                $.post("<?php echo CController::createUrl('/expert/message/check') ?>",
                {check: '1'},
                function(data) {
                    if (data != 0) {
                        $('#shoutbox').text('(' + data + ')');
                        $('.loginbox .icon_mess').css("display", "block");
                    }
                    else
	                    $('.loginbox .icon_mess').css("display", "none");
                });
            }
//            setInterval("updateShouts()", 5000);

        </script>

    </body>
</html>
