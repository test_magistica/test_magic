<?php
    $opponent = $this->opponent;
    // Administrator or expert
    $is_user = Yii::app()->user->role == Users::ROLE_USER;
    $chat = Chat::model()->with(array(
        'user' => array(
            'select' => 'user.bill'
        ),
        'expert' => array(
            'select' => 'expert.tariff'
        )
    ))->findByPk($this->chatId);
    // Время в минутах доступное пользователю
    // Баланс пользователя / тариф эксперта
    $booked_time = $chat->user->bill / Chat::CHAT_COST;
    $spentTime = $booked_time;  
    // Если эксперт ответил на чат
    // Или во время чата пользователь или эксперт нажали F5
    if($chat->countdown_date != NULL){
        $lastMsg = ChatMessage::model()->find(array(
            'condition' => "chat_id=".$chat->id,
            'order' => "id DESC",
            'limit' => 1,
            'select' => "send_time, message",
        ));
        if($lastMsg != NULL){
            $dtStart = new DateTime($chat->countdown_date);
            $dtLastMsg = new DateTime($lastMsg->send_time);
            // Разница между временем первого ответа эксперта и временем последнего сообщения
            $diff = $dtLastMsg->diff($dtStart);
            // Время оставшееся до конца чата
            $spentTime = ($chat->user->bill / $chat->expert->tariff) - (($diff->h * 60) + $diff->i);
        }
        
        
    }
    // Блок отрабатывает для ссылки "просмотреть историю"
    // в разделе Мои Чаты -> Статистика -> Просмотреть историю
    if($this->reviewHistory){
        $dtStart = new DateTime($chat->countdown_date);
        $dtEnd = new DateTime($chat->end_date);
        $diff = $dtStart->diff($dtEnd);
        $spentTime = ($diff->h * 60 + $diff->i) - $chat->free_minutes;
    }
    
    // Подготавливаем данные для истории чата
//    if (count($this->messages) > 0){
        $uid = $is_user ? Yii::app()->user->id : $opponent->id;
        $eid = $is_user ? $opponent->id : Yii::app()->user->id;
        $row = Yii::app()->db->createCommand("select max(send_time) as lastMsg, min(send_time) as firstMsg "
                . "from chats c "
                . "left join chat_messages cm on cm.chat_id = c.id "
                . "where (cm.author_id in (".$uid.", ".$eid.")) and (c.user_id=".$uid." and c.expert_id=".$eid.") "
                . "order by send_time DESC")->queryRow();
        $lastMsg = $row['lastMsg'];
        $firstMsg = $row['firstMsg'];
        $dtFirstMsg = new DateTime($firstMsg);
        $dtLastMsg = new DateTime($lastMsg);
        $hDiff = $dtFirstMsg->diff($dtLastMsg);
        $howLongHistory = 0;
        if($hDiff->y > 0 || $hDiff->m > 6){
            $howLongHistory = 4;
        }elseif($hDiff->m > 1 && $hDiff->m <= 6){
            $howLongHistory = 3;
        }elseif($hDiff->m == 1){
            $howLongHistory = 2;
        }elseif($hDiff->d <= 7){
            $howLongHistory = 1;
        }
        $arrHistoryRefs = array(
            Yii::t("ChatModule.chat", "by week"),
            Yii::t("ChatModule.chat", "by month"),
            Yii::t("ChatModule.chat", "half-year"),
            Yii::t("ChatModule.chat", "since the beginning"),
            );
    // End of Подготавливаем данные для истории чата
        
//    }
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . "/js/vote/vote.js", CClientScript::POS_HEAD);
   $mp3 = Yii::app()->assetManager->getPublishedUrl(Yii::app()->controller->module->basePath."/mp3/test.mp3");
//   var_dump($mp3);//die();
?>
<audio src="<?php echo $mp3; ?>" preload="auto" ></audio>
<?php // if($this->reviewHistory):?>
<?php // echo CHtml::link("<< назад", $this->createUrl('/chat'));?>
<?php // endif;?>
<div class="chat-module" id="chat-<?php echo $this->chatId; ?>">
    <div id="chat-conf" style="display: none;">
        <input type="hidden" value="<?php echo $is_user ? "1" : "0"; ?>" id="u-type" />
    </div>
    <div class="chat-expert-container">
        <?php
        $opponentPhoto = $this->getUserImage($opponent->id);
        ?>
        <a href="<?php
        if ($is_user) {
            echo Yii::app()->createUrl('/extrasens/view/', array('id' => $opponent->id, 'title' => $this->getCHPU($opponent['name'] . '-' . $opponent['profession']), 'class' => 'review'));
        } else {
            echo "#customer-card";
        }
        ?>" rel="facybox">
            <img 
                src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $opponentPhoto; ?>" 
                title="<?php echo Yii::t("ChatModule.chat", "Photo of the {role}", array("{role}" => ($is_user ? "експерта" : "пользователя") )); ?>" 
                valign='top' 
                width="63px"
                height="63px"
                />
        </a>
        <div class="chat-expert-data">
            <div class="chat-expert-name"><?php echo $opponent->name . " " . $opponent->surname; ?></div>
            <div>
                <div class="status">
                    <?php if ($is_user): ?>
                        <div class="short">
                            <span class="prof"><?php echo $opponent->profession; ?></span>
                            <span class="stat"><?php echo $opponent->online; ?></span>
                        </div>
                    <?php endif; ?>
                    <?php if(!$this->reviewHistory): ?>
                    <div class="chat-booked-minutes"><?php echo Yii::t("ChatModule.chat", "booked minutes:"); ?><span id="chat-booked-minutes"><?php echo $this->prepareTime($booked_time); ?></span></div>
                    <?php else:?>
                    <div class="chat-booked-minutes">&nbsp;<span id="chat-booked-minutes">&nbsp;</span></div>
                    <?php endif; ?>
                    <?php if ($is_user): ?>
                        <div class="raiting_star">
                            <div id="raiting-<?php echo $opponent->id; ?>" 
                                 class="raiting vote-static">
                                <div id="raiting_blank"></div> <!--блок пустых звезд-->
                                <div id="raiting_hover"></div> <!--блок  звезд при наведении мышью-->
                                <div id="raiting_votes" class="raiting_votes-<?php echo $opponent->id; ?>"></div> <!--блок с итогами голосов -->
                            </div>
                            <div class="raiting_info"><h5><?php echo $opponent->expert_raiting; ?></h5></div>
                            <input type="hidden" name="totalMaxRaiting" value="<?php echo Yii::app()->params['maxExpertRaiting']; ?>"/>
                            <input type="hidden" name="displayRaiting" value="<?php echo $opponent->expert_raiting; ?>"/>
                            <input type="hidden" name="r_id" value="<?php echo $opponent->id; ?>"/>
                            <input type="hidden" name="countVotes" value="<?php echo $opponent->countVotes; ?>"/>
                        </div>
                    <?php endif; ?>
                    <?php if(!$this->reviewHistory): ?>
                    <div class="chat-left-minutes"><?php echo Yii::t("ChatModule.chat", "left minutes: "); ?><span id="chat-left-minutes"><?php echo $this->prepareTime($spentTime); ?></span></div>
                    <?php else:?>
                    <div class="chat-left-minutes"><?php echo Yii::t("ChatModule.chat", "spent minutes: "); ?><span id="chat-left-minutes"><?php echo $this->prepareTime($spentTime); ?></span></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="chat-history-container">
        <div class="chat-msg-canvas" id="chat-msg-canvas">
            <?php if ($howLongHistory > 0): ?>
            <div class="chat-history">
                <span><?php echo Yii::t("ChatModule.chat", "Show history:"); ?></span>
                <?php for ($i = 0;  $i < $howLongHistory; $i++): ?>
                <span><a href="javascript:void(0);"><?php echo $arrHistoryRefs[$i]; ?></a></span>
                <?php if($i < $howLongHistory - 1):?> | <?php endif; ?>
                <?php endfor; ?>
            </div>
            <?php endif;?>
            <?php if (count($this->messages) > 0): ?>
                <?php $this->clips['prevDate'] = 0; ?>
                <?php $this->clips['prevAuthor'] = 0; ?>
                <?php foreach ($this->messages as $msg): ?>
                    <?php $this->clips['msg'] = $msg; ?>
                    <?php $this->renderPartial("/layouts/msg_template"); ?>
                <?php endforeach; ?>
            <?php elseif($is_user && !$this->reviewHistory):?>
                <div class="msg-container system">
                    <div class="msg-name">Системное</div>
                    <div class="msg-" id="msg-">
                        <div class="msg-row">
                            <div class="msg-body">
                                Добро пожаловать в платный онлайн чат.<br />
                                Пожалуйста, подождите, эксперт подключится через несколько секунд…<br />
                                Таймер включается после первого сообщения эксперта.<br />
                                <?php if($is_user): ?>
                                    <?php
                                        $hasMinutes = (int)Yii::app()->db->createCommand("select free_chat_minutes from users where id=".$uid)->queryScalar();
                                        if($hasMinutes > 0){
                                            echo "У Вас есть <span style='color:red;'>".Yii::app()->params['freeChatMinutes']." бесплатных </span> минуты.";
                                        }
                                    ?>
                                <?php endif; ?>
                            </div>
                            <?php $dt = new DateTime();?>
                            <div class="msg-time"><?php echo Yii::app()->dateFormatter->format("HH:mm", $dt->format(Chat::DATE_TIME_FORMAT)); ?></div>
                            <div style="clear: both;"></div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="msg-container-last">
                <div class="msg-name"><?php echo $opponent->name . " " . $opponent->surname; ?></div>
                <div class="msg-" id="">
                    <div class="msg-row"><?php echo Yii::t("ChatModule.chat", "Writing"); ?></div>
                </div>
            </div>
        </div>
    </div>
    <?php if($this->reviewHistory == FALSE):?>
    <?php if(0):?>
    <div class="chat-message-sender-container">
        <textarea class="chat-new-msg" id="chat-new-msg" placeholder="<?php echo Yii::t("ChatModule.chat", "Your message"); ?>"></textarea>
        <div class="chat-buttons">
            <div class="chat-button-stop" id="chat-button-stop"><button><?php echo Yii::t("ChatModule.chat", "Left a chat"); ?></button></div>
            <div class="chat-button-load-photo" id="chat-button-load-photo">
                <form>
                <input id="chat-button-load-photo-btn" type="file" name="ChatAttachment[url]">
                </form>
            </div>
            <div class="chat-button-send" id="chat-button-send"><button><?php echo Yii::t("ChatModule.chat", "Send"); ?></button></div>
        </div>
    </div>
    <?php endif;?>
    <div class="chat-message-sender-container-2">
        <div class="chat-new-msg-form">
            <textarea rows="1" class="chat-new-msg-2" id="chat-new-msg" placeholder="<?php echo Yii::t("ChatModule.chat", "Your message"); ?>"></textarea>
            <div class="chat-button-send-2" id="chat-button-send"><button><?php echo Yii::t("ChatModule.chat", "Send"); ?></button></div>
        </div>
        <div class="chat-buttons-2">
            <div class="chat-button-stop-2" id="chat-button-stop">
                <a href="javascript:void(0)"><?php echo Yii::t("ChatModule.chat", "Left a chat"); ?></a>
            </div>
            <div class="chat-button-load-photo-2" id="chat-button-load-photo">
                <form>
                    <a href="javascript:void(0)" onclick="$('#chat-button-load-photo input').click();">Загрузить фото</a>
                    <input style="display: none;" id="chat-button-load-photo-btn" type="file" name="ChatAttachment[url]">
                </form>
            </div>
            
        </div>
    </div>
    <?php endif;?>
    <?php if(Yii::app()->user->role == Users::ROLE_EXPERT || Yii::app()->user->role == Users::ROLE_ADMIN): ?>
        <?php $this->renderPartial("user_chart", array(
            'user' => Users::model()->findByPk($chat->user->id),
        ));?>
    <?php endif;?>
</div>
<div class="available-chat-tpl" style="display: none;">
    Клиент: <span class="actpl-name">Мария Иванова</span> приглашает Вас в чат (доступное время <span class="actpl-time">15</span> минут)  
</div>
<?php if($is_user):?>
<div id="chat-end-form" style="display: none;" title="Чат закрыт" class="chat-notification">
    <p>Ваш чат завершен по таймауту.</p>
    <p>Вы будете перенаправлены на страницу эксперта</p>
</div>
<div id="chat-end-form-money" style="display: none;" title="Чат закрыт" class="chat-notification">
    <p>Ваш чат завершен.</p>
    <p><a href="<?php echo $this->createUrl("/payment"); ?>">Продолжить чат</a>  или  <a href="#" onclick="$('#chat-end-form-money').dialog('close');">закрыть чат</a></p>
</div>
<div id="chat-end-form-user" style="display: none;" title="Чат закрыт" class="chat-notification">
    <p>Эксперт сейчас не может ответить Вам в чате, попробуйте запросить чат позднее...</p>
</div>
<?php else:?>
<div id="chat-end-form" style="display: none;" title="Чат закрыт" class="chat-notification">
    <p>Чат завершен по таймауту.</p>
    <p>Клиент не отвечал в течении 2 минут</p>
</div>
<div id="chat-end-form-money" style="display: none;" title="Чат закрыт" class="chat-notification">
    <p>Ваш чат завершен.</p>
    <p>У пользователя не достаточно средств для продолжения чата.</p>
</div>
<div id="chat-end-form-user" style="display: none;" title="Чат закрыт" class="chat-notification">
    <p>Чат был закрыт пользователем.</p>
</div>
<?php endif;?>
<div id="one-minute-notification" style="display: none;" title="Время истекает" class="chat-notification">
    <p>У Вас осталось менее минуты для общения в чате.</p>
</div>
