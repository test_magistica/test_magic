<div class="usercab_page">
    <div class="head">
        <h1>Мои чаты</h1>
        <div class="chat-available">
            <?php
             $activeChat = Yii::app()->db->createCommand("select id from chats where user_id=".Yii::app()->user->id." and end_date IS NULL order by id DESC limit 1")->queryScalar();
             if($activeChat !== FALSE){
                 echo CHtml::link("Продолжить чат", Yii::app()->createUrl("chat/chatOnline", array("id" => $activeChat)));
             }
            ?>
            
        </div>
    </div>
</div>

<?php if (Yii::app()->user->role == Users::ROLE_EXPERT): ?>
    <div class="tab-container">
        <?php if (Yii::app()->user->hasFlash('error')): ?>
            <div class="flash-error">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php endif; ?>
        <ul class="tabs">
            <li class="current">Доступные чаты </li>
            <li>Статистика</li>
        </ul>
        <div class="box visible">
            <table class="extra-mail-table">
                <tr>
                    <th>Фото</th>
                    <th class="second">Имя</th>
                    <th class="last">Дата</th>
                    <th class="last">Действие</th>
                </tr>
                <?php
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider' => $availableChats,
                    'itemView' => 'available_chats_item_template', // refers to the partial view named '_post'
                    'pagerCssClass' => 'pagination',
                    'template' =>
                    '{items}'
                    . '<tr>'
                    . '<td colspan=4>{pager}</td>'
                    . '</tr>'
                ));
                ?>
            </table>
        </div>
        <div class="box">
            <table class="extra-mail-table">
                <tr>
                    <th>Пользователь</th>
                    <th class="second">Дата</th>
                    <th class="third">Длительность</th>
                    <th class="forth">История</th>
                    <th class="fifth">Гонорар</th>
                </tr>
                <?php
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider' => $statistics,
                    'itemView' => 'statistic_chat_item_template_expert', // refers to the partial view named '_post'
                    'pagerCssClass' => 'pagination',
                    'ajaxUpdate' => true,
                    'template' =>
                    '{items}'
                    . '<tr>'
                    . '<td colspan=5>{pager}</td>'
                    . '</tr>'
                ));
                ?>
            </table>
        </div>
    </div>
<?php else: ?>
    <div class="tab-container">
        <?php if (Yii::app()->user->hasFlash('error')): ?>
            <div class="flash-error">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php endif; ?>
        <ul class="tabs">
            <li class="current">Статистика</li>

        </ul>
        <div class="box visible">
            <table class="extra-mail-table">
                <tr>
                    <th>Эксперт</th>
                    <th class="second">Дата</th>
                    <th class="third">Длительность</th>
                    <th class="forth">История</th>
                    <th class="fifth">Стоимость</th>
                </tr>
                <?php
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider' => $statistics,
                    'itemView' => 'statistic_chat_item_template_user', // refers to the partial view named '_post'
                    'pagerCssClass' => 'pagination',
                    'ajaxUpdate' => true,
                    'template' => '{items}<tr><td colspan=5>{pager}</td></tr>',
                    'pager' => array(
                        "maxButtonCount" => 4
                    ),
                ));
                ?>
            </table>
        </div>

        <!--<a href="<?php echo Yii::app()->createAbsoluteUrl("chat/Chat/startChat/", array('id' => 3613)) ?>">Начать чат</a>-->
    </div>
<?php endif; ?>
<script>
    (function($) {
        $(function() {

            $('ul.tabs').on('click', 'li:not(.current)', function() {
                $.cookie('curr_tab', $(this).index());
                $(this).addClass('current').siblings().removeClass('current')
                        .parents('div.tab-container').find('div.box').eq($(this).index()).fadeIn(150).siblings('div.box').hide();
            });

            var tabIndex = window.location.hash.replace('#tab', '') - 1;
            var cur_tab = $.cookie('curr_tab');
            if (typeof cur_tab != "undefined") {
                $('ul.tabs li').eq(cur_tab).click();
            } else {
                if (tabIndex != -1) {
                    $('ul.tabs li').eq(tabIndex).click();
                }
            }
            $('a[href*=#tab]').click(function() {
                var tabIndex = $(this).attr('href').replace(/(.*)#tab/, '') - 1;
                $('ul.tabs li').eq(tabIndex).click();
            });

        })
    })(jQuery)
</script>
<script type="text/javascript">
//window.onbeforeunload = function(){alert("onbeforeunload")};
//window.onbeforeunload = function(){return ("onbeforeunload")};
//window.onunload = function(){alert("onunload")};
//window.onclose = function(){alert("onclose")};
//window.onRemoved = (function(winId){alert("chrome.windows.onRemoved");}) 
//$(window).unload(function(){alert("$ unload");});
//alert(chrome.windows);
//console.log(chrome);
//chrome.windows = window;
//chrome.windows.onRemoved.addListener(function(winId){alert("chrome.windows.onRemoved");}) 
</script>
