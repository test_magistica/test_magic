<?php $user = $data->user; ?>
<tr>
    <?php if (1): ?>
    <?php $usrPhoto = $this->getUserImage($user->id);?>
    <td>
       <a href="javascript:void(0);"><img 
                src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $usrPhoto; ?>" 
                title="Картинка пользователя" 
                valign='top' 
                width="60px"
                height="60px"
                /></a>
    </td>
    <td>
        <?php echo $user->name." ".$user->surname; ?>
    </td>
    <td>
        <?php echo Yii::app()->dateFormatter->format("HH:mm", $data->start_date); ?>
    </td>
    <td>
        <?php echo CHtml::link("Начать чат", Yii::app()->createAbsoluteUrl("chat/Chat/confirmChat", array('id' => $data->id)), array("target" => "_blank")); ?>
    </td>
    <?php endif; ?>
<!--    <td>
        <?php
//        echo CHtml::link(
//                (strlen($data->title) >= 50 ? mb_substr($data->title, 0, 50, 'UTF-8') . '...' : $data->title), Yii::app()->createAbsoluteUrl("question/view", array('id' => $data->id, '#' =>"quest-".$data->id ))
//        );
        ?>
    </td>
    <td class="last">
        <?php // echo Yii::app()->dateFormatter->formatDateTime($data->create_time, 'long', null); ?>
    </td>-->
</tr>