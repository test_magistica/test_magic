<?php
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/../css/extrasens/view.css"); 
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/../css/facybox/facybox.css"); 
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/../js/facybox.js");
?>
<div style="display: none;" id="customer-card">
    <div class="customer_card_container">
        <div class="cc-photo"><?php $img = $this->getUserImage($user->id);?>
            <img 
                src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $img; ?>" 
                title="Картинка пользователя" 
                valign='top' 
                width="58px"
                height="58px"
                />
        </div>
        <div class="cc-data-container">
            <div class="cc-data-title">карточка клиента</div>
            <div class="cc-data-body">
                <div class="cc-name">
                    <span>имя:</span><span><?php echo $user->name." ".$user->surname; ?></span>
                </div>
                <div class="cc-age">&nbsp;
                    <!--<span>возраст:</span><span>18</span>-->
                </div>
                <div class="cc-country">
                    <span>страна:</span><span><?php echo $user->country; ?></span>
                </div>
                <div class="cc-date-of-birth">
                    <span>дата рожд.:</span><span><?php echo $user->birthday;?></span>
                </div>
                <div class="cc-questions">
                    <span>вопросы:</span><span><a href="<?php echo Yii::app()->createAbsoluteUrl("question/userQuestions", array('id' => $user->id)); ?>">(<?php // echo $question->author->countQuestions;?>)</a></span>
                </div>
                <div class="cc-reg-date">
                    <span>рег:</span><span><?php 
                    echo Yii::app()->dateFormatter->formatDateTime($user->reg_date, 'long', null)
                    ?></span>
                </div>
                <div class="cc-balance">
                    <span>баланс:</span><span><?php echo $user->bill;?></span>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('a[rel*=facybox]').facybox({
            noAutoload: false,
            modal: true
        });
    }); 
</script>