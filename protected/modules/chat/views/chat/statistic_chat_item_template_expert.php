<?php 
    $user = $data->user; 
    $freeChatMinutes = (int)$data->free_minutes;
?>
<tr>
    <?php if (1): ?>
    <?php $usrPhoto = $this->getUserImage($user->id);?>
    <td>
       <a href="javascript:void(0);"><img 
                src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $usrPhoto; ?>" 
                title="Картинка пользователя" 
                valign='top' 
                width="60px"
                height="60px"
                /></a><br />
        <?php echo $user->name." ".$user->surname; ?>
    </td>
    <td>
        <?php echo Yii::app()->dateFormatter->format("dd MMMM yyyyг. HH:mm", $data->start_date); ?>
    </td>
    <td>
        <?php
            $dtStart = new DateTime($data->countdown_date);
            $dtEnd = new DateTime($data->end_date);
            $diff = $dtStart->diff($dtEnd);
            echo (((int)$diff->h * 60 + (int)$diff->i) - $freeChatMinutes)."мин." ;
        ?>
    </td>
    <td>
        <!--<a href="javascript:void(0);" onclick="window.open('<?php // echo $this->createUrl("/chat/Chat/reviewChatHistory", array('id' => $data->id)); ?>', '_blank', 'width=1000, height=800')">История онлайн</a>-->
         <?php echo CHtml::link("История онлайн", $this->createUrl("/chat/Chat/reviewChatHistory", array('id' => $data->id)), array("target" => "_blank")); ?>
    </td>
    <td>
        <?php
            // $data->expert->ratio == NULL - это у тестовых экспертов
            $callDuration = (((int)$diff->h * 60 + (int)$diff->i) - $freeChatMinutes) * Chat::CHAT_COST * (($data->expert->ratio_chat == NULL) ? (float)0.5 : (float)$data->expert->ratio_chat);
            echo $callDuration ;//. " | ".$data->id." | ".$data->expert->tariff." | ".$data->expert->ratio." - ".$data->expert->id." ".$user->id;
        ?>
    </td>
    <?php endif; ?>
<!--    <td>
        <?php
//        echo CHtml::link(
//                (strlen($data->title) >= 50 ? mb_substr($data->title, 0, 50, 'UTF-8') . '...' : $data->title), Yii::app()->createAbsoluteUrl("question/view", array('id' => $data->id, '#' =>"quest-".$data->id ))
//        );
        ?>
    </td>
    <td class="last">
        <?php // echo Yii::app()->dateFormatter->formatDateTime($data->create_time, 'long', null); ?>
    </td>-->
</tr>