USE `test_magistik`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: pariss_magistik
-- ------------------------------------------------------
-- Server version	5.5.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chat_attachments`
--

DROP TABLE IF EXISTS `chat_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chat_msg_id` int(11) NOT NULL COMMENT 'Внешний ключ на таблицу chat_messages. Указывает какому сообщению принадлежит вложение\n(chat_messages.id)',
  `url` varchar(255) NOT NULL COMMENT 'URL по которому возможно будет скачать вложение',
  PRIMARY KEY (`id`),
  KEY `msg_idx` (`chat_msg_id`),
  CONSTRAINT `msg` FOREIGN KEY (`chat_msg_id`) REFERENCES `chat_messages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='Таблица вложений к сообщениям в чате';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chat_messages`
--

DROP TABLE IF EXISTS `chat_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chat_id` int(11) NOT NULL COMMENT 'Внешний ключ на таблицу chats. Указывающий какому чату принадлежит данное сообщение. (chats.id)',
  `author_id` int(11) NOT NULL COMMENT 'Автор сообщения. Внешний ключ на таблицу Users',
  `message` longtext NOT NULL COMMENT 'Сообщение',
  `send_time` datetime NOT NULL COMMENT 'Время отправки сообщения',
  `has_attach` tinyint(1) DEFAULT '0' COMMENT 'Указывает есть ли вложения у данного сообщения\nПо умолчанию 0 - нет вложений',
  PRIMARY KEY (`id`),
  KEY `chat_idx` (`chat_id`),
  CONSTRAINT `chat` FOREIGN KEY (`chat_id`) REFERENCES `chats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8 COMMENT='Сообщения чатов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `chats`
--

DROP TABLE IF EXISTS `chats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'Внешний ключ на пользователя который \nначал чат (users.id & role = user)',
  `expert_id` int(11) NOT NULL COMMENT 'Внешний ключ на эксперта с которым\nидет чат( users.id & role = expert)',
  `start_date` datetime NOT NULL COMMENT 'Дата начала чата',
  `countdown_date` datetime DEFAULT NULL COMMENT 'Время с которого пойдет отсчет продолжительности чата. Это то время когда на чат ответил эксперт.',
  `end_date` datetime DEFAULT NULL COMMENT 'Дата окончания чата. Если значение NULL, то чат в активном состояниии',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COMMENT='Таблица с чатами';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-18 20:54:26
