<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class mController extends Controller {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '/layouts/column2';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function getMonthYear() {
        $curDate = date('Y-m-d');
        $toDate = date("Y-m-d", mktime(0, 0, 0, date("m") - 6, date("d"), date("Y")));
        $datesArray = $this->dateArray($toDate, $curDate, true);

        return $datesArray;
    }

    function dateArray($from, $to, $value = NULL) {
        $begin = new DateTime($from);
        $end = new DateTime($to);
        $interval = DateInterval::createFromDateString('1 month');
        $days = new DatePeriod($begin, $interval, $end);

        $baseArray = array();
        foreach ($days as $day) {
            $dateKey = $day->format("Y-m-d");
            $baseArray[$dateKey] = $this->getNumDate($dateKey);
        }
        $baseArray[date('Y-m-d')] = $this->getNumDate(date('Y-m-d'));

        return $baseArray;
    }

    public function getNumDate($date) {
        $ar = Yii::app()->locale->getMonthNames($width = 'wide', $standAlone = true);
        $result = $ar[date('n', strtotime($date))] . ' ' . date('Y', strtotime($date));
        return $result;
    }

    public static function getUser($id, $attr) {
        $model = Users::model()->findByPk($id);
        return $model->$attr;
    }

    public function getLastTopic($toID) {
        $rows = Yii::app()->db->createCommand('SELECT topic FROM `message` WHERE `toID` = ' . $toID . ' ORDER BY `id` DESC LIMIT 1')->queryAll();
        foreach ($rows as $row)
            return $row['topic'];
    }

    public function getLastTopicService($fromID) {
        $rows = Yii::app()->db->createCommand('SELECT topic FROM `message` WHERE `fromID` = ' . $fromID . ' ORDER BY `id` DESC LIMIT 1')->queryAll();
        foreach ($rows as $row)
            return $row['topic'];
    }

    public function getUserImage($userID) {
        $image = Userphoto::model()->find("userID = '" . $userID . "' AND main=1");
        $image = !empty($image['image']) ? $image['image'] : 'nophoto.jpg';
        return $image;
    }

    public static function getMessageCount() {
        $rows = Yii::app()->db->createCommand("SELECT id FROM `message`  WHERE `toID` = '" . (int) Yii::app()->user->id . "' AND `userID` != '" . (int) Yii::app()->user->id . "' AND status = 0")->queryAll();
        return count($rows);
    }

    public static function getBookingCount() {
        $consultation = new Consultation('search');
        $cCount = $consultation->count("expertID = '" . Yii::app()->user->id . "' and status = 1");
        return $cCount;
    }

    public static function checkBooking($id) {
        $rows = Yii::app()->db->createCommand("SELECT id FROM `servicemessage`  WHERE `toID` = '" . (int) Yii::app()->user->id . "' AND `userID` != '" . (int) Yii::app()->user->id . "' AND bookingID = '" . $id . "' AND status = 0")->queryAll();
        return count($rows);
    }

    public static function changeTheNameId($id) {
        if ($id != 1)
        {
    	    switch ($id) 
    	    {
    		case 'ANSWER':
        	    $data = "<span class='z-done'>Выполнен</span>";
        	    break;
        	case 'CANCEL':
        	    $data = "<span class='z-cancel'>Отменен</span>";
        	    break;
        	case 'BUSY':
        	    $data = "<span class='z-cancel'>Занят/Отклонен</span>";
        	    break;
        	case 'NOANSWER':
        	    $data = "<span class='z-cancel'>Нет ответа</span>";
        	    break;
    		case 'BEGIN':
    		    $data = "<span class='z-waiting'>В процессе</span>";
    		    break;
    		case 'BREAK':
    		    $data = "<span class='z-cancel'>Cорвался</span>";
    		    break;
    		case '0':
    		    $data = "<span class='z-cancel'>Отклонено пользователем</span>";
    		    break;
    		case '3':
    		    $data = "<span class='z-cancel'>Отклонено экспертом</span>";
    		    break;
		default:
        	    $data = "<span class='z-cancel'>Неизвестно</span>";
    		    break;
    	    };
    	}
    	else 
    	{
    	    $data = "<span class='z-waiting'>В ожиданиии</span>";
    	}
        return $data;
    }

    public static function getUserName($id) {
        $model = Users::model()->findByPk($id);
        return "$model->name" . " $model->surname";
    }

    public static function getServiceName($id) {
        $model = Service::model()->findByPk($id);
        return "$model->notes";
    }

    public function BuildUrlParamsWithSignature($params, $password) {
        ksort($params);

        $url = '';

        if (!is_array($params))
            return $url;

        foreach ($params as $key => $value) {
            $url .= $key . "=" . urlencode($value) . "&";
        }

        $signature = md5($url . "&password=" . urlencode($password));

        $url .= "signature=" . $signature;

        return $url;
    }

    public static function getBookingCountExpert() {
        $rows = Yii::app()->db->createCommand("SELECT id FROM `servicemessage`  WHERE `toID` = '" . (int) Yii::app()->user->id . "' AND `userID` != '" . (int) Yii::app()->user->id . "' AND status = 0")->queryAll();
        return count($rows);
    }

    public function addCallBack($data) {
        $query = 'INSERT INTO `call_back` (';
        $q = '';
        
        $expert = Users::model()->findByPk($data['user_id']);
	    if ($expert->sip_tel == 'sip')
	        if ($expert->sip)
	    	    $data['from_num'] = $expert->sip;
		else 
		    $data['from_num'] = $expert->tele;
	    else 
		$data['from_num'] = $expert->tele; 
        
        
        foreach ($data as $key => $value) {
            $query .= "`$key`,";
            $q .= "'$value',";
        }

        $l1 = strlen($query);
        $query = substr($query, 0, $l1 - 1);

        $l2 = strlen($q);
        $q = substr($q, 0, $l2 - 1);

        $query .= ")VALUES ($q)";

        Yii::app()->db->createCommand($query)->query();
        return TRUE;
    }
    /**
     * Если эксперт не занят, то возвращается URL на чат иначе FALSE
     * @param \Users $expert expert id
     * @return mixed URL - string или false
     */
    public function getUrlToExpertChat(Users $expert) {
        $isBusy = (int)Yii::app()->db->createCommand("select count(1) from chats where expert_id=".$expert->id." and end_date IS NULL AND countdown_date IS NOT NULL")->queryScalar();
        if($isBusy == 0){
            return Yii::app()->createUrl("/chat/Chat/startChat", array('id' => $expert->id));
        }else{
            return FALSE;
        }
    }

}
