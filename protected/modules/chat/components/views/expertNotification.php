<div id="chatnotification" style="display: none;" title="Новый чат" class="chat-notification">
    <p>Клиент (<span class="reminder-name"></span>) запросил чат с Вами.</p>
    <p class="chat-link"></p>
    <p class="chat-link-close"></p>
</div>
<?php $mp3 = Yii::app()->assetManager->getPublishedUrl(Yii::getPathOfAlias("chat") . "/mp3/".Yii::app()->params['expertMP3notification']); ?>
<div class="chat-mp3" style="display: none;">
    <audio src="<?php echo $mp3; ?>" preload="auto" ></audio>
</div>

<script type="text/javascript">
    var notificationE = function() {
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createUrl("/chat/notification")?>",
            data: ({uid: <?php echo Yii::app()->user->id; ?>}),
            dataType: "json",
            success: function(data) {
                if ( typeof data != "undefined" && data !== null ) {
                    if (data.empty == null && data.error == null) {
                        if (!$("#chatnotification").dialog("isOpen")) {
                            $("#chatnotification .reminder-name").text(data.name);
                            $("#chatnotification .chat-link").html(data.link);
                            $("#chatnotification .chat-link-close").html(data.link_close);

                            $("#chatnotification").dialog("open");
                            startPlay();
                        }
                    } else {//if (typeof data.empty != "undefined") {
                        if ($("#chatnotification").dialog("isOpen")) {
                            $("#chatnotification").dialog("close");
                            stopPlay();
                        }
                    }
                }
            },
            error: function(err) {
                console.log("error");
            }
        });
    }

    function startPlay() {
        window.as[0].play();
    }

    function stopPlay() {
        window.as[0].pause();
        window.as[0].trackEnded();
    }

    $("#chatnotification").dialog({
        modal: true,
        autoOpen: false,
        draggable: false,
//        appendTo: ""
    });
    $(document).ready(function() {
        setInterval(notificationE, 3000);
        $("#chatnotification a").on("click", function() {
            $("#chatnotification").dialog("close");
        });
    });
</script>