<?php

class DefaultController extends mController {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations			
        );
    }
    
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'accept', 'change'),
                'users' => array('expert', '@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

}
