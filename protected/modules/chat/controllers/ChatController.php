<?php

class ChatController extends mController {
    //	public $layout='//layouts/column2';

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;
    public $time;

    /**
     * The user which is a dialogue
     * @var Users 
     */
    public $opponent = null;
    public $messages = array();
    public $chatId = 0;
    // Флаг указывающий что идет просмотр истории
    // Применяется в представлении chat.php
    // Добавил эту переменную что бы не создавать еще один шаблон
    public $reviewHistory = false;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * You should override this method to provide stronger access control 
     * to specifc restfull actions via AJAX
     */
    public function validateAjaxUser($action) {
        return true;
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated users to access all actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Возвращает клиенту новые сообщения
     * @param int $id Идентификатор чата 
     */
    public function actionRefresh() {
        if (isset($_POST['id']) && isset($_POST['cid'])) {

            $json_array = array();
            $lastMsg = (int) $_POST['id'];
            $chatId = (int) $_POST['cid'];
            $hasNew = Yii::app()->db->createCommand("select count(1) from `chat_messages` where id > " . $lastMsg . " and chat_id = " . $chatId)->queryScalar();

            // Если есть новые сообщения
            if ($hasNew > 0) {
                $freeChatTime = $this->calculateFreeChatTime(Chat::model()->findByPk($chatId));
                $chat = Chat::model()->findByPk($chatId);
                // Проверяем были ли ответы эксперта
                $expertAnswers = ChatMessage::model()->count("chat_id=" . $chatId . " and author_id=" . $chat->expert_id);
                $messages = ChatMessage::model()->with(array(
                            'author' => array(
                                'select' => "name, surname, role",
                                'condition' => "t.id > " . $lastMsg . " AND chat_id = " . $chatId
                            ),
                            'chatAttachments',
                        ))->findAll(array(
                    'order' => 't.id ASC'
                ));
                foreach ($messages as $msg) {
                    $tmp['msg_id'] = $msg->id;
                    $tmp['author_id'] = $msg->author->id;
                    $tmp['author_role'] = $msg->author->role == Users::ROLE_USER ? "1" : "0";
                    $tmp['author_name'] = $msg->author->name . " " . $msg->author->surname;
                    $tmp['msg_text'] = $msg->message;
                    $tmp['msg_time'] = Yii::app()->dateFormatter->format("HH:mm", $msg->send_time);
                    if (isset($msg->chatAttachments)) {
                        foreach ($msg->chatAttachments as $attach) {
                            $tmp['msg_attach'][] = $this->createUrl("/chat/Chat/downloadFile", array('id' => $attach->id));
                        }
                    }
                    
                    // Если эксперт отвечал передаем джаваскрипту бесплатное время чата
                    // по значению $freeChatTime будет запущен обратный отсчет времени чата
                    // magistika_chat.js    ln.433 - 451
                    if ($expertAnswers > 0) {
                        $tmp['countdown'] = 1;
                        $tmp['free_time'] = $freeChatTime;
//                        if($freeChatTime >= 0){
//                            $msgNotif = new MessageNotificationComponent();
//                            $msgNotif->addMessage("freeTimesUp", Yii::t("Ваше бесплатное время на общение в чате исчерпано. Для продолжения чата пожалуйста <a href='".Yii::app()->createUrl("/payment")."'>пополните счет</a>."));
//                        }
                    }
                    
                    $json_array[] = $tmp;
                }
            }
            echo json_encode($json_array);
        }
        Yii::app()->end();
    }

    /**
     * Method wich to check time is up of the chat.
     */
    public function actionWatchDog() {
        $sessData = Yii::app()->session['chatData'];
        $free_chat_minutes = 0;
        if ($sessData !== NULL) {
            $chat = Chat::model()->with(array(
                        "user" => array(
                            'select' => 'select.bill, select.free_chat_minutes'
                        ),
                        "expert" => array(
                            'select' => 'expert.tariff'
                        ),
                    ))->findByPk((int) $sessData['chat_id']);
            $free_chat_minutes = $chat->user->free_chat_minutes;
            $resp = array();
            if ($chat->end_date != NULL) { // Чат уже закрыт
                $resp["chat_live"] = 2;
                $resp["msg"] = "Чат закрыт опонентом.";
                Yii::app()->session['chatData'] = NULL;
                $expert = Users::model()->findByPk((int) $chat->expert_id);
                $expert->updateByPk((int) $chat->expert_id, array('online' => 'online'));
                Yii::app()->clearGlobalState($chat->id . "_" . Users::ROLE_EXPERT);
                Yii::app()->clearGlobalState($chat->id . "_" . Users::ROLE_USER);
            } else { // Чат активен
                $dt = new DateTime();
                $dtStart = new DateTime($chat->countdown_date); // Дата когда ответил эксперт
                $diff = $dt->diff($dtStart, true);

                $time = (((int) $chat->user->bill / (int)Chat::CHAT_COST ) - (int) $diff->i) + $free_chat_minutes;
                $lastMessageTime = Yii::app()->db->createCommand("select max(send_time) from chat_messages where chat_id=" . $chat->id)->queryScalar();
                if ($chat->countdown_date != NULL) {
                    // Если эксперт ответил, значит чат активен 
                    // и нужно проверить что бы оппоненты  
                    // не бездействовали более 2 минут

                    if ($lastMessageTime != NULL) {
                        // Время последнего сообщения
                        $dtLastMessageTime = new DateTime($lastMessageTime);
                        // Разница между последним сообщением не должна превышать 2 мин
                        $diff = $dt->diff($dtLastMessageTime, true);

//                        $deltaTime = $dtStart->diff($dtLastMessageTime, TRUE);
//                        $time -= $deltaTime->i;
                    }
                    if ($diff->i >= 2) {
                        // Чат не активен в течении 2 мин.
                        // Закрыть чат из-за отсутствия активности
                        $resp["chat_live"] = 3;
                        $resp["msg_i"] = "Чат закрывается из-за отсутствия активности в чате более 2мин.";
                        if (self::closeChat($chat)) {
                            Yii::app()->session['chatData'] = NULL;
                            $expert = Users::model()->findByPk((int) $chat->expert_id);
                            $expert->updateByPk((int) $chat->expert_id, array('online' => 'online'));
                            // Пересчитать время и деньги для пользователя ....
                            $this->calculateBill($chat);
                            // Очищаем глобальные данные для чата
                            Yii::app()->clearGlobalState($chat->id . "_" . Users::ROLE_EXPERT);
                            Yii::app()->clearGlobalState($chat->id . "_" . Users::ROLE_USER);
                        }
                    }
                } else {
                    // Проверяем каждые 2 мин ответил ли эксперт
                    // то пользователю в чат нужно вывести сообщение
                    // "К сожалению, эксперт не подключился  пожалуйста, подождите…"
                    $diff = $dt->diff(new DateTime($chat->start_date), true);
                    if ($diff->i % 2 == 0) {
                        $resp['need_wait'] = array(
                            0 => array(
                                'msg_id' => 0,
                                'author_id' => 0,
                                'author_name' => "Системное",
                                'msg_text' => "К сожалению, эксперт не подключился  пожалуйста, подождите…",
                                'msg_time' => Yii::app()->dateFormatter->format("HH:mm", $dt->format(Chat::DATE_TIME_FORMAT))
                            )
                        );
                    }
                }
                if ($time < 1) {
                    // Денег меньше чем на 1 минуту, нужно закрыть чат
                    $resp["chat_live"] = 0;
                    if (self::closeChat($chat)) {
                        $chat->refresh();
                        $expert = Users::model()->findByPk((int) $chat->expert_id);
                        $expert->updateByPk((int) $chat->expert_id, array('online' => 'online'));
                        Yii::app()->session['chatData'] = NULL;
                        // Пересчитать время и деньги для пользователя ....
                        $this->calculateBill($chat);
                        // Очищаем глобальные данные для чата
                        Yii::app()->clearGlobalState($chat->id . "_" . Users::ROLE_EXPERT);
                        Yii::app()->clearGlobalState($chat->id . "_" . Users::ROLE_USER);
                        if (Yii::app()->user->role == Users::ROLE_USER) {
                            
                        }
                    }
                    if (Yii::app()->user->role == Users::ROLE_USER) {
                        $resp["msg"] = "Чат закрывается.\nДля дальнейшей возможности общаться в чате, пополните счет.";
                    } else {
                        $resp["msg"] = "Чат закрывается.\nУ пользователя недостаточно средств для продолжения чата.";
                    }
                } else {
                    if (!isset($resp["chat_live"])) {
                        // Продолжаем чат
                        $resp["chat_live"] = 1;
                        $resp["free_time"] = $this->calculateFreeChatTime($chat);
                    }
                }
                $resp["time"] = $this->prepareTime($time);
                // Передаем джаваскрипту значение для начала отсчета времени
                $resp["free_time"] = $this->calculateFreeChatTime($chat);
            }

            echo json_encode($resp);
        } else {
            echo json_encode(array("chat_live" => 0));
        }
        Yii::app()->end();
    }

    /**
     * Рассчитывает бесплатное время чата. Если значение возхвращенное
     * методом больше либо равно нулю. Бесплатные минуты общения в чате закончились.
     */
    public function calculateFreeChatTime(Chat $chat) {
        $dt = new DateTime();
        $dtStart = new DateTime($chat->countdown_date); // Дата когда ответил эксперт
        $diff = $dt->diff($dtStart, true);
        return $diff->i - (int) $chat->user->free_chat_minutes;
    }

    /**
     * Create new chat by user
     * @param int $id Expert ID
     */
    public function actionStartChat($id) {
        $chatData = Yii::app()->session['chatData'];
        if ($chatData != NULL) {
            // Пользователь пытается запустить еще один чат
            if (isset($chatData['chat_id'])) {
                // Предупредить пользователя о том что ему нужно остановить текущий чат
                // и только после этого начинать новый
                // Пока останавливаем чат автоматом.
                $oldChat = Chat::model()->findByPk((int) $chatData['chat_id']);
                $dt = new DateTime();
                $oldChat->end_date = $dt->format(Chat::DATE_TIME_FORMAT);
                $oldChat->save();
            }
        }
        // чатится ли эксперт уже с кем нибудь
        $expertHasChat = (int) Yii::app()->db->createCommand("select count(1) from `chats` where expert_id = " . (int) $id . " AND end_date IS NULL AND countdown_date IS NOT NULL")->queryScalar();
        if ($expertHasChat > 0) {
            Yii::app()->user->setFlash('error', "Извините. На данный момент эксперт занят.\nПовторите попытку позже.");
            // Редиректим обратно на страницу
            $this->redirect($_SERVER["HTTP_REFERER"]);
        }
        $user = Users::model()->findByPk(Yii::app()->user->id);
        $expertTimeCost = Chat::CHAT_COST;//(int) Yii::app()->db->createCommand("select tariff from `users` where id = " . (int) $id)->queryScalar();
        // Если у пользователя есть бесплатные минуты чата - начать чат
        if ($user->free_chat_minutes > 0 || (int) $user->bill >= ($expertTimeCost * Chat::MIN_CHAT_DURATION)) {
            $chat = new Chat();
            $chat->user_id = Yii::app()->user->id;
            $chat->expert_id = (int) $id; // (int) $_POST['eid'];
            // Бесплатные минуты общения в чате.
//            $chat->free_minutes = isset(Yii::app()->params['freeChatMinutes']) ? (int) Yii::app()->params['freeChatMinutes'] : 0;
            if ($chat->save(FALSE)) {
                $chat->refresh();
            } else {
                throw new Exception("eowiurtw");
            }
            // Идентификатор опонента в чате эксперт или пользователь
            Yii::app()->session['chatData'] = array(
                'opponentId' => $id, //$chat->expert_id,
                'chat_id' => $chat->id,
            );
            $this->redirect(Yii::app()->createAbsoluteUrl("chat/Chat/chatOnline", array('id' => $chat->id)));
        } else {
            $paymentLink = $this->createUrl("/payment");
            $msgNotif = new MessageNotificationComponent();
            $msgNotif->addMessage(__CLASS__ . __LINE__, "Минимальная длительность онлайн чата составляет " . Chat::MIN_CHAT_DURATION . " минут (" . $expertTimeCost * Chat::MIN_CHAT_DURATION . "руб.)<br /> <a href='" . $paymentLink . "'> Пожалуйста пополните счет</a>");
//            Yii::app()->user->setFlash('error', "Минимальная длительность онлайн чата составляет " . Chat::MIN_CHAT_DURATION . " минут (" . $expertTimeCost * Chat::MIN_CHAT_DURATION . "руб.)<br /> <a href='" . $paymentLink . "'> Пожалуйста пополните счет</a>");
            // Редиректим обратно на страницу
            $this->redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public static function closeChat(Chat $chat) {
        $dt = new DateTime();
        $chat->end_date = $dt->format(Chat::DATE_TIME_FORMAT);
        // Количество бесплатных минут использованных в чате.
        // Необходимо для отображения статистики по чату
        $chat->free_minutes = $chat->user->free_chat_minutes;
        // Пользователь исчерпал бесплатные минуты общения в чате
        Users::model()->updateByPk($chat->user->id, array('free_chat_minutes' => 0));
        return $chat->save(FALSE, array('end_date', 'free_minutes'));
    }

    public static function closeAllExpertChats($expertId) {
        $openedChats = Chat::model()->findAll("expert_id=" . $expertId . " AND end_date IS NULL");
        foreach ($openedChats as $openedChat) {
            self::closeChat($openedChat);
        }
    }

    /**
     * To finish a chat can user or expert
     * @param int $id ChatID
     */
    public function actionFinishChat($id) {
        if (isset($id) && !empty($id)) {
            $chat = Chat::model()->findByPk((int) $id);
            $chatData = Yii::app()->session['chatData'];
            if ($chat !== NULL) {
                // Chat was finished by opponent.
                if ($chat->end_date != NULL) {
                    if ($chatData != NULL) {
                        Yii::app()->session['chatData'] = NULL;
                        $expert = Users::model()->findByPk((int) $chat->expert_id);
                        $expert->updateByPk((int) $chat->expert_id, array('online' => 'online'));
                        // Need set flash
//                        $byWhom = Yii::app()->user->role == Users::ROLE_USER ? "экспертом" : "пользователем";
//                        Yii::app()->user->setFlash("error", "Чат был закрыт " . $byWhom . "!");
                        // Redirect to chat/index.html
//                        $this->redirect(Yii::app()->createUrl("chat/index"));
                    }
                } else {
                    if (self::closeChat($chat)) {
                        $chat->refresh();
                        if ($chatData != NULL) {
                            if (isset($chatData['chat_id'])) {
                                Yii::app()->session['chatData'] = NULL;
                            }
                        }
                        $expert = Users::model()->findByPk((int) $chat->expert_id);
                        $expert->updateByPk((int) $chat->expert_id, array('online' => 'online'));
                        // Пересчитать время и деньги для пользователя ....
                        $this->calculateBill($chat);
                        // Очищаем глобальные данные для чата
                        Yii::app()->clearGlobalState($chat->id . "_" . Users::ROLE_EXPERT);
                        Yii::app()->clearGlobalState($chat->id . "_" . Users::ROLE_USER);
                    }
                }
                // После закрытия чата редиректим пользователя на страницу экстрасенса
                if (Yii::app()->user->role == Users::ROLE_USER) {
                    $expertUrl = Yii::app()->createUrl('/extrasens/view/', array('id' => $chat->expert->id, 'title' => $this->getCHPU($chat->expert->name . '-' . $chat->expert->profession), 'class' => 'review'));
                    $this->redirect($expertUrl);
                }
            }
            // редирект лучше на статистику!
            $this->redirect(Yii::app()->createAbsoluteUrl("/chat/index"));
        }
    }

    /**
     *  Функция для экспертов. Мониторят новые чаты.
     *  Будет использоваться через ajax
     */
    public function actionMonitorNewChat() {
        $newChats = Chat::model()->with('user')->findAll('expert_id=' . Yii::app()->user->id . " and end_date=NULL");
        if ($newChats != NULL) {
            $data = array();
            //$expert = Users::model()->findByPk(Yii::app()->user->id);
            foreach ($newChats as $chat) {
                $data[$chat->id] = array(
                    'uid' => $chat->user->id,
                    'photo' => $chat->user->image,
                    'name' => $chat->user->name,
                    'surname' => $chat->user->surname,
                    'availableTime' => (int) ((int) $chat->user->bill / (int) Chat::CHAT_COST),
                    'linkToChat' => $this->createUrl("chat/chatOnline", array('id' => $chat->id))
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array("empty"));
        }
        Yii::app()->end();
    }

    /**
     * Main function of the chat dialogs window
     * @param int $id ID of the chat
     */
    public function actionChatOnline($id) {
        // Функция которая отрисовывает основное окно чата
        $chat = Chat::model()->find("id=" . (int) $id);
        if ($chat !== NULL) {
            // Chat exists and don't finished by any opponents
            if ($chat->end_date == NULL) {
                $sessData = Yii::app()->session['chatData'];
                if ($sessData != NULL) {
                    $this->chatId = (int) $sessData['chat_id'];
                    $this->opponent = Users::model()->findByPk((int) $sessData['opponentId']);
                    // Сообщения
                    $this->messages = ChatMessage::model()->with('chatAttachments', 'author')->findAll('chat_id=' . $this->chatId);
                    $this->render('chatOnline');
                    return;
                }
            } else {
                $sessData = Yii::app()->session['chatData'];
                if ($sessData != NULL) {
                    // Нужно почистить сессию
                    // если текущий юзер - пользователь, значит чат прекратил експерт
                    // рассчет времени и денег!
                    // если текущий юзер - эксперт, значит чат прекратил пользователь
                    // и рассчет времени и денег произведен
                    if (Yii::app()->user->role == Users::ROLE_EXPERT) {
                        Yii::app()->session['chatData'] = NULL;
                    }
                }
            }
        }
        $this->redirect(Yii::app()->createUrl("chat/"));
    }

    /**
     * Expert confirm of a chat. He starts the chat with user who initiate this chat.
     * @param int $id ChatID Wich chat confirm expert
     */
    public function actionConfirmChat($id) {
        $chat = Chat::model()->findByPk((int) $id);
        $dt = new DateTime();
        $chat->updateByPk($chat->id, array("countdown_date" => $dt->format(Chat::DATE_TIME_FORMAT)));
        $user = Users::model()->findByPk((int) $chat->user_id);
        $expert = Users::model()->findByPk((int) $chat->expert_id);
        $expert->updateByPk((int) $chat->expert_id, array('online' => 'busy'));
        // Идентификатор опонента в чате эксперт или пользователь
        Yii::app()->session['chatData'] = array(
            'opponentId' => (int) $chat->user_id, //$chat->expert_id,
            'chat_id' => $chat->id,
            'prepaid_time' => $user->prepaid_chat_time
        );

        $this->redirect(Yii::app()->createAbsoluteUrl("chat/Chat/chatOnline", array('id' => $chat->id)));
    }

    /**
     * AJAX method. Send new message to a chat.
     */
    public function actionSendMessage() {
        // cnm - chat-new-msg (textarea)
        if (isset($_POST['cnm']) && !empty($_POST['cnm'])) {
            $chatSessData = Yii::app()->session['chatData'];
            $chatId = (int) $_POST['cid']; // ? $_POST['cid'] : null;
            if ($chatSessData !== NULL && $chatId == (int) $chatSessData['chat_id']) {
                $newMsg = new ChatMessage();
                $dt = new DateTime();
                $newMsg->chat_id = (int) $chatSessData['chat_id'];
                $newMsg->author_id = Yii::app()->user->id;
                $newMsg->message = $_POST['cnm'];
                $newMsg->send_time = $dt->format(Chat::DATE_TIME_FORMAT);
                if ($newMsg->save()) {
                    echo json_encode(
                            array(
                                'res' => 'ok'
                            )
                    );
                } else {
                    echo json_encode(
                            array(
                                'error' => 'Сообщение не сохранилось'
                            )
                    );
                }
                // Взводим флаг что пользователь перестал печатать
                Yii::app()->setGlobalState($chatId . "_" . Yii::app()->user->role, 0);
            }
        }
        Yii::app()->end();
    }

    /**
     * Данный метод взводит флаг если пользователь начинает печатать
     * Для реализации пишущего карандаша аналог скайпа
     */
    public function actionOpponentTyping() {
        $chatId = $_POST['cid'];
        // Взводим флаг что пользователь печатает
        Yii::app()->setGlobalState($chatId . "_" . Yii::app()->user->role, 1);
    }

    public function actionIsOpponentTyping() {
        $chatId = $_POST['cid'];
        $opponentRole = Yii::app()->user->role == Users::ROLE_USER ? Users::ROLE_EXPERT : Users::ROLE_USER;
        $isTyping = Yii::app()->getGlobalState($chatId . "_" . $opponentRole, 0);
        echo json_encode(array("r" => $isTyping));
        Yii::app()->end();
    }

    public function actionUploadFile() {
        if (isset($_POST['cid']) && isset($_POST['mid'])) {
            if (is_numeric($_POST['cid']) && is_numeric($_POST['mid'])) {
                $chatId = (int) $_POST['cid'];
                $msgId = (int) $_POST['mid'];
//                if($msgId == 0){ // messages not send yet
                $msg = new ChatMessage();
                $msg->chat_id = $chatId;
                $msg->author_id = Yii::app()->user->id;
                $msg->message = "Отправка файла";
                $dt = new DateTime();
                $msg->send_time = $dt->format(Chat::DATE_TIME_FORMAT);
                $msg->has_attach = 1;
                $msg->save(FALSE);
                $msgId = $msg->id;
//                }else{
//                    ChatMessage::model()->updateByPk($msgId, array('has_attach' => 1));
//                }
                $img = CUploadedFile::getInstance(ChatAttachment::model(), "url");
                $attach = new ChatAttachment();
                $attach->chat_msg_id = $msgId;
                $attach->url = CUploadedFile::getInstance(ChatAttachment::model(), "url");
                if ($attach->save()) {
                    echo json_encode($attach->id);
                } else {
                    echo json_encode('error');
                }

                Yii::app()->end();
            }
        }
    }

    /**
     * Download a files for users
     * @param int $id AttachmentID
     */
    public function actionDownloadFile($id) {
        if (isset($id) && !empty($id)) {
            $attach = ChatAttachment::model()->findByPk($id);
            if ($attach !== NULL) {
                $file = Yii::app()->params['uploadDir'] . ChatAttachment::PATH_TO_UPLOAD_DIR . $attach->url;
                return Yii::app()->request->sendFile($file, file_get_contents($file));
            }
        }
    }

    public function actionGetHistory() {
        if (isset($_POST['hp']) && !empty($_POST['hp']) && isset($_POST['cid']) && !empty($_POST['cid'])) {
            $historyPeriod = $_POST['hp'];
            $chatId = $_POST['cid'];
            $dt = new DateTime();
            switch ($historyPeriod) {
                case 1:
                    $dt = $dt->sub(new DateInterval("P7D"));
                    break;
                case 2:
                    $dt = $dt->sub(new DateInterval("P31D"));
                    break;
                case 3:
                    $dt = $dt->sub(new DateInterval("P6M"));
                    break;
                case 4:
                    $dt = $dt->sub(new DateInterval("P20Y"));
                    break;
            }
            $chatSessData = Yii::app()->session['chatData'];
            $chat = Chat::model()->findByPk($chatId);
            $criteria = new CDbCriteria();
            $criteria->with = array(
                'chatMessages',
                'chatMessages.chatAttachments',
            );
            $criteria->condition = "send_time <= NOW() and send_time >='" . $dt->format(Chat::DATE_TIME_FORMAT) . "' and user_id=$chat->user_id and expert_id=$chat->expert_id";
            $criteria->order = "send_time ASC";

            $chats = Chat::model()->findAll($criteria);
            foreach ($chats as $c) {
                foreach ($c->chatMessages as $msg) {
                    $tmp['msg_id'] = $msg->id;
                    $tmp['author_id'] = $msg->author->id;
                    $tmp['author_role'] = $msg->author->role == Users::ROLE_USER ? "1" : "0";
                    $tmp['author_name'] = $msg->author->name . " " . $msg->author->surname;
                    $tmp['msg_text'] = $msg->message;
                    $tmp['msg_time'] = Yii::app()->dateFormatter->format("HH:mm", $msg->send_time);
                    $tmp['msg_date'] = Yii::app()->dateFormatter->format("dd MMMM yyyyг.", $msg->send_time);
                    if (isset($msg->chatAttachments)) {
                        foreach ($msg->chatAttachments as $attach) {
                            $tmp['msg_attach'][] = $this->createUrl("/chat/Chat/downloadFile", array('id' => $attach->id));
                        }
                    }
                    $json_array[] = $tmp;
                    unset($tmp['msg_attach']);
                }
            }
            echo json_encode($json_array);
        } else {
            echo json_encode(array());
        }
        Yii::app()->end();
    }

    /**
     * List of all models.
     */
    public function actionIndex() {
        $availableChats = array();
        $statistics = NULL;
        if (Yii::app()->user->role == Users::ROLE_EXPERT || Yii::app()->user->role == Users::ROLE_ADMIN) {
            $criteria = new CDbCriteria();
            $criteria->condition = "expert_id=" . Yii::app()->user->id . " AND end_date IS NULL";
            $criteria->with = "user";
            $criteria->order = "start_date ASC";
            $availableChats = new CActiveDataProvider("Chat", array(
                'id' => 'avchats',
                'pagination' => array(
                    'pageSize' => Yii::app()->params['questionsPerPage'],
//                'pageSize' => 5,
                ),
                'criteria' => $criteria,
            ));
            // Выбираем статистические данные
            $criteria = new CDbCriteria();
            $criteria->with = "user";
            $criteria->condition = "expert_id=" . Yii::app()->user->id . " AND countdown_date is not null and end_date IS NOT NULL";
            $criteria->order = "end_date DESC";
            $statistics = new CActiveDataProvider("Chat", array(
                'id' => 'stats',
                'pagination' => array(
                    'pageSize' => Yii::app()->params['questionsPerPage'],
                ),
                'criteria' => $criteria,
            ));
        } elseif (Yii::app()->user->role == Users::ROLE_USER) {
            // Выбираем статистические данные
            $criteria = new CDbCriteria();
            $criteria->with = "expert";
            $criteria->condition = "user_id=" . Yii::app()->user->id . " AND countdown_date is not null and end_date IS NOT NULL";
            $criteria->order = "end_date DESC";
            $statistics = new CActiveDataProvider("Chat", array(
                'id' => 'stats',
                'pagination' => array(
                    'pageSize' => Yii::app()->params['questionsPerPage'],
                ),
                'criteria' => $criteria,
            ));
        }//var_dump(Yii::app()->user->role);die();
        // Для данной страницы отключаем все аяксовые мониторы за чатом
        Yii::app()->clientScript->registerScript(__FILE__ . __METHOD__ . __LINE__, "var magistika_chat_config = {"
                . "watchDog: 0,"
                . "refreshHistory: 0,"
                . "checkNewChats: 0,"
                . "isOpponentTyping: 0,"
                . "}"
                , CClientScript::POS_BEGIN);
//        var_dump(Yii::app()->user->id);
        $this->render('index', array(
            'availableChats' => $availableChats,
            'statistics' => $statistics,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $criteria = new CDbCriteria(array(
            'condition' => 'status=' . Question::STATUS_PUBLISHED,
            'order' => 'update_time DESC',
        ));

        $dataProvider = new CActiveDataProvider('Question', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['questionsPerPage'],
//                'pageSize' => 5,
            ),
            'criteria' => $criteria,
        ));

//        $model = new Question('search');
//        $questions = Question::model()->findall($criteria);
//        $pages = new CPagination(count($questions));
//        $pages->pageSize = 5;
//        $pages->applyLimit($criteria);
        $this->render('admin', array(
//            'questions' => $questions,
//            'pages' => $pages,
            "dataProvider" => $dataProvider,
        ));
    }

    /**
     * Suggests tags based on the current user input.
     * This is called via AJAX when the user is entering the tags input.
     */
    public function actionSuggestTags() {
        if (isset($_GET['q']) && ($keyword = trim($_GET['q'])) !== '') {
            $tags = Tag::model()->suggestTags($keyword);
            if ($tags !== array())
                echo implode("\n", $tags);
        }
    }

    /**
     * Преобразует число в представление времени. Например - 11:52
     * @param float $time Время в минутах
     */
    public function prepareTime($time) {
        $strTime = "00:00:00";
        if ($time > 0) {
            // 1:23:52
            $h = $i = $s = 0;
            while ($time > 0) {
                if ($time >= 60) {
                    $h = (int) ($time / 60);
                    $time = $time - $h * 60;
                } else if ($time < 60) {
                    $i = (int) $time;
                    $time -= $time;
                    break;
                } elseif ($time < 1) {
                    // здесь можно рассчитывать секунды
                }
            }
            $strTime = $h <= 9 ? "0" . $h : $h; // часы
            $strTime .= $i <= 9 ? ":0" . $i : ":" . $i; // минуты
            $strTime .= $s <= 9 ? ":0" . $s : ":" . $s; // секунды
        }
        return $strTime;
    }

    /**
     * Списывает у пользователя потраченные деньги на чат
     * @param \Cha $chat
     */
    public function calculateBill(\Chat $chat) {
        try {
            $freeChatMinutes = (int) $chat->free_minutes;
            $user = $chat->user;
            //$expert = $chat->expert;
            $dtStart = new DateTime($chat->countdown_date);
            $dtStop = new DateTime($chat->end_date);
            $diff = $dtStop->diff($dtStart);
            $user->bill = (int) $user->bill - ((int) Chat::CHAT_COST * (((int) $diff->h * 60 + (int) $diff->i) - $freeChatMinutes));
            if ($user->save(FALSE, array('bill'))) {
                return TRUE;
            }
        } catch (Exception $ex) {
            
        }
        return FALSE;
    }

    public function actionReviewChatHistory($id) {
//        if(Yii::app()->request->isAjaxRequest){
        $id = (int) $id;
        $chat = Chat::model()->findByPk($id);
        if ($chat != NULL) {
            $this->chatId = (int) $id;
            if (Yii::app()->user->role == Users::ROLE_USER) {
                $this->opponent = $chat->expert;
            } else {
                $this->opponent = $chat->user;
            }
            // Сообщения
            $this->messages = ChatMessage::model()->with('chatAttachments', 'author')->findAll('chat_id=' . $this->chatId);
            $this->reviewHistory = TRUE;
            // Для данной страницы отключаем все аяксовые мониторы за чатом
            Yii::app()->clientScript->registerScript(__FILE__ . __METHOD__ . __LINE__, "var magistika_chat_config = {"
                    . "watchDog: 0,"
                    . "refreshHistory: 0,"
                    . "checkNewChats: 0,"
                    . "isOpponentTyping: 0,"
                    . "}"
                    , CClientScript::POS_BEGIN);
            $this->render('chatOnline');
        }
//        }
    }

    /**
     * Проверяет, запросил ли какой нибудь пользователь общение в чате с экспертом
     */
    public function actionNotification() {
        $answer = array();
        if (isset($_POST['uid']) && !empty($_POST['uid'])) {
            $expert_id = (int) $_POST['uid'];
            $newChat = Chat::model()->find(array(
                "condition" => "expert_id=" . $expert_id . " AND end_date IS NULL AND countdown_date IS NULL",
                "with" => array(
                    "user" => array(
                        "alias" => "u",
                        "select" => "u.name, u.surname"
                    )
                ),
                "limit" => 1
            ));
            if ($newChat !== null) {
                $answer['name'] = $newChat->user->name . " " . $newChat->user->surname;
                $answer["link"] = CHtml::link("Войти в чат", Yii::app()->createUrl("chat/Chat/confirmChat", array("id" => $newChat->id)), array("target" => "_blank"));
                $answer["link_close"] = CHtml::link("Отказаться", Yii::app()->createUrl("chat/finishChat", array("id" => $newChat->id)));
            } else {
                $answer['empty'] = 1;
            }
        } else {
            $answer['error'] = 1;
        }
        echo json_encode($answer);
        Yii::app()->end();
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel($id) {
        if ($this->_model === null) {
            if (isset($id)) {
                if (Yii::app()->user->isGuest)
                    $condition = 'status=' . Question::STATUS_PUBLISHED . ' OR status=' . Question::STATUS_ARCHIVED;
                else
                    $condition = '';
                $this->_model = Question::model()->findByPk($id, $condition);
            }
            if ($this->_model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $this->_model;
    }

}
