<?php

/**
 * This is the model class for table "chat_messages".
 *
 * The followings are the available columns in table 'chat_messages':
 * @property integer $id
 * @property integer $chat_id
 * @property string $message
 * @property string $send_time
 * @property integer $has_attach
 *
 * The followings are the available model relations:
 * @property ChatAttachments[] $chatAttachments
 * @property Chats $chat
 */
class ChatMessage extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ChatMessage the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'chat_messages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('chat_id, message, send_time', 'required'),
            array('chat_id, has_attach', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, chat_id, message, send_time, has_attach', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'chatAttachments' => array(self::HAS_MANY, 'ChatAttachment', 'chat_msg_id'),
            'chat' => array(self::BELONGS_TO, 'Chats', 'chat_id'),
            'author' => array(self::BELONGS_TO, 'Users', 'author_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'chat_id' => 'Chat',
            'message' => 'Message',
            'send_time' => 'Send Time',
            'has_attach' => 'Has Attach',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('chat_id', $this->chat_id);
        $criteria->compare('message', $this->message, true);
        $criteria->compare('send_time', $this->send_time, true);
        $criteria->compare('has_attach', $this->has_attach);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
