<?php

/**
 * This is the model class for table "chat_attachments".
 *
 * The followings are the available columns in table 'chat_attachments':
 * @property integer $id
 * @property integer $chat_msg_id
 * @property string $url
 *
 * The followings are the available model relations:
 * @property ChatMessages $chatMsg
 */
class ChatAttachment extends CActiveRecord
{
    const PATH_TO_UPLOAD_DIR = 'chats/';

    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ChatAttachment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chat_attachments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('chat_msg_id, url', 'required'),
			array('chat_msg_id', 'numerical', 'integerOnly'=>true),
			array('url', 'length', 'max'=>255),
                        array('url', 'file', 'types'=>'jpg, gif, png'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, chat_msg_id, url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'chatMsg' => array(self::BELONGS_TO, 'ChatMessages', 'chat_msg_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'chat_msg_id' => 'Chat Msg',
			'url' => 'Url',
		);
	}

//        public function afterSave() {
//		// сохраняем файл
//		$fileimage = CUploadedFile::getInstance($this, 'url');
//		if (!empty($fileimage))
//		{
//			$filename = $this->primaryKey . '.' . $fileimage->getExtensionName();
//
//			$folder = YiiBase::getPathOfAlias('webroot.files.user.image');
//			if (!is_dir($folder))
//			{
//				mkdir($folder, 0755, true);
//			}
//
//			if ($fileimage->saveAs('files/user/image/' . $filename))
//			{
//				self::updateByPk($this->primaryKey, array('image' => 'files/user/image/' . $filename));
//			}
//		}
//
//		return parent::afterSave();
//	}
        
        public function beforeSave() {
             $uploadedFile = CUploadedFile::getInstance($this, 'url');
                if ($uploadedFile !== NULL) {
//                    if (!$this->isNewRecord) {
//                        if (file_exists("/uploads/article/" . $this->image)) {
//                            unlink("/uploads/article/" . $this->image);
//                        }
//                    }
//                    $image = Yii::app()->simpleImage->load($uploadedFile->tempName);
//                    $image->resizeToWidth(410);
//                    $image->resizeToHeight(300);
                    $newFileName = uniqid() . '.' . pathinfo($uploadedFile->name, PATHINFO_EXTENSION);
                    $uploadedFile->saveAs(Yii::app()->params['uploadDir']. self::PATH_TO_UPLOAD_DIR . $newFileName);
                    $this->url = $newFileName;
                    return TRUE;
                }
                return FALSE;
        }

                /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('chat_msg_id',$this->chat_msg_id);
		$criteria->compare('url',$this->url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}