<?php

/**
 * This is the model class for table "chats".
 *
 * The followings are the available columns in table 'chats':
 * @property integer $id
 * @property integer $user_id
 * @property integer $expert_id
 * @property string $start_date
 * @property string $end_date
 *
 * The followings are the available model relations:
 * @property ChatMessages[] $chatMessages
 */
class Chat extends CActiveRecord {

    const DATE_TIME_FORMAT = "Y-m-d H:i:s";
    const MIN_CHAT_DURATION = 1;
    const CHAT_COST= 25;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Chat the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'chats';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, expert_id, start_date', 'required'),
            array('user_id, expert_id', 'numerical', 'integerOnly' => true),
            array('end_date', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, user_id, expert_id, start_date, end_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'chatMessages' => array(self::HAS_MANY, 'ChatMessage', 'chat_id'),
            'user' => array(self::HAS_ONE, 'Users', array('id' => 'user_id')),
            'expert' => array(self::HAS_ONE, 'Users', array('id' => 'expert_id')),
//            'lastMessageTime' => array(self::STAT, "ChatMessage", "chat_id", 'select' => "MAX(send_time)"),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'expert_id' => 'Expert',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('expert_id', $this->expert_id);
        $criteria->compare('start_date', $this->start_date, true);
        $criteria->compare('end_date', $this->end_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave() {
        if ($this->isNewRecord) {
            $dt = new DateTime();
            $this->start_date = $dt->format(Chat::DATE_TIME_FORMAT);
            return TRUE;
        }
        return TRUE;
    }

}
