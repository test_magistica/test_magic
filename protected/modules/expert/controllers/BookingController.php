<?php

class BookingController extends Controller {

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '/layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
		return array(
			array('allow', // allow all users to perform 'index' and 'view' actions
				'actions' => array('index', 'view', 'accept'),
				'users' => array('expert'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('create', 'update'),
				'users' => array('expert'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('admin', 'delete'),
				'users' => array('expert'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {

		Yii::app()->db->createCommand("UPDATE `servicemessage` SET `status` = '1' WHERE `userID` != '" . (int) Yii::app()->user->id . "' AND bookingID = '" . $id . "' AND status = 0")->query();
		$model = $this->loadModel($id);
		$mod = ucfirst($model->entity);
		$paymentService = $model->entity ? $mod::model()->findByPk($model->entity_id) : '';
		$message = new Servicemessage();
		if (isset($_POST['Servicemessage'])) {
			$message->message   = $_POST['Servicemessage']['message'];
			$message->fromID    = $model->userID;
			$message->toID      = Yii::app()->user->id;
			$message->userID    = Yii::app()->user->id;
			$message->createTime = new CDbExpression('NOW()');
			$message->status    = 0;
			$message->bookingID = $model->id;
			$message->answere   = $_POST['Servicemessage']['answere'];
			if ($message->save()) {

				if($model->entity){
					if(  $_POST['Servicemessage']['answere'] == 1 ) {


						$serviceParams = Yii::app()->params['paymentServices'];
						$serviceParams = $serviceParams[$model->entity];

						Yii::app()->db->createCommand("UPDATE booking SET status = '2' WHERE id={$id}")->query();
						Yii::app()->db->createCommand("UPDATE {$serviceParams['table']} SET status = 'ANSWER' WHERE id={$model->entity_id}")->query();

						$expImg = Controller::getUserImage($model->expertID);

						// доработать
						$service = Service::model()->findByPk(35);

						$params = array(
							'service'           => $service->notes,
							'expertAnswere'     => $message->message,
							'expertName'        => Controller::getUser($model->expertID, 'name'),
							'expertSurname'     => Controller::getUser($model->expertID, 'surname'),
							'expertProfession'  => Controller::getUser($model->expertID, 'profession'),
							'expertHref'        => "http://{$_SERVER['HTTP_HOST']}/extrasens/view/{$model->expertID}",
							'expertImage'       => "http://{$_SERVER['HTTP_HOST']}/uploads/user/{$expImg}",
						);

						$tp = new TemplateParams();
						$tp->setSubjectParams($params);
						$tp->setBodyParams($params);
						$mt = EmailTemplates::parseTemplate($serviceParams['keyComplete'], $tp);
						Controller::sendMailUser($paymentService->email, $mt->parsedSubject, $mt->parsedBody);
					}
				}

				if (isset($_FILES['files'])) {
					foreach ($_FILES['files']['name'] as $key => $filename):
						move_uploaded_file($_FILES['files']['tmp_name'][$key], Yii::app()->params['uploadDir'] . $filename);
						Yii::app()->db->createCommand("INSERT INTO `bookingfiles` (`id`, `bookingID`, `file`, `expertID`, `messageID`) VALUES (NULL, '$id', '$filename', '" . (int) Yii::app()->user->id . "', '{$message->id}')")->query();
					endforeach;
				}
				$this->redirect(array('admin'));
			}
		}

		$this->render(( $model->entity ? $model->entity : 'view'),
			array('model' => $model, 'message' => $message, 'paymentService' => $paymentService));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new Booking;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Booking'])) {
			$model->attributes = $_POST['Booking'];
			if ($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	public function actionAccept() {
		if (isset($_GET['action']) && isset($_GET['id']) && is_numeric($_GET['action'])) {
			Yii::app()->db->createCommand("UPDATE booking SET status='{$_GET['action']}' WHERE id={$_GET['id']} ")->query();
			$this->redirect(array('booking/view', 'id' => $_GET['id']));
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Booking'])) {
			$model->attributes = $_POST['Booking'];
			if ($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Booking');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin() {
		$model = new Booking('search');
		$bCount = $model->count("expertID = '" . Yii::app()->user->id . "' and status = 1");
		$model->unsetAttributes();  // clear any default values
		if (isset($_GET['Booking']))
			$model->attributes = $_GET['Booking'];

		$consultation = new Consultation('search');//Consultation::model()->findAll($criteria);
		$cCount = $consultation->count("expertID = '" . Yii::app()->user->id . "' and status = 1");
		$consultation->unsetAttributes();  // clear any default values
		if (isset($_GET['Consultation']))
			$consultation->attributes = $_GET['Consultation'];

		// консультации гостей
		//$criteria = new CDbCriteria();
		//$criteria->compare('userID',(int)0);
		//$criteria->compare('expertID', Yii::app()->user->id);
		//$criteria->order = "Field(`status`,'1','BREAK') desc,`date` desc";

		$consult_guest = new Consultation('search_guest');
		//$cons_guest = Consultation::model()->findAll($criteria);
		$dCount = $consult_guest->count("expertID = '" . Yii::app()->user->id . "' and status = 'paid'");
		//Consultation::model()->count($criteria);


		$this->render('admin', array(
			'model' => $model, 'consultation' => $consultation, 'bCount' => $bCount,
			'cCount' => $cCount,'consult_guest'=>$consult_guest , 'dCount' => $dCount
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Booking the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id) {
		$model = Booking::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Booking $model the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'booking-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}