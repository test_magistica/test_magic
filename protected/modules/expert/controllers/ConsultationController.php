<?php

class ConsultationController extends Controller {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    //public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'call', 'check'),
                'users' => array('expert'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('expert'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('expert'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionCall() {
        //$id = $_GET['userID'];
        
        //if ($id > 0 )
    	//    $user_info = Users::model()->findByPk($id);
    	//else 
    	//    $user_info = Guest::model()->findByPk($id);
        
        
        
        $expert_info = Users::model()->findByPk(Yii::app()->user->id);
        //$expert_id = $id;


        $maxdur = $_GET['maxdur'];
	$consult = Consultation::model()->findByPk($_GET['c']);
        //$tele = substr($user_info['tele'], 1);
        if ($maxdur > 0) 
        {
    	    $data = array(
                'user_id' => $expert_info->id,
                'cbk_id' => $consult->id, //$cbk_id,
                'from_num' => str_replace('+', '', $expert_info->tele),
                'to_num' => str_replace('+','',$consult->phone),//str_replace('+', '', $user_info->tele),
                'maxdur' => $maxdur//$consult->duration
            );
    	    //print_r ($data);
    	    $this->addCallBack($data);
            //Yii::app()->db->createCommand("UPDATE `users` SET `online` = 'busy' WHERE `id` = '" . (int) $expert_info->id . "'")->query();
            Yii::app()->db->createCommand("UPDATE `consultation` SET `status` = 'BEGIN' WHERE `id` = '" . (int) $consult->id . "'")->query();
            Yii::app()->user->setFlash('сonsultation', 'Ожидайте вызов! <br/> Если вызов не поступил в течении 5 минут обратитесь к администратору..');
            $this->redirect(array('/expert/booking/admin'));
            
        }
    }

    public function actionCheck() {
        if (isset($_POST['check'])):
            $c = CallBack::model()->find("cbk_id = '" . $_POST['check'] . "'");
            if ($c['check'] == 0) {
                $baseurl = "http://api.comtube.com/scripts/api/callback.php";
                $params = array();
                $params["username"] = "parissema";
                $params["action"] = "statistics";
                $params['uid'] = $_POST['check'];
                $params['type'] = 'xml';
                // Создаем подпись к параметрам
                $urlparams = $this->BuildUrlParamsWithSignature($params, "20071981");
                // Формируем полный URL для обращения к серверу
                $url = $baseurl . "?" . $urlparams;
                // Инициализируем curl и отправляем запрос
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $data = curl_exec($ch);
                curl_close($ch);
                // Анализируем результат
                $xml = simplexml_load_string($data);
                if ($xml->callback->calllegs[1]->call == "SUCCEEDED") {

                    $expert = Users::model()->find("`tele`='" . '+' . $c['to_num'] . "'");
                    $user = Users::model()->find("`tele`='" . '+' . $c['from_num'] . "'");

                    $consultation = Consultation::model()->findByPk(Yii::app()->session['cID']);
                    if ($consultation['duration'] > $xml->callback->calllegs[1]->duration/60) {
                        $bill = ($consultation['duration'] - $xml->callback->calllegs[1]->duration/60) * $expert['tariff'];
                        $bill = $user['bill'] + $bill;
                        Yii::app()->db->createCommand("UPDATE `call_back` SET `maxdur` = '".$xml->callback->calllegs[1]->duration."' WHERE `cbk_id` = '" . $_POST['check'] . "'")->query();
                        Yii::app()->db->createCommand("UPDATE `users` SET `bill` = '$bill' WHERE `id` = '" . (int) $user['id'] . "'")->query();
                    }
                    Yii::app()->db->createCommand("UPDATE `consultation` SET `status` = '2', duration = '".$xml->callback->calllegs[1]->duration."' WHERE `id` = " . Yii::app()->session['cID'])->query();
                    Yii::app()->db->createCommand("UPDATE `users` SET `online` = 'online' WHERE `id` = '" . (int) $expert['id'] . "'")->query();
                    Yii::app()->db->createCommand("UPDATE `call_back` SET `check` = '1' WHERE `cbk_id` = '" . $_POST['check'] . "'")->query();
                    unset(Yii::app()->session['cbk_id']);
                    unset(Yii::app()->session['cID']);

                    print_r($xml);
                }
            }

        endif;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Consultation;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Consultation'])) {
            $model->attributes = $_POST['Consultation'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Consultation'])) {
            $model->attributes = $_POST['Consultation'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Consultation');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Consultation('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Consultation']))
            $model->attributes = $_GET['Consultation'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Consultation the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Consultation::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Consultation $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'consultation-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
