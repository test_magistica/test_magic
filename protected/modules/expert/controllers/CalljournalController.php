<?php

class CalljournalController extends Controller {

	public function filters() {
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules() {
		return array(
			array('allow', // allow all users to perform 'index' and 'view' actions
				'actions' => array('view', 'index'),
				'users' => array('expert'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	public function actionView($id) {
		$model = $this->loadModel($id);
		$this->render('view', array(
			'model' => $model
		));
	}

	public function loadModel($id) {
		$model = CallBack::model()->findByPk($id);
		if ($model === null || $model->check == 0)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	public function actionIndex() {

		// Выбираем статистические данные звонков........................................................
		$criteria = new CDbCriteria();
		$criter = new CDbCriteria();

		$criteria->order = 'id desc';
		$criteria->select = array('*','DATE_FORMAT(`call_date`,\'%d.%m.%Y %H:%i:%s\') as call_date');

		$criter->select =  array('sum(`profit_expert`) as `profit_expert`',
			'SUM(CEILING(`duration` / 60)) as `duration`');

		$expert_id = Yii::app()->user->id;
		$criteria->addCondition('`caller_id`='.$expert_id.' OR `called_id`='.$expert_id);

		$criter->addCondition('`caller_id`='.$expert_id.' OR `called_id`='.$expert_id);

		if (isset($_GET['date'])) {
			if (preg_match("/[0-9]{4}-[0-9]{2}-[0-9]{2}/", $_GET['date']))
			{
				$ar = explode('-', $_GET['date']);
				//print_r($ar);
				//print($date);
				$criteria->compare('MONTH(`call_date`)',$ar[1]);
				// 'MONTH(STR_TO_DATE(\''.$date.'\',\'%y-%m-%d\'))');
				$criteria->compare('YEAR(`call_date`)', $ar[0]);

				$criter->compare('MONTH(`call_date`)',$ar[1]);
				$criter->compare('YEAR(`call_date`)', $ar[0]);
			}
		}

		$models = CallJournal::model()->findAll($criteria);

		$all_summ = CallJournal::model()->find($criter);
		//.............................................................................................


		// Выбираем статистические данные чатов........................................................
		$criteria2 = new CDbCriteria();
		$criteria2->with = "user";
		$criteria2->condition = "expert_id=" . Yii::app()->user->id . " AND countdown_date is not null and end_date IS NOT NULL";

		if (isset($_GET['date'])) {
			if (preg_match("/[0-9]{4}-[0-9]{2}-[0-9]{2}/", $_GET['date']))
			{
				$ar = explode('-', $_GET['date']);
				$criteria2->compare('MONTH(`countdown_date`)',$ar[1]);
				$criteria2->compare('YEAR(`countdown_date`)', $ar[0]);
			}
		}
		$criteria2->order = "countdown_date DESC";
		Yii::import('application.modules.chat.models.Chat');
		$statistics = Chat::model()->findAll($criteria2);
		//.............................................................................................

		// Выбираем статистические данные услуг........................................................
		$criteria3 = new CDbCriteria();
		//$criteria3->with = "service";
		$criteria3->condition = "expertID='".Yii::app()->user->id."' AND status=".Booking::STATUS_DONE;

		if (isset($_GET['date'])) {
			if (preg_match("/[0-9]{4}-[0-9]{2}-[0-9]{2}/", $_GET['date']))
			{
				$ar = explode('-', $_GET['date']);
				$criteria3->compare('MONTH(`createTime`)',$ar[1]);
				$criteria3->compare('YEAR(`createTime`)', $ar[0]);
			}
		}
		$criteria3->order = "createTime DESC";
		$stat_service = Booking::model()->findAll($criteria3);
		//.............................................................................................

		$this->render('index', array(
			'all_summ'  => $all_summ,
			'models'    => $models,
			'statis_chat' => $statistics,
			'statis_serv' => $stat_service,
		));
	}

}