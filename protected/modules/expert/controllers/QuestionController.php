<?php

class QuestionController extends Controller {
//	public $layout='//layouts/column2';

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;

    /**
     * @return array action filters
     */
    public function _filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * You should override this method to provide stronger access control 
     * to specifc restfull actions via AJAX
     */
    public function validateAjaxUser($action) {
        return true;
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function _accessRules() {
        return array(
//            array('allow', // allow all users to access 'index' and 'view' actions.
//                'actions' => array('index', 'view'),
//                'users' => array('*'),
//            ),
//            array('allow', // allow authenticated users to access all actions
//                'actions' => array('create'),
//                'users' => array('@'),
//            ),
            array('allow', // allow authenticated users to access all actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * @return array action filters
     *
      public function filters()
      {
      return array(
      'accessControl', // perform access control for CRUD operations
      );
      }

      /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     *
      public function accessRules()
      {
      return array(
      array('allow',  // allow all users to access 'index' and 'view' actions.
      'actions'=>array('index','view'),
      'users'=>array('*'),
      ),
      array('allow', // allow authenticated users to access all actions
      'users'=>array('@'),
      ),
      array('deny',  // deny all users
      'users'=>array('*'),
      ),
      );
      }

     */

    /**
     * Displays a particular model.
     */
    public function actionView($id) {
        $this->layout = 'colquest2';

//		$question = $this->loadModel($id);
        $question = Question::model()->with('author', 'аrea_expertise', 'answers', 'answerCount')->find("t.id=" . $id);
//                var_dump($question->аrea_expertise);die();
        $user = Users::model()->findByPk($question->author_id);
//		$answer=$this->newAnswer($question);

        $this->render('view', array(
            'question' => $question,
                /* 'answer'=>$answer, */
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $this->layout = 'colquest2';
        //Отсекаем незарегистрированных пользователей
        if (Yii::app()->user->isGuest) {
            Yii::app()->user->returnUrl = Yii::app()->request->requestUri;
            Yii::app()->request->redirect(Yii::app()->createAbsoluteUrl("site/login"));
        }
        $model = new Question;
//                var_dump(Yii::app()->session->isStarted);die();
        // st - шаг создания фопроса
        //st = 1  - заполняется форма
        //st = 2  - выбирается форма размещения вопроса (платно | бесплатно)
        $step = isset($_GET['st']) ? $_GET['st'] : NULL;

        if ($step !== NULL) {
            if ($step == "1") {
                $model->attributes = $_POST['Question'];
            } else if ($step == "2") {
                // Метод размещения вопроса
                $methodOfPlacingTheQuestion = isset($_POST['Question'] ["pay"]) ? $_POST['Question'] ["pay"] : FALSE;
//                        var_dump( (CUploadedFile::getInstance($model,'image')));die();
                if ($methodOfPlacingTheQuestion === FALSE && isset($_POST['Question'])) {
                    $model->attributes = $_POST['Question'];
                    $model->image = CUploadedFile::getInstance($model, 'image');
                    if ($model->save()) {
                        if ($model->image !== NULL)
                            $model->image->saveAs(Yii::app()->params['uploadDir'] . "question_img/" . $model->image->getName());
                        // Сохраняем первичный ключ вопроса для дальнейших шагов
                        $id = $model->id;
                        Yii::app()->session->writeSession("qid", $id);

                        // $this->redirect(array('view','id'=>$model->id));
                    }
                }
                elseif (in_array($methodOfPlacingTheQuestion, array("0", "1"))) {
                    switch ($methodOfPlacingTheQuestion) {
                        // Разместить вопрос бесплатно
                        case "0":
                            $model = $this->placeTheQuestionAsPaid();
                            break;
                        // Разместить вопрос платно
                        case "1":
                            $model = $this->placeTheQuestionAsFree();
                            break;
                    }
                    $this->redirect(Yii::app()->createAbsoluteUrl("question/view", array('id' => $model->id)));
                }
            }
        }

        $this->render('create', array(
            'model' => $model,
            'step' => $step,
        ));

        /*        $booking = new Booking;
          $servive = Service::model()->findByPk($id);
          $message = new Servicemessage();
          if (isset($_POST['Servicemessage'])) {
          $modelS = $this->loadModel($id);
          if ($this->checkUserBill(Yii::app()->user->id, $modelS->honorarium)) {
          $booking->userID = Yii::app()->user->id;
          $booking->expertID = $servive->expertID;
          $booking->serviceID = (int) $id;
          $booking->type = 'Услуга';
          $booking->createTime = new CDbExpression('NOW()');
          if ($booking->save(false)) {
          $message->message = $_POST['Servicemessage']['message'];
          $message->fromID = Yii::app()->user->id;
          $message->toID = $servive->expertID;
          $message->userID = Yii::app()->user->id;
          $message->createTime = new CDbExpression('NOW()');
          $message->status = 0;
          $message->bookingID = $booking->id;
          if ($message->save()) {
          $this->takeUserBill(Yii::app()->user->id, $modelS->honorarium);

          $msg = '<p style="margin-bottom: 0.14in;">
          Здравствуйте, ' . $this->getUser(Yii::app()->user->id, 'name') . ' ' . $this->getUser(Yii::app()->user->id, 'surname') . '!</p>
          <p style="margin-bottom: 0.14in;">
          Вы заказали услугу &laquo;' . $servive->notes . '&raquo;</p>
          <p style="margin-bottom: 0.14in;">
          Я приступлю к составлению вашего предсказания в самое ближайшее время.</p>
          <p style="margin-bottom: 0.14in;">
          Как только у меня будет ответ для Вас,</p>
          <p style="margin-bottom: 0.14in;">
          Уведомление о готовности заказа вы получите на на этот адрес электронной почты, который вы указали.</p>
          <p style="margin-bottom: 0.14in;">
          Всех Вам благ,</p>
          <p style="margin-bottom: 0.14in;">
          ' . $this->getUser($servive->expertID, 'profession') . ' ' . $this->getUser($servive->expertID, 'name') . '</p>
          ';
          Yii::import('application.extensions.phpmailer.JPhpMailer');
          $mail = new JPhpMailer();
          $mail->SetFrom('no-reply@magistika.com', 'magistika.com');
          $mail->Subject = 'Вы заказали услугу ' . $servive->notes . '';
          $mail->AltBody = '';
          $mail->CharSet = 'UTF-8';
          $mail->MsgHTML($msg);
          $mail->AddAddress($this->getUser(Yii::app()->user->id, 'email'), $this->getUser(Yii::app()->user->id, 'name'));
          $mail->Send();

          $this->sendMailUser($servive->expertID, 'Вам заказали услугу', 'Вам заказали услугу');

          Yii::app()->user->setFlash('booking', 'Ваш заказ принят.');
          $this->redirect(array('/service/booking/', 'id' => $id));
          }
          }
          } else {
          $this->redirect(array('/payment'));
          }
          }
          $model = new XUploadForm;


          $this->render('booking', array('model' => $model, 'booking' => $booking, 'id' => $id, 'servive' => $servive, 'message' => $message)); */
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpdate() {
        $model = $this->loadModel();
        if (isset($_POST['Question'])) {
            $model->attributes = $_POST['Question'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     */
    public function actionDelete() {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel()->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(array('index'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $criteria = new CDbCriteria(array(
            'condition' => 'status=' . Question::STATUS_PUBLISHED,
            'order' => 'update_time DESC',
            'with' => 'answerCount',
        ));
        if (isset($_GET['tag']))
            $criteria->addSearchCondition('tags', $_GET['tag']);

        $dataProvider = new CActiveDataProvider('Question', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['questionsPerPage'],
            ),
            'criteria' => $criteria,
        ));


        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'model' => Question::model(),
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $criteria = new CDbCriteria(array(
            'condition' => 'status=' . Question::STATUS_PUBLISHED,
            'order' => 'update_time DESC',
        ));
        
        $dataProvider = new CActiveDataProvider('Question', array(
            'pagination' => array(
                'pageSize' => Yii::app()->params['questionsPerPage'],
//                'pageSize' => 5,
            ),
            'criteria' => $criteria,
        ));
        
//        $model = new Question('search');
//        $questions = Question::model()->findall($criteria);
//        $pages = new CPagination(count($questions));
//        $pages->pageSize = 5;
//        $pages->applyLimit($criteria);
        $this->render('admin', array(
//            'questions' => $questions,
//            'pages' => $pages,
            "dataProvider" => $dataProvider,
        ));
    }

    /**
     * Suggests tags based on the current user input.
     * This is called via AJAX when the user is entering the tags input.
     */
    public function actionSuggestTags() {
        if (isset($_GET['q']) && ($keyword = trim($_GET['q'])) !== '') {
            $tags = Tag::model()->suggestTags($keyword);
            if ($tags !== array())
                echo implode("\n", $tags);
        }
    }

    public function actionStepOne() {
        $this->render('_stepOne', array(
            'model' => new Question(),
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel($id) {
        if ($this->_model === null) {
            if (isset($id)) {
                if (Yii::app()->user->isGuest)
                    $condition = 'status=' . Question::STATUS_PUBLISHED . ' OR status=' . Question::STATUS_ARCHIVED;
                else
                    $condition = '';
                $this->_model = Question::model()->findByPk($id, $condition);
            }
            if ($this->_model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $this->_model;
    }

    /**
     * Creates a new answer.
     * This method attempts to create a new answer based on the user input.
     * If the answer is successfully created, the browser will be redirected
     * to show the created answer.
     * @param Question the question that the new answer belongs to
     * @return Answer the answer instance
     */
    protected function newAnswer($question) {
        $answer = new Answer;
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'answer-form') {
            echo CActiveForm::validate($answer);
            Yii::app()->end();
        }
        if (isset($_POST['Answer'])) {
            $answer->attributes = $_POST['Answer'];
            if ($question->addAnswer($answer)) {
                if ($answer->status == Answer::STATUS_PENDING)
                    Yii::app()->user->setFlash('answerSubmitted', 'Thank you for your answer. Your answer will be questioned once it is approved.');
                $this->refresh();
            }
        }
        return $answer;
    }

    private function placeTheQuestionAsFree() {
        // 1. Отправить на модерацию !!!
        // 2. Опубликовать (статус Question::STATUS_PUBLISHED)
        // 3. Разослать экспертам (через 20 часов)
        $qid = Yii::app()->session->readSession("qid");
        $model = Question::model()->findByPk($qid);
        $model->status = Question::STATUS_PUBLISHED;
        $model->update(array('status'));
        Yii::app()->session->destroySession("qid");
        return $model;
    }

    private function placeTheQuestionAsPaid() {
        $qid = Yii::app()->session->readSession("qid");
        $model = Question::model()->findByPk($qid);
        $model->status = Question::STATUS_PUBLISHED;
        $model->pay = 1;
        $model->update(array('status', 'pay'));
        Yii::app()->session->destroySession("qid");
        return $model;
    }

}
