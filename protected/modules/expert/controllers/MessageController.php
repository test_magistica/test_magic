<?php

class MessageController extends Controller {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    //public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl - check', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update','check'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('expert'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $model = new MessageEx();
        $this->performAjaxValidation2($model);
        if (isset($_POST['MessageEx'])) {
            $model->attributes = $_POST['MessageEx'];
            if ($model->save())
                $this->refresh();
        }
        $criteria = new CDbCriteria();
        $criteria->order = 'id desc';
        $criteria->compare('toID', (int) Yii::app()->user->id);
        $criteria->compare('fromID', (int) $id);
        $count = Message::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 15;
        $pages->applyLimit($criteria);
        $models = Message::model()->findAll($criteria);
        Yii::app()->db->createCommand("UPDATE `message` SET `status` = '1' WHERE `fromID` ='" . (int) $id . "' AND `toID` ='" . (int) Yii::app()->user->id . "' AND `userID` = '" . (int) $id . "'")->query();
        $this->render('view', array(
            'models' => $models, 'pages' => $pages, 'model' => $model, 'count' => $count
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Message;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Message'])) {
            $model->attributes = $_POST['Message'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Message'])) {
            $model->attributes = $_POST['Message'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
//        $dataProvider = new CActiveDataProvider('Message');
//        $this->render('index', array(
//            'dataProvider' => $dataProvider,
//        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $criteria = new CDbCriteria();
        $criteria->order = 'createTime desc';
        $criteria->group = 'fromID';
        $criteria->compare('toID', Yii::app()->user->id);
        $count = Message::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 15;
        $pages->applyLimit($criteria);
        $models = Message::model()->findAll($criteria);

        $this->render('admin', array(
            'models' => $models, 'pages' => $pages
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Message the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Message::model()->findByPk($id);
       // if ($model === null)
            //throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation2($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'register-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Performs the AJAX validation.
     * @param Message $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'message-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionCheck() {
        if (isset($_POST['check'])) {
            $rows = Yii::app()->db->createCommand("SELECT id FROM `message`  WHERE `toID` = '" . (int) Yii::app()->user->id . "' AND `userID` != '" . (int) Yii::app()->user->id . "' AND status = 0")->queryAll();
            echo count($rows);
        }

    }

    public function actionAjaxCheck() {
        if (isset($_POST['check'])) {
            $rows = Yii::app()->db->createCommand("SELECT id FROM `message`  WHERE `toID` = '" . (int) Yii::app()->user->id . "' AND `userID` != '" . (int) Yii::app()->user->id . "' AND status = 0")->queryAll();
            echo count($rows);
            Yii::app()->end();
        }

    }
}
