<?php

class StatisticController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations            
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('view', 'index'),
                'users' => array('expert'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id) {
        $model = $this->loadModel($id);
        $this->render('view', array(
            'model' => $model
        ));
    }

    public function loadModel($id) {
        $model = CallBack::model()->findByPk($id);
        if ($model === null || $model->check == 0)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionIndex() {

        $criteria = new CDbCriteria();
        $criteria->order = 'id desc';
        $criteria->compare('`check`', 1);
        $criteria->compare('to_num', str_replace('+', '', $this->getUser(Yii::app()->user->id, 'tele')));
        if (isset($_GET['date'])) {
            if (preg_match("/[0-9]{4}-[0-9]{2}-[0-9]{2}/", $_GET['date'])) {
                $ar = explode('-', $_GET['date']);
                $criteria->addCondition("YEAR(`date`) = ".(int)$ar[0]." AND MONTH(`date`) = ".(int)$ar[1]);
            }
        }
        $count = CallBack::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = 15;
        $pages->applyLimit($criteria);
        $models = CallBack::model()->findAll($criteria);

        $this->render('index', array('models' => $models,
            'pages' => $pages));
    }

}