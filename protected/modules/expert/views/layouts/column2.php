<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//../modules/expert/views/layouts/main'); ?>
<?php $c = Controller::getMessageCount() > 0 ? '(' . Controller::getMessageCount() . ')' : ''; ?>
<?php $b = Controller::getBookingCount() > 0 ? '(' . Controller::getBookingCount() . ')' : ''; ?>


<!-- Sidebar здесь начинаеться column2 -->
<section class="sRight">
    <?php echo $content; ?>
</section>



<aside>
    <div class="profile_wrap">

        <div class="user_status">
            <?php $photo = Userphoto::model()->find("`userID` = '" . Yii::app()->user->id . "' AND `main`=1"); ?>
                <img src="<?php echo Yii::app()->request->baseUrl.'/uploads/user/Thumb/'.$photo['image']; ?>" alt="" width="50px" height="50px" />
            <h1><?php echo Controller::getUser(Yii::app()->user->id, 'name'); ?> <?php echo Controller::getUser(Yii::app()->user->id, 'surname'); ?></h1>
            <span class="<?php echo Controller::getUser(Yii::app()->user->id, 'online'); ?>"><?php echo Yii::app()->params['status'][Controller::getUser(Yii::app()->user->id, 'online')]; ?></span> 
            <!--   you may change status class: online, busy, autonomous   -->
            <?php if(Yii::app()->user->role == Users::ROLE_EXPERT): ?>
            <div class="expert-chat-available">
            <?php echo CHtml::activeCheckBox(Yii::app()->user->getModel(), "chat_available", array('class' => "expert-chat-available-chb"));?>
            <?php echo CHtml::activeLabel(Yii::app()->user->getModel(), "chat_available") ;?>
            <?php 
            Yii::app()->clientScript->registerScript(
                __FILE__.__LINE__,
                "$('#Users_chat_available').on('change', function(){ "
                . " var chaton = 0;"
                . " if(typeof $(this).attr('checked') != 'undefined'){"
                . "  chaton = 1;"
                . " }"
                . " $.ajax({"
                . "     type: 'POST',"
                . "     url: '/user/updateSettings',"
                . "     data: ({uid: ".Yii::app()->user->id.", attr: 'chat_available', v: chaton})"
                . "     });"
                . "});"
                . "");?>
            </div>
        <?php endif; ?>
        </div>
        <?php
	    $t = Controller::getBookingCountExpert() + Controller::getBookingCount();
	    $b = $t > 0 ? '(' . $t . ')' : '';
	    ?>
        <?php
        $this->widget('zii.widgets.CMenu', array(
            'htmlOptions' => array('class' => 'profile_list'),
            'items' => array(
                array('label' => 'Мои статьи<i class="i_post"></i>', 'url' => array('/expert/article/admin'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Мои сообщения<i class="i_msg"></i> <span id="shoutbox"></span></span>', 'url' => array('/expert/message/admin'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Мои услуги<i class="i_service"></i> ', 'url' => array('/expert/service/admin'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Мои заказы<i class="i_order"></i> ' . $b . '', 'url' => array('/expert/booking/admin'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Мой профайл<i class="i_profile"></i>', 'url' => array('//user/update/', 'id' => Yii::app()->user->id), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Сменить пароль<i class="i_lock"></i>', 'url' => array('//user/password/'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Мои фотографии<i class="i_gallery"></i>', 'url' => array('//user/photo/', 'id' => Yii::app()->user->id), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'График работы<i class="i_schedule"></i>', 'url' => array('/expert/schedule/admin'), 'visible' => !Yii::app()->user->isGuest),
                //array('label' => 'Статистика<i class="i_rates"></i>', 'url' => array('/expert/statistic/'), 'visible' => !Yii::app()->user->isGuest),
                //array('label' => 'Заказ консультации<i class="i_rates"></i>', 'url' => array('/expert/consultation/admin'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Статистика звонков, чатов и услуг<i class=""></i>', 'url' => array('/expert/calljournal/'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Мои чаты<i class="i_msg"></i> <span id="shoutbox"></span></span>', 'url' => array('/chat'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Вопросы<i class="i_msg"></i> <span id="shoutbox"></span></span>', 'url' => array('/expert/question/admin'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => '<span class="exit-but">Выход (' . Yii::app()->user->name . ')</span>', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
            ), 'encodeLabel' => false,
                )
        );
        ?>     

        <div class="clr"></div>
    </div>
</aside>
<!-- #Sidebar -->


<!--<div id="sidebar">
<?php
//    $this->beginWidget('zii.widgets.CPortlet', array(
//        'title' => 'Operations',
//    ));
//    $this->widget('zii.widgets.CMenu', array(
//        'items' => $this->menu,
//        'htmlOptions' => array('class' => 'operations'),
//    ));
//    $this->endWidget();
?>
</div>-->


<!-- sidebar -->


<?php $this->endContent(); ?>
