<?php
/* @var $this ServiceController */
/* @var $model Service */

$this->breadcrumbs=array(
	'Услуги'=>array('admin'),
	'Добавление из списка',
);

$this->menu=array(
	//array('label'=>'List Service', 'url'=>array('index')),
	array('label'=>'Управление услугой', 'url'=>array('admin')),
);
?>

<h1>Добавление статической услуги</h1>
<br>

<div class="form">

	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'serviceStatic-form',
		'enableAjaxValidation' => false,
	));
	?>

<?
	$list = CHtml::listData($model, 'id', 'notes');

	$i = 0;
	foreach($expertSS as $row){
		if($i==0)
			echo "Установленные статические услуги:<br>";
		echo "<i>".Controller::getServiceName($row->serviceID)."</i> <a href=\"/expert/service/removeStatic?id={$row->serviceID}\" title=\"удалить из личного списка\">х</a><br>";
			$i++;
	}

	echo "	<br>
	<br>
	<b>Выберите услугу:</b><br>".CHtml::dropDownList('staticServices', '', $list, array('empty' => '--'));

?>
	<script>
		$(document).ready(function(){
			$('#staticServices').on('change', function(){
				if(this.value){
					$.ajax({type: 'GET',
						url: '/expert/service/viewAjax/id/'+this.value,
						dataType : "html",
						success: function(data){
							$('.container_detail').html(data);
						}
					});
				}
				else{$('.container_detail').html("")}
			})
		})
	</script>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Добавить в личный список услуг', array('class' => 'btn btn-primary')); ?>
	</div>

    <?php $this->endWidget(); ?>
	<br>
	<div class="container_detail">
	</div>
