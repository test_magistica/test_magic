<?php
/* @var $this ServiceController */
/* @var $model Service */

$this->breadcrumbs=array(
	'Услуги'=>array('admin'),
	'Добавление новой услуги',
);

$this->menu=array(
	//array('label'=>'List Service', 'url'=>array('index')),
	array('label'=>'Управление услугой', 'url'=>array('admin')),
);
?>

<h1>Добавление новой услуги</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>