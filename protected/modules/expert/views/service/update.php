<?php
/* @var $this ServiceController */
/* @var $model Service */

//$this->breadcrumbs=array(
//	'Services'=>array('index'),
//	$model->id=>array('view','id'=>$model->id),
//	'Update',
//);

$this->menu=array(
	//array('label'=>'List Service', 'url'=>array('index')),
	//array('label'=>'Create Service', 'url'=>array('create')),
	array('label'=>'Редактировать услугу', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete Service', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Управление услугой', 'url'=>array('admin')),
);
?>

<h1>Редактировать услугу <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>