<?php
/* @var $this ServiceController */
/* @var $model Service */

//$this->breadcrumbs = array(
//    'Услуги' => array('admin'),
//    'Управление услугой',
//);

$this->menu = array(
    //array('label'=>'List Service', 'url'=>array('index')),
    array('label' => 'Добавление новой услуги', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#service-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="usercab_page">
    <div class="head">
        <h1>Список моих услуг</h1>
	    <p class="cmnt_lnk">
		    <?php echo CHtml::link('Добавление новой услуги', array('create'),
		    array('class' => 'btn btn-primary right-my-button')); ?>
	    </p>
    </div>

	<br>
    <?php // echo CHtml::link('Расширенный поиск', '#', array('class' => 'search-button')); ?>
    <div class="search-form" style="display:none">
        <?php
        $this->renderPartial('_search', array(
            'model' => $model,
        ));
        ?>
    </div><!-- search-form -->

    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'service-grid',
        'dataProvider' => $model->expert(),
        'filter' => $model,
        'cssFile' => Yii::app()->baseUrl . '/css/CGridView.css',
        'columns' => array(
            array(
                'name' => 'id',
                'header' => '№',
                'value' => '$data->id',
                'htmlOptions' => array('style' => 'width: 10px; text-align: center;'),
            ),
            'notes',
             array(
                'name' => 'honorarium',
                'header' => 'Цена',
                'value' => '$data->honorarium',
                'htmlOptions' => array('style' => 'width: 10px; text-align: center;'),
            ),
            'period',
            'volume',
            array(
                'name' => 'status',
                'header' => 'Статус',
                'filter' => array('1' => 'Принят', '0' => 'На модерации'),
                'value' => '($data->status=="1")?("Принят"):("На модерации")'
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '{view}{update}',
                'visible' => 'none',
            ),
        ),
    ));
    ?>
	<br>
	<br>
	<br>
	<div class="head">
		<h1>Установленные статические услуги</h1>
		<p class="cmnt_lnk">
			<?php echo CHtml::link('Добавить услугу из списка', array('createStatic'),
			array('class' => 'btn btn-primary right-my-button')); ?>
		</p>
	</div>
	<?
	$this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'staticService-grid',
		'dataProvider' => $model->staticService(),
		'columns' => array(
			array(
				'name' => 'id',
				'header' => '№',
				'value' => '$data->id',
				'htmlOptions' => array('style' => 'width: 10px; text-align: center;'),
			),
			'notes',
			array(
				'name' => 'honorarium',
				'header' => 'Цена',
				'value' => '$data->honorarium',
				'htmlOptions' => array('style' => 'width: 10px; text-align: center;'),
			),
			'period',
			'volume',
			array(
				'class' => 'CButtonColumn',
				'template' => '{view}',
				'visible' => 'none',
			),
		),
	));
	?>
</div>