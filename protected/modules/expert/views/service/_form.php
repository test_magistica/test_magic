<?php
/* @var $this ServiceController */
/* @var $model Service */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'service-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <p class="note">Поля, отмеченные  <span class="required">*</span> обязательны для заполнения.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'categoryID'); ?><br>
        <?php echo $form->dropDownList($model, 'categoryID',  Category::model()->getDropdownItems(), array('encode'=>false)); ?>
        <?php echo $form->error($model, 'categoryID'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'notes'); ?><br>
        <?php echo $form->textField($model, 'notes', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'notes'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'honorarium'); ?><br>
        <?php echo $form->textField($model, 'honorarium', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'honorarium'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'period'); ?><br>
        <?php echo $form->textField($model, 'period', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'period'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'volume'); ?><br>
        <?php echo $form->textField($model, 'volume', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'volume'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'annotation'); ?><br>
        <?php echo $form->textArea($model, 'annotation', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'annotation'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'description'); ?><br>
        <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>

    <script type="text/javascript">
        var Test = CKEDITOR.replace('Service[description]');
        AjexFileManager.init({returnTo: 'ckeditor', editor: Test});
    </script>

    <div class="row">
        <?php echo $form->labelEx($model, 'requirements'); ?><br>
        <?php echo $form->textArea($model, 'requirements', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'requirements'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'image'); ?><br>
        <?php echo $form->fileField($model, 'image'); ?>
        <?php echo $form->error($model, 'image'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'metaTitle'); ?><br>
		<?php echo $form->textField($model,'metaTitle',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'metaTitle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'metaKey'); ?><br>
		<?php echo $form->textArea($model,'metaKey',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'metaKey'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'metaDesc'); ?><br>
		<?php echo $form->textArea($model,'metaDesc',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'metaDesc'); ?>
	</div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать услугу' : 'Сохранить', array('class' => 'btn btn-primary')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->