<?php
/* @var $this ServiceController */
/* @var $model Service */

//$this->breadcrumbs=array(
//	'Services'=>array('admin'),
//	$model->id,
//);

$this->menu = array(
    //array('label'=>'List Service', 'url'=>array('index')),
    //array('label'=>'Create Service', 'url'=>array('create')),
    array('label' => 'Редактировать услугу', 'url' => array('update', 'id' => $model->id)),
    //array('label'=>'Delete Service', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label' => 'Управление услугой', 'url' => array('admin')),
);
?>
<div class="usercab_page">
    <div class="head">
        <h1>Просмотр услуги №<?php echo $model->id; ?></h1>
    </div>

    <?php
    $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            'id',
            'notes',
            'honorarium',
            'period',
            'volume',
            'annotation',
            'description:html',
            'requirements',            
            array(
            'name' => 'expertID',
            'header' => 'Пользователь',
            'type' => 'html',
            'value' => Controller::getUserName($model->expertID),
            'htmlOptions' => array('style' => 'width: 100px;'),
        ),
        ),
    ));
    ?>

</div>
