<div class="usercab_page">
    <div class="head">
        <h1>МОИ СООБЩЕНИЯ</h1>
    </div>


    <!-- Мои сообщения -->
    <div class="my_msg_list block my_user_msg">
        <p class="cmnt_lnk"><?php // echo CHtml::link('Написать письмо другому пользователю',array('message/create'), array('class'=>'btn btn-primary'));           ?></p>
        <div class="commentform">
            <div class="commentstat">Количество сообщений: <?php echo $count; ?> </div>
            <div class="chatformbox chatformbox2">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'register-form',
                    //'enableClientValidation' => TRUE,
                    'htmlOptions' => array('class' => 'searchbox'),
                    'enableAjaxValidation' => TRUE,
                    'clientOptions' => array(
                        'validateOnSubmit' => TRUE,
                    ),
                ));
                ?> 
                <div style="margin-left: 95px;">
                    <?php echo $form->errorSummary($model); ?>
                </div>

                <div class="field" style="display: none;">
                    <?php echo $form->error($model, 'toID'); ?> 
                    <?php echo $form->error($model, 'topic'); ?>
                    <?php echo $form->error($model, 'message'); ?>
                </div>
                <p>Написать ответ:</p>
                <?php echo $form->textArea($model, 'message', array('class' => 'border5')); ?>                    
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Отправить' : 'Сохранить', array('class' => 'submit')); ?>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
    <div class="my_msg_list2">

        <?php foreach ($models as $model): ?>
            <div class="sh-block mymess2">
                <?php $image = $this->getUserImage($model['userID']); ?>
                <img width="82" height="75" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $image; ?>" alt="" />
                <div class="user_msg">
                    <p class="lnk-name">
                        <a href="#"><?php echo $model['userID'] == Yii::app()->user->id ? 'Вы' : $this->getUser($model->userID, 'name') . '&nbsp;' . $this->getUser($model->userID, 'surname') ?></a>
                        <span class="date-kabexp"><?php echo date('d.m.Y', strtotime($model->createTime)); ?></span>
                    </p>
                    <p class="msg-txt">                       
                        <?php
                        if (!strlen($model['topic']) == 0) {
                            echo "<b>Тема сообщений:</b> ";
                            echo $model['topic'];
                            echo "<br>";
                        }
                        ?>
                        <?php echo $model['message']; ?>
                    </p>
                </div>
            </div> 
        <?php endforeach; ?>  
        <div class="pager">
            <?php
            $this->widget('CLinkPager', array(
                'pages' => $pages,
                'header' => '',
            ));
            ?>
        </div>
    </div>
    <!-- #Мои сообщения -->
</div><!-- #page -->