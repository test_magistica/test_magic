<?php
/* @var $this MessageController */
/* @var $model Message */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'message-form',
	'enableAjaxValidation'=>false,
)); ?>

	

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fromID'); ?><br>
		<?php echo $form->textField($model,'fromID'); ?>
		<?php echo $form->error($model,'fromID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'toID'); ?><br>
		<?php echo $form->textField($model,'toID'); ?>
		<?php echo $form->error($model,'toID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'topic'); ?><br>
		<?php echo $form->textField($model,'topic',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'topic'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'message'); ?><br>
		<?php echo $form->textArea($model,'message',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'message'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?><br>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'createTime'); ?><br>
		<?php echo $form->textField($model,'createTime'); ?>
		<?php echo $form->error($model,'createTime'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Написать сообщение' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->