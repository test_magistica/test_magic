<div class="usercab_page">
    <div class="head">
        <h1>Мои сообщения</h1>
        <!--        <a href="#" class="border10 all">Сменить пароль</a>-->
    </div>
    <br />
    <br />
    <br />
    <table class="extra-mail-table"><tr><th>Отправитель</th><th class="second">Тема сообщения</th><th class="last">Дата</th></tr>
        <!-- Мои сообщения -->
        <?php foreach ($models as $model): ?>
            <?php
            $criteria = new CDbCriteria();
            $criteria->order = 'id desc';
            $criteria->compare('fromID', (int) $model->fromID);
            $criteria->compare('toID', (int) Yii::app()->user->id);
            $criteria->compare('userID', (int) $model->fromID);
            $criteria->compare('status', 0);
            $cnt = Message::model()->count($criteria);
            ?>
            <tr>
                <?php if ($cnt > 0): ?>
                    <td>  
<!--                        <img src="images/avatar.jpg" alt="" class="extra-avatar">-->
                        (<?php echo $cnt; ?>) <?php echo CHtml::link($this->getUser($model->fromID, 'name') . '&nbsp;' . $this->getUser($model->fromID, 'surname'), array('message/view/', 'id' => $model->fromID)); ?>
                    </td>
                <?php else: ?>
                    <td>  
                        <?php echo CHtml::link($this->getUser($model->fromID, 'name') . '&nbsp;' . $this->getUser($model->fromID, 'surname'), array('message/view/', 'id' => $model->fromID)); ?>
                    </td>
                <?php endif; ?>
                <td>
                    <?php echo strlen($this->getLastTopicService($model->fromID)) >= 50 ? mb_substr($this->getLastTopicService($model->fromID), 0, 50, 'UTF-8') . '...' : $this->getLastTopicService($model->fromID); ?>
                </td>
                <td class="last">
                    <?php echo date('d.m.Y', strtotime($model->createTime)); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <!-- Мои сообщения -->

    </table>

    <div class="pagination">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $pages,
            'header' => '',
        ));
        ?>
    </div>
</div><!-- #page -->