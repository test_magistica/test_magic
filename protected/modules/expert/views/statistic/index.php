<div class="cab_exp">

    <h1 class="z-title">Статистика</h1>

    <p>
        Здесь представлена статистика по всем вашим консультациям помесячно, в конце списка указанно общее количество минут.Просим обрать внимание, что статистика за текущий день обновлятся раз в три часа.
    </p>
    <p>
        Вы можете посмотреть статистику за последние четыре месяца выбрав соответствующиий месяц из сптска в выпадающем меню.
    </p>
    <?php $s = isset($_GET['date']) ? $_GET['date'] : ''; ?>
    <?php echo CHtml::dropDownList('filter', $s, $this->getMonthYear(), array('onchange'=>'javascript:window.location = "?date="+this.value;','empty'=>'--')); ?>
   
    <div class="summary">  
        Итого предоплатных звонков: 212 минут<br>
        Итого бесплатных звонков: 323 минут<br>
        Всего потрачено: 1221 рублей
    </div>    
    <table class="my_stat">
        <tbody>
            <tr>
                <th>№</th>
                <th>Пользователь</th>
                <th>Дата/время</th>
                <th>Длительность</th>
                <!--<th>Потрачено</th>-->
            </tr>

            <?php foreach ($models as $key => $model): ?>
                <?php $expert = Users::model()->findByPk(Yii::app()->user->id); ?>
                <?php $summary = $expert['tariff'] * ceil($model['maxdur'] / 60) ?>
                <tr>
                    <td><?php echo $key + 1; ?></td>
                    <td><?php echo $expert['name']; ?></td>
                    <td><?php echo $model['date']; ?></td>
                    <td><?php echo ceil($model['maxdur'] / 60); ?></td>
                    <!--<td><?php echo $summary; ?></td>-->
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="pager">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $pages,
            'header' => '',
        ));
        ?>
    </div>
</div>
