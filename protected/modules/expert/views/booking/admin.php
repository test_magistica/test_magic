<!-- Центральный блок -->
<section>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/waitCall.js"></script>
    <h1 class="z-title">Управление заказами</h1>
    
    <?php if (Yii::app()->user->hasFlash('сonsultation')): ?>
        <div class="flash-error" id="waitCall">
            <?php echo Yii::app()->user->getFlash('сonsultation'); ?>
        </div>
    <?php elseif (Yii::app()->user->hasFlash('info-message')): ?>
        <div class="flash-error" id="waitCall">
            <?php echo Yii::app()->user->getFlash('info-message'); ?>
        </div>
    <?php endif; ?>
    <div class="tab-container">
        <ul class="tabs">
	        <li class="current">Заказ звонков (<?php echo $cCount; ?>)</li>
	        <li>Заказ звонков (гости) (<?php echo $dCount;?>)</li>
            <li>Заказ услуг (<?php echo $bCount; ?>)</li>
        </ul>



        <div class="box visible">
            <?php
            $expert_id = Yii::app()->user->id; 
            $expert = Users::model()->findByPk($expert_id);
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'consultation-grid',
                'dataProvider' => $consultation->search(),
                'filter' => $consultation,
                'cssFile' => Yii::app()->baseUrl . '/css/CGridView.css',
                'columns' => array(
                	array(
                        'name' => 'userID',
                        'header' => 'Пользователь',
                        'type' => 'html',
                        'value' => 'Controller::getUserName($data->userID)',
                        'htmlOptions' => array('style' => 'width: 100px;'),
                    ),
                        array(
                	'type'=>'raw',
                	'header'=>'Дата звонка',
                	'value'=>'date(\'d.m.Y H:i:s\', strtotime($data->date))'
                    ),
                    
		        array(
                        'name' => 'duration',
                        'header' => 'Длительность<br/>(минут)',
                        'type' => 'html',
                        'value' => 'ceil($data->duration/60)',
                    ),
            	    array(
                        'name' => 'cost',
                        'header' => 'Предполагаемая<br/>прибыль',
                        'type' => 'html',
                        'value' => function ($data) use ($expert)
                        {
                    	    if (! empty($expert->ratio))
                    		return $data->cost*$expert->ratio;
                    	    else 
                    		return $data->cost;
                        },
                    ),
                    array(
                	'type'=>'raw',
                	'header'=>'Действие',
                	'value'=>function($data)
                	{
                	    switch ($data->status)
        		    {
                	     /*(($data->status == 'BREAK') OR ($data->status == 1)):
                		return CHtml::link('Позвонить','/expert/consultation/call/?maxdur='.$data->duration.'&userID='.$data->userID.'&c='.$data->id).'<br/>'.
                		     CHtml::link('Удалить','/site/cancel/'.$data->id.'/?act=expert',
                		      array('onclick'=>'return confirm(\'Вы действительно хотите удалить консультацию?\') '.
                    			'? true : false;'));
                	    else:
                		if (($data->status))
                		return CHtml::link('Перезвонить<br/>(Осталось '.'м.)','/expert/consultation/call/?maxdur='.$data->duration.'&userID='.$data->userID.'&c='.$data->id).'<br/>'.
                		     CHtml::link('Отменить','/site/cancel/'.$data->id.'/?act=expert',
                		      array('onclick'=>'return confirm(\'Вы действительно хотите удалить консультацию?\') '.
                    			'? true : false;'));*/
                    		case 'BREAK':
                    		case '1':
                    		    return CHtml::link('Позвонить','/expert/consultation/call/?maxdur='.$data->duration.'&userID='.$data->userID.'&c='.$data->id).'<br/>'.
                		     CHtml::link('Удалить','/site/cancel/'.$data->id.'/?act=expert',
                		      array('onclick'=>'return confirm(\'Вы действительно хотите удалить консультацию?\') '.
                    			'? true : false;'));
                    		    break;
                    		case '0':
                    		case '3':
                    		case 'BEGIN':
                    		    break;
                    		default: 
    				    $command = Yii::app()->db->createCommand();
    				    $pass_m = $command->select('SUM(CEILING(`duration` / 60)) as `duration`')
    					->from('calljournal')
    					->where('call_back.cbk_id=:cbk_id',array(':cbk_id'=>$data->id))
    					->join('call_back','call_back.id = calljournal.id_callback')
    					->queryRow();
    				    
    				    //Уже было проговорено
    				    // print $pass_m['duration'];
    				    
    				    $remainder = ceil($data->duration/60) - $pass_m['duration']; //осталось минут
    				    //print $remainder;
    				    if ($remainder > 0):
    					$maxdur = $remainder*60;
    					return CHtml::link('Перезвонить<br/>Осталось'.$remainder.'м.','/expert/consultation/call/?maxdur='.$maxdur.'&userID='.$data->userID.'&c='.$data->id).'<br/>'.
                		    	    CHtml::link('Отменить','/site/cancel/'.$data->id.'/?act=expert',
                		    		array('onclick'=>'return confirm(\'Вы действительно хотите отменить остаточные '.$remainder.'мин.?\') '.
                    				    '? true : false;'));
    				    endif;
                    		    break;
                	    };
                	},
                	'htmlOptions' => array('style' => 'width: 70px;'),
                    ),
                    array(
                        'name' => 'status',
                        'type' => 'html',
                        'value' => 'Controller::changeTheNameId($data->status)',
                        'htmlOptions' => array('style' => 'width: 50px;'),
                    ),
                    
                ),
            ));
            ?>
		 </div>
        <div class="box">
        <?php
            $expert_id = Yii::app()->user->id; 
            $expert = Users::model()->findByPk($expert_id);
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'consult_guest-grid',
                'dataProvider' => $consultation->search_guest(),
                'filter' => $consultation,
                'cssFile' => Yii::app()->baseUrl . '/css/CGridView.css',
                'columns' => array(
                	array(
                        'name' => 'userID',
                        'header' => 'Пользователь',
                        'type' => 'html',
                        'value' => '$data->guest_name',
                        'htmlOptions' => array('style' => 'width: 100px;'),
                    ),
                        array(
                	'type'=>'raw',
                	'header'=>'Дата звонка',
                	'value'=>'date(\'d.m.Y H:i:s\', strtotime($data->date))'
                    ),
                    
		        array(
                        'name' => 'duration',
                        'header' => 'Длительность<br/>(минут)',
                        'type' => 'html',
                        'value' => 'ceil($data->duration/60)',
                    ),
            	    array(
                        'name' => 'cost',
                        'header' => 'Предполагаемая<br/>прибыль',
                        'type' => 'html',
                        'value' => function ($data) use ($expert)
                        {
                    	    if (! empty($expert->ratio))
                    		return $data->cost*$expert->ratio;
                    	    else 
                    		return $data->cost;
                        },
                    ),
                    array(
                	'type'=>'raw',
                	'header'=>'Действие',
                	'value'=>function($data)
                	{
                	    switch ($data->status)
        		    {
                    		case 'paid':
                    		    return CHtml::link('Позвонить','/expert/consultation/call/?maxdur='.$data->duration.'&c='.$data->id);
                    		    break;
                    		case 'nopaid':
                    		case 'BEGIN':
                    		    break;
                    		default: 
    				    $command = Yii::app()->db->createCommand();
    				    $pass_m = $command->select('SUM(CEILING(`duration` / 60)) as `duration`')
    					->from('calljournal')
    					->where('call_back.cbk_id=:cbk_id',array(':cbk_id'=>$data->id))
    					->join('call_back','call_back.id = calljournal.id_callback')
    					->queryRow();
    				    
    				    //Уже было проговорено
    				    // print $pass_m['duration'];
    				    
    				    $remainder = ceil($data->duration/60) - $pass_m['duration']; //осталось минут
    				    //print $remainder;
    				    if ($remainder > 0):
    					$maxdur = $remainder*60;
    					return CHtml::link('Перезвонить<br/>Осталось'.$remainder.'м.','/expert/consultation/call/?maxdur='.$maxdur.'&c='.$data->id);
    				    endif;
                    		    break;
                	    };
                	},
                	'htmlOptions' => array('style' => 'width: 70px;'),
                    ),
                    array(
                        'name' => 'status',
                        'type' => 'html',
                        'value' => 'Controller::changeTheNameId($data->status)',
                        'htmlOptions' => array('style' => 'width: 50px;'),
                    ),
                    
                ),
            ));
            ?>
        </div>
        <div class="box">
		        <?php
		        $this->widget('zii.widgets.grid.CGridView', array(
			        'id' => 'booking-grid',
			        'dataProvider' => $model->search(),
			        'filter' => $model,
			        'cssFile' => Yii::app()->baseUrl . '/css/CGridView.css',
			        'columns' => array(
				        array(
					        'name' => 'id',
					        'header' => 'Не прочитано',
					        'type' => 'html',
					        'value' => '(Controller::checkBooking($data->id)>-1) ? "(".Controller::checkBooking($data->id).")" :""',
					        'htmlOptions' => array('style' => 'width: 10px;'),
				        ),
				        array(
					        'name' => 'userID',
					        'header' => 'Пользователь',
					        'type' => 'html',
					        'value' => 'Controller::getUserName($data->userID)',
					        'htmlOptions' => array('style' => 'width: 100px;'),
				        ),
				        array(
					        'name' => 'serviceID',
					        'header' => 'Услуга',
					        'type' => 'html',
					        'value' => 'Controller::getServiceName($data->serviceID)',
					        'htmlOptions' => array('style' => 'width: 100px;'),
				        ),
				        array(
					        'name' => 'createTime',
					        'header' => 'Время',
					        'type' => 'html',
					        'value' => '$data->createTime',
					        'htmlOptions' => array('style' => 'width: 50px;'),
				        ),
				        array(
					        'name' => 'type',
					        'header' => 'Тип',
					        'type' => 'html',
					        'value' => '$data->type',
					        'htmlOptions' => array('style' => 'width: 50px;'),
				        ),
				        array(
					        'name' => 'status',
					        'type' => 'html',
					        'value' => 'Controller::changeTheNameId($data->status)',
					        'htmlOptions' => array('style' => 'width: 50px;'),
				        ),
				        array(
					        'class' => 'CButtonColumn',
					        'template' => '{view}',
					        'visible' => 'none',
					        'htmlOptions' => array('style' => 'width: 50px;'),
				        ),
			        ),
		        ));
		        ?>
        </div>
    </div><!-- #tab-container -->
</section>
<!-- #Центральный блок -->
<script>
    (function($) {
        $(function() {

            $('ul.tabs').on('click', 'li:not(.current)', function() {
                $(this).addClass('current').siblings().removeClass('current')
                        .parents('div.tab-container').find('div.box').eq($(this).index()).fadeIn(150).siblings('div.box').hide();
            })

            var tabIndex = window.location.hash.replace('#tab', '') - 1;
            if (tabIndex != -1)
                $('ul.tabs li').eq(tabIndex).click();

            $('a[href*=#tab]').click(function() {
                var tabIndex = $(this).attr('href').replace(/(.*)#tab/, '') - 1;
                $('ul.tabs li').eq(tabIndex).click();
            });

        })
    })(jQuery)
</script>