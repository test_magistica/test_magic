<?php Yii::app()->clientScript->registerCoreScript('yiiactiveform'); ?>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/lightbox.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/lightbox.js" type="text/javascript"></script>
<?php
/* @var $this BookingController */
/* @var $model Booking */

$this->menu = array(
	array('label' => 'List Booking', 'url' => array('index')),
	array('label' => 'Create Booking', 'url' => array('create')),
	array('label' => 'Update Booking', 'url' => array('update', 'id' => $model->id)),
	array('label' => 'Delete Booking', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
	array('label' => 'Manage Booking', 'url' => array('admin')),
);
?>

<h1>Просмотр заказанной услуги №<?php echo $model->id; ?></h1>
<? $service = Service::model()->findByPk($model->serviceID)?>
<? if($model->status != 2) { ?>
<div class="top">
	<p>
		<?/*php echo CHtml::link('Не принят', $model->status == 2 ? '#' : array('accept?action=0&id=' . $model->id), array('class' => $model->status == 2 ?'btn' : 'btn btn-primary')); ?>
        <?php echo CHtml::link('В ожидании', $model->status == 2 ? '#' : array('accept?action=1&id=' . $model->id), array('class' => $model->status == 2 ?'btn' : 'btn btn-primary')); */?>
		<?php echo CHtml::link('Выполнен', $model->status == 2 ? '#' : array('accept?action=2&id=' . $model->id), array('class' => $model->status == 2 ?'btn' : 'btn btn-primary')); ?>
	</p>
</div>
<? } ?>
<?php
$status = array('Не Принят', 'В ожидании', 'Выполнен');

$userName = Controller::getUser($model->userID, 'name') . ' ' . Controller::getUser($model->userID, 'surname');

$this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		array(
			'name' => 'userID',
			'value' => $userName,
			'type' => 'raw',
		),
		array(
			'name' => 'serviceID',
			'value' => Service::model()->findByPk($model->serviceID)->notes,
			'type' => 'raw',
		),
		//'message:html',
		'createTime',
		array(
			'name' => 'status',
			'value' => $status[$model->status],
			'type' => 'raw',
		),

	),
));

$images = Sendphoto::model()->findAll("userID = '" . $model->userID . "' and serviceID = '" . $model->serviceID . "'");
?>
<?php foreach ($images as $image): ?>
<tr class="template-download fade in" style="height: 81px;">
	<td class="preview">
		<a rel="lightbox[gallery]"
		   download="<?php echo $image['image']; ?>"
		   rel="gallery" title="<?php echo $image['image']; ?>"
		   href="<?php echo Yii::app()->request->baseUrl . '/uploads/send/' . $image['image']; ?>">

			<img width="80" height="60" src="<?php echo Yii::app()->request->baseUrl . '/uploads/send/Thumb/' . $image['image']; ?>">
		</a>
	</td>
</tr>
<?php endforeach; ?>

<div style="clear: both;"></div>
<br />
<?php
$criteria = new CDbCriteria();
$criteria->order = 'id desc';
$criteria->compare('toID', (int) Yii::app()->user->id);
$criteria->compare('bookingID', $model->id);
$criteria->compare('fromID', (int) $model->userID);
$count = Servicemessage::model()->count($criteria);
$pages = new CPagination($count);
$pages->pageSize = 20;
$pages->applyLimit($criteria);
$models = Servicemessage::model()->findAll($criteria);
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'register-form',
	//'enableClientValidation' => TRUE,
	'enableAjaxValidation' => FALSE,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'clientOptions' => array(
		'validateOnSubmit' => FALSE,
	),
));
?>
<div class="tarifbox graphicbox" id="list8">
	<label>Ваше сообщение:</label>
	<?php echo $form->textarea($message, 'message', array('class' => 'border5 text470')); ?>
	<?php echo $form->error($message, 'message'); ?>
	<?php echo $form->hiddenField($message, 'answere', array('value' => '0')); ?>
	<br /><br />
	<label>Файл:</label>
	<p>
		<?php
		$this->widget('CMultiFileUpload', array(
			'name' => 'files',
			'accept' => 'jpg|png|gif|doc|docx|rar|zip|txt',
			//'max' => 3,
			'remove' => 'удалить',
			//'denied'=>'', message that is displayed when a file type is not allowed
			//'duplicate'=>'', message that is displayed when a file appears twice
			'htmlOptions' => array('size' => 25),
		));
		?>
	</p>
	<p>
		<?php echo CHtml::submitButton('Ответить ', array('class' => 'submit')); ?>
	</p>
	<?php $this->endWidget(); ?>
</div>

<div class="usercab_page">
	<div class="my_msg_list block">
	<? 	foreach ($models as $model) {
			$image = Controller::getUserImage($model['userID']); ?>
		<div class="shadowblock mymess">
			<span class="user_info">
				<img width="82" height="75" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $image; ?>" alt="" />
			</span>
			<!--change class new message class: "name new_msg" -->
			<p class="lnk-name">
				<a><?php echo $model['userID'] == Yii::app()->user->id ? 'Вы' : $userName ?></a>
			</p>
			<div class="user_msg" style="padding-top:10px; padding-left: 100px;">
				<p>
					<?php echo $model['message']; ?>
					<?php $files = Bookingfiles::model()->findAll("messageID = '" . $model['id'] . "'"); ?>
					<?php foreach ($files as $file) : ?>
					<p><?php echo CHtml::link($file['file'], array('/myBooking/download', 'id' => $file['id'])); ?></p>
					<?php endforeach; ?>
				</p>
				<span class="date"><?php echo date('d.m.Y', strtotime($model->createTime)); ?></span>
			</div>
		</div>
	<? } ?>
	</div>
	<div class="pager_box">
		<?php
		$this->widget('CLinkPager', array(
			'pages' => $pages,
			'header' => '',
			'cssFile'=>Yii::app()->request->baseUrl.'/css/mypager.css',
		));
		?>
	</div>
	<!-- #Мои сообщения -->
</div><!-- #page -->


