<?php Yii::app()->clientScript->registerCoreScript('yiiactiveform'); ?>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/lightbox.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/lightbox.js" type="text/javascript"></script>
<?php
/* @var $this BookingController */
/* @var $model Booking */


$this->menu = array(
	array('label' => 'List Booking', 'url' => array('index')),
	array('label' => 'Create Booking', 'url' => array('create')),
	array('label' => 'Update Booking', 'url' => array('update', 'id' => $model->id)),
	array('label' => 'Delete Booking', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
	array('label' => 'Manage Booking', 'url' => array('admin')),
);
?>

<h1>Просмотр заказанной услуги №<?php echo $model->id; ?></h1>
<? $service = Service::model()->findByPk($model->serviceID)?>

<?php
$status = array('Не Принят', 'В ожидании', 'Выполнен');
$whatList = Yii::app()->params['whatList'];
$userName = $paymentService->username;

$photo = '  <a rel="lightbox[gallery]"
				download="'.$paymentService->image.'"
				rel="gallery" title="'.$paymentService->image.'"
				href="'.Yii::app()->request->baseUrl . '/uploads/consultation/' . $paymentService->image.'">
				<img width="80" height="60" src="'.Yii::app()->request->baseUrl . '/uploads/consultation/Thumb/' . $paymentService->image.'">
			</a>';

$this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		array(
			'name' => 'userID',
			'value' => $userName,
			'type' => 'raw',
		),
		array(
			'name' => 'serviceID',
			'value' => Service::model()->findByPk($model->serviceID)->notes,
			'type' => 'raw',
		),
		//'message:html',
		'createTime',
		array(
			'name' => 'status',
			'value' => $status[$model->status],
			'type' => 'raw',
		),
		array(
			'name' => 'date_birth',
			'value' => date('d.m.Y', strtotime($paymentService->date_birth)),
			'type' => 'raw',
		),
		array(
			'name' => 'time_birth',
			'value' => $paymentService->time_birth == '99:99' ? 'не помню' : $paymentService->time_birth,
			'type' => 'raw',
		),
		array(
			'name' => 'what',
			'value' => $whatList[$paymentService->what],
			'type' => 'raw',
		),
		array(
			'name' => 'Фото',
			'value' => $photo,
			'type' => 'raw',
		),
		array(
			'name' => 'Сообщение',
			'value' => $paymentService->question,
			'type' => 'raw',
		),
	),
));

?>

<div style="clear: both;"></div>
<br />
<?php
$criteria = new CDbCriteria();
$criteria->order = 'id desc';
$criteria->compare('toID', (int) Yii::app()->user->id);
$criteria->compare('bookingID', $model->id);
$criteria->compare('fromID', (int) $model->userID);
$count = Servicemessage::model()->count($criteria);
$pages = new CPagination($count);
$pages->pageSize = 20;
$pages->applyLimit($criteria);
$models = Servicemessage::model()->findAll($criteria);
?>
<script>
	$(document).ready(function(){
		$('.show_button a').on('click', function() {
			if($(this).hasClass('c2')) {
				$('.answerbox').show().removeClass('tarifbox').removeClass('graphicbox');
				var Text = CKEDITOR.replace('Servicemessage[message]',{toolbar : 'Basic'});
				AjexFileManager.init({returnTo: 'ckeditor', editor: Text});
				$('#Servicemessage_answere').val(1);
				$('#register-form .submit').val("отправить письмо");
				$('.usercab_page').hide();
				$('.warning_message').show();
			}
			else $('.answerbox').show().removeClass('answerbox');
			$(this).parent().remove();
		});
	});
</script>
	<div class="show_button">
		<a href="javascript: void(0);" class="c1">написать</a>
		<?if($model->status != 2){?>
		|
		<a href="javascript: void(0);" class="c2">ответить на услугу</a>
		<?}?>
	</div>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'register-form',
	//'enableClientValidation' => TRUE,
	'enableAjaxValidation' => FALSE,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'clientOptions' => array(
		'validateOnSubmit' => FALSE,
	),
));
?>
<div class="answerbox tarifbox graphicbox" id="list8" style="display:none;">
	<label>Ваше сообщение:</label>
	<?php echo $form->textarea($message, 'message', array('class' => 'border5 text470')); ?>
	<?php echo $form->error($message, 'message'); ?>
	<?php echo $form->hiddenField($message, 'answere', array('value' => '0')); ?>


	<p>
		<?php echo CHtml::submitButton('Ответить ', array('class' => 'submit')); ?>
	</p>
	<br />
	<br />
	<br />
	<?php $this->endWidget(); ?>
</div>

<div class="warning_message">
	* внимание !!! ответ на услугу, должен быть подробный (более 1000 символов) и затрагивать всю уточняющую информацию у клиента.
	подписываться не нужно. ваша подпись будет автоматически подставлена в письме.
</div>
<div class="usercab_page">
	<div class="my_msg_list block">
		<? $i = 1;
		foreach ($models as $model) {
			if($i != count($models)) {
				$image = Controller::getUserImage($model['userID']); ?>
				<div class="shadowblock mymess <?=($model['answere'] == 1 ? 'answere_message' : '')?>">
					<span class="user_info">
						<img width="82" height="75" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $image; ?>" alt="" />
					</span>
					<p class="lnk-name">
						<a><?php echo $model['userID'] == Yii::app()->user->id ? 'Вы' : $userName ?></a>
					</p>
					<div class="user_msg" style="padding-top:10px; padding-left: 100px;">
							<?=$model['message']; ?>
							<? if($files = Bookingfiles::model()->findAll("messageID = '" . $model['id'] . "'")) {?>
								<? foreach ($files as $file) { ?>
								<p><?php echo CHtml::link($file['file'], array('/myBooking/download', 'id' => $file['id'])); ?></p>
								<? }
							} ?>
						<span class="date"><?php echo date('d.m.Y', strtotime($model->createTime)); ?></span>
					</div>
				</div>
			<? } $i++;
		} ?>
	</div>
	<div class="pager_box">
		<?php
		$this->widget('CLinkPager', array(
			'pages' => $pages,
			'header' => '',
			'cssFile'=>Yii::app()->request->baseUrl.'/css/mypager.css',
		));
		?>
	</div>
	<!-- #Мои сообщения -->
</div><!-- #page -->


