<?php
/* @var $this ScheduleController */
/* @var $data Schedules */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expert_id')); ?>:</b>
	<?php echo CHtml::encode($data->expert_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mon')); ?>:</b>
	<?php echo CHtml::encode($data->mon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tues')); ?>:</b>
	<?php echo CHtml::encode($data->tues); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wednes')); ?>:</b>
	<?php echo CHtml::encode($data->wednes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('thurs')); ?>:</b>
	<?php echo CHtml::encode($data->thurs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fri')); ?>:</b>
	<?php echo CHtml::encode($data->fri); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('satur')); ?>:</b>
	<?php echo CHtml::encode($data->satur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sun')); ?>:</b>
	<?php echo CHtml::encode($data->sun); ?>
	<br />

	*/ ?>

</div>