<?php
/* @var $this ScheduleController */
/* @var $model Schedules */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'schedules-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля отмеченные <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'mon'); ?><br>
		<?php
        $this->widget('CMaskedTextField', array(
            'model' => $model,
            'attribute' => 'mon',
            'mask' => '**:** - **:**',
            'placeholder' => '_',
            
        ));
        ?>
		<?php echo $form->error($model,'mon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tues'); ?><br>
		<?php
        $this->widget('CMaskedTextField', array(
            'model' => $model,
            'attribute' => 'tues',
            'mask' => '**:** - **:**',
            'placeholder' => '_',
            
        ));
        ?>
		<?php echo $form->error($model,'tues'); ?><br>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wednes'); ?>
		<?php
        $this->widget('CMaskedTextField', array(
            'model' => $model,
            'attribute' => 'wednes',
            'mask' => '**:** - **:**',
            'placeholder' => '_',
            
        ));
        ?>
		<?php echo $form->error($model,'wednes'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'thurs'); ?><br>
		<?php
        $this->widget('CMaskedTextField', array(
            'model' => $model,
            'attribute' => 'thurs',
            'mask' => '**:** - **:**',
            'placeholder' => '_',
            
        ));
        ?>
		<?php echo $form->error($model,'thurs'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fri'); ?><br>
		<?php
        $this->widget('CMaskedTextField', array(
            'model' => $model,
            'attribute' => 'fri',
            'mask' => '**:** - **:**',
            'placeholder' => '_',
            
        ));
        ?>
		<?php echo $form->error($model,'fri'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'satur'); ?><br>
		<?php
        $this->widget('CMaskedTextField', array(
            'model' => $model,
            'attribute' => 'satur',
            'mask' => '**:** - **:**',
            'placeholder' => '_',
            
        ));
        ?>
		<?php echo $form->error($model,'satur'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sun'); ?><br>
		<?php
        $this->widget('CMaskedTextField', array(
            'model' => $model,
            'attribute' => 'sun',
            'mask' => '**:** - **:**',
            'placeholder' => '_',
            
        ));
        ?>
		<?php echo $form->error($model,'sun'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
