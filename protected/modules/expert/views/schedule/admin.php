<?php
/* @var $this ScheduleController */
/* @var $model Schedules */




Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#schedules-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="head">        
        <p class="cmnt_lnk"><?php echo CHtml::link('Добавить', array('create'), array('class' => 'btn btn-primary right-my-button')); ?></p>
    </div>

    <br>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'schedules-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'cssFile' => Yii::app()->baseUrl . '/css/CGridView.css',
	'columns'=>array(
//		'id',
//		'expert_id',
		'mon',
		'tues',
		'wednes',
		'thurs',		
		'fri',
		'satur',
		'sun',		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
