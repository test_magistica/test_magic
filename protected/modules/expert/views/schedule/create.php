<?php
/* @var $this ScheduleController */
/* @var $model Schedules */

$this->breadcrumbs=array(
	'Schedules'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Schedules', 'url'=>array('index')),
	array('label'=>'Manage Schedules', 'url'=>array('admin')),
);
?>

<h1>Создать расписание</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>