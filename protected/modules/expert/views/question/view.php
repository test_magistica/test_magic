<?php
/*$this->breadcrumbs=array(
	$model->title,
);
$this->pageTitle=$model->title;*/
?>

<?php
/* $this->renderPartial('_view', array(
	'data'=>$model,
)); */?>

<?php $this->renderPartial('/site/_sliderbox'); ?>

<div id="question" class="m-t380">
    <div class="form bg-grey radius-5">
        <form> 
            <input type="button" value="Задать вопрос онлайн" class="button ask" />
            <input type="button" value="Просмотреть все вопросы" class="button view" />
        </form>
    </div>
    <div class="about bg-light-ellow radius-10" >
        <div id="quest-<?php echo $question->id;?>" class="item m-0">
            <div class="pic fl" >
               <a href="#" title="Переход на страницу пользователя" class="info-user" >
                  <img src="/images/avatar-quester.png" title="Картинка пользователя" valign='top' />
               </a> 
            </div>
            <div class="info ff-tahoma" >
                <h3>
                   <a href="#" class="title fs-16 c-fiolet" ><?php echo CHtml::encode($question->title);?></a>
                </h3>
                <p class="desc c-green-grey fs-13" >
                    <?php echo CHtml::encode($question->content);?>
                </p>
                <div class="user ff-tahoma fs-12" >
                    <div class="name p-0 fl" >Автор вопроса:&nbsp;<span><?php echo $question->author->name; ?></span><span>&nbsp;|&nbsp;</span></div>
                   <div class="date p-0 fl" >
                       <?php 
//                     var_dump( $question->create_time);
//                     Yii::app()->dateFormatter->format(Question::DATE_TIME_FORMAT, $question->create_time);
//                            $dt = new DateTime($question->create_time);
                        echo Yii::app()->dateFormatter->format("dd MM yyyy", $question->create_time);
                        
                        ?>
                       <span>&nbsp;|&nbsp;</span>
                   </div> 
                   <div class="clear"></div>
                 </div>
             </div>                    
        </div>        
    </div>
    <div id="quest-answers">
        <h3><span class="title">Ответы экспертов</span> <span class="section">Категория: <?php echo $question->аrea_expertise->name; ?></span></h3>
        <?php for ($a=0;$a<5;$a++) :?>
        <div id="answer-<?//=$a;?>" class="item answer ">
            <div class="pic expert fl" >
               <a href="#" title="Переход на страницу пользователя" class="link expert" >
                  <img src="/images/avatar-expert.png" title="Картинка експерта" valign='top' />
               </a> 
            </div>
            <div class="info expert ff-tahoma" >
                <div class="name">
                   <a href="#" class="link expert fs-16 c-fiolet" >Ольга</a>
                </div>
                <div class="about">
                    <div class="status">
                        <div class="short">
                            <span class="prof">Гадание на картах</span>
                            <span class="stat">online</span>
                        </div>
                        <div class="ratting">
                            <div class="rat fl"></div>
                            <div class="rat fl"></div>
                            <div class="rat fl"></div>
                            <div class="rat fl"></div>
                            <div class="rat fl"></div>
                            <div class="cnt">11743</div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="sub form">
                        <form>
                            <input type="submit" value="Написать" class="button write" />
                        </form>                            
                    </div>
                    <div class="clear"></div>
                </div>
                <p class="desc answer c-green-grey fs-13" >
                   Здравствуйте эксперты. Скажите сколько в моей судьбе официальных браков - сколько уже было и сколько будет в будущем и сколько по судьбе детей по возможности когда родятся. выйду ли замуж в ближайшие 2 года.
                </p>
            </div>                    
        </div>  
        <?php endfor; ?>
            <?php
            /* if($model->questionCount>=1): ?>
                    <h3>
                            <?php echo $model->questionCount>1 ? $model->questionCount . ' questions' : 'One question'; ?>
                    </h3>

                    <?php $this->renderPartial('_list',array(
                            'post'=>$model,
                            'questions'=>$model->questions,
                    )); ?>
            <?php endif; ?>

            <h3>Leave a Question</h3>

            <?php if(Yii::app()->user->hasFlash('questionSubmitted')): ?>
                    <div class="flash-success">
                            <?php echo Yii::app()->user->getFlash('questionSubmitted'); ?>
                    </div>
            <?php else: ?>
                    <?php $this->renderPartial('/question/_form',array(
                            'model'=>$question,
                    )); ?>
            <?php endif; */?>

    </div><!-- questions -->
</div>
