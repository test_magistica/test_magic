<div id="questions" class="rtvs">
        <div class="info radius-5 bg-light-blue c-light-grey" >
            <ul class="list" >
                <li>- Вы можете задать <b>вопрос онлайн бесплатно</b> сразу всем экпертам.</li>
                <li>- Ваши личныйе даные и фотографии доступны для просмотра, только эксертам</li>
                <li>- Вы можете <b>задать вопрос ясновидящим</b>, оставаясь инкогнито без публикации на сайте</li>
            </ul>    
            
            <div class="form question">
                <?php 
                    $form = $this->beginWidget('CActiveForm', array(
                        'action' => Yii::app()->createUrl('question/create', array('st' => Question::STATUS_DRAFT)),
                    )); 
                ?>
                    <div class="row">
                        <?php 
                            echo $form->textField(
                                $model, 
                                'title', 
                                array(
                                    'class' => 'quest-text radius-5',
                                    'placeholder' => "Введите ваш вопрос",
                                    )
                                );
                        ?>
                        <?php echo $form->error($model,'title'); ?>
                        <?php echo CHtml::submitButton("Отправить", array('class' => 'quest-sub bg-fiolet radius-5'));?>
                    </div>
                <?php $this->endWidget();?>
                <?php
                /* $form=$this->beginWidget('CActiveForm'); ?>

                <?/*<p class="note">Fields with <span class="required">*</span> are required.</p>?>

                <?php echo CHtml::errorSummary($model); ?>

                    <div class="row">
                       <?php echo $form->labelEx($model,'title'); ?>
                       <?php echo $form->textField($model,'title',array('size'=>80,'maxlength'=>128)); ?>
                       <?php echo $form->error($model,'title'); ?>
                  </div>
                  <div class="row buttons">
                        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
                  </div>

                <?php $this->endWidget(); */?>

                <!-- form -->
            </div>    
        </div>                      
        <div id="tabs-question" class="list-view question bg-light-ellow radius-5" >
            <ul class="tabs-menu c-light-grey tr-upper" >Последние вопросы
                    <li class="fr p-l10 tr-none" ><a href="#ui-tabs-1">Платные</a></li> 
                    <li class="fr tr-none"><a href="#ui-tabs-2">Бесплатные</a></li> 
            </ul>
            <div id="ui-tabs-1" class="tab-pay-quest" >
                <?php for ($i=0;$i<8;$i++) { ?>
                <div id="quest-<?php echo $i;?>" class="item-quest">
                    <div class="pic-quest fl" >
                        <a href="#" title="Переход на страницу пользователя" class="info-user" >
                            <img src="/images/avatar-quester.png" title="Картинка пользователя" valign='top' />
                        </a> 
                    </div>
                    <div class="info-quest ff-tahoma" >
                        <h3>
                            <a href="#" class="title fs-16 c-fiolet" >Когда я выйду замуж?</a>
                        </h3>
                        <p class="desc-quest c-green-grey fs-13" >
                            Как сделать так, чтоб не дойти до критической ситуации в семье? Самые просты правила. Как сделать так, чтоб не дойти до читать далее...
                        </p>
                        <div class="icons-quest fs ff-tahoma fs-12" >
                            <div class="date-quest fl" >2 ноября 2013</div>
                            <div class="maps-taro-quest fl" >Карты тарро</div>
                            <div class="answers-quest fl" >Ответов(20)</div>
                            <div class="clear"></div>
                        </div>
                    </div>                    
                </div>  
               <?php }?> 
               <!-- пагинация --> 
               <center class="m-b14">
                    <div class="pager"><?php /*Перейти к странице:*/?>
                        <ul class="yiiPager" id="yw4">
                             <li class="first hidden"><a href="/ru/site/index.html">&lt;&lt; First</a></li>
                             <li class="previous <?php //hidden;?>"><a href="/ru/site/index.html">&lt; Вперед</a></li>
                             <li class="page selected"><a href="/ru/site/index.html">1</a><span >|</span></li>
                             <li class="page"><a href="/ru/site/index.html?page=2">2</a><span>|</span></li>
                             <li class="page"><a href="/ru/site/index.html?page=3">3</a><span>|</span></li>
                             <li class="page"><a href="/ru/site/index.html?page=4">4</a><span>|</span></li>
                             <li class="page"><a href="/ru/site/index.html?page=5">5</a><span>|</span></li>
                             <li class="page"><a href="/ru/site/index.html?page=6">6</a></li>
                             <li class="next"><a href="/ru/site/index.html?page=2">Назад &gt;</a></li>
                             <li class="last"><a href="/ru/site/index.html?page=6">Last &gt;&gt;</a></li>
                         </ul>
                     </div>
               </center>                
            </div>
            <div id="ui-tabs-2" class="tab-nopay-quest">
                <?php for ($i=0;$i<7;$i++) { ?>
                <div id="quest-1<?=$i;?>" class="item-quest">
                    <div class="pic-quest fl" >
                        <a href="#" title="Переход на страницу пользователя" class="info-user" >
                            <img src="/images/avatar-quester.png" title="Картинка пользователя" valign='top' />
                        </a> 
                    </div>
                    <div class="info-quest ff-tahoma" >
                        <h3>
                            <a href="#" class="title fs-16 c-fiolet" >Когда я выйду замуж?</a>
                        </h3>
                        <p class="desc-quest c-green-grey fs-13" >
                            Как сделать так, чтоб не дойти до критической ситуации в семье? Самые просты правила. Как сделать так, чтоб не дойти до читать далее...
                        </p>
                        <div class="icons-quest fs ff-tahoma fs-12" >
                            <div class="date-quest fl" >2 ноября 2013</div>
                            <div class="maps-taro-quest fl" >Карты тарро</div>
                            <div class="answers-quest fl" >Ответов(20)</div>
                            <div class="clear"></div>
                        </div>
                    </div>                    
                </div>
               <?php}?>
               <!-- пагинация --> 
               <center class="m-b14">
                    <div class="pager"><?php/*Перейти к странице:*/?>
                        <ul class="yiiPager" id="yw4">
                             <li class="first hidden"><a href="/ru/site/index.html">&lt;&lt; First</a></li>
                             <li class="previous <?php hidden;?>"><a href="/ru/site/index.html">&lt; Вперед</a></li>
                             <li class="page selected"><a href="/ru/site/index.html">1</a></li>
                             <li class="page"><a href="/ru/site/index.html?page=2">2</a></li>
                             <li class="page"><a href="/ru/site/index.html?page=3">3</a></li>
                             <li class="page"><a href="/ru/site/index.html?page=4">4</a></li>
                             <li class="page"><a href="/ru/site/index.html?page=5">5</a></li>
                             <li class="page"><a href="/ru/site/index.html?page=5">5</a></li>
                             <li class="next"><a href="/ru/site/index.html?page=2">Назад &gt;</a></li>
                             <li class="last"><a href="/ru/site/index.html?page=5">Last &gt;&gt;</a></li>
                         </ul>
                     </div>
               </center>  
            </div> 
            
        <?php 
                /* if(!empty($_GET['tag'])): ?>
                <h1>Questions Tagged with <i><?php echo CHtml::encode($_GET['tag']); ?></i></h1>
                <?php endif; ?>

                /*$this->widget('zii.widgets.CListView', array(
                    'dataProvider'=>$dataProvider,
                    'itemView'=>'_view',
                    'template'=>"{items}\n{pager}",
                ));*/ 
        ?>
            
        </div>
        <script>
            // Tabs
            $("#tabs-question").tabs({hide: {effect: "fadeOut", duration: 100},collapsible: true});
        </script>
</div>