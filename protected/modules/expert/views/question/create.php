<?php
/*$this->breadcrumbs=array(
	'Create Question',
);*/
?>
<div id="ask-expert">
    <h1 class="title" >Задать свой вопрос эксперту</h1>
    <?php if($step == "1"): ?>
    <p class="info" >
        Чтобы задать вопрос экспертам онлайн  бесплатно заполните поля на этой странице. Это займет у вас не более трех минут. 
        После публикации вашего вопроса эксперты вам и помогут. Кроме того, на все платные вопросы эксперты гарантированно 
        ответят в течение 1–3 часов.        
    </p>
    <div class="form question-answer" style="margin-top:62px;">
        <?/*<form name="ask-question">*/?>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
                'id' => 'ask-question',
                //'enableClientValidation' => TRUE,
                'enableAjaxValidation' => FALSE,
                'clientOptions' => array(
                    'validateOnSubmit' => FALSE,
                ),
                'action' => Yii::app()->createUrl('question/create', array('st' => 2)),
                    
                'htmlOptions' => array('enctype' => 'multipart/form-data'),
                 )                 
            );
      ?>
       <div class="head"> 
           <div class="title inline left w-248">Задать вопрос</div>
             <div class="area inline fr right">
             <?php echo $form->labelEx($model,'аrea_expertise_id',array('label'=>'Область знаний')); ?>
             <?php echo $form->dropDownList($model,'аrea_expertise_id',                           
                       CHtml::listData( AreaExpertise::model()->findAll("(status=1)",
                         array("order"=>"sort")), 'id', 'name'), array('prompt'=>Yii::t('all','Выбор')."...")); ?>             

             <?php echo $form->error($model,'area_​​expertise_id'); ?>             
              </div>
          </div>    
          <div class="row">            
              <?php echo $form->labelEx($model,'title',array('label'=>'Заголовок:')); ?>
              <?php echo $form->textField($model, 'title', array('class' => 'title')); ?>              
              <?php echo $form->error($model,'area_​​expertise_id'); ?>  
          </div>
          <div class="row">    
              <?php echo $form->labelEx($model,'content',array('label'=>'Вопрос:')); ?>
              <?php echo $form->textArea($model, 'content', array('class' => 'text')); ?>              
              <?php echo $form->error($model,'content'); ?>               
          </div>            
          <div class="row">    
              <?php echo $form->labelEx($model,'image',array('label'=>'Фотография:')); ?>		              
              <?php echo CHtml::activeFileField($model,'image',array('class'=>'upload-file')); ?>                
              <?php echo $form->error($model,'image'); ?>
              <?/*<div id="photo" class="upload-file">*/?>
              <?php
                /* $this->widget('xupload.XUpload', array(
                            'url' => Yii::app()->createUrl("question/upload"),
                            'model' => $model,                        
                            'attribute' => 'file',
                            'multiple' => true,
                            'options' => array(                                
                                'maxFileSize' => 5000000,
                                'acceptFileTypes' => "js:/(\.|\/)(jpe?g|png)$/i",
                            )
                        )
                   );
                */?>
              <?/*</div>*/?>  
              <?php echo $form->error($model,'image'); ?>                                          
          </div> 
          <div class="row button center">
              <?php echo CHtml::submitButton('Задать вопрос', array('class' => 'ask')); ?>              
          </div> 
        <?/*</form>*/?>
        <?php $this->endWidget();?>
        <?php endif;?>
        <?php if($step == "2"): ?>
            <div style="margin-top:52px;"></div>
            <?/*<form name="terms-placement">*/?>
            <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'terms-placement',
                                //'enableClientValidation' => TRUE,
                                'enableAjaxValidation' => FALSE,
                                'clientOptions' => array(
                                    'validateOnSubmit' => FALSE,
                                ),
                          'htmlOptions'=>array(/*'class'=>'hidden'*/)
                         )                 
                     );
             ?>

                  <div class="head"> 
                      <div class="title inline left w-248">Условия размещения</div>
                  </div>    
                  <div class="row">
                      <div class="column pay">
                          <div class="title">       
                            <?php echo $form->radioButton($model, 'pay', array('value' => 0, 'class' => 'radio')); ?> 
                            <?php echo $form->labelEx($model,'pay',array('label'=>'<span class="name">Платный</span><span class="price">200 руб.</span>')); ?>  
                          </div>
                          <div class="info">
                              <div class="item <? echo $model->pay ? '' : 'active';?>">
                                  <div class="title">Приватный вопрос</div>
                                  <p class="desc">
                                     Вопрос не публикуется в открытом доступе. Его видят только эксперты
                                  </p>
                              </div>
                              <div class="item <? echo $model->pay ? '' : 'active';?>">
                                  <p class="desc">
                                      Вы получете ответ на Ваш вопрос в течение 30 минут
                                  </p>    
                              </div>   
                          </div>
                      </div>
                      <div class="column nopay">
                          <div class="title">
                            <?php echo $form->radioButton($model, 'pay', array('value' => 1, 'class' => 'radio')); ?>                     
                            <?php echo $form->labelEx($model,'pay',array('label'=>'<span class="name">Бесплатный</span>')); ?>                        
                          </div>
                          <div class="info">
                            <div class="item <? echo $model->pay ? 'active' : '';?>">
                                <div class="title">Модерация вопроса</div>
                                <p class="desc">
                                   Ваш вопрос будет опубликован в порядке очереди
                                </p>
                            </div>
                            <div class="item <? echo $model->pay ? 'active' : '';?>">
                                <p class="desc" >
                                Ответы экспертов могут даны в течение  24 часов часов 
                                </p>
                            </div>        
                          </div>                   
                      </div>
                      <div class="clear"></div>
                  </div>
                  <div class="row button center">
                      <?php echo CHtml::submitButton('Задать вопрос', array('class' => 'ask')); ?>     
                  </div>  

            <?/*</form>*/?> 
            <?php $this->endWidget(); ?>
        <?php endif; ?>
        <script>             
           /*$(function() {
               if ($('.column.pay input:radio').attr('checked')=='checked' ) {
                $("div").has(".column.nopay input:radio").removeClass('ez-selected');                    
                $("div").has(".column.pay input:radio").addClass('ez-selected');                    
           }else{
                $("div").has(".column.nopay input:radio").addClass('ez-selected');                    
                $("div").has(".column.pay input:radio").removeClass('ez-selected');                    
           }});*/
       
           $('input:radio').live('change',function(){  
                    $('.column.pay .info .item').toggleClass('active');
                    $('.column.nopay .info .item').toggleClass('active');
           });           
        </script>
    </div>
</div>
<?php 
// echo $this->renderPartial('_form', array('model'=>$model)); 
?>