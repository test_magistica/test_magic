<?php
/* @var $this ArticleController */
/* @var $model Article */

$this->breadcrumbs = array(
    'Articles' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List Article', 'url' => array('index')),
    array('label' => 'Create Article', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#article-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="usercab_page">
    <div class="head">
        <h1>Мои статьи</h1>
        <p class="cmnt_lnk"><?php echo CHtml::link('Добавление новой статьи', array('create'), array('class' => 'btn btn-primary right-my-button')); ?></p>
    </div>     
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'enableAjaxValidation' => true,
            ));
    ?>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
//    'id' => 'article-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'cssFile' => Yii::app()->baseUrl . '/css/CGridView.css',
      //  'htmlOptions' => array('class' => 'std-table'),
      //  'cssFile' => Yii::app()->baseUrl . '/css/styles_drugoye.css',
        'columns' => array(
            array(
                'id' => 'id',
                'class' => 'CCheckBoxColumn',
                'selectableRows' => '50',
            ),
            'title',
            array(
                'name' => 'status',
                'header' => 'Статус',
                'filter' => array('1' => 'Разблокирован', '0' => 'Блокирован'),
                'value' => '($data->status=="1")?("Разблокирован"):("Блокирован")'
            ),
//		'anons',
//		'description',
//		'metaKey',
//		'metaDesc',
            /*
              'createTime',
              'url',
             */
            array(
                'class' => 'CButtonColumn',
            ),
        ),
    ));
    ?>



</div>
<?php $this->endWidget(); ?>