<?php
/* @var $this ArticleController */
/* @var $model Article */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'article-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
            ));
    ?>  
    <p class="note">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?><br>
        <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'anons'); ?><br>
        <?php echo $form->textArea($model, 'anons', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'anons'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'description'); ?><br>
        <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>

    <script type="text/javascript">
        var Test = CKEDITOR.replace('Article[description]');
        AjexFileManager.init({returnTo: 'ckeditor', editor: Test});
    </script>

    <div class="row">
        <?php echo $form->labelEx($model, 'createTime'); ?><br>
        <?php
        Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            //'name' => 'createTime',
            'attribute' => 'createTime',
            'options' => array("dateFormat" => 'yy-mm-dd '),
            'mode' => 'datetime',
        ));
        ?>
        <?php echo $form->error($model, 'createTime'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'image'); ?><br>
        <?php echo $form->fileField($model, 'image'); ?>
        <?php echo $form->error($model, 'image'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'metaKey'); ?><br>
        <?php echo $form->textArea($model, 'metaKey', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'metaKey'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'metaDesc'); ?><br>
        <?php echo $form->textArea($model, 'metaDesc', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'metaDesc'); ?>
    </div>



    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->