<?php
/* @var $this ArticleController */
/* @var $model Article */

$this->breadcrumbs = array(
    'Articles' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List Article', 'url' => array('index')),
    array('label' => 'Manage Article', 'url' => array('admin')),
);
?>

<div class="usercab_page"> 
    <div class="head">
        <h1>Создать статью</h1>
    </div>

</div>
<?php echo $this->renderPartial('_form', array('model' => $model)); ?>