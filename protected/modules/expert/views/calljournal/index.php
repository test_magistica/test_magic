<script>
	$(document).ready(function(){
		$('#filter_call, #filter_chat, #filter_serv').on('change', function(){
			window.location = "/expert/calljournal?act="+this.id+"&date="+this.value;
		})
	})
<?
$act = 'filter_call';
if(isset($_GET['act']))
	$act = $_GET['act'];
?>
</script>
<div class="cab_exp">

	<h1 class="z-title">Статистика звонков, чатов и услуг</h1>
	<?php $s = isset($_GET['date']) ? $_GET['date'] : ''; ?><div class="tab-container">
	<ul class="tabs">
		<li class=" <?=($act == 'filter_call' ? 'current' : '')?>">звонки </li>
		<li class=" <?=($act == 'filter_chat' ? 'current' : '')?>">чаты</li>
		<li>услуги</li>
	</ul>
	<div class="box <?=($act == 'filter_call' ? 'visible' : '')?>">
		<?php echo CHtml::dropDownList('filter_call', $s, $this->getMonthYear(), array('empty'=>'За всё время')); ?>

		<div class="summary" style="display:block;">
			Общая продолжительность: <b><span class="stat_min"><?=empty($all_summ['duration'])?  '0': $all_summ['duration'];?></span> мин.</b> <br>
			Всего заработано: <b><span class="stat_sum"><?=empty($all_summ['profit_expert'])? '0': $all_summ['profit_expert'];?></span> руб. </b>
		</div>

		<table class="my_stat">
			<tbody>
			<tr>
				<th>Тип</th>
				<th>Пользователь</th>
				<th>Номер пользователя</th>
				<th>Дата/время</th>
				<th>Длительность<br/>(минут)</th>
				<th>Прибыль</th>
				<th>Статус</th>
			</tr>
			<?php
			//$expert = Users::model()->findByPk(
			//$user_id = Yii::app()->user->id;
			$expert = Users::model()->findByPk(Yii::app()->user->id);
			//$cj = CallJournal::model()->findAllBySql('SELECT calljournal.*,sec_to_time(duration) as duration  FROM `calljournal` WHERE `caller_id`='.$user_id.
			//    ' OR `called_id`='.$user_id);
			foreach ($models as $key=>$model) { ?>
			<tr>
				<?php
				if($model['caller_id'] != $expert->id) {//входящий
					echo "<td>Входящий</td>";
					$client = Users::model()->findByPk($model['caller_id']);
				}
				else {
					echo "<td>Исходящий</td>";
					$client = Users::model()->findByPk($model['called_id']);
				}
				?>
				<td><?php echo $client['name'].' '.$client['surname'];?></td>
				<td><?php echo '+'.$model['tel_client'];?></td>
				<td><?php echo $model['call_date'];?></td>
				<td><?php echo ceil($model['duration']/60);?></td>
				<td><?php
					/*if ($expert->ratio)
																						$res = $model['cost'] * $expert->ratio;
																						else
																						$res = $model['cost'];
																						echo $res;*/
					echo $model['profit_expert'];
					?></td>
				<td><?php echo Controller::changeTheNameId($model->status);?></td>
			</tr>
				<?php };?>
			</tbody>
		</table>
	</div>

	<div class="box <?=($act == 'filter_chat' ? 'visible' : '')?>">
		<?php echo CHtml::dropDownList('filter_chat', $s, $this->getMonthYear(), array('empty'=>'За всё время')); ?>

<?
		$count_min = 0;
		foreach($statis_chat as $data) {
			$dtStart = new DateTime($data->countdown_date);
			$dtEnd = new DateTime($data->end_date);
			$diff = $dtStart->diff($dtEnd);
			$count_min += (((int)$diff->h * 60 + (int)$diff->i));
		}
?>
		<div class="summary" style="display:block;">
			Общая продолжительность: <b><span class="stat_min"><?=$count_min;?></span> мин.</b> <br>
			Всего заработано: <b><span class="stat_sum"><?=empty($count_min)? '0': $count_min * Chat::CHAT_COST * (($data->expert->ratio_chat == NULL) ? (float)0.5 : (float)$data->expert->ratio_chat);?></span> руб. </b>
		</div>
		<table class="my_stat">
			<tr>
				<th>Пользователь</th>
				<th class="second">Дата</th>
				<th class="third">Длительность</th>
				<th class="forth">История</th>
				<th class="fifth">Гонорар</th>
			</tr>
			<? foreach($statis_chat as $data) {
			$user = $data->user;?>
			<tr>
				<?php $usrPhoto = $this->getUserImage($user->id);?>
				<td>
					<a href="javascript:void(0);"><img
						src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $usrPhoto; ?>"
						title="Картинка пользователя"
						valign='top'
						width="60px"
						height="60px"
						/></a><br />
					<?=$user->name." ".$user->surname; ?>
				</td>
				<td>
					<?=Yii::app()->dateFormatter->format("dd MMMM yyyyг. HH:mm", $data->start_date); ?>
				</td>
				<td>
					<?php
					$dtStart = new DateTime($data->countdown_date);
					$dtEnd = new DateTime($data->end_date);
					$diff = $dtStart->diff($dtEnd);
					echo (((int)$diff->h * 60 + (int)$diff->i))."мин." ;
					?>
				</td>
				<td>
					<!--<a href="javascript:void(0);" onclick="window.open('<?php // echo $this->createUrl("/chat/Chat/reviewChatHistory", array('id' => $data->id)); ?>', '_blank', 'width=1000, height=800')">История онлайн</a>-->
					<?php echo CHtml::link("История онлайн", $this->createUrl("/chat/Chat/reviewChatHistory", array('id' => $data->id)), array("target" => "_blank")); ?>
				</td>
				<td>
					<?php
					// $data->expert->ratio == NULL - это у тестовых экспертов
					$callDuration = (((int)$diff->h * 60 + (int)$diff->i)) * Chat::CHAT_COST * (($data->expert->ratio_chat == NULL) ? (float)0.5 : (float)$data->expert->ratio_chat);
					echo $callDuration ;//. " | ".$data->id." | ".$data->expert->tariff." | ".$data->expert->ratio." - ".$data->expert->id." ".$user->id;
					?>
				</td>
			</tr>
			<?}?>
		</table>
	</div>

	<div class="box <?=($act == 'filter_serv' ? 'visible' : '')?>">
		<?php echo CHtml::dropDownList('filter_serv', $s, $this->getMonthYear(), array('empty'=>'За всё время')); ?>

		<?
		$honorarus = 0;
		foreach($statis_serv as $data) {
			$honorarus += $data->service->honorarium;
		}
		?>
		<div class="summary" style="display:block;">
			Всего заработано: <b><span class="stat_sum"><?=$honorarus;?></span> руб. </b>
		</div>
		<table class="my_stat">
			<tr>
				<th>Пользователь</th>
				<th class="second">Дата</th>
				<th class="third">Тип</th>
				<th class="forth">История</th>
				<th class="fifth">Гонорар</th>
			</tr>
			<? foreach($statis_serv as $data) {
			$user = $data->user;?>
			<tr>
				<?php $usrPhoto = $this->getUserImage($user->id);?>
				<td>
					<a href="javascript:void(0);"><img
						src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $usrPhoto; ?>"
						title="Картинка пользователя"
						valign='top'
						width="60px"
						height="60px"
						/></a><br />
					<?=$user->name." ".$user->surname; ?>
				</td>
				<td>
					<?=Yii::app()->dateFormatter->format("dd MMMM yyyyг. HH:mm", $data->createTime); ?>
				</td>
				<td>
					<?php
					echo $data->service->action == 'static' ? 'Статическая услуга' :  'Моя услуга' ;
					?>
				</td>
				<td>
					<!--<a href="javascript:void(0);" onclick="window.open('<?php // echo $this->createUrl("/chat/Chat/reviewChatHistory", array('id' => $data->id)); ?>', '_blank', 'width=1000, height=800')">История онлайн</a>-->
					<?php echo CHtml::link("История онлайн", $this->createUrl("/expert/booking/view/", array('id' => $data->id)), array("target" => "_blank")); ?>
				</td>
				<td>
					<?php
					echo $data->service->honorarium." руб." ;//. " | ".$data->id." | ".$data->expert->tariff." | ".$data->expert->ratio." - ".$data->expert->id." ".$user->id;
					?>
				</td>
			</tr>
			<?}?>
		</table>
	</div>
</div>