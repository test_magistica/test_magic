<?php
/* @var $this ConsultationController */
/* @var $model Consultation */

$this->breadcrumbs = array(
    'Consultations' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List Consultation', 'url' => array('index')),
    array('label' => 'Create Consultation', 'url' => array('create')),
);
?>

<h1>Manage Consultations</h1>

<?php if (Yii::app()->user->hasFlash('сonsultation')): ?>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('сonsultation'); ?>
    </div>
<?php endif; ?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'consultation-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'duration',
        array(
            'class' => 'CLinkColumn',
            'header' => 'Телефон',
            'labelExpression' => '$data->phone',
            'urlExpression' => '"call/"."?maxdur=".($data->duration*60)."&userID=".$data->userID."&c=".$data->id',
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
<?php if (isset(Yii::app()->session['cbk_id'])): ?>
    <script>
        function checkCall() {
            $.post("<?php echo CController::createUrl('/expert/consultation/check') ?>",
                    {check: '<?php echo Yii::app()->session['cbk_id']; ?>'},
            function(data) {
                if (data != "success") {
                    alert('Звонок завершен.');
                }
            });
        }
        setInterval("checkCall()", 30000);
    </script>
<?php endif; ?>