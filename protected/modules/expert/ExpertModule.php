<?php

class ExpertModule extends CWebModule {

    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'expert.models.*',
            'expert.components.*',
        ));
        Yii::app()->errorHandler->errorAction = '/admin/default/error';
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            if (Yii::app()->user->isGuest)
                Yii::app()->user->loginRequired();
            if (Yii::app()->user->checkAccess('expert'))
                return true;
            else
                throw new CHttpException(403, 'У вас недостаточно прав для выполнения указанного действия.');
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }

}
