<?php
/* @var $this DivinationController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Divinations',
);

$this->menu=array(
	array('label'=>'Create Divination', 'url'=>array('create')),
	array('label'=>'Manage Divination', 'url'=>array('admin')),
);
?>

<h1>Divinations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
