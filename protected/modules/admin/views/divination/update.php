<?php
/* @var $this DivinationController */
/* @var $model Divination */

$this->breadcrumbs=array(
	'Divinations'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Divination', 'url'=>array('index')),
	array('label'=>'Create Divination', 'url'=>array('create')),
	array('label'=>'View Divination', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Divination', 'url'=>array('admin')),
);
?>

<h1>Update Divination <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>