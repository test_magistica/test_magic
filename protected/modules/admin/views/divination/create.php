<?php
/* @var $this DivinationController */
/* @var $model Divination */

$this->breadcrumbs=array(
	'Divinations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Divination', 'url'=>array('index')),
	array('label'=>'Manage Divination', 'url'=>array('admin')),
);
?>

<h1>Create Divination</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>