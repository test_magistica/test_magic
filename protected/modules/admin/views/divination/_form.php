<?php
/* @var $this DivinationController */
/* @var $model Divination */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'divination-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'description'); ?>
        <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>

    <script type="text/javascript">
        var Test = CKEDITOR.replace('Divination[description]');
        AjexFileManager.init({returnTo: 'ckeditor', editor: Test});
    </script>

    <div class="row">
        <?php echo $form->labelEx($model, 'metaKey'); ?>
        <?php echo $form->textArea($model, 'metaKey', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'metaKey'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'metaDesc'); ?>
        <?php echo $form->textArea($model, 'metaDesc', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'metaDesc'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->