<?php
/* @var $this FaqAdminController */
/* @var $data Faq */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fquestion')); ?>:</b>
	<?php echo CHtml::encode($data->fquestion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fanswer')); ?>:</b>
	<?php echo CHtml::encode($data->fanswer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fgroup')); ?>:</b>
	<?php echo CHtml::encode($data->fgroup0->gname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status == 1 ? "Да" : "Нет"); ?>
	<br />


</div>