<?php
/* @var $this FaqAdminController */
/* @var $model Faq */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'faq-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fquestion'); ?>
		<?php echo $form->textArea($model,'fquestion',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'fquestion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fanswer'); ?>
		<?php echo $form->textArea($model,'fanswer',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'fanswer'); ?>
	</div>

	<div class="row">
            <?php 
                $menuItems = array(0=> Yii::t("adminka", "--- Не выбрано ---"));
                $menuItems = CMap::mergeArray($menuItems, Faq::model()->getDropdownItems());
            ?>
		<?php echo $form->labelEx($model,'fgroup'); ?>
                <?php echo $form->dropDownList($model, 'fgroup', $menuItems, array('encode'=>FALSE)); ?>
		<?php echo $form->error($model,'fgroup'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
