<?php
/* @var $this ExpertController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Экспреты',
);

$this->menu=array(
	array('label'=>'Создать эксперта', 'url'=>array('create')),
	array('label'=>'Управление экспертами', 'url'=>array('admin')),
);
?>

<h1>Список экспертов</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
