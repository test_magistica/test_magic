<?php
/* @var $this ExpertController */
/* @var $model Expert */

$this->breadcrumbs = array(
    'Пользователи' => array('index'),
    'Управление',
);

$this->menu = array(
    array('label' => 'Список экспертов', 'url' => array('index')),
    array('label' => 'Создать эксперта', 'url' => array('create')),
);


?>

<h1>Управление пользователями</h1>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => true,
        ));
?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'expert-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'id' => 'id',
            'class' => 'CCheckBoxColumn',
            'selectableRows' => '50',
        ),
        'name',
        'second_name',
        'surname',
        'email',        
        array(
            'name' => 'status',
            'header' => 'Статус',
            'filter' => array('1' => 'Разблокирован', '0' => 'Блокирован'),
            'value' => '($data->status=="1")?("Разблокирован"):("Блокирован")'
        ),
        /*
          'tele',
          'sex',
          'birthday',
          'country',
          'reg_date',
          'role',
          'bill',
          'image',
          'contact_email',
          'skype',
          'profession',
          'status',
         */
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('expert-grid');
    }
</script>

<div class="grid_3 push_2 omega">
    <?php echo CHtml::ajaxSubmitButton('Разблокировать', array('expert/ajaxupdate', 'act' => 'doActive'), array('success' => 'reloadGrid'), array('class' => 'content_button', 'id' => 'button_left3')); ?>

</div>
<div class="grid_3 push_2 omega">
    <?php echo CHtml::ajaxSubmitButton('Блокировать', array('expert/ajaxupdate', 'act' => 'doInactive'), array('success' => 'reloadGrid'), array('class' => 'content_button', 'id' => 'button_left')); ?>

</div>
<?php $this->endWidget(); ?>