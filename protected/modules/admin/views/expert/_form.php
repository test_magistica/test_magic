<?php
/* @var $this ExpertController */
/* @var $model Expert */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'expert-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательны.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?><br>
        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'second_name'); ?><br>
        <?php echo $form->textField($model, 'second_name', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'second_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'surname'); ?><br>
        <?php echo $form->textField($model, 'surname', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'surname'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?><br>
        <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'pwd'); ?><br>
        <?php echo $form->textField($model, 'pwd', array('size' => 32, 'maxlength' => 32)); ?>
        <?php echo $form->error($model, 'pwd'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'tele'); ?><br>
        <?php echo $form->textField($model, 'tele', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'tele'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'sex'); ?><br>
        <?php echo $form->radioButton($model, 'sex', array('value' => '2', 'id' => 'fermale', 'class' => 'radio')); ?>             
        <label for="fermale" class="female">Женский</label>
        <?php echo $form->radioButton($model, 'sex', array('value' => '1', 'id' => 'male', 'class' => 'radio', 'checked' => 'checked')); ?>            
        <label for="male" class="male">Мужской</label>
        <?php echo $form->error($model, 'sex'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'birthday'); ?><br>
        <?php
        Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
        $this->widget('CJuiDateTimePicker', array(
            'model' => $model,
            //'name' => 'createTime',
            'attribute' => 'birthday',
            'options' => array("dateFormat" => 'd.m.yy'),
            'mode' => 'date',
        ));
        ?>
        <?php echo $form->error($model, 'birthday'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'country'); ?><br>
        <?php
        echo $form->dropDownList($model, 'country', Controller::getCountry(), array(
            'class' => 'selectBlock',
                //'onchange' => 'document.getElementById("tele_field").value = this.value',
        ));
        ?>
        <?php echo $form->error($model, 'country'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'bill'); ?><span>?</span><br>
        <?php echo $form->textField($model, 'bill', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'bill'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'ratio'); ?><span>?</span><br>
        <?php echo $form->textField($model, 'ratio', array('size' => 60, 'maxlength' => 10)); ?>
        <?php echo $form->error($model, 'ratio'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'ratio_chat'); ?><span>?</span><br>
        <?php echo $form->textField($model, 'ratio_chat', array('size' => 60, 'maxlength' => 10)); ?>
        <?php echo $form->error($model, 'ratio_chat'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'contact_email'); ?><br>
        <?php echo $form->textField($model, 'contact_email', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'contact_email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'skype'); ?><br>
        <?php echo $form->textField($model, 'skype', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'skype'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'profession'); ?><br>
        <?php echo $form->textField($model, 'profession', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'profession'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'mobile'); ?><br>
        <?php echo $form->textField($model, 'mobile', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'mobile'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'mobileText'); ?><br>
        <?php echo $form->textField($model, 'mobileText', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'mobileText'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'home'); ?><br>
        <?php echo $form->textField($model, 'home', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'home'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'homeText'); ?><br>
        <?php echo $form->textField($model, 'homeText', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'homeText'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'about'); ?><br>
        <?php echo $form->textArea($model, 'about', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'about'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'metaTitle'); ?><br>
        <?php echo $form->textField($model, 'metaTitle', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'metaTitle'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'metaKey'); ?><br>
        <?php echo $form->textArea($model, 'metaKey', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'metaKey'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'metaDesc'); ?><br>
        <?php echo $form->textArea($model, 'metaDesc', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'metaDesc'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
