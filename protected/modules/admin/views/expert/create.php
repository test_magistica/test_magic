<?php
/* @var $this ExpertController */
/* @var $model Expert */

$this->breadcrumbs=array(
	'Эксперты'=>array('index'),
	'Создать',
);

$this->menu=array(
	array('label'=>'Список экспертов', 'url'=>array('index')),
	array('label'=>'Управление экспертами', 'url'=>array('admin')),
);
?>

<h1>Создание эксперта</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>