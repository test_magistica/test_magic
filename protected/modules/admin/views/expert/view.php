<?php
/* @var $this ExpertController */
/* @var $model Expert */

$this->breadcrumbs=array(
	'Эксперты'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Список экспертов', 'url'=>array('index')),
	array('label'=>'Создать эксперта', 'url'=>array('create')),
	array('label'=>'Редактировать эксперта', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Удалить эксперта', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Вы уверены что хотите удалить данного эксперта?')),
	array('label'=>'Управление экспертом', 'url'=>array('admin')),
);
?>

<h1>View Expert #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'second_name',
		'surname',
		'email',
		'pwd',
		'tele',
		'sex',
		'birthday',
		'country',
		'reg_date',
		'role',
		'bill',
		'ratio',
		'ratio_chat',
		'image',
		'contact_email',
		'skype',
		'profession',
		'status',
		'about',
		'metaTitle',
		'metaKey',
		'metaDesc',
	),
)); ?>
