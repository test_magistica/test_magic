<?php
/* @var $this ExpertController */
/* @var $model Expert */

$this->breadcrumbs=array(
	'Experts'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Список экспертов', 'url'=>array('index')),
	array('label'=>'Создать эксперта', 'url'=>array('create')),
	array('label'=>'Просмотреть эксперта', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление экспертом', 'url'=>array('admin')),
);
?>

<h1>Update Expert <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>