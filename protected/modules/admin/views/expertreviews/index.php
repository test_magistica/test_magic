<?php
/* @var $this ExpertreviewsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Expertreviews',
);

$this->menu=array(
	array('label'=>'Create Expertreviews', 'url'=>array('create')),
	array('label'=>'Manage Expertreviews', 'url'=>array('admin')),
);
?>

<h1>Expertreviews</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
