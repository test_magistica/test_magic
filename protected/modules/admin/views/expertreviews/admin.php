<?php
/* @var $this ExpertreviewsController */
/* @var $model Expertreviews */

$this->breadcrumbs = array(
    'Expertreviews' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List Expertreviews', 'url' => array('index')),
    array('label' => 'Create Expertreviews', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#expertreviews-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Expertreviews</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->
<?php
$form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => true,
        ));
?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'expertreviews-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'id' => 'id',
            'class' => 'CCheckBoxColumn',
            'selectableRows' => '50',
        ),
        array(
            'name' => 'userID',
            'header' => 'Пользователь',
            'type' => 'html',
            'value' => 'Controller::getUserName($data->userID)',
            'htmlOptions' => array('style' => 'width: 100px;'),
        ),
        array(
            'name' => 'expertID',
            'header' => 'ЭКСПЕРТ',
            'type' => 'html',
            'value' => 'Controller::getUserName($data->expertID)',
            'htmlOptions' => array('style' => 'width: 100px;'),
        ),
        'expertID',
        'description',
        'date',
        array(
            'name' => 'status',
            'header' => 'Статус',
            'filter' => array('1' => 'Разблокирован', '0' => 'Блокирован'),
            'value' => '($data->status=="1")?("Разблокирован"):("Блокирован")'
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('expertreviews-grid');
    }
</script>

<div class="grid_3 push_2 omega">
    <?php echo CHtml::ajaxSubmitButton('Разблокировать', array('expertreviews/ajaxupdate', 'act' => 'doActive'), array('success' => 'reloadGrid'), array('class' => 'content_button', 'id' => 'button_left3')); ?>

</div>
<div class="grid_3 push_2 omega">
    <?php echo CHtml::ajaxSubmitButton('Блокировать', array('expertreviews/ajaxupdate', 'act' => 'doInactive'), array('success' => 'reloadGrid'), array('class' => 'content_button', 'id' => 'button_left')); ?>

</div>
<?php $this->endWidget(); ?>