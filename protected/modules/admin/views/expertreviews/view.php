<?php
/* @var $this ExpertreviewsController */
/* @var $model Expertreviews */

$this->breadcrumbs=array(
	'Expertreviews'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Expertreviews', 'url'=>array('index')),
	array('label'=>'Create Expertreviews', 'url'=>array('create')),
	array('label'=>'Update Expertreviews', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Expertreviews', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Expertreviews', 'url'=>array('admin')),
);
?>

<h1>View Expertreviews #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'userID',
		'expertID',
		'description',
		'date',
		'status',
	),
)); ?>
