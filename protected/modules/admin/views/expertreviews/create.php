<?php
/* @var $this ExpertreviewsController */
/* @var $model Expertreviews */

$this->breadcrumbs=array(
	'Expertreviews'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Expertreviews', 'url'=>array('index')),
	array('label'=>'Manage Expertreviews', 'url'=>array('admin')),
);
?>

<h1>Create Expertreviews</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>