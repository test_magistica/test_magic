<?php
/* @var $this ExpertreviewsController */
/* @var $model Expertreviews */

$this->breadcrumbs=array(
	'Expertreviews'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Expertreviews', 'url'=>array('index')),
	array('label'=>'Create Expertreviews', 'url'=>array('create')),
	array('label'=>'View Expertreviews', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Expertreviews', 'url'=>array('admin')),
);
?>

<h1>Update Expertreviews <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>