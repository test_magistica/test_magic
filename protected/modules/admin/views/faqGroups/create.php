<?php
/* @var $this FaqGroupsController */
/* @var $model FaqGroups */

$this->breadcrumbs=array(
	'Faq Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FaqGroups', 'url'=>array('index')),
	array('label'=>'Manage FaqGroups', 'url'=>array('admin')),
);
?>

<h1>Create FaqGroups</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>