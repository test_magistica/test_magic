<?php
/* @var $this FaqGroupsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Faq Groups',
);

$this->menu=array(
	array('label'=>'Create FaqGroups', 'url'=>array('create')),
	array('label'=>'Manage FaqGroups', 'url'=>array('admin')),
);
?>

<h1>Faq Groups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
