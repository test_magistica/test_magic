<?php
/* @var $this FaqGroupsController */
/* @var $model FaqGroups */

$this->breadcrumbs=array(
	'Faq Groups'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FaqGroups', 'url'=>array('index')),
	array('label'=>'Create FaqGroups', 'url'=>array('create')),
	array('label'=>'Update FaqGroups', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FaqGroups', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FaqGroups', 'url'=>array('admin')),
);
?>

<h1>View FaqGroups #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'gname',
		'parent_group',
		'status',
	),
)); ?>
