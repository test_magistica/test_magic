<?php
/* @var $this FaqGroupsController */
/* @var $model FaqGroups */

$this->breadcrumbs=array(
	'Faq Groups'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FaqGroups', 'url'=>array('index')),
	array('label'=>'Create FaqGroups', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#faq-groups-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
//Yii::app()->db->createCommand("select gname from faq_groups where id=:id and parent_group=:pg")->queryScalar();
?>

<h1>Manage Faq Groups</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'faq-groups-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'gname',
		'parent_group',
                array(
                    'name' => "status",
                    'value' => '$data->status == 1 ? "Да" : "Нет"',
                ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
