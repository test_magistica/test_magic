<?php
/* @var $this FaqGroupsController */
/* @var $data FaqGroups */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gname')); ?>:</b>
	<?php echo CHtml::encode($data->gname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parent_group')); ?>:</b>
	<?php echo CHtml::encode($data->parent_group); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>