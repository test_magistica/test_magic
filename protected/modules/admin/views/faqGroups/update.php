<?php
/* @var $this FaqGroupsController */
/* @var $model FaqGroups */

$this->breadcrumbs=array(
	'Faq Groups'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FaqGroups', 'url'=>array('index')),
	array('label'=>'Create FaqGroups', 'url'=>array('create')),
	array('label'=>'View FaqGroups', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FaqGroups', 'url'=>array('admin')),
);
?>

<h1>Update FaqGroups <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>