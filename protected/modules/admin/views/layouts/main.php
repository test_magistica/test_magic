<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/AjexFileManager/ajex.js"></script>
    </head>

    <body>
        
        <div class="container" id="page">

            <div id="header">
                <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
            </div><!-- header -->

            <div id="mainmenu">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => 'На сайт', 'url' => array('/site/index'), 'visible' => !Yii::app()->user->isGuest), array('target' => '_blank'),
	                    array('label' => 'Шаблоны писем', 'url' => array('emailTemplates/admin'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Статьи', 'url' => array('article/admin'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Пользователи', 'url' => array('expert/admin'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Услуги', 'url' => array('service/admin'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Категории', 'url' => array('category/admin'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Отзывы', 'url' => array('expertreviews/admin'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Баннер', 'url' => array('slider/admin'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Онлайн-гадания', 'url' => array('divination/admin'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Страницы', 'url' => array('page/admin'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Акции', 'url' => array('stock/admin'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'FAQ-группы', 'url' => array('faqGroups/admin'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'FAQ', 'url' => array('faqAdmin/admin'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Выход', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
                    )
                        )
                );
                ?>
            </div><!-- mainmenu -->
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif; ?>

            <?php echo $content; ?>

            <div class="clear"></div>

            <div id="footer">
                Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
                All Rights Reserved.<br/>
                <?php echo Yii::powered(); ?>
            </div><!-- footer -->

        </div><!-- page -->

    </body>
</html>
