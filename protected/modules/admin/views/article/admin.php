<?php
/* @var $this ArticleController */
/* @var $model Article */

$this->breadcrumbs = array(
    'Статьи' => array('index'),
    'Управление',
);

$this->menu = array(
    array('label' => 'Список статей', 'url' => array('index')),
    array('label' => 'Создать статью', 'url' => array('create')),
);

?>

<h1>Управление статьями</h1>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => true,
        ));
?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'article-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'id' => 'id',
            'class' => 'CCheckBoxColumn',
            'selectableRows' => '50',
        ),
        'title',
        array(
            'name' => 'status',
            'header' => 'Статус',
            'filter' => array('1' => 'Разблокирован', '0' => 'Блокирован'),
            'value' => '($data->status=="1")?("Разблокирован"):("Блокирован")'
        ),
//		'anons',
//		'description',
//		'metaKey',
//		'metaDesc',
        /*
          'createTime',
          'url',
         */
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('article-grid');
    }
</script>

<div class="grid_3 push_2 omega">
    <?php echo CHtml::ajaxSubmitButton('Разблокировать', array('article/ajaxupdate', 'act' => 'doActive'), array('success' => 'reloadGrid'), array('class' => 'content_button', 'id' => 'button_left3')); ?>

</div>
<div class="grid_3 push_2 omega">
    <?php echo CHtml::ajaxSubmitButton('Блокировать', array('article/ajaxupdate', 'act' => 'doInactive'), array('success' => 'reloadGrid'), array('class' => 'content_button', 'id' => 'button_left')); ?>

</div>
<?php $this->endWidget(); ?>