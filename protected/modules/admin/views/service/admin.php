<?php
/* @var $this ServiceController */
/* @var $model Service */

$this->breadcrumbs = array(
    'Услуги' => array('admin'),
    'Управление услугой',
);

$this->menu = array(
    //array('label'=>'List Service', 'url'=>array('index')),
    array('label' => 'Добавление новой услуги', 'url' => array('create')),
);

?>

<h1>Управление услугой</h1>

<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->
<?php
$form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => true,
        ));
?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'service-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
         array(
            'id' => 'id',
            'class' => 'CCheckBoxColumn',
            'selectableRows' => '50',
        ),        
        'notes',
        'honorarium',
        'period',
        'volume',        
        array(
            'name' => 'status',
            'header' => 'Статус',
            'filter' => array('1' => 'Разблокирован', '0' => 'Блокирован'),
            'value' => '($data->status=="1")?("Разблокирован"):("Блокирован")'
        ),
        //'annotation',
        /*
          'description',
          'requirements',
          'expertID',
         */
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
<script>
    function reloadGrid(data) {
        $.fn.yiiGridView.update('service-grid');
    }
</script>

<div class="grid_3 push_2 omega">
    <?php echo CHtml::ajaxSubmitButton('Разблокировать', array('service/ajaxupdate', 'act' => 'doActive'), array('success' => 'reloadGrid'), array('class' => 'content_button', 'id' => 'button_left3')); ?>

</div>
<div class="grid_3 push_2 omega">
    <?php echo CHtml::ajaxSubmitButton('Блокировать', array('service/ajaxupdate', 'act' => 'doInactive'), array('success' => 'reloadGrid'), array('class' => 'content_button', 'id' => 'button_left')); ?>

</div>
<?php $this->endWidget(); ?>
