<?php
/* @var $this EmailTemplatesController */
/* @var $model EmailTemplates */

$this->breadcrumbs=array(
	'Шаблоны писем'=>array('index'),
	'Управление',
);

$this->menu=array(
	array('label'=>'Весь список шаблонов', 'url'=>array('index')),
	array('label'=>'Добавление', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#email-templates-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Управление шаблонами, для рассылки</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'email-templates-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'subject',
		'code',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
