<?php
/* @var $this EmailTemplatesController */
/* @var $model EmailTemplates */

$this->breadcrumbs=array(
	'Шаблоны писем'=>array('index'),
	'Добавление',
);

$this->menu=array(
	array('label'=>'Список шаблонов', 'url'=>array('index')),
	array('label'=>'Управление', 'url'=>array('admin')),
);
?>

<h1>Добавление шаблона</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>