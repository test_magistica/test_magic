<?php
/* @var $this EmailTemplatesController */
/* @var $dataProvider CActiveDataProvider */
$this->breadcrumbs=array(
	'Шаблоны писем',
);

$this->menu=array(
	array('label'=>'Добавление', 'url'=>array('create')),
	array('label'=>'Управление', 'url'=>array('admin')),
);
?>

<h1>Шаблоны писем</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
