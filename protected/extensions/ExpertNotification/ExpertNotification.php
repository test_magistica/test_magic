<?php

class ExpertNotification extends CWidget {

    public function init() {
        parent::init();
    }

    public function run() {
        $this->render("expertNotification");
    }

    protected function registerScripts() {
        $path = Yii::getPathOfAlias("chat");
        $am = Yii::app()->assetManager;
        if (!Yii::app()->clientScript->isScriptFileRegistered($am->getPublishedUrl($path . "/js/jquery-ui.js", CClientScript::POS_HEAD))) {
            Yii::app()->clientScript->registerScriptFile(
                    Yii::app()->assetManager->publish(
                            $path . DIRECTORY_SEPARATOR . "js" . DIRECTORY_SEPARATOR . "jquery-ui.js"
                    )
                    , CClientScript::POS_HEAD);
        }
        if (!Yii::app()->clientScript->isCssFileRegistered($am->getPublishedUrl($path . "/css/jquery-ui.css"))) {
            Yii::app()->clientScript->registerCssFile(
                    Yii::app()->assetManager->publish(
                            $path . DIRECTORY_SEPARATOR . "css" . DIRECTORY_SEPARATOR . "jquery-ui.css"
            ));
        }
    }

}
