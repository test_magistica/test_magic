<div id="exptnotification-sys" style="display: none;" title="Оповещение" class="chat-notification">
    <div id="ntf-content" class="ntf-content">
<!--        <p class="add-after"></p>
        <p class="chat-link"></p>
        <p class="chat-link-close"></p>-->
    </div>
</div>

<script type="text/javascript">
    var notification = function() {
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::app()->createUrl("/user/notification")?>",
            data: ({uid: <?php echo Yii::app()->user->id; ?>}),
            dataType: "json",
            success: function(data) {
	            //alert('user');
                if ( typeof data != "undefined" && data !== null ) {
                    if (data.empty == null) {
                        if (!$("#exptnotification-sys").dialog("isOpen")) {
                             $("#ntf-content").html("");
                            for(var i = 0; i < data.length; i++){
                                var a = $("<a>").attr("href", data[i].url).text(data[i].title);
                                var p = $("<p>").append(a);
                                $("#ntf-content").append(p);
                            }
                            $("#exptnotification-sys").dialog("open");
                        }
                    }// else {//if (typeof data.empty != "undefined") {
//                        if ($("#exptnotification-sys").dialog("isOpen")) {
//                            $("#exptnotification-sys").dialog("close");
//                        }
//                    }
                }
            },
            error: function(err) {
                console.log("error");
            }
        });
    }

    $("#exptnotification-sys").dialog({
        modal: true,
        autoOpen: false,
        draggable: false,
//        appendTo: ""
    });
    $(document).ready(function() {
        setInterval(notification, 1000);
        $("#exptnotification-sys a").on("click", function() {
            $("#exptnotification-sys").dialog("close");
        });
    });
</script>