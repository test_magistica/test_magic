<?php if (Yii::app()->user->isGuest): ?>
    <!-- Авторизация -->
    <div class="loginbox">
        <div class="ttl">Авторизация</div>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'newsletter-form',
            'action' => Yii::app()->createUrl('//site/login'),
            'enableAjaxValidation' => false,
        ));
        ?>
        <?php echo $form->textField($model, 'username', array('class' => 'inp', 'value' => 'Ваш E-mail', 'onblur' => 'if (this.value == "") {
                       this.value = "Ваш E-mail"
                   }', 'onfocus' => 'if (this.value == "Ваш E-mail")
                       this.value = "";')); ?>

        <?php echo $form->passwordField($model, 'password', array('class' => 'inp', 'value' => '*******', 'onblur' => 'if (this.value == "") {
                       this.value = "*******"
                   }', 'onfocus' => 'if (this.value == "*******")
                       this.value = "";')); ?>

        <ul>
            <li><?php echo CHtml::link('Регистрация', array('user/create')); ?></li>
            <li><?php echo CHtml::link('Забыли пароль? ', array('forgot/password')); ?></li>
        </ul>
        <?php echo CHtml::submitButton('вход', array('class' => 'submit')); ?>

        <?php $this->endWidget(); ?>
        <div class="icon"></div>
    </div>
    <!-- #Авторизация -->
<?php else: ?>
    <!-- Авторизация -->
    <div class="loginbox loginbox_expert">
        <?php echo CHtml::link('<span class="icon_mess" style="display:none;"></span>', array('expert/message/admin')); ?>
        <div class="ttl">Добро пожаловать,</div>
        <span><?php echo Controller::getUser(Yii::app()->user->id, 'name'); ?> <?php echo Controller::getUser(Yii::app()->user->id, 'surname'); ?></span>
        <?php if (Yii::app()->user->role == 'expert'): ?>
            <?php
            $online = Yii::app()->params['status'];
           //print_r(Yii::app());
            $userStatus = Controller::getUser(Yii::app()->user->id, 'online');
            ?>
            <ul class="onl">
                <?php foreach ($online as $key => $value): ?>
                    <li class="<?php echo $key; ?> <?php echo $userStatus == $key ? 'active' : ''; ?>"><?php echo CHtml::link($value, array('/expert/default/change/', 'id' => Yii::app()->user->id, 'status' => $key)); ?></li>
                <?php endforeach; ?>
            </ul>
            <div class="clr"></div>
        <?php endif; ?>

        <?php if (Yii::app()->user->role == 'user'): ?>
            <div class="balance"><?php echo CHtml::link("Пополнить баланс:", array('/payment'),array('class'=>'txt')); ?>  <?php echo CHtml::link("<b>".Controller::getUser(Yii::app()->user->id, 'bill')." руб.</b>", array('/payment')); ?></div>
            <div class="clr"></div>
        <?php endif; ?>
        <span class="pr">
            <?php $url = Yii::app()->user->role == 'expert' ? array('/expert') : array('//user/update', 'id' => Yii::app()->user->id); ?>
	        <?php
	        $t = Controller::getBookingCountExpert() + Controller::getBookingCount();
	        $b = $t > 0 ? '<b class="daffy">(' . $t . ')</b>' : ''; ?>
	        <?php echo CHtml::link('Личный кабинет ', $url).$b; ?>
	        <script>
		        $(document).ready(function(){
			        setInterval("updateMessShow()", 1000);

		        })
		        function updateMessShow(){
			        //alert($('.daffy').css('color'));
			        var clr = $('.daffy').css('color') == 'rgb(255, 255, 255)' ? 'rgb(255, 0, 0)' : 'rgb(255, 255, 255)';
			        $('.daffy').css('color', clr);
		        }

	        </script>
        </span>
    <!--        <span class="pr"><a href="#">сообщения</a></span>-->
        <?php echo CHtml::button('выход', array('submit' => array('//site/logout'), 'class' => 'submit')); ?>
        <div class="icon"></div>
    </div>
    <!-- #Авторизация -->
<?php endif; ?>
