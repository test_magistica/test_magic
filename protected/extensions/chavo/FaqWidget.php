<?php

class FaqWidget extends CWidget {

    public $groups;

    public function init() {
        $path = Yii::getPathOfAlias("ext.chavo");
        Yii::app()->clientScript->registerScriptFile(
                Yii::app()->assetManager->publish(
                        $path . "/js/jquery-ui-1.10.4.custom.min.js"
                )
                , CClientScript::POS_HEAD);
        Yii::app()->clientScript->registerScriptFile(
                Yii::app()->assetManager->publish(
                        $path . "/js/faq.js"
                )
                , CClientScript::POS_HEAD);
        $uipath = Yii::app()->assetManager->publish(
                $path . "/css"
        );
        Yii::app()->clientScript->registerCssFile(
                $uipath . "/faq/jquery-ui-1.10.4.custom.css"
        );
        Yii::app()->clientScript->registerCssFile(
                $uipath . "/faq.css"
        );
        parent::init();
    }

    public function run() {
//        echo "<pre>";
//        var_dump($this->groups);
//        die();
//        echo "</pre>";
        $this->render("chavo");
    }

    public function getGroupsMarkUp($pg = 0, $html = "") {
        $groups = FaqGroups::model()->with("nested", "faqs")->findAll(array(
            'condition' => "t.parent_group=" . $pg . " and t.status=" . FaqGroups::STATUS_SHOW,
        ));

        if (count($groups) == 0)
            return "";

        foreach ($groups as $group) {
            if (count($group->faqs) > 0) {
                $html .= CHtml::tag("h3", array("id" => "faq-g-" . $group->id), $group->gname);
            } else {
                $html .= CHtml::tag("h3", array(), $group->gname);
            }
            if (count($group->nested) > 0) {

                $t = $this->getGroupsMarkUp($group->id);
                if ($t !== "") {
                    $html .= "<div>";
                    $html .= $t;
                    $html .= "</div>";
                }
            } else {
//                $html .= CHtml::tag("h3", array("id" => "faq-g-" . $group->id), CHtml::tag("a", array(), $group->gname));
            }
        }
        return $html;
    }

    public function getQuestionsMarkUp($pg = 0) {
        $groups = FaqGroups::model()->with("nested", "faqs")->findAll(array(
            'condition' => "t.parent_group=" . $pg . " and t.status=" . FaqGroups::STATUS_SHOW,
        ));

        if (count($groups) == 0)
            return "";
        $html = "";
        foreach ($groups as $group) {
            if (count($group->nested) > 0) {
                $t1 = $this->getQuestionsMarkUp($group->id);
                if ($t1 !== "") {
                    $html .= $t1;
                }
            }
            if (count($group->faqs) > 0) {
                $html .= "<div class='chavo-accordion' id='faq-qag-" . $group->id . "'>";
                foreach ($group->faqs as $question) {
                    $t = CHtml::tag("h3", array("id" => "faq-q_" . $question->id), $question->fquestion);
                    $t .= "<div><p>";
                    $t .= CHtml::decode($question->fanswer);
                    $t .= "</p></div>";
                    $html .= $t;
                }
                $html .= "</div>";
            }
        }
        return $html;
    }

}
