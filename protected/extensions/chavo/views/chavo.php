<section>
    <div id="faq">
        <div itemprop="bredcrumb" class="bread">
            <a href="<?php echo Yii::app()->homeUrl; ?>">Экстрасенсы онлайн</a>
            <a href="<?php echo Yii::app()->createUrl("service"); ?>">FAQ</a>
        </div>
            <h1><?php echo Yii::t("app", "Часто задаваемые вопросы"); ?></h1>
        <div class="clr"></div>
        <div class="faq">
            <?php echo $this->getQuestionsMarkUp(); ?>
        </div> 
    </div>
</section>
<aside>
    <div class="faq">
        <div class="ttl">Группы вопросов</div>
        <div class="faq-separator"></div>
        <div class="chavo-groups">
            <?php echo $this->getGroupsMarkUp(); ?>
        </div>
    </div>
</aside>
