$(function() {
    var faq_id = "";
    var selectedGroupColor = "#C864AD";
    var icons = {
        header: "ui-icon-circle-arrow-e",
        activeHeader: "ui-icon-circle-arrow-s"
    };
    $(".chavo-accordion, .chavo-groups").accordion({
        icons: icons,
        heightStyle: "content",
        collapsible: true
    });
    function init() {
        $(".chavo-groups:first h3[id^='faq-g-']:first").click();
    }
    $("[id^='faq-g-']").on("click", function() {
        if ($(this).attr("id") != faq_id) {
            if (faq_id != "") {
                $("#"+faq_id).css("color","")
            }
            faq_id = $(this).attr("id");
        }
        //Get group id of a questions
        var gid = $(this).attr("id").split("-")[2];
        $(this).css({color: selectedGroupColor});
        //Hide all blocks with questions
        $("[id^='faq-qag-']").hide();
        //set focus to the first question of the selected question group (expand)
        $("[id^='faq-qag-" + gid + "'] h3[id^='faq-q_']:first").click();
        //Show questions with related group
        $("[id^='faq-qag-" + gid + "']").show();
    });
    init();
});