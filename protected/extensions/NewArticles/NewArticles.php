<?php
class NewArticles extends CWidget {
    /**
     * Expert ID
     * @var int 
     */
    public $expId = 0;
    /**
     * ID of articles which need exqlude from a showing
     * @var array 
     */
    public $exqludeArticle = array();
    public $count = 5;
    public function run() {
        $newArticles = Article::NewArticles($this->expId, $this->count, $this->exqludeArticle);
        $this->render("articles", array(
            'newArticles' => $newArticles
        ));
    }
}