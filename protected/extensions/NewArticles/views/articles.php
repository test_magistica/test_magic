<?php if(count($newArticles) > 0): ?>
<div class="articlebox">
    <div class="ttl"><?php echo Yii::t("NewArticles.article", "Articles") ?></div>
    <ul>
            
        <?php foreach ($newArticles as $article): ?>
                <?php $fullArticleUrl = Yii::app()->createUrl("article/view", array('id' => $article->id, "title" => $article->url));?>
            <li>
                <div class="img"><img alt="" src="/<?php echo Article::PATH_TO_IMAGE. $article->image; ?>"></div>
                <span><a href="<?php echo $fullArticleUrl;?>"><?php echo $article->title; ?></a></span>
                <em><?php echo ($article->authort->name)." ".  ($article->authort->profession); ?></em>
                <?php echo $article->anons; ?>
                <a class="more" href="<?php echo $fullArticleUrl; ?>"><?php echo Yii::t("NewArticles.article", "More"); ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
    <a class="all" href="<?php echo Yii::app()->createUrl("article/"); ?>"><?php echo Yii::t("NewArticles.article", "All articles"); ?></a>
</div>
<?php endif; ?>