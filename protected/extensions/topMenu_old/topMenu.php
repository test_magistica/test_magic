<?php

class topMenu extends CWidget {

    public function run() {
        $models = Category::model()->findAll();
        $menu = $this->recurse($models);
        $this->render('menu', array('menu' => $menu));
    }

    function recurse($categories, $parent = 0, $level = 0) {
        $ret = '<ul>';
        foreach ($categories as $category) {
            if ($category['parentID'] == $parent) {
                $ret .= '<li>' . CHtml::link($category['name'], array('/service/index/', 'categoryID' => $category['id']));
                if ($category['parentID'] > 0)
                    $ret .= $this->recurse($categories, $category['id'], $level + 1);
                $ret .= '</li>';
            }
        }
        return $ret . '</ul>';
    }

}