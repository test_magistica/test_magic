<?php

class Horoscope extends CWidget {

    public $_zodiacs = array(
        'aries' => 'Овен',
        'taurus' => 'Телец',
        'gemini' => 'Близнецы',
        'cancer' => 'Рак',
        'leo' => 'Лев',
        'virgo' => 'Дева',
        'libra' => 'Весы',
        'scorpio' => 'Скорпион',
        'sagittarius' => 'Стрелец',
        'capricorn' => 'Козерог',
        'aquarius' => 'Водолей',
        'pisces' => 'Рыбы'
    );

    public function run() {
        $this->parseByZodiac();
    }

    public function parseByZodiac() {
        if ($this->addToCache()) {

        	if (isset($_GET['zodiac']))
         		$zodiac= $_GET['zodiac'];
            else
                $zodiac= 'aries';

            $HoroscopeForTheWeek = Yii::app()->cache->get('HoroscopeForTheWeek');
            $EroticHoroscopeOnline = Yii::app()->cache->get('EroticHoroscopeOnline');
            $GeneralHoroscopeOnline = Yii::app()->cache->get('GeneralHoroscopeOnline');
            $BusinessHoroscopeOnline = Yii::app()->cache->get('BusinessHoroscopeOnline');
            $HealthHoroscopeOnline = Yii::app()->cache->get('HealthHoroscopeOnline');
            $LoveHoroscopeOnline = Yii::app()->cache->get('LoveHoroscopeOnline');
            //echo '<pre>';print_r($HoroscopeForTheWeek);exit;
            $resultWeek = '';
                $resultWeek .=
                        '<div id="Week-' . $zodiac . '" class="horo-hide">
                            <div class="post bottom-shadow">
                                <article class="shadowblock">
                                    <h3 class="lnk-title">
                                        <a href="#">Гороскоп на неделю</a>
                                        <span class="forecast-date">'.$HoroscopeForTheWeek['date']['@attributes']['weekly'].'.</span>
                                    </h3>
                                    <div class="clr"></div>
                                    <p>' . $HoroscopeForTheWeek[$zodiac]['common'] . '</p>
                                </article>
                            </div>
                         </div>';

            $resultDay = '';
            //echo '<pre>';print_r($GeneralHoroscopeOnline);exit;
                $resultDay .=
                        '<div id="Day-' . $zodiac . '"  class="horo-hide">
                            <div class="post bottom-shadow">
                               <article class="shadowblock">
                                <h3 class="lnk-title show-today">
                                    <a href="#">Сегодня</a>
                                    <span class="forecast-date">'.$GeneralHoroscopeOnline['date']['@attributes']['today'].'</span>
                                </h3>
                                <h3 class="lnk-title show-tomorrow">
                                    <a href="#">Завтра</a>
                                    <span class="forecast-date">'.$GeneralHoroscopeOnline['date']['@attributes']['tomorrow'].'</span>
                                </h3>
                                <div class="clr"></div>
                                <h4 class="horoscope-title">'.$this->_zodiacs[$zodiac].'</h4>
                                <img class="icon-horoscp" alt="calf" src="'.Yii::app()->request->baseUrl.'/images/hrcp-' . ucfirst($zodiac) . '.jpg">
                                <p class="today">
                                    ' . $GeneralHoroscopeOnline[$zodiac]['today'] . '
                                </p>
                                <p class="tomorrow">
                                    ' . $GeneralHoroscopeOnline[$zodiac]['tomorrow'] . '
                                </p>
                            </article>
                            </div>
                         </div>';


            $resultDay2 = '';
            //echo '<pre>';print_r($GeneralHoroscopeOnline);exit;
                $resultDay2 .=
                        '<div id="Day2-' . $zodiac . '" class="horo-hide">
                        <div class="post bottom-shadow">
                            <article class="shadowblock">
                                <h3 class="lnk-title"><a href="#">Эротический гороскоп</a><span class="forecast-date"> '.$EroticHoroscopeOnline['date']['@attributes']['today'].'</span></h3>
                                <div class="clr"></div>
                                <p class="today">
                                    ' . $EroticHoroscopeOnline[$zodiac]['today'] . '
                                </p>
                                <p class="tomorrow">
                                    ' . $EroticHoroscopeOnline[$zodiac]['tomorrow'] . '
                                </p>
                            </article>
                        </div>

                        <div class="post bottom-shadow">
                            <article class="shadowblock">
                                <h3 class="lnk-title"><a href="#">Бизнес гороскоп</a><span class="forecast-date"> '.$BusinessHoroscopeOnline['date']['@attributes']['today'].'</span></h3>
                                <div class="clr"></div>
                                <p class="today">
                                    ' . $BusinessHoroscopeOnline[$zodiac]['today'] . '
                                </p>
                                <p class="tomorrow">
                                    ' . $BusinessHoroscopeOnline[$zodiac]['tomorrow'] . '
                                </p>
                            </article>
                        </div>

                        <div class="post bottom-shadow">
                            <article class="shadowblock">
                                <h3 class="lnk-title"><a href="#">Гороскоп здоровья</a><span class="forecast-date"> '.$HealthHoroscopeOnline['date']['@attributes']['today'].'</span></h3>
                                <div class="clr"></div>
                                <p class="today">
                                    ' . $HealthHoroscopeOnline[$zodiac]['today'] . '
                                </p>
                                <p class="tomorrow">
                                    ' . $HealthHoroscopeOnline[$zodiac]['tomorrow'] . '
                                </p>
                            </article>
                        </div>

                        <div class="post bottom-shadow">
                            <article class="shadowblock">
                                <h3 class="lnk-title"><a href="#">Любовный гороскоп</a><span class="forecast-date"> '.$LoveHoroscopeOnline['date']['@attributes']['today'].'</span></h3>
                                <div class="clr"></div>
                                <p class="today">
                                    ' . $LoveHoroscopeOnline[$zodiac]['today'] . '
                                </p>
                                <p class="tomorrow">
                                    ' . $LoveHoroscopeOnline[$zodiac]['tomorrow'] . '
                                </p>
                            </article>
                        </div>
                    </div>';


            echo $resultWeek;
            echo $resultDay;
            echo $resultDay2;
        }
    }

    public function addToCache() {
        $value = Yii::app()->cache->get('HoroscopeForTheWeek');
        if ($value === false) {
            Yii::app()->cache->set('EroticHoroscopeOnline', $this->getEroticHoroscopeOnline(), 6 * 60 * 60);
            Yii::app()->cache->set('GeneralHoroscopeOnline', $this->getGeneralHoroscopeOnline(), 6 * 60 * 60);
            Yii::app()->cache->set('BusinessHoroscopeOnline', $this->getBusinessHoroscopeOnline(), 6 * 60 * 60);
            Yii::app()->cache->set('HealthHoroscopeOnline', $this->getHealthHoroscopeOnline(), 6 * 60 * 60);
            Yii::app()->cache->set('LoveHoroscopeOnline', $this->getLoveHoroscopeOnline(), 6 * 60 * 60);
            Yii::app()->cache->set('HoroscopeForTheWeek', $this->getHoroscopeForTheWeek(), 6 * 60 * 60);
        }
        return TRUE;
    }

    public function getHoroscopeForTheWeek() {
        $xml = simplexml_load_file('http://img.ignio.com/r/export/utf/xml/weekly/cur.xml');
        $array = json_decode(json_encode((array) $xml), 1);
        return $array;
    }

    public function getGeneralHoroscopeOnline() {
        $xml = simplexml_load_file('http://img.ignio.com/r/export/utf/xml/daily/com.xml');
        $array = json_decode(json_encode((array) $xml), 1);
        return $array;
    }

    public function getEroticHoroscopeOnline() {
        $xml = simplexml_load_file('http://img.ignio.com/r/export/utf/xml/daily/ero.xml');
        $array = json_decode(json_encode((array) $xml), 1);
        return $array;
    }

    public function getBusinessHoroscopeOnline() {
        $xml = simplexml_load_file('http://img.ignio.com/r/export/utf/xml/daily/bus.xml');
        $array = json_decode(json_encode((array) $xml), 1);
        return $array;
    }

    public function getHealthHoroscopeOnline() {
        $xml = simplexml_load_file('http://img.ignio.com/r/export/utf/xml/daily/hea.xml');
        $array = json_decode(json_encode((array) $xml), 1);
        return $array;
    }

    public function getLoveHoroscopeOnline() {
        $xml = simplexml_load_file('http://img.ignio.com/r/export/utf/xml/daily/lov.xml');
        $array = json_decode(json_encode((array) $xml), 1);
        return $array;
    }

}