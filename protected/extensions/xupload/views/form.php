<!-- The file upload form used as target for the file upload widget -->
<?php if ($this->showForm) echo CHtml::beginForm($this->url, 'post', $this->htmlOptions); ?>
<div class="row fileupload-buttonbar">
    <div class="span7">
        <!-- The fileinput-button span is used to style the file input field as button -->
        <span class="fileinput-button">
            <span>
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/user-avatar.jpg" alt="photo" />
            </span>
            <?php
            if ($this->hasModel()) :
                echo CHtml::activeFileField($this->model, $this->attribute, $htmlOptions) . "\n";
            endif;
            ?>
        </span>

    </div>
    <div class="span5">
        <!-- The global progress bar -->
        <div class="progress progress-success progress-striped active fade">
            <div class="bar" style="width:0%;"></div>
        </div>
    </div>
</div>
<!-- The loading indicator is shown during image processing -->
<div class="fileupload-loading"></div>
<br>
<div class="row fileupload-buttonbar">
    <div class="span7">
        <!-- The table listing the files available for upload/download -->
        <?php if ($this->multiple) { ?>
            <button type="submit" class="btn btn-primary start">
                <i class="icon-upload icon-white"></i>
                <span>Загрузить все файлы</span>
            </button>
            <!--		<button type="reset" class="btn btn-warning cancel">
                                    <i class="icon-ban-circle icon-white"></i>
                                    <span>Cancel upload</span>
                            </button>
                            <button type="button" class="btn btn-danger delete">
                                    <i class="icon-trash icon-white"></i>
                                    <span>Delete</span>
                            </button>-->
            <!--		<input type="checkbox" class="toggle">-->
        <?php } ?>
        <br /><br />
    </div>
</div>
<table class="table table-striped">
    <tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery">
        <?php foreach ($images as $image): ?>
            <tr class="template-download fade in" style="height: 81px;">
                <td class="preview">
                    <a rel="lightbox[gallery]" download="<?php echo $image['image']; ?>" rel="gallery" title="<?php echo $image['image']; ?>" href="<?php echo Yii::app()->request->baseUrl . '/uploads/user/' . $image['image']; ?>"><img width="60" src="<?php echo Yii::app()->request->baseUrl . '/uploads/user/' . $image['image']; ?>"></a>
                </td>
                <td class="delete">
                    <a href="<?php echo Yii::app()->createUrl('/user/setphoto/') ?>?file=<?php echo $image['image']; ?>">
                        <i class="icon-trash icon-white"></i>
                        <span>На аватар</span>
                    </a>
        <!--            <a href="<?php echo Yii::app()->createUrl('/user/updatephoto/') ?>?file=<?php echo $image['image']; ?>">
                        <i class="icon-trash icon-white"></i>
                        <span>Редактировать</span>
                    </a>-->
                </td>
                <td class="delete">
                    <button data-url="<?php echo Yii::app()->createUrl('/user/upload/') ?>?_method=delete&file=<?php echo $image['image']; ?>" data-type="POST" class="btn btn-danger">
                        <i class="icon-trash icon-white"></i>
                        <span>Удалить</span>
                    </button>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php if ($this->showForm) echo CHtml::endForm(); ?>
