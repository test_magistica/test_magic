<!-- The file upload form used as target for the file upload widget -->
<?php if ($this->showForm) echo CHtml::beginForm($this->url, 'post', $this->htmlOptions); ?>
<div class="row fileupload-buttonbar">
	<div class="span7">
		<!-- The fileinput-button span is used to style the file input field as button -->
        <span class="fileinput-button">
            <span>
                <a href="javascript: void(0);">Прикрепить фотографию</a>
            </span>
	        <?php
	        if ($this->hasModel()) :
		        echo CHtml::activeFileField($this->model, $this->attribute, $htmlOptions) . "\n";
	        endif;
	        ?>
        </span>

	</div>
	<div class="span5">
		<!-- The global progress bar -->
		<div class="progress progress-success progress-striped active fade">
			<div class="bar" style="width:0%;"></div>
		</div>
	</div>
</div>
<!-- The loading indicator is shown during image processing -->
<div class="fileupload-loading"></div>
<br>
<div class="row fileupload-buttonbar">
	<div class="span7">
		<!-- The table listing the files available for upload/download -->
		<?php if ($this->multiple) { ?>
		<button type="submit" class="btn btn-primary start">
			<i class="icon-upload icon-white"></i>
			<span>Загрузить все файлы</span>
		</button>
		<!--		<button type="reset" class="btn btn-warning cancel">
											<i class="icon-ban-circle icon-white"></i>
											<span>Cancel upload</span>
									</button>
									<button type="button" class="btn btn-danger delete">
											<i class="icon-trash icon-white"></i>
											<span>Delete</span>
									</button>-->
		<!--		<input type="checkbox" class="toggle">-->
		<?php } ?>
	</div>
</div>
<table class="table table-striped">
	<tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery">
	</tbody>
</table>
<?php if ($this->showForm) echo CHtml::endForm(); ?>
