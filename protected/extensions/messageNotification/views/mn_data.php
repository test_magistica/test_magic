<div id="msg-notif-sys" style="display: none;" title="Внимание!" class="">
    <div id="msg-notif-content" class=""></div>
</div>

<script type="text/javascript">
<?php echo $this->nfComponent->getJSObjectData() . "\n"; ?>
    function MessageNotificationManager() {
        var self = this;
        var messages;

        self.showMessage = function(msg) {
            $("#msg-notif-content").html(msg);
            $("#msg-notif-sys").dialog('open');
        }
        self.showMessages = function(){
            for (var i = 0; i < messages.length; i++) {
                self.showMessage(messages[i]);
            }
        }
        var init = function() {
            var data = window.<?php echo $this->nfComponent->sessionKey; ?>;
            if (typeof data !== "undefined") {
                messages = [];
                for (var key in data) {
                    messages.push(data[key]);
                }
                $("#msg-notif-sys").dialog({
                    modal: true,
                    autoOpen: false,
                    draggable: false,
                });
                self.showMessages();
            }
        }

        init();
    }
    $(document).ready(function() {
        $.NotificationManager = new MessageNotificationManager();
    });
</script>