<?php

/**
 * Виджет для системных и прочих уведомлений пользователя.
 * Для отображения сообщений используются модальные окна.
 * Библиотека JQueryUI
 */
class MessageNotification extends CWidget {

    public $nfComponent;

    public function init() {
        // Компонент содержащий сообщения для пользователя
        $this->nfComponent = new MessageNotificationComponent();
        Yii::app()->clientScript->registerScriptFile(
                Yii::app()->assetManager->publish(
                        $this->getViewPath() . "/../js/jquery-ui.js"
                )
                , CClientScript::POS_HEAD);
        $uipath = Yii::app()->assetManager->publish(
                $this->getViewPath() . "/../css"
        );
        Yii::app()->clientScript->registerCssFile(
                $uipath . "/jquery-ui.css"
        );
    }

    public function run() {
        $this->render("mn_data");
    }

}
