<div class="profile_wrap">
    
    <div class="user_status">
            <?php $photo = Userphoto::model()->find("`userID` = '" . Yii::app()->user->id . "' AND `main`=1"); ?>
            <img src="<?php echo $photo['fullPath']; ?>" alt="" />
        <h2><?php echo Controller::getUser(Yii::app()->user->id, 'name'); ?> <?php echo Controller::getUser(Yii::app()->user->id, 'surname'); ?></h2>
<!--        <span class="online">онлайн</span> -->
        <!--   you may change status class: online, busy, autonomous   -->
    </div>


    <?php if (Yii::app()->user->role == 'expert') : ?>
        <ul class="profile_list">
            <li><?php echo CHtml::link('Кабинет эксперта<i class="i_profile"></i>', array('expert/')); ?></li>
            <li><?php echo CHtml::link('Мои статьи<i class="i_post"></i>', array('expert/article/admin')); ?></li>
            <?php $c = Controller::getMessageCount() > 0 ? '(' . Controller::getMessageCount() . ')' : ''; ?>
            <li><?php echo CHtml::link('Мои сообщения<i class="i_msg"></i> <span id="shoutbox"></span>', array('expert/message/admin')); ?></li>
            <li><?php echo CHtml::link('Мои услуги<i class="i_service"></i> ', array('expert/service/admin', 'id' => Yii::app()->user->id)); ?></li>
            <?php $b = Controller::getBookingCountExpert() > 0 ? '(' . Controller::getBookingCountExpert() . ')' : ''; ?>
            <li><?php echo CHtml::link('Мои заказы<i class="i_order"></i> ' . $b . '', array('expert/booking/admin/')); ?></li>
            <li><?php echo CHtml::link('Мой профайл<i class="i_profile"></i>', array('user/update/', 'id' => Yii::app()->user->id)); ?></li>
            <li><?php echo CHtml::link('Сменить пароль <i class="i_lock"></i>', array('user/password/')); ?></li>
            <li><?php echo CHtml::link('Мои фотографии<i class="i_gallery"></i>', array('user/photo/', 'id' => Yii::app()->user->id)); ?></li>
            <li><?php echo CHtml::link('Мои чаты<i class="i_msg"></i> <span id="shoutbox"></span>', array('chat/')); ?></li>
            <li><?php echo CHtml::link('Выход (' . Yii::app()->user->name . ')', array('/site/logout', 'id' => Yii::app()->user->id)); ?></li>
        </ul>
    <?php else : ?>
        <ul class="profile_list">
            <li><?php echo CHtml::link('Мой профайл<i class="i_profile"></i>', array('user/update/', 'id' => Yii::app()->user->id)); ?></li>
            <li><?php echo CHtml::link('Сменить пароль <i class="i_lock"></i>', array('user/password/')); ?></li>
            <li><?php echo CHtml::link('Мои фотографии<i class="i_gallery"></i>', array('user/photo/', 'id' => Yii::app()->user->id)); ?></li>
            <?php $c = Controller::getMessageCount() > 0 ? '(' . Controller::getMessageCount() . ')' : ''; ?>
            <li><?php echo CHtml::link('Мои сообщения<i class="i_msg"></i> <span id="shoutbox"></span>', array('message/')); ?></li>
            <?php $b = Controller::getBookingCount() > 0 ? '(' . Controller::getBookingCount() . ')' : ''; ?>
            <li><?php echo CHtml::link('Мои заказы<i class="i_order"></i> ' . $b . '', array('myBooking/')); ?></li>
            <li><?php echo CHtml::link('Пополнить счет <i class="i_rates"></i>', array('/payment')); ?></li>
            <li><?php echo CHtml::link('Мои чаты<i class="i_msg"></i> <span id="shoutbox"></span>', array('chat/')); ?></li>
            <li><?php echo CHtml::link('Мои вопросы<i class="i_msg"></i> <span id="shoutbox"></span>', array('question/myProfile')); ?></li>
            <li><?php echo CHtml::link('Статистика<i class="i_schedule"></i>', array('statistic/')); ?></li>
        </ul>
    <?php endif; ?>

    <div class="clr"></div>
</div>
