<?php

class StockWidget extends CWidget {

    public function run() {
        $c = new CDbCriteria();
        $c->order = 'id desc';
        $c->limit = 2;
        $models = Stock::model()->findAll($c);

        $this->render('stock', array('models' => $models));
    }

}