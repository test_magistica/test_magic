<?php

class topMenu extends CWidget {

    public function run() {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.parentID=0';
        $criteria->with = 'childrenMenu';
        $criteria->order =  't.id ASC';               
        $models = Category::model()->findAll($criteria);
//        echo "<pre>";var_dump(count($models[1]->childrenMenu));die();
        $menu = $this->recurse($models);
        $this->render('menu', array('menu' => $menu));
    }

    function recurse($categories) {
            $ret = '<ul>';
            foreach ($categories as $category) {
                $ret .= '<li>' . CHtml::link($category['name'], $category['url']);
                if(isset($category->childrenMenu) && count($category->childrenMenu) > 0){
                    $ret .= '<ul>';
                    foreach ($category->childrenMenu as $childMenu) {
                        $ret .= '<li>' . CHtml::link($childMenu->name, $childMenu->url);
                        $c = Yii::app()->db->createCommand("select count(1) as c from category where parentID=".$childMenu->id)->queryScalar();
                        if($c > 0){
                            $children = Category::model()->with('childrenMenu')->findAll('parentID='.$childMenu->id);
                            $ret .= $this->recurse($children);
                        }
                        else {

                        }
                        $ret .= '</li>';
                    }
                    $ret .= '</ul>';
                }
                $ret .= '</li>';
            }
            return $ret . '</ul>';
        
    }
    
    function recurse2($categories, $parent = 0, $level = 0) {
        $ret = '<ul>';
        foreach ($categories as $category) {
            if ($category['parentID'] == $parent) {
                $ret .= '<li>' . CHtml::link($category['name'], array('/service/index/', 'categoryID' => $category['id']));
                if ($category['parentID'] > 0)
                    $ret .= $this->recurse($categories, $category['id'], $level + 1);
                $ret .= '</li>';
            }
        }
        return $ret . '</ul>';
    }

}