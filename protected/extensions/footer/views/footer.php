<?php

$diviations = Divination::model()->findAll(array(
    'condition' => "t.id in (2,3,5,6,7)",
    'order' => "t.id ASC",
    'select' => "t.id, title",
        ));
$services = Service::model()->findAll(
        array(
            "condition" => "t.id in (1,25)",
            "order" => "t.id ASC",
            "select" => "t.id, notes",
        )
);
if (count($services) != 0 && count($diviations) != 0) {
    $c = new Controller("Controller");
    echo CHtml::link(
            Yii::t('app', "Гадание онлайн бесплатно по книге судеб"), Yii::app()->createUrl(
                    "divinations/view", array(
                'id' => $diviations[1]->id,
                'title' => $c->getCHPU($diviations[1]->title)
                    )
            )
    );
    echo CHtml::link(
            Yii::t('app', "Виртуально Online гадание Египетский оракул"), Yii::app()->createUrl(
                    "divinations/view", array(
                'id' => $diviations[0]->id,
                'title' => $c->getCHPU($diviations[0]->title)
                    )
            )
    );
    echo CHtml::link(
            Yii::t('app', "Интернет гадание на желание"), Yii::app()->createUrl(
                    "divinations/view", array(
                'id' => $diviations[4]->id,
                'title' => $c->getCHPU($diviations[4]->title)
                    )
            )
    );
    echo CHtml::link(
            Yii::t('app', "Гадание по книге перемен"), Yii::app()->createUrl(
                    "divinations/view", array(
                'id' => $diviations[2]->id,
                'title' => $c->getCHPU($diviations[2]->title)
                    )
            )
    );
    echo "<br />";
    echo CHtml::link(
            Yii::t('app', "Любовная совместимость партнеров"), Yii::app()->createUrl(
                    "service/view", array(
                'id' => $services[0]->id,
                'title' => $c->getCHPU($services[0]->notes)
                    )
            )
    );
    echo CHtml::link(
            Yii::t('app', "Астрологический прогноз совместимости"), Yii::app()->createUrl(
                    "service/view", array(
                'id' => $services[1]->id,
                'title' => $c->getCHPU($services[1]->notes)
                    )
            )
    );
    echo CHtml::link(
            Yii::t('app', "Погадать на картах марьсельское таро онлайн"), Yii::app()->createUrl(
                    "divinations/view", array(
                'id' => $diviations[3]->id,
                'title' => $c->getCHPU($diviations[3]->title)
                    )
            )
    );
    echo CHtml::link(
            Yii::t('app', "Карта сайта"), Yii::app()->createUrl(
                    "site/showStaticPage", array(
                'curl' => 'karta-sajjta',
                    )
            )
    );
}
?>

