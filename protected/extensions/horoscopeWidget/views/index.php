<div class="goroskopbox pos-r">
    <div class="title">
        <span>Гороскоп</span>
        <div class="selectbox">
            <select class="selectBlock" id="horo-category">
                <option value="LoveHoroscopeOnline">Любовный</option>
                <option value="HealthHoroscopeOnline">Здоровье</option>
                <option value="BusinessHoroscopeOnline">Бизнес</option>
            </select>
        </div>
    </div>
    <div class="clr"></div>

    <ul class="horo_icons">
        <?php
        $_zodiacs = array(
            'aries' => 'Овен',
            'taurus' => 'Телец',
            'gemini' => 'Близнецы',
            'cancer' => 'Рак',
            'leo' => 'Лев',
            'virgo' => 'Дева',
            'libra' => 'Весы',
            'scorpio' => 'Скорпион',
            'sagittarius' => 'Стрелец',
            'capricorn' => 'Козерог',
            'aquarius' => 'Водолей',
            'pisces' => 'Рыбы'
        );
        $a = 0;
        foreach ($_zodiacs as $key => $value):
            $a++;
            ?>
            <li data="<?php echo $key; ?>" class="ic<?php echo $a.($a == 1 ? ' current' : ''); ?>"><a href="#" title="<?php echo $value; ?>"></a></li>
        <?php endforeach; ?>

    </ul>
<script>
    $(document).ready(function() {

	    $('#horo-category').on('change', function(){getHoroscope($('.horo_icons li.current').attr('data'), this.value);});

        getHoroscope('aries', $('#horo-category').val());

            function getHoroscope(zodiak, category) {

                $.post("<?php echo Yii::app()->request->baseUrl . '/horoscope/ajax'; ?>",
                        {zodiak: zodiak, category: category},
                function(data) {
                    j = jQuery.parseJSON(data);
                    $('#horo-text').html(j.text);
                    $('#horo-date').html(j.date);
                    $('#horo-zodiak').html(j.zodiak);
                    $('#horo-image').attr('src', j.image);
                });
            }

            $('.horo_icons li').on('click', function() {
                getHoroscope($(this).attr('data'), $('#horo-category').val());
	            $('.horo_icons li').removeClass('current');
	            $(this).addClass('current');
	            return false;
            });
        });
    </script>
    <div class="box1">
        <p>
            <span id="horo-zodiak">Овен</span>
            <b id="horo-date">5.01.2013</b>
        </p>
        <img id="horo-image" src="<?php echo Yii::app()->request->baseUrl; ?>/images/hrcp-Aries.jpg" alt="" />
    </div>

    <div class="box2" id="horo-text">
        Сегодня будут ярко проявляться целеустремлённость и находчивость Овнов. 
        С точки зрения работы и бизнеса это неплохо, а вот отношения с близкими 
        могут пострадать: уж очень трудно переспорить Вас или высказать свою 
        точку зрения.
    </div>
    <?php if(!$is_gortoday) : ?>
    <div class="bline-goroskopbox pos-a" style="background: url('/images/bline-horo.png') no-repeat left bottom transparent;display:inline-block;width:294px;height:20px;">&nbsp;</div>
    <?php endif; ?>
</div>
