<?php

class StaticServicesWidget extends CWidget {

    public function run() {

	    $criteria = new CDbCriteria;
	    $criteria->group = 'serviceID';
        $staticServices = ExpertStaticServices::model()->findAll($criteria);

        $this->render('staticServices', array('staticServices' => $staticServices));
    }

}