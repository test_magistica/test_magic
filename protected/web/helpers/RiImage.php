<?php 

class RiImage 
{

    public $scr_photo;             // Маленькая фотография (тип param_image)
    public $small_photo;           // Маленькая фотография (тип param_image)
    public $big_photo;             // Большая фотография (тип param_image)
    public $file;                  // Компонента для загрузки файлов
    public $is_ajax_css=false;     // Подключать файл стилей ajax
    public $folder_name='images';  // Название каталога в хранилище файлов, куда будет сохраняться информация
    public $is_ajax_reg=false;     // Использовать ajax при генерации картинок

    // Для внутреннего использования
    protected $ext;

  	function __construct()
	{	
		$this->init();
		$this->file=new RiFile();
	}

    function init() 
    {
      $this->small_photo=& new Param_Image($this);
      $this->big_photo=& new Param_Image($this);
      $thia->scr_photo=& new Param_Image($this);
    }

    /** Устанавливает необходимый размер фотографии
     * 
     * @param $file_name - название файла
     * @param $param - объект с параметрами изображения. Имеет тип param_image
     */
    function resize( $file_name, $file_rez=null, $param=null )
    {
         // Получаем размер изображения
         $size_src = getimagesize($file_name);
         
         if ($size_src == false) 
         {
            $this->cxi->d("По указаному пути: ".$file_name." нет файл источника </br>");
            return false;
         }

    	$param->quality=(int)$param->quality; // приводим качество к инту, чтобы не было проблем
    	$param->width=(int)$param->width;     // тоже и с размерами
	    $param->height=(int)$param->height;

	    // если качество меньше 1 или больше 99, тогда ставим его 100
	    if($param->quality<1 || $param->quality>99){
		    $param->quality=100;
 	    }

        // Создаем каталог
        $this->file->create_dir($file_rez,0775, true);

    	// Определяем исходный формат по MIME-информации, предоставленной
	    // функцией getimagesize, и выбираем соответствующую формату
	    // imagecreatefrom-функцию.
	$format = strtolower(substr($size_src['mime'], strpos($size_src['mime'], '/')+1));
    	$icfunc = "imagecreatefrom" . $format;

        // Проверяем является ли файл графическим
    	if (!function_exists($icfunc)) {
            echo "Файл источник: ".$src." не являеться графическим </br>";
            return false;
        }


      	$isrc = $icfunc($file_name);

        ///////////////////////////////////////////////////////////////////////
        // Назначение параметров машбирования по умолчанию
        ///////////////////////////////////////////////////////////////////////
        $top_src=0;               // Расположение по вертикали на холсте
        $left_src=0;              // Расположение по горизонтали на холсте
        $src_x=0;                 // Центрирование изображения по х
        $src_y=0;                 // Центрирование изображения по y
        $src_width=$size_src[0];  // Ширина области картинки
        $src_height=$size_src[1]; // Высота области картинки
        ///////////////////////////////////////////////////////////////////////

        // Определяем пропорцию по ширине. Если ширина не задана (null), тогда пропоцию определяем по высоте.
        // Если высота тоже не задане - не делаем масштабирование
        if (($param->width<>null)and($param->width<>0)and($param->height<>null)and($param->height<>0)){

                if($param->isdisp) {

                   $width_img=$param->width;
                   $height_img=$param->height;
                   $width_area=$width_img;   // Ширина области фонирования
                   $height_area=$height_img; // Высота области фонирования

                } else {

                   $koef_width=$param->width/$size_src[0];
                   $koef_height=$param->height/$size_src[1];

                   if (($param->smootdisp=='fill') or ($param->smootdisp=='none')) {

                       // При заливке фоном
                       if ($param->smootdisp=='fill') {

                           $koef = min($koef_width, $koef_height);
                           $onwidth = ($koef_width == $koef);
                           $width_img   = $onwidth  ? $param->width  : floor($size_src[0] * $koef);
                           $height_img  = !$onwidth ? $param->height : floor($size_src[1] * $koef);
                           $src_x    = 0 ;
                           $src_y    = 0 ;
                           $width_area=$param->width;   // Ширина области фонирования
                           $height_area=$param->height; // Высота области фонирования
                           $src_width = $size_src[0];
                           $src_height= $size_src[1];
                           $isfill=true;                    // Заливать фоном
                       // Без заливки фоном
                       } else {

                           $koef = min($koef_width, $koef_height);
                           $onwidth = ($koef_width == $koef);
                           $width_img   = $onwidth  ? $param->width  : floor($size_src[0] * $koef);
                           $height_img  = !$onwidth ? $param->height : floor($size_src[1] * $koef);
                           $src_width = $size_src[0];
                           $src_height= $size_src[1];
                           $width_area=$width_img;   // Ширина области фонирования
                           $height_area=$height_img; // Высота области фонирования
                           $isfill=false;                   // Не заливать фоном
                       }


                   } else if ($param->smootdisp=='cutt') {

                       $koef = max($koef_width, $koef_height);
                       $onwidth = ($koef_width == $koef);
                       $src_x    = $onwidth ? 0 : floor(($size_src[0]-floor($param->width/$koef)) / 2);
                       $src_y    = !$onwidth ? 0 : floor(($size_src[1]-floor($param->height/$koef)) / 2);
                       $src_width = floor($param->width/$koef);  // Ширина обрезанная
                       $src_height= floor($param->height/$koef); // Высота обрезанная
                       $width_img =$param->width;
                       $height_img=$param->height;
                       $width_area=$width_img;   // Ширина области фонирования
                       $height_area=$height_img; // Высота области фонирования
                       $isfill=false;

                   } else if ($param->smootdisp=="apro") {
                       $koef = max($koef_width, $koef_height);
                       $onwidth = ($koef_width == $koef);
                       $width_img   = $onwidth  ? $param->width  : floor($size_src[0] * $koef);
                       $height_img  = !$onwidth ? $param->height : floor($size_src[1] * $koef);
                       $src_x    = 0 ;
                       $src_y    = 0 ;
                       $width_area=  (($width_img<$param->width) ? $param->width : $width_img) ;   // Ширина области фонирования
                       $height_area= (($height_img<$param->height) ? $param->height : $height_img) ; // Высота области фонирования
                       $src_width = $size_src[0];
                       $src_height= $size_src[1];
                       $isfill=true;                    // Заливать фоном
                   }

                   // Погашение диспропорции
                   if ($isfill) {

                       // Выравнивание по горизонтали
                       if (trim($param->align_img)=="left" or trim($param->align_img)==='') {
                           $left_src = 0;
                       } else if (trim($param->align_img)=="center") {
                           $left_src = $onwidth  ? 0 : floor(($width_area - $width_img) / 2);
                       } else if (trim($param->align_img)=="right") {
                           $left_src = $onwidth  ? 0 : ($width_area - $width_img);
                       }
                       // Выравнивание по вертикали
                       if (trim($param->valign_img)=="top" or trim($param->valign_img)==='') {
                           $top_src=0;
                       } else if (trim($param->valign_img)=="middle") {
                           $top_src     = !$onwidth ? 0 : floor(($height_area - $height_img) / 2);
                       } else if (trim($param->valign_img)=="bottom") {
                           $top_src     = !$onwidth ? 0 : ($height_area - $height_img) ;
                       }
                   }

                }

        }else{

                if (($param->width<>null)and($param->width<>0)){
                    $koef=$param->width/$size_src[0];

                }else if (($param->height<>null)and($param->height<>0)){
                    $koef=$param->height/$size_src[1];
                }else{
                    $koef=1;
                }

                $width_img=$size_src[0]*$koef;
                $height_img=$size_src[1]*$koef;

                // Проверяем граничные размеры
                if (($param->max_width<>null)and($param->max_width<$width_img)){                    
                    
                   $koef=$param->max_width/$size_src[0];
                   
                   $width_img=$size_src[0]*$koef;
                   $height_img=$size_src[1]*$koef;
                   
                }

                if (($param->max_height<>null)and($param->max_height<$height_img)){
                    $koef=$param->max_height/$size_src[1];

                    $width_img=$size_src[0]*$koef;
                    $height_img=$size_src[1]*$koef;
                }

                $src_width=$size_src[0];
                $src_height=$size_src[1];
                $width_area=$width_img;   // Ширина области фонирования
                $height_area=$height_img; // Высота области фонирования

        }

   	$idest = imagecreatetruecolor($width_area, $height_area);
       	imagefill($idest, 0, 0, $param->background);        
       	imagecopyresampled($idest, $isrc, $left_src, $top_src, $src_x, $src_y, $width_img, $height_img, $src_width, $src_height);
        //imagecolortransparent ($dest, $param->background);

        if ($file_rez==null){
          $file_rez=$file_name;
        }

        imagejpeg($idest, $file_rez, $param->quality);

      	imagedestroy($isrc);
      	imagedestroy($idest);

    }
    

  /**
   * Функция img_resize(): генерация thumbnails
   *  $src             - имя исходного файла
   *  $logo            - имя логотипа
   *  $dest            - имя генерируемого файла
   *  $width, $height  - ширина и высота генерируемого изображения, в пикселях
   *  Необязательные параметры:
   *  $rgb             - цвет фона, по умолчанию - белый
   *  $quality         - качество генерируемого JPEG, по умолчанию - максимальное (100)
   *  $vid             - 1 - указывает, если картинка получается меньше указанных размеров, тогда заполнить избыток фоном.
   *                     2 - налажывает на рисунок логотип
   */ 
  function img_resize($src, $logo, $dest, $width, $height,  $quality=100, $rgb=0xFFFFFF, $logo_intensite=0, $vid_scr=0, $vid_dest=0 )
  {
      if (!file_exists($src))
      {
          echo "По указаному пути: ".$src." нет файл источника </br>";
          return false;
      }
      
      $size_src = getimagesize($src);
      
      if ($size_src === false)
      {
            echo "По указаному пути: ".$src." нет файл источника </br>";
            return false;
      }
      
      $quality=(int)$quality; // приводим качество к инту, чтобы не было проблем
      $width=(int)$width;     // тоже и с размерами
      $height=(int)$height;

      // если качество меньше 1 или больше 99, тогда ставим его 100
      if($quality<1 || $quality>99)
      {
         $quality=100;
      }

      // если вдруг не пришла высота или ширина, тогда размеры будем оставлять как размеры самой картинки, без уменьшения
      if(!$width || !$height)
      {
        $width=$size_src[0];
        $height=$size_src[1];
      }

      // если реальная ширина и высота рисунка меньше, чем размеры до которых надо уменьшить,
      // тогда уменьшаемые размеры станут равны реальным размерам, чтобы не произошло увеличение
      //if($size_src[0]<$width && $size_src[1]<$height)
      //{
	  //  $width=$size_src[0];
	  //  $height=$size_src[1];
      //}

      // Определяем исходный формат по MIME-информации, предоставленной
      // функцией getimagesize, и выбираем соответствующую формату
      // imagecreatefrom-функцию.
      $format = strtolower(substr($size_src['mime'], strpos($size_src['mime'], '/')+1));
      $icfunc = "imagecreatefrom" . $format;

      if (!function_exists($icfunc))
      {
            echo "Файл источник: ".$src." не являеться графическим </br>";
            return false;
      }

      $x_ratio = $width / $size_src[0];
      $y_ratio = $height / $size_src[1];

      $isrc = $icfunc($src);

      switch ($vid_scr) {
      case 1:
        // чистое изменение размеров картинки
        $ratio       = min($x_ratio, $y_ratio);
        $use_x_ratio = ($x_ratio == $ratio);
       	$new_width_src   = $use_x_ratio  ? $width  : floor($size_src[0] * $ratio);
        $new_height_src  = !$use_x_ratio ? $height : floor($size_src[1] * $ratio);
        $new_left_src    = $use_x_ratio  ? 0 : floor(($width - $new_width_src) / 2);
        $new_top_src     = !$use_x_ratio ? 0 : floor(($height - $new_height_src) / 2);
        // чистое изменение размеров картинки
   	$new_left_src    = 0;
       	$new_top_src     = 0;
        $src_x    = 0 ;
        $src_y    = 0 ;
   	$idest = imagecreatetruecolor($new_width_src, $new_height_src);
        $src_width = $size_src[0];
        $src_height= $size_src[1];
        break;
      case 2:
        // так создается картинка узаканного размера,
        // а все где картинки нет, заполнится фоном.
        // чтобы так создавать картинку, нижнюю строку надо удалить,
        // а с этой снять комментарии
        $ratio       = min($x_ratio, $y_ratio);
        $use_x_ratio = ($x_ratio == $ratio);
       	$new_width_src   = $use_x_ratio  ? $width  : floor($size_src[0] * $ratio);
        $new_height_src  = !$use_x_ratio ? $height : floor($size_src[1] * $ratio);
        $new_left_src    = $use_x_ratio  ? 0 : floor(($width - $new_width_src) / 2);
        $new_top_src     = !$use_x_ratio ? 0 : floor(($height - $new_height_src) / 2);
        $src_x    = 0 ;
        $src_y    = 0 ;
       	$idest = imagecreatetruecolor($width, $height);
        $src_width = $size_src[0];
        $src_height= $size_src[1];
        break;
     default:
        // подрезание фотографии
        $ratio       = max($x_ratio, $y_ratio);
        $use_x_ratio = ($x_ratio == $ratio);
        // чистое изменение размеров картинки
        $new_left_src    = 0;
       	$new_top_src     = 0;
        $src_x    = $use_x_ratio  ? 0 : floor(($size_src[0]-floor($width/$ratio)) / 2);
        $src_y    = !$use_x_ratio ? 0 : floor(($size_src[1]-floor($height/$ratio)) / 2);
        $src_width = floor($width/$ratio);
        $src_height= floor($height/$ratio);
        $new_width_src=$width;
        $new_height_src=$height;
   		$idest = imagecreatetruecolor($width, $height);
        break;
     }

       	imagefill($idest, 0, 0, $rgb);
      	imagecopyresampled($idest, $isrc, $new_left_src, $new_top_src, $src_x, $src_y, $new_width_src, $new_height_src, $src_width, $src_height);

      	imagedestroy($isrc);

        // Наложение логотипа
        if ($logo<>"") {  // Проверка на существование параметра
          $size_logo = getimagesize($logo);
          $format = strtolower(substr($size_logo['mime'], strpos($size_logo['mime'], '/')+1));
          $lcfunc = "imagecreatefrom" . $format;
          if ($size_logo === false)
          {
            echo "Путь к файлу логотипа: ".$logo." указан не коректно </br>";
            return false;
          }
          if (!function_exists($lcfunc)) {
             echo "Тип файл логотипа: ".$logo." не являеться графическим </br>";
             return false;
          }
          
          if ($vid_src==0) { // В зависимости от вида подложки
              $width=$new_width_src;
              $height=$new_height_src;
          }
          
          $x_ratio = $width / $size_logo[0];
  	  $y_ratio = $height / $size_logo[1];

          $ratio       = min($x_ratio, $y_ratio);
          $use_x_ratio = ($x_ratio == $ratio);
          $new_width_logo   = $use_x_ratio  ? $width  : floor($size_logo[0] * $ratio);
          $new_height_logo  = !$use_x_ratio ? $height : floor($size_logo[1] * $ratio);
          $new_left_logo    = $use_x_ratio  ? 0 : floor(($width - $new_width_logo) / 2);
          $new_top_logo     = !$use_x_ratio ? 0 : floor(($height - $new_height_logo) / 2);

          $lsrc = $lcfunc($logo);

          if($size_logo[0]<$width && $size_logo[1]<$height) {
          // налаживаем логотип на картинку $dest
          switch ($vid_dest)
          {
             case 1: // Увелечение до размеров картинки
                  imagecopyresampled($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $new_width_logo, $new_height_logo, $size_logo[0], $size_logo[1]);
                  break;
             case 2: // Расположение вверху-слева top_left
                  if ($logo_intensite<>0) // Если используеться интенсивность перехода
                  {
                    $new_top_logo     = 0;
               		  $new_left_logo    = 0;
                    imagecopymerge($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1], $logo_intensite);
                  } else { // Без использования интенсивности перехода
                    $new_top_logo     = 0;
               		  $new_left_logo    = 0;
                    imagecopy($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1]);
                  }
                  break;
             case 3: // Расположение вверху-по центру top_center
                  if ($logo_intensite<>0) // Если используеться интенсивность перехода
                  {
                    $new_top_logo     = 0;
               		  $new_left_logo    = floor(($width-$size_logo[0])/2);
                    imagecopymerge($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1], $logo_intensite);
                  } else { // Без использования интенсивности перехода
                    $new_top_logo     = 0;
               		  $new_left_logo    = floor(($width-$size_logo[0])/2);
                    imagecopy($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1]);
                  }
                  break;
             case 4: // Расположение вверху-справа top-right
                  if ($logo_intensite<>0) // Если используеться интенсивность перехода
                  {
                    $new_top_logo     = 0;
               		  $new_left_logo    = $width-$size_logo[0];
                    imagecopymerge($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1], $logo_intensite);
                  } else { // Без использования интенсивности перехода
                    $new_top_logo     = 0;
               		  $new_left_logo    = $width-$size_logo[0];
                    imagecopy($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1]);
                  }
                  break;
             case 5: // Расположение по центру-слева middle-left
                  if ($logo_intensite<>0) // Если используеться интенсивность перехода
                  {
                    $new_top_logo     = floor(($height-$size_logo[1])/2);
               		  $new_left_logo    = 0;
                    imagecopymerge($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1], $logo_intensite);
                  } else { // Без использования интенсивности перехода
                    $new_top_logo     = floor(($height-$size_logo[1])/2);
               		  $new_left_logo    = 0;
                    imagecopy($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1]);
                  }
                  break;
             case 6: // Расположение по центру-по центру middle-center
                  if ($logo_intensite<>0) // Если используеться интенсивность перехода
                  {
                    $new_top_logo     = floor(($height-$size_logo[1])/2);
               		  $new_left_logo    = floor(($width-$size_logo[0])/2);
                    imagecopymerge($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1], $logo_intensite);
                  } else { // Без использования интенсивности перехода
                    $new_top_logo     = floor(($height-$size_logo[1])/2);
               		  $new_left_logo    = floor(($width-$size_logo[0])/2);
                    imagecopy($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1]);
                  }
                  break;
             case 7: // Расположение по центру-справа middle-right
                  if ($logo_intensite<>0) // Если используеться интенсивность перехода
                  {
                    $new_top_logo     = floor(($height-$size_logo[1])/2);
               		  $new_left_logo    = $width-$size_logo[0];
                    imagecopymerge($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1], $logo_intensite);
                  } else { // Без использования интенсивности перехода
                    $new_top_logo     = floor(($height-$size_logo[1])/2);
               		  $new_left_logo    = $width-$size_logo[0];
                    imagecopy($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1]);
                  }
                  break;
             case 8: // Расположение c низу-слева bottom-left
                  if ($logo_intensite<>0) // Если используеться интенсивность перехода
                  {
                    $new_top_logo     = $height-$size_logo[1];
               		  $new_left_logo    = 0;
                    imagecopymerge($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1], $logo_intensite);
                  } else { // Без использования интенсивности перехода
                    $new_top_logo     = $height-$size_logo[1];
               		  $new_left_logo    = 0;
                    imagecopy($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1]);
                  }
                  break;
             case 9: // Расположение c низу-по центру bottom-center
                  if ($logo_intensite<>0) // Если используеться интенсивность перехода
                  {
                    $new_top_logo     = $height-$size_logo[1];
               		  $new_left_logo    = floor(($width-$size_logo[0])/2);
                    imagecopymerge($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1], $logo_intensite);
                  } else { // Без использования интенсивности перехода
                    $new_top_logo     = $height-$size_logo[1];
               		  $new_left_logo    = floor(($width-$size_logo[0])/2);
                    imagecopy($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1]);
                  }
                  break;
             case 10: // Расположение c низу-справа bottom-right
                  if ($logo_intensite<>0) // Если используеться интенсивность перехода
                  {
                    $new_top_logo     = $height-$size_logo[1];
               		  $new_left_logo    = $width-$size_logo[0];
                    imagecopymerge($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1], $logo_intensite);
                  } else { // Без использования интенсивности перехода
                    $new_top_logo     = $height-$size_logo[1];
               		  $new_left_logo    = $width-$size_logo[0];
                    imagecopy($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1]);
                  }
                  break;
             case 11: // Размножить
                  $cnt_col=ceil($width/$size_logo[0]);
                  $cnt_row=ceil($height/$size_logo[1]);
                  $width_best=$cnt_col*$size_logo[0];
                  $height_best=$cnt_row*$size_logo[1];
             		  $ibest = imagecreate($width_best,$height_best);
                  $new_top_logo=0;
                  $new_left_logo=0;
                  for ($i=0;$i<$cnt_row;$i++) {
                   for ($j=0;$j<$cnt_col;$j++) {
                    if ($logo_intensite<>0) // Если используеться интенсивность перехода
                    {
                      $new_top_logo     = $i*$size_logo[1];
                 		  $new_left_logo    = $j*$size_logo[0];
                      imagecopymerge($ibest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1], $logo_intensite);
                    } else { // Без использования интенсивности перехода
                      $new_top_logo     = $i*$size_logo[1];
                 		  $new_left_logo    = $j*$size_logo[0];
                      imagecopy($ibest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1]);
                    }
                   }
                  }
                  $new_left_logo=0;
                  $new_top_logo=0;
                  $left_best     = floor(($width_best-$width)/2);
             		  $top_best    = floor(($height_best-$height)/2);
                  imagecopy($idest, $ibest, $new_left_logo, $new_top_logo, $left_best, $top_best, $width, $height);
                  break;
             default:
                  if ($logo_intensite<>0) // Если используеться интенсивность перехода
                  {
                    $new_top_logo     = floor(($height-$size_logo[1])/2);
               		  $new_left_logo    = floor(($width-$size_logo[0])/2);
                    imagecopymerge($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1], $logo_intensite);
                  } else { // Без использования интенсивности перехода
                    $new_top_logo     = floor(($height-$size_logo[1])/2);
               		  $new_left_logo    = floor(($width-$size_logo[0])/2);
                    imagecopy($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $size_logo[0], $size_logo[1]);
                  }
                  break;
                }
          } else { // уменьшение логотипа до размеров картинки
                 $new_top_logo     = $height-$new_height_logo;
            		 $new_left_logo    = $width-$new_width_logo;
                 /*$str_width_logo=$new_width_logo;
                 $str_height_logo=$new_height_logo;
                 $new_width_logo=floor($new_width_logo*0.80);
                 $new_height_logo=floor($new_height_logo*0.80);
                 $new_left_logo=$new_left_logo+floor(($str_width_logo-$new_width_logo)/2);
                 $new_top_logo=$new_top_logo+floor(($str_height_logo-$new_height_logo)/2);*/
                 imagecopyresampled($idest, $lsrc, $new_left_logo, $new_top_logo, 0, 0, $new_width_logo, $new_height_logo, $size_logo[0], $size_logo[1]);
          }
          }

     	imagejpeg($idest, $dest, $quality);
        imagedestroy($lsrc);
    	imagedestroy($idest);
	return true;
    }

}

// Параметры картинки
class Param_Image 
{
    public $width=140;
    public $height=140;

    public $max_width=null;   			// Максимальная ширина
    public $max_height=null;  			// Максимальная высота

    public $quality=100;
    public $background=0xFFFFFF;
    public $logo_file=null;
    public $align_img='left';           // Расположение картинки по горизонтали при диспропорции
    public $valign_img='top';           // Расположение картинки по вертикали при диспропорции
    public $isdisp=false;               // Диспропропарция
    public $smootdisp='none';           // Выравнивание диспопорции 'none','fill','cutt'
}

// Класс управления файлами
class RiFile 
{

   	public $fsecs=array("index.php","index.html"); // Список файлов защиты

   	/** Массив типов файлов
   	 * 
   	 */ 
   	public $aext=array("image"=>array("jpg","gif","png"), // Картинки
                           "video"=>array("flv"),             // Видео
                           "music"=>array("mp3"),             // Музыка
                           "files"=>array(""));               // Все остальные типы файлов

   	
   	/** Проверка на существоание директории с поледующим ее соданием
     * 
   	 * @param $dir 			- Полное название директории с абсолютным путем
   	 * @param $prav 		- Права
   	 * @param $create_index - автоматическое создание индексного файла-заглушки
   	 */
   	function is_dir($dir,$prav=0750,$create_index=false)
   	{
   	//  echo 'dir='.$dir.'<br>';

       if (!is_dir($dir)) { //Проверка на существоание каталога
            mkdir($dir,$prav); //Создание каталога

            if ($create_index==true){
                $this->save_fsec($dir."/index.html");
            }
             return false;
        }
        return true;
    }

    
   	/** Запись файла защиты
   	 *
   	 * @param $fname - полное имя файла
   	 * @param $instr - режим открытия файла
   	 * @param $cont  - текст
   	 */
   	function save_fsec( $filename )
   	{
        $file = fopen( $filename, "w+" );
        fwrite( $file, "" );
        fclose( $file ); // Закрываем дескриптор файла
   	}

   	
   	/** Запись текста в файл
   	 * 
   	 * @param	$fname - полное имя файла
   	 * @param 	$instr - режим открытия файла
   	 * @param	$cont  - текст 
   	 */
   	function save_text($filename,/*$instr="r",*/$text)
   	{
   		// Убеждаемся что файл доступен и существует для записи.	
       if ( is_writable( $filename) ) 
       { 
           $savesize=file_put_contents( $filename, $text );
       }
       else 
       {
           echo "<b>Файл $filename недоступен для записи</b>";
       }
   	}
   
   	
	/** Рекурсивное создание папок (при необходимости)
	 * 
	 * @param $path_inp - полный путь к папке
	 * @param $prav_inp - право доступа на папку
	 */
	function create_dir( $path_inp, $prav_inp, $create_index=false )
	{
	    $poz=strlen( $_SERVER["DOCUMENT_ROOT"] );
	    while ( $num = strpos( $path_inp,"/", $poz ) )
	    {
	        $poz  	= $num+1;
	        $fpath	= substr( $path_inp, 0, $num );
	        //echo '<pre>'.$fpath.'</pre>';
	
	        $this->is_dir( $fpath, $prav_inp, $create_index );
	    }
	 }
	
	
	/** Удаление каталогов как пустых так и с файлами
	 * 
	 * ПРИМЕЧАНИЕ: ПОИСКАТЬ ПОДОБНУЮ КОМАНДУ В PHP 
	 */
	function full_del_dir ($directory)
	{
	   $dir = opendir($directory);
	   while( ( $file = readdir($dir) ) )
	   {
	      if ( is_file( $directory."/".$file ) )
	      {
	         unlink ( $directory."/".$file );
	      }
	      else if ( is_dir( $directory."/".$file ) &&
	              ( $file != "." ) && ( $file != ".." ) )
	      {
	         full_del_dir ($directory."/".$file);
	      }
	    }
	
	    closedir ($dir);
	    rmdir ($directory);
	
	}
	
	
	/** Возвращает расширение файла
	 * 
	 * @param $filename
	 */
	function ext( $filename ) 
	{
	    $path_info = pathinfo( $filename );
	    return $path_info['extension'];
	}
	
	/** Возвращает название файла
	 * 
	 */
	function filename( $filename ) 
	{
	    $path_info = pathinfo( $filename );
	
	    $rez	   = $path_info['filename'];
	
	    if ( trim( $path_info['extension'] )<>'' )
	    {
	        $rez.='.'.$path_info['extension'];
	    }
	    
	    return $rez;
	}
	
	
	/** Возвращает название первой части файла (без расширения)
	 * 
	 * @param $filename
	 */
	function base_filename($filename) 
	{
	      $path_info = pathinfo($filename);
	      $rez=$path_info['filename'];
	      
	      return $rez;
	}
	
	  
	/** Возращает по расширению файла - тип файла согласно зарегистрированных типов
	 *  файлов
	 *  
	 * @param $filename - название файла
	 */
	function type_file( $filename ) 
	{
	    $ext=$this->ext( $filename );
	     
	    foreach( $this->aext as $key=>$val ) 
	    {
	       if ( in_array($ext,$val) ) 
	       {
	           return $key;
	       }
	    }
	    
	}
	
	  
	/** Сохраняет файл по указанному пути
	 * 
	 * @param $filename
	 * @param $new_filename
	 */
	function save( $filename, $new_filename )
	{
	    $rez=$new_filename;
	    if (!rename( $filename, $new_filename ) )
	    {
	       $this->cxi->d('Файл '.$filename.' не возможно переместить.<br>','echo');
               return false;
	    }
	    return $rez;
    }

}