<?php

class HFormat {
    public static function phone($t, $region_id) {
        $t = preg_replace("/\D/", "", $t);

        if (strlen($t) == 7) {
            switch($region_id) {
                case 77:
                    $t = "495".$t;
                break;

                default:
                    //$t = "812".$t;
                break;
            }
        }

        if (strlen($t) == 11) {
            $t = substr($t, 1);
        }

        if (strlen($t) == 10) {
            $t = "+7".$t;
        }

        $pos = 0;
        for($c=strlen($t)-1; $c>=0; $c--) {
            if (in_array($pos, array(2, 4, 7, 10))) {
                $nums[] = " ";
            }
            $nums[] = $t{$c};
            $pos++;
        }

        return strrev(implode("", $nums));
    }
  
    public static function telephone($phone) {
        /* Delete all char */
        $phone = preg_replace('/[^0-9+-\s]/', '', $phone);
        $phone = trim($phone);

        /* Standart Russia prefix */
        if ( substr($phone, 0, 2) == '+7' ) {
            $phone = substr($phone, 2);
        } else
            if (strlen($phone) > 7) {
                $ch = substr($phone, 0, 1);
                if ( ( $ch == '7' ) || ( $ch == '8' ) ) {
                    $phone = substr($phone, 1);
                }
            }

            $phone = trim($phone);

            /* Check branch */
            if ( preg_match('/^(\((\d+)\))/', $phone, $m) ) {

                $areaCode = $m[2];

                $phone = str_replace( $m[1], '', $phone);

            }

            /* Have a space */
            $space = strpos($phone, ' ');
            if ( empty( $areaCode ) && ( 3 <= $space ) && ( $space <= 6 ) ) {
                $areaCode = substr($phone, 0, $space);
                $phone = substr($phone, $space);
            }

            /* From here work only with DIGIT */

            $phone = preg_replace('/[^0-9]/', '', $phone);
            $length = strlen( $phone );

            if (( $length != 5 ) && ( $length != 6 ) && ( $length != 7 )) {
                if ( empty( $areaCode ) ) {
                    $areaCode = substr($phone, 0, 3);
                    $phone = substr($phone, 3);
                }
            }
            return $telephone = array('country'=>7,'code'=>$areaCode, 'number'=>$phone);
    }      
}
