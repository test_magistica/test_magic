<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/lightbox.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/lightbox.js" type="text/javascript"></script>
<div class="sonnikpage">
    <h1>Мои фотографии</h1>	
    <?php
    $this->widget('xupload.XUpload', array(
        'url' => Yii::app()->createUrl("user/upload"),
        'model' => $model,
        'attribute' => 'file',
        'multiple' => true,
        'options' => array(
            'maxFileSize' => 5000000,
            'acceptFileTypes' => "js:/(\.|\/)(jpe?g|png)$/i",
        )
    ));
    ?>
    <br /> <br />
    <div class="avatar-wrap">
        <div class="clr"></div>
        <div class="avatar-content">
            <p>
                Вы можете выбрать несколько фотографий одновременно<br />
                Максимальный размер файла — 5 Мб<br/>
                Допустимые форматы файлов: JPG, PNG, GIF<br/>
                Все основные действия с фотографией можно совершить,<br/> наведя на неё курсор
            </p>
        </div>
        <hr />
    </div>
</div>