<div class="registerpage">
    <h1>Регистрация нового пользователя</h1>
    <div class="login2">У вас уже есть аккаунт? <a href="<?php echo Yii::app()->homeUrl; ?>site/login.html">Войти в личный кабинет</a></div>

    <!--article>
        <h2>Это заголовок акции, создан для примера</h2>
        <p>Это пример текста, создан для того, чтобы было понятно, где будет текст.
            Это пример текста, создан для того, чтобы было понятно, где будет текст.
            Это пример текста, создан для того, чтобы было понятно, где будет текст.
            Это пример текста, создан для того, чтобы было понятно, где будет текст.</p>
    </article-->

    <!-- Форма -->
    <div class="shadowblock registerform">
        <div class="socialminiicons">
            <em>У Вас есть аккаунт в социальных сетях, войти:</em>
            <!--            <a href="#" class="ok"></a>
                        <a href="#" class="tw"></a>
                        <a href="#" class="fb"></a>
                        <a href="#" class="vk"></a>
                        <a href="#" class="gl"></a>-->
            <?php //$this->widget('ext.eauth.EAuthWidget', array('action' => 'site/login')); ?>
        </div>

        <div class="clr"></div>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'register-form',
    //'enableClientValidation' => TRUE,
    'enableAjaxValidation' => FALSE,
    'clientOptions' => array(
        'validateOnSubmit' => TRUE,
    ),
        ));
?>
        <div style="margin-left: 95px; padding-right: 80px;">
        <?php echo $form->errorSummary($model); ?>
        </div>

        <div class="field" style="display: none;">
<?php echo $form->error($model, 'name'); ?>
            <?php echo $form->error($model, 'surname'); ?>
            <?php echo $form->error($model, 'sex'); ?>
            <?php echo $form->error($model, 'day'); ?>
            <?php echo $form->error($model, 'month'); ?>
            <?php echo $form->error($model, 'birthday'); ?>
            <?php echo $form->error($model, 'country'); ?>
            <?php echo $form->error($model, 'tele'); ?>
            <?php echo $form->error($model, 'email'); ?>
            <?php echo $form->error($model, 'pwd'); ?>
            <?php echo $form->error($model, 'confirmPassword'); ?>

            <?php echo $form->error($model, 'rul'); ?>
        </div>
        <br />
        <div class="field">
            <span>Имя*</span>
<?php echo $form->textField($model, 'name', array('class' => 'inp')); ?>
        </div>
        <div class="field">
            <span>Фамилия*</span>
<?php echo $form->textField($model, 'surname', array('class' => 'inp')); ?>
        </div>

        <div class="field f_radio">
            <span>Пол</span>
            <?php echo $form->radioButton($model, 'sex', 
                            ( $model->sex==null || $model->sex==0 || $model->sex==2 ? array_merge( array('value' => 2, 'id' => 'fermale', 'class' => 'radio'), array('checked' => 'checked'))
                                                                  : array('value' => 2, 'id' => 'male', 'class' => 'radio'))
                       ); ?>
            <label for="fermale">Женский</label>
            <?php echo $form->radioButton($model, 'sex', 
                            ( $model->sex && $model->sex==1 ? array_merge(array('value' => 1, 'id' => 'male', 'class' => 'radio'), array('checked' => 'checked')) 
                                                            : array('value' => 1, 'id' => 'male', 'class' => 'radio') )
                       ); ?>
            <label for="male">Мужской</label>
        </div>

        <!-- Дата рождения -->
        <div class="field">
            <span>Дата рождения</span>
            <div class="selectbox">
<?php
echo $form->dropDownList($model, 'day', $this->getDay(), array(
    'class' => 'selectBlock',
    'empty' => '--',
));
?>
            </div>
            <div class="selectbox">
<?php
echo $form->dropDownList($model, 'month', $this->getMonth(), array(
    'class' => 'selectBlock',
    'empty' => '--',
));
?>
            </div>
            <div class="selectbox">
<?php
echo $form->dropDownList($model, 'birthday', $this->getYear(), array(
    'class' => 'selectBlock',
    'empty' => '--',
));
?>
            </div>
        </div>
        <!-- #Дата рождения -->


        <!-- Номер телефона -->
        <div class="field">
            <span>Номер телефона*</span>
            <div class="selectbox">
<?php
echo $form->dropDownList($model, 'country', Controller::getCountry(), array(
    'class' => 'selectBlock',
    /*'empty' => '--',*/
    'onchange' => 'document.getElementById("tel_code").value = this.value',
));
?>
            </div>
                 <input id="tel_code" name="tel_code" type="text" class="inp inp2" style="width:30px" value="<?php echo ($model->country ? '' : '+7'); ?>" disabled>
          <?php echo $form->textField($model, 'tele', array('class' => 'inp inp2', 'id' => 'tele_field', 'style' => 'width:115px')); ?>
        </div>
        <!-- #Номер телефона -->

        <div class="field">
            <span>E-mail*</span>
<?php echo $form->textField($model, 'email', array('class' => 'inp')); ?>
        </div>
        <div class="field"><span>Пароль*</span> <?php echo $form->passwordField($model, 'pwd', array('class' => 'inp')); ?> </div>
        <div class="field"><span>Подтверждение пароля*</span> <?php echo $form->passwordField($model, 'confirmPassword', array('class' => 'inp')); ?> </div>

        <div class="rules">
<?php echo $form->checkBox($model, 'rul', array('class' => 'check', 'uncheckValue' => null)); ?>
            <label>Я принимаю условия <a href="<?php echo Yii::app()->homeUrl; ?>site/agreement.html">пользовательского соглашения</a></label>
        </div>
        <div class="clr"></div>
<?php echo CHtml::submitButton($model->isNewRecord ? 'Регистрация' : 'Сохранить', array('class' => 'submit')); ?>

        <?php $this->endWidget(); ?>
    </div>
    <!-- #Форма -->
</div>
