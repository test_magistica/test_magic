<div class="sonnikpage">
    <h1>МОЙ ПРОФИЛЬ</h1>		
    <div class="post contact-wrap">
        <div class="shadowblock registerform">
            <div class="socialminiicons">
                <b class="b-title">Введите ваши данные:</b>
            </div>
            <!-- Форма -->
            <div id="list1" class="myprofileform">

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'register-form',
                    //'enableClientValidation' => TRUE,
                    'enableAjaxValidation' => TRUE,
                    'clientOptions' => array(
                        'validateOnSubmit' => TRUE,
                    ),
                ));
                ?>
                <div style="margin-left: 95px;">
                    <?php echo $form->errorSummary($model); ?>
                </div>
                <br />
                <div class="field" style="display: none;">
                    <?php echo $form->error($model, 'name'); ?> 
                    <?php echo $form->error($model, 'surname'); ?>
                    <?php echo $form->error($model, 'sex'); ?>
                    <?php echo $form->error($model, 'day'); ?>
                    <?php echo $form->error($model, 'month'); ?>
                    <?php echo $form->error($model, 'birthday'); ?>
                    <?php echo $form->error($model, 'country'); ?>
                    <?php echo $form->error($model, 'tele'); ?>
                    <?php echo $form->error($model, 'email'); ?>                    
                </div>
                <p><label>Имя*</label> <?php echo $form->textField($model, 'name', array('class' => 'border3 inp')); ?></p>
                <p><label>Фамилия*</label> <?php echo $form->textField($model, 'surname', array('class' => 'border3 inp')); ?></p>
                <p><label>Телефон*</label> <label style="text-align:left;"><?php echo $model->tele; ?></label>
                <?php echo $form->hiddenField($model, 'tele', array('class' => 'border3 inp', 'id' => 'tele_field')); ?></p>

                <div class="field">
                    <label>Страна*</label>
                    <div class="selectbox selectbox260">
                        <?php
                        echo $form->dropDownList($model, 'country', Controller::getCountry(), array(
                            'class' => 'selectBlock',
                            'onchange' => 'document.getElementById("tele_field").value = this.value',
                        ));
                        ?>                        
                    </div>
                </div>

                <!-- Дата -->
                <div class="field">
                    <label>Дата рождения*</label>
                    <div class="selectbox">
                        <?php
                        echo $form->dropDownList($model, 'day', $this->getDay(), array(
                            'class' => 'selectBlock',
                        ));
                        ?>
                    </div>
                    <div class="selectbox">
                        <?php
                        echo $form->dropDownList($model, 'month', $this->getMonth(), array(
                            'class' => 'selectBlock',
                            'empty' => '--',
                        ));
                        ?>
                    </div>
                    <div class="selectbox">
                        <?php
                        echo $form->dropDownList($model, 'birthday', $this->getYear(), array(
                            'class' => 'selectBlock',
                            'empty' => '--',
                        ));
                        ?>
                    </div>
                
                    <br><br>
                    <p>
                        <label>О себе</label> 
                       <?php echo $form->textArea($model, 'about', array('rows' => 6, 'cols' => 50, 'class' => 'border3 inp', 'id'=>'suitup-textarea')); ?>
                    </p>       
        
                    
                    <?php if (Yii::app()->user->role == 'expert'): ?>
                        <p><label>Tariff*</label> <?php echo $form->textField($model, 'tariff', array('class' => 'border3 inp')); ?></p>
                        
                        <p><label>SIP</label> <?php echo $form->textField($model, 'sip', array('class' => 'border3 inp')); ?></p>
                        <div class="field">
                	    <label>Направление вызовов</label>
                	    <div class="selectbox">
                    	    <?php
                    		echo $form->dropDownList($model, 'sip_tel', array(''=>'Телефон','sip'=>'SIP'), array(
                        	    'class' => 'selectBlock',
                    	    ));
                    	    ?>
                	    </div>
                	</div>
                    <?php endif; ?>
                </div>
                <!-- #Дата -->                   						

                <p>
                <label>E-mail*</label> 
                <label style="text-align:left;"><?php echo $model->email; ?></label>
                <?php echo $form->hiddenField($model, 'email', array('class' => 'border3 inp')); ?>
                </p>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Регистрация' : 'Сохранить', array('class' => 'submit')); ?>               
                <?php $this->endWidget(); ?>
            </div><!-- #list1 -->
            <!-- #Форма -->
        </div>
    </div>
</div>
