<div class="usercab_page">
    <div class="head">
        <h1>Личный кабинет пользователя</h1>
        <a href="#" class="border10 all">Сменить пароль</a>
    </div>


    <!-- Мой профайл -->
    <div class="block">
        <h2 onclick="slideToggleDiv1();">Мой профайл</h2>

        <div id="list1" class="shadowblock myprofileform">
            <div class="socialminiicons">
                <em>У Вас есть аккаунт в социальных сетях, войти:</em>
                <a href="#" class="ok"></a>
                <a href="#" class="tw"></a>
                <a href="#" class="fb"></a>
                <a href="#" class="vk"></a>
                <a href="#" class="gl"></a>
            </div>
            <div class="clr"></div>

            <form action="#" method="get">
                <p><label>Имя*</label> <input type="text" name="" class="border3 inp" /></p>
                <p><label>Фамилия*</label> <input type="text" name="" class="border3 inp" /></p>
                <p><label>Телефон*</label> <input type="text" name="" class="border3 inp" /> <a href="#" class="how"></a></p>

                <div class="field">
                    <label>Страна*</label>
                    <div class="selectbox selectbox260">
                        <select class="selectBlock">
                            <option>Россия</option>
                                            </select>
                    </div>
                    <a href="#" class="how"></a>
                </div>

                <!-- Дата -->
                <div class="field">
                    <label>Дата рождения*</label>
                    <div class="selectbox">
                        <select class="selectBlock2">
                            <option>2013</option>
                            <option>2012</option>
                            <option>2011</option>
                        </select>
                    </div>
                    <div class="selectbox">
                        <select class="selectBlock2">
                            <option>Ноябрь</option>
                            <option>Октябрь</option>
                            <option>Сентябрь</option>
                        </select>
                    </div>
                    <div class="selectbox">
                        <select class="selectBlock2">
                            <option>25</option>
                            <option>15</option>
                            <option>10</option>
                            <option>05</option>
                            <option>01</option>
                        </select>
                    </div>
                    <a href="#" class="how"></a>
                </div>
                <!-- #Дата -->

                <div class="field">
                    <label>Город</label>
                    <div class="selectbox selectbox260">
                        <select class="selectBlock">
                            <option>Москва</option>
                            <option>Санкт-Петербург</option>
                            <option>Харьков</option>
                            <option>Челябинск</option>
                            <option>Прага</option>
                        </select>
                    </div>
                    <a href="#" class="how"></a>
                </div>						

                <p><label>E-mail*</label> <input type="text" name="" class="border3 inp" /> <a href="#" class="how"></a></p>

                <input type="submit" class="submit" value="СОХРАНИТЬ" />
            </form>
        </div><!-- #list1 -->
    </div>
    <!-- #Мой профайл -->


    <!-- Сменить пароль -->
    <div class="block">
        <h2 onclick="slideToggleDiv2();">Сменить пароль</h2>
        <div id="list2" class="myprofileform">

            <form action="#" method="get">
                <p><label>Текущий пароль*</label> <input type="text" name="" class="border3 inp" /></p>
                <p><label>Новый пароль*</label> <input type="text" name="" class="border3 inp" /></p>
                <p><label>Подтверждение пароля*</label> <input type="text" name="" class="border3 inp" /></p>
                <input type="submit" class="submit" value="СОХРАНИТЬ" />
            </form>

        </div><!-- #list2 -->
    </div>
    <!-- #Сменить пароль -->



    <!-- Мои фотографии -->
    <div class="block">
        <h2 onclick="slideToggleDiv3();">Мои фотографии</h2>
        <div id="list3" class="myphotosbox">

            <span><a href="#"><img src="images/img04.jpg" alt="" /></a></span>
            <span><a href="#"><img src="images/img04.jpg" alt="" /></a></span>
            <span><a href="#"><img src="images/img04.jpg" alt="" /></a></span>
            <span><a href="#"><img src="images/img04.jpg" alt="" /></a></span>

            <div class="filebox3">
                <input type="file" size="6" id="fileInput3" />
                <div class="mask"></div>
            </div>

            <div class="clr"></div>
            <div class="warn">Это пример текста о фотографиях, заливке фото и их размере, 
                сколько можно максимум залить.</div>
        </div><!-- #list3 -->
    </div>
    <!-- #Мои фотографии -->



    <!-- Мои сообщения -->
    <div class="block">
        <h2 onclick="slideToggleDiv4();">Мои сообщения</h2>
        <div id="list4">

            <div id="accordion">

                <div class="shadowblock mymess">
                    <span class="name"><a href="#">Анна Сергеева</a></span>						
                    <div>
                        <p><img src="images/img03.jpg" alt="" />
                            Расскажите пожалуйста, подходим ли мы с мужем друг-другу? Как нам строить 
                            отношения, будут ли у нас дети, или стоит поискать кого-то ещё? К сообщению
                            прикладываю фото.</p>
                    </div>
                    <span class="date">11.05.2012</span>
                </div>

                <div class="shadowblock mymess">
                    <span class="name"><a href="#">Клара Цеткин</a></span>						
                    <div>
                        <p><img src="images/img03.jpg" alt="" />
                            Расскажите пожалуйста, подходим ли мы с мужем друг-другу? Как нам строить 
                            отношения, будут ли у нас дети, или стоит поискать кого-то ещё? К сообщению
                            прикладываю фото.</p>
                    </div>
                    <span class="date">11.05.2012</span>
                </div>

                <div class="shadowblock mymess">
                    <span class="name"><a href="#">Роза Люксембург</a></span>						
                    <div>
                        <p><img src="images/img03.jpg" alt="" />
                            Расскажите пожалуйста, подходим ли мы с мужем друг-другу? Как нам строить 
                            отношения, будут ли у нас дети, или стоит поискать кого-то ещё? К сообщению
                            прикладываю фото.</p>
                    </div>
                    <span class="date">11.05.2012</span>
                </div>

                <!-- Форма чата -->
                <div class="chatformbox chatformbox2">
                    <div class="ttl">Ваш ответ:</div>
                    <form action="#" method="get">
                        <textarea class="border5"></textarea>
                        <input type="submit" class="submit" value="Отправить" />
                    </form>
                </div>
                <!-- #Форма чата -->

            </div>

        </div><!-- #list4 -->
    </div>
    <!-- #Мои сообщения -->



    <!-- Мои заказы -->
    <div class="block block2">
        <h2 onclick="slideToggleDiv5();">Мои заказы</h2>

        <div id="list5">

            <!-- Заказ -->
            <div class="shadowblock myzakbox">
                <div class="desc">
                    <div class="meta">
                        <span class="author"><a href="#">Астра</a></span>
                        <span class="date border10">22.03.2013</span>
                    </div>
                    <div class="title"><a href="#">Разбор личных отношений по фотографии</a></div>
                    <p>По фотографиям и датам рождения провожу психологический анализ каждого из партнеров, 
                        описываю особенности характера, которые влияют на характер развития отношений и 
                        определяют степень совместимости, ...</p>
                    <div class="cost"><b>3750</b> руб.</div>
                </div>

                <div class="imgs">
                    <img src="images/img04.jpg" alt="" />
                    <img src="images/img04.jpg" alt="" />
                    <img src="images/img04.jpg" alt="" />
                    <img src="images/img04.jpg" alt="" />
                </div>
            </div>
            <!-- #Заказ -->

        </div><!-- #list5 -->
    </div>
    <!-- #Мои заказы -->

</div><!-- #page -->