<div class="sonnikpage">
    <h1>Сменить пароль</h1>		
    <div class="post contact-wrap">
        <div class="shadowblock registerform">
            <!-- Форма -->
            <br />
            <div id="list1" class="myprofileform">
                <?php if (Yii::app()->user->hasFlash('password')): ?>

                    <div class="flash-success">
                        <?php echo Yii::app()->user->getFlash('password'); ?>
                    </div>

                <?php else: ?>
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'login-form',
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                        ),
                    ));
                    ?>
                    <div style="margin-left: 95px;">
                        <?php echo $form->errorSummary($model); ?>
                    </div>
                    <div class="field" style="display: none;">
                        <?php echo $form->error($model, 'curPassword'); ?> 
                        <?php echo $form->error($model, 'password'); ?>
                        <?php echo $form->error($model, 'confirmPassword'); ?>                    
                    </div>
                <br /><br />
                    <p><label>Текущий пароль*</label> 
                        <?php echo $form->passwordField($model, 'curPassword', array('class' => 'border3 inp')); ?>
                    </p>
                    <p><label>Новый пароль*</label>                     
                        <?php echo $form->passwordField($model, 'password', array('class' => 'border3 inp')); ?></p>
                    <p><label>Подтверждение пароля*</label> 
                        <?php echo $form->passwordField($model, 'confirmPassword', array('class' => 'border3 inp')); ?></p>
                    <?php echo CHtml::submitButton('Отправить', array('class' => 'submit')); ?>                
                    <?php $this->endWidget(); ?>
                <?php endif; ?>
            </div><!-- #list1 -->
            <!-- #Форма -->
        </div>
    </div>
</div>