<div class="usercab_page">
	<div class="head">
		<? if ( ! array_key_exists('guest_id', $_GET)) {?>
			<h1>Пополнить баланс </h1>
			<div class="balance">
				<span class="c1">На вашем счете: </span>
				<span class="c2"><?=$this->getUser(Yii::app()->user->id, 'bill')?></span>
				<span class="c3">руб.</span>
			</div>
		<? } else {?>
			<h1>Выберите платежную систему для оплаты</h1>
		<? } ?>
	</div>
<?php
		if (isset($_GET['guest_id'])) {
			$guest_id = $_GET['guest_id'];
			if (($guest_id != 'error') AND (! empty ($guest_id))) {
				$criteria = new CDbCriteria();
				$criteria->compare('id_guest', $guest_id);
				$guest = Guest::model()->find($criteria);
				if (Guest::model()->count($criteria) == 1) {
					$entity = ucfirst($_GET['service']);
					$consult = $entity::model()->findByPk($guest->consultation);

					$user_phone = isset($consult->phone) ? str_replace('+', '', $consult->phone) : '';
				} // Если консультация найдена.
				else{$error_id = "Ошибка идентификатора, обратитесь к администратору.";}
			}
			else { // Если идентификатор не пустой
				$error_id =  "Не задан верный идентификатор.";
			}
		}
		else{ // Если задан в гет идентификатор.
			$error_id = "Нет переданных данных. ";
		}
		if (isset($error_id)){
			echo $error_id;
		}
?>
	<div class="container_variants">
		<div class="pg_ mts_"><a href="javascript:void(0);"><img src="/images/payment/mts.png"></a></div>
		<div class="pg_ megaphone_"><a href="javascript:void(0);"><img src="/images/payment/megaphone.png"></a></div>
		<div class="pg_ beeline_"><a href="javascript:void(0);"><img src="/images/payment/beeline.png"></a></div>
		<div class="robo_ tele2_"><a href="javascript:void(0);"><img src="/images/payment/tele2.png"></a></div>
	</div>
	<div class="container_variants">
		<div class="robo_ cards_"><a href="javascript:void(0);"><img src="/images/payment/cards.png"></a></div>
		<div class="robo_ qiwi_"><a href="javascript:void(0);"><img src="/images/payment/qiwi.png"></a></div>
		<div class="robo_ yad_"><a href="javascript:void(0);"><img src="/images/payment/yandex_money.png"></a></div>
		<div class="robo_"><a href="javascript:void(0);"><img src="/images/payment/other.png"></a></div>
	</div>

	<div class="clear"></div>
	<b class="title_rules">Правила пользования личным счетом службы Magistika</b>
	<div class="clear"></div>
	<p class="rules_mag">
	1)	Чтобы пополнить счет или произвести оплату услуги выберите платежную систему<br>
	2)	Выберете сумму и нажмите на кнопку «далее»<br>
	3)	Заполните необходимые данные, которая запрашивает платежная система<br>
	4)	Ожидайте подтверждения списания денежных средств.<br>
	5)	После успешной оплаты ваш счет будет автоматически пополнен на сайте (услуга будет оплачена)
	<br><br>
	Если вы столкнулись с трудностями оплаты Вы можете обратиться на линию поддержки:
	<br><br><br><br>
	<strong>ЛИНИЯ ПОДДЕРЖКИ</strong><br><br>
	<b>8 800 555 33 17</b> <span>звонок из всех регионов России бесплатный</span><br><br>
	<b>8 499 705 54 11</b> <span>для Москвы + другие страны</span><br><br>
	E-mail: help@magistika.ru<br>
	Skype: magistika-com
	</p>


	<?php echo Yii::app()->bootstrap->register();?>

	<script type="text/javascript">
		$('.modal-body #user_phone').hide();
		$(document).ready(function(){

			$("#other_amount").keyup(function(e) {
				if(e.keyCode != '13')
					$('.modal-header .sum').html(this.value);
			});
			if($('.modal-body input[type=radio]:checked').val() == '')
				$('.modal-footer #other_amount, .separator.top_line').show();

			$('.modal-body input[type=radio]').on('change', function(){
				if(this.value == '')
					$('.modal-footer #other_amount, .separator.top_line').show();
				else{
					$('.modal-footer #other_amount, .separator.top_line').hide();
					$('.modal-header .sum').html(this.value);
				}
			});

			$('.container_variants div:not(.disable)').on('click', function(){
				$('.modal-body #user_phone, .modal-footer #other_amount').hide();
				$('.modal-footer #payment_system').val('');
				$('.separator.bottom_line').hide();
				var sms = false;
				if($(this).hasClass('pg_')) {
					$('.separator.bottom_line').hide();
					if($(this).hasClass('mts_') || $(this).hasClass('megaphone_') || $(this).hasClass('beeline_')){
						$('.modal-body #user_phone, .separator.bottom_line').show();
						// RURU - билайн, INPLATMTS, - мтс, INPLATMEGAFON - мегафо, YANDEXMONEY - ЯД
						if($(this).hasClass('mts_') ){
							$('.modal-footer #payment_system').val('INPLATMTS');
							sms = true;
						}
						if($(this).hasClass('megaphone_') ){
							$('.modal-footer #payment_system').val('INPLATMEGAFON');
							sms = true;
						}
						if($(this).hasClass('beeline_')){
							$('.modal-footer #payment_system').val('RURU');
							sms = true;
						}
					}
					$('.modal-footer #action').val('PLATRON')
				}
				if($(this).hasClass('robo_')){
					// BANKOCEAN2R - банковские карты, Qiwi29OceanR, - qiwi, MobicomTele2R - tele2, YandexMerchantOceanR - ЯД
					if($(this).hasClass('cards_') ){
						$('.modal-footer #payment_system').val('BANKOCEAN2R');
					}
					if($(this).hasClass('qiwi_') ){
						$('.modal-footer #payment_system').val('Qiwi29OceanR');
					}
					if($(this).hasClass('tele2_') ){
						$('.modal-body #user_phone, .separator.bottom_line').show();
						$('.modal-footer #payment_system').val('MobicomTele2R');
						sms = true;
					}
					if($(this).hasClass('yad_') ){
						$('.modal-footer #payment_system').val('YandexMerchantR');
					}
					$('.modal-footer #action').val('ROBOKASSA');
				}
				<? if ( ! isset($_GET['guest_id']) && ! isset($_GET['other_amount'])){ ?>
					$('#myModal').modal('show');
				<? } else { ?>
					if(sms)
						$('#myModal').modal('show');
					else
						$('#payment-form').submit();
				<? } ?>
			});
		});
	</script>
<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'myModal'));?>
<div class="modal-header">
	<a class="close" data-dismiss="modal">×</a>
	<h4>Пополнить счет на:
		<span class="sum">
			<?=(isset($consult['cost'])
					? $consult['cost']
					: (!isset($_POST['action']) && isset($_GET['other_amount']) ? $_GET['other_amount'] : '?'))?>
		</span> <span class="cur">руб.</span></h4>
</div>
<form method="post" id="payment-form">
	<div class="modal-body">
		<div class="separator bottom_line">
			Номер телефона для оплаты: <b><big>+</big></b>
			<input type="text" value="<?=(( ! isset($_GET['guest_id']))
				? (str_replace('+', '', $this->getUser(Yii::app()->user->id, 'tele')))
				: (isset($user_phone)?$user_phone:'')); ?>" name="user_phone" id="user_phone">
		</div>
<? if ( ! isset($_GET['guest_id']) && ! isset($_GET['service'])) {?>
		<div class="f_left">
			<label><input type="radio" name="amount" value="10"/> <b>10</b> руб.</label>
			<label><input type="radio" name="amount" value="100"/> <b>100</b> руб.</label>
			<label><input type="radio" name="amount" value="200"/> <b>200</b> руб.</label>
			<label><input type="radio" name="amount" value="500"/> <b>500</b> руб.</label>
			<label><input type="radio" name="amount" value="1000"/> <b>1000</b> руб.</label>
		</div>
		<div class="f_right">
			<label><input type="radio" name="amount" value="1500"/> <b>1500</b> руб.</label>
			<label><input type="radio" name="amount" value="3000"/> <b>3000</b> руб.</label>
			<label><input type="radio" name="amount" value="5000"/> <b>5000</b> руб.</label>
			<label><input type="radio" name="amount" value="10000"/> <b>10000</b> руб.</label>
			<label><input type="radio" name="amount" value=""
				<?=(!isset($_POST['action']) && isset($_GET['other_amount']) ? 'checked="checked"' : '')?> /> Другая сумма</label>
		</div>
		<div class="separator top_line">
			Укажите желаемую сумму: <input type="text" name="other_amount" id="other_amount"
			                               value="<?=(!isset($_POST['action']) && isset($_GET['other_amount']) ? $_GET['other_amount'] : '')?>" />
		</div>
<? } else {?>
		<input type="hidden" id="amount" name="amount" value="<?=isset($consult['cost']) ? $consult['cost'] : (isset($_GET['other_amount']) ? $_GET['other_amount'] : '')?>">
		<? if ( isset($_GET['guest_id']) ) { ?>
			<input type="hidden" name="guest_id" value="<?=isset($guest_id) ? $guest_id :'';?>">
		<?}?>
<? } ?>
	</div>
	<div class="modal-footer">
		<input type="hidden" id="action" name="action" value="">
		<input type="hidden" id="payment_system" name="payment_system" value="">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
		<button class="btn btn-primary" type="submit">Далее</button>
	</div>
</form>
<?php $this->endWidget(); ?>


<? if(Yii::app()->user->hasFlash('not_enough_money')) { ?>
        <div class="flash-notice_question">
                <?php echo Yii::app()->user->getFlash('not_enough_money'); ?>
        </div>
 <?}?>

</div><!-- #page -->

