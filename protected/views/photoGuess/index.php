<section>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/lightbox.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/lightbox.js" type="text/javascript"></script>
<?
if($page = Service::model()->findByPk(35)){

	if($page->metaKey != "")
		$this->pageKeywords = explode(',',  mb_strtolower($page->metaKey, 'UTF-8'));
	if($page->metaDesc != "")
		$this->pageDescription = $page->metaDesc;
	if($page->metaTitle != "")
		$this->pageTitle = $page->metaTitle;
}
?>
<div class="zakaz_page photo">
<? $serv = Service::model()->findByPk(35);?>
<div class="payment_head">
<h1><?=$serv->notes?></h1>

<?php if (Yii::app()->user->hasFlash('photoGuess')) { ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('photoGuess'); ?>
</div>

	<? }
else {?>
	<? if (Yii::app()->user->hasFlash('error')) { ?>
	<div class="flash-error">
		<?php echo Yii::app()->user->getFlash('error'); ?>
	</div>
		<? } ?>
	<?=$serv->description;?>

	<div class="details_payment">
		<div class="cost">
			<span>Стоимость:</span>
			<b><?=$serv->honorarium;?> руб.</b>
		</div>
		<div class="time">
			<span>Время исполнения:</span>
			<b><?=$serv->period;?></b>
		</div>
	</div>

<div class="clear"></div>
	<div class="example_payment">
		<h4>примеры гадания по фотографии</h4>
		<div class="example">
			<a href="/gadanie-po-foto-primer" target="_blank">Марина Мартышева</a>, <span>г. Москва</span>
		</div>
	</div>
</div>

<div class="shadowblock zakaz_page_block photoguess">
	<div class="imgbox_exp"><?=$infoBlock;?></div>
	<div class="expts">
		<div class="selectbox selectbox2">
			<?=CHtml::dropDownList('expts', 'expts', CHtml::listData($expertList, 'id', 'name'),
			array('options' => array($expert->id=>array('selected'=>true)),'class' => 'selectBlock2'));
			?>
		</div> <div class="clear"></div>
		<p class="pr15px">Вы заказываете консультацию с экспертом <b class="exp_name"><?php echo $expert['name']; ?></b></p>
	</div>

	<div class="clear"></div>
	<br>
	<hr>



	<h2>Оформление заказа</h2>
	<?
	$this->widget('xupload.XUpload', array(
		'url'           => Yii::app()->createUrl("site/upload"),
		'model'         => new XUploadForm,
		'attribute'     => 'file',
		'showForm'      => true,
		'multiple'      => false,
		'formView'      => 'form_consult',
		'downloadView'  => 'download_consult',
		'autoUpload'    => true,
		'options'       => array(
			'maxFileSize' => 5000000,
			'acceptFileTypes' => "js:/(\.|\/)(jpe?g|png)$/i",
			'complete' => 'js:function (result) {
				a = jQuery.parseJSON(result.responseText.slice(1, -1));
				$(".upload_image").val(a.realName);
			}',
		)
	));
	?>

	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'photo_guess-form',
		'enableAjaxValidation' => true,
		'clientOptions'=> array(
			'validateOnSubmit'=>true
		),
	));
	?>

	<!-- Форма -->
	<div class="formblock">
		<div style="margin-left: 95px;">
			<?php echo $form->errorSummary($model); ?>
		</div>
		<div class="field" style="display: none;">
			<?php echo $form->error($model, 'image'); ?>
			<?php echo $form->error($model, 'username'); ?>
			<?php echo $form->error($model, 'date_birth'); ?>
			<?php echo $form->error($model, 'time_birth'); ?>
			<?php echo $form->error($model, 'city_birth'); ?>
			<?php echo $form->error($model, 'email'); ?>
			<?php echo $form->error($model, 'what'); ?>
			<?php echo $form->error($model, 'question'); ?>
			<?php echo $form->error($model, 'therms'); ?>
		</div>

		<div class="clr"></div>
		<label>Ваше имя</label>
		<?=$form->textField($model, 'username', array('class' => 'border3 inp'))?>

		<div class="clr"></div>
		<label class="pl160px mt-5px mb13px c_grey f-s11px">Полное имя заказчика</label>
		<div class="clr"></div>

		<div class="select-patch select-patch-photo">
			<?
			$day = $month = $year = array();
			for($i=1996; $i > 1945; $i--) $year[$i] = $i;
			for($i=1; $i < 13; $i++) $month[$i] = $i<10?'0'.$i:$i;
			for($i=1; $i < 32; $i++) $day[$i] = $i<10?'0'.$i:$i;
			for($i=0; $i < 24; $i++) $hour[$i] = $i<10?'0'.$i:$i;
			for($i=0; $i < 60; $i++) $minute[$i] = $i<10?'0'.$i:$i;
			?>
			<label>Дата рождения:</label>
			<div class="selectbox selectbox2">
				<?php
				echo CHtml::dropDownList('day', '', $day, array(
					'class' => 'selectBlock2',
					'id' => 'day',
					'empty' => 'Число',
				));
				echo CHtml::dropDownList('month', '', $month, array(
					'class' => 'selectBlock2',
					'id' => 'month',
					'empty' => 'Месяц',
				));
				echo CHtml::dropDownList('year', '', $year, array(
					'class' => 'selectBlock2',
					'id' => 'year',
					'empty' => 'Год',
				));
				echo $form->hiddenField($model, 'date_birth', array('value' => '0000-00-00'))
				?>
			</div>

			<div class="clr"></div>
			<label class="pl160px mt-10px">&nbsp;</label>
			<div class="clr"></div>

			<label>Время рождения:</label>
			<div class="selectbox selectbox2">
				<?php
				echo CHtml::dropDownList('hour', '', $hour, array(
					'class' => 'selectBlock2',
					'empty' => 'Часы',
				));
				echo CHtml::dropDownList('minute', '', $minute, array(
					'class' => 'selectBlock2',
					'empty' => 'Минут',
				)).'<label>'.CHtml::CheckBox('forget','',array('class' => 'check')).' не помню</label>';
				echo $form->hiddenField($model, 'time_birth', array('value' => ''));
				echo $form->hiddenField($model, 'forget', array('value' => '0'));
				?>
			</div>
			<div class="clr"></div>
			<label class="pl160px mt-10px">&nbsp;</label>
			<div class="clr"></div>
			<label>Город рождения:</label>
			<?=$form->textField($model, 'city_birth', array('class' => 'border3 inp'))?>
			<div class="clr"></div>
			<script>
				$(document).ready(function() {
					$.ajax({
						type: "GET",
						url: "/extrasens/view?id=<?=$expert->id?>&tab=otzyvy&all=1&nonform=1",
						dataType: "html",
						success: function(data) {
							$('.review_res').html(data);
						}
					});
					$('#day').on('change', function(){
						val = $('#PhotoGuess_date_birth').val().split('-');
						$('#PhotoGuess_date_birth').val(val[0]+'-'+val[1]+'-'+(this.value<10?'0'+this.value:this.value));
					});
					$('#month').on('change', function(){
						val = $('#PhotoGuess_date_birth').val().split('-');
						$('#PhotoGuess_date_birth').val(val[0]+'-'+(this.value<10?'0'+this.value:this.value)+'-'+val[2]);

					});
					$('#year').on('change', function(){
						val = $('#PhotoGuess_date_birth').val().split('-');
						$('#PhotoGuess_date_birth').val(this.value+'-'+val[1]+'-'+val[2]);
					});
					$('#therms').on('change', function(){
						$('#PhotoGuess_therms').val(this.checked?1:'');
					});
					$('#forget').on('change', function(){
						$('#PhotoGuess_forget').val(this.checked?'1':'0');
						$('#PhotoGuess_time_birth').val(this.checked?'99:99':'');
					});

					$('#hour').on('change', function(){
						val = $('#PhotoGuess_time_birth').val().split(':');
						if(val.length > 1){
							$('#PhotoGuess_time_birth').val((this.value<10?'0'+this.value:this.value)+':'+val[1]);
						}
						else {
							$('#PhotoGuess_time_birth').val((this.value<10?'0'+this.value:this.value)+':');
						}
					});
					$('#minute').on('change', function(){
						val = $('#PhotoGuess_time_birth').val().split(':');
						if(val.length > 1){
							$('#PhotoGuess_time_birth').val(val[0]+':'+(this.value<10?'0'+this.value:this.value));
						}
						else {
							$('#PhotoGuess_time_birth').val(':'+(this.value<10?'0'+this.value:this.value));
						}
					});

					$('#expts').on('change', function(){
						$('#PhotoGuess_expertID').val(this.value);
						$('.exp_name').html($('#expts option:selected').text());
						$('.imgbox_exp').html('<img src="/images/facybox/loading.gif">');
						$('.review_res').html('<img src="/images/facybox/loading.gif">');

						$.ajax({
							type: "GET",
							url: "/photoGuess/expertInfo?idx="+this.value+"&nonform=1&all=1",
							dataType: "json",
							success: function(data) {
								$('.imgbox_exp').html(data.info);
								$('.review_res').html(data.reviews);
							}
						});
					})
				});
			</script>

			<?=$form->hiddenField($model,'userID', array('value'=> (Yii::app()->user->isGuest ? '0' : Yii::app()->user->id)));?>
			<?=$form->hiddenField($model, 'expertID', array('value'=> $expert->id));?>
			<?=$form->hiddenField($model, 'image', array('value'=> '', 'class' => 'upload_image'));?>
			<?=$form->hiddenField($model, 'cost', array('value' => $serv->honorarium));?>

			<div class="clr"></div>
			<label class="pl160px mt-10px">&nbsp;</label>
			<div class="clr"></div>
			<label class="pl160px">&nbsp;</label>
			<div class="clr"></div>

			<label>Электронная почта</label>
			<?php echo $form->textField($model, 'email', array('class' => 'inp border3')); ?>

			<div class="clr"></div>
			<label class="pl160px mt-5px mb13px c_grey f-s11px w80pnt">Мы отправим Вам результат по почте</label>
			<div class="clr"></div>

			<label class="mt-3px">На что<br>хотите погадать</label>
			<div class="selectbox selectbox2 what">
				<?php
				echo $form->dropDownList($model, 'what', Yii::app()->params['whatList'], array(
					'class' => 'selectBlock2 what',
					'empty' => '--',
				));
				?>
			</div>
			<div class="clr"></div>
			<label class="pl160px mt-10px">&nbsp;</label>
			<div class="clr"></div>

			<label class="mt-3px">Напишите<br>Ваш вопрос</label>
			<?=$form->textArea($model, 'question', array('class' => 'border5 inp-area', 'value'=>'')); ?>

			<br>
			<br>
			<?='<label class="pl160px therms_lbl">'.CHtml::CheckBox('therms','',array('class' => 'check')).' Я соглашаюсь с <a href="/site/agreement" target="_blank">условиями</a> предоставления сервиса</label>'?>

			<?=$form->hiddenField($model, 'therms', array('class' => ''));?>
			<br>
			<br><div class="clr"></div>
			<div class="itog photo">
				<?=CHtml::submitButton("Оплатить заказ {$serv->honorarium} руб." , array('class' => 'submit')); ?>
			</div>
		</div>
	</div>




	<?php $this->endWidget(); ?>
</div>
<div class="clr"></div>

<h1>отзывы о гадание по фотографии</h1>
<div class="review_res"></div>
	<? } ?>


</div><!-- #page -->

</section>