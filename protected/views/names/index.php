<div class="sonnikpage">
    
    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a href="<?php echo Yii::app()->createUrl("names"); ?>">Имена</a>        
    </div>
    
    <h1>Значение имени. Что означает мое имя. </h1>		

    <div class="box searchbox">
        <p>Введите имя, чтоб узнать его значение:</p>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'names-form',
            'method' => 'post',
            'action' => Yii::app()->createUrl('//names/search'),
        ));
        ?>
        <input type="text" class="inp" name="q" value="<?php echo isset($_POST['q']) ? $_POST['q'] : ''; ?>">
        <input type="submit" value="Мужские имена" class="submit submit2" name="man">
        <input type="submit" value="Женские имена" class="submit submit3" name="woman">
        <div class="clr"></div>
        <span>Поиск по всем именам</span>
        <?php $this->endWidget(); ?>
    </div>

    <div class="box">
        <h3>Значение мужских имен</h3>
        <ul class="alphabet">
            <?php foreach ($characters as $key => $value): ?>
                <li><?php echo CHtml::link($value, array('names/man', 'character' => $value)); ?></li>
            <?php endforeach; ?>            
        </ul>
        <div class="clr15"></div>

        <h3>Значение женских имен</h3>
        <ul class="alphabet">
            <?php foreach ($characters as $key => $value): ?>
                <li><?php echo CHtml::link($value, array('names/woman', 'character' => $value)); ?></li>
            <?php endforeach; ?>            
        </ul>
    </div>

    <?php if (isset($_GET['character']) && in_array($_GET['character'], $this->_characters)): ?>
        <div class="post">
            <h2>Результаты поиска</h2>
            <article class="shadowblock">
                <p>

                </p>
                <ul class="search-sonnic">                    
                    <?php foreach ($models as $model2): ?>
                        <li style="float: left; width: 120px;"><?php echo CHtml::link($model2['name'], array('names/view/', 'id' => $model2['id'], 'title' => $this->getCHPU($model2['name']), 't' => Yii::app()->controller->action->id)); ?></li>
                    <?php endforeach; ?>
                </ul>
                <p>

                </p>

            </article>
        </div>
        <div class="clr"></div>        
    <?php endif; ?>

    <?php if (isset($_POST['q'])): ?>
        <div class="post">
            <h2>Результаты поиска</h2>
            <article class="shadowblock">
                <p>

                </p>
                <ul class="search-sonnic">                    
                    <?php foreach ($models as $model2): ?>
                        <li style="float: left; width: 120px;"><?php echo CHtml::link($model2['name'], array('names/view/', 'id' => $model2['id'], 'title' => $this->getCHPU($model2['name']), 't' => $t)); ?></li>
                    <?php endforeach; ?>
                </ul>
                <p>

                </p>

            </article>
        </div>
        <div class="clr"></div>        
    <?php endif; ?>

</div>
