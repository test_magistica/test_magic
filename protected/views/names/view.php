<div class="sonnikpage">
    
    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a href="<?php echo Yii::app()->createUrl("names"); ?>">Имена</a>       
        <a><?php echo $model->name; ?></a>       
    </div>
    
<h1>Значение имени</h1>
	<div class="box searchbox">
        <p>Введите интересующее вас имя:</p>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'names-form',
            'method' => 'post',
            'action' => Yii::app()->createUrl('//names/search'),
        ));
        ?>
        <input type="text" class="inp" name="q" value="<?php echo isset($_POST['q']) ? $_POST['q'] : ''; ?>">
        <input type="submit" value="Мужские имена" class="submit submit2" name="man">
        <input type="submit" value="Женские имена" class="submit submit3" name="woman">
        <div class="clr"></div>
        <span>Поиск по всем именам</span>
        <?php $this->endWidget(); ?>
    </div>

<div class="box">
        <h3>Мужские имена</h3>
        <ul class="alphabet">
            <?php foreach ($characters as $key => $value): ?>
                <li><?php echo CHtml::link($value, array('names/man', 'character' => $value)); ?></li>
            <?php endforeach; ?>            
        </ul>
        <div class="clr15"></div>

        <h3>Женские имена</h3>
        <ul class="alphabet">
            <?php foreach ($characters as $key => $value): ?>
                <li><?php echo CHtml::link($value, array('names/woman', 'character' => $value)); ?></li>
            <?php endforeach; ?>            
        </ul>
    </div>

    <div class="post">
        <h2>Происхождение имени <?php echo $model->name; ?></h2>
        <article class="shadowblock">
            <p><?php echo $model->description; ?></p>
        </article>
    </div>

    <div class="post">
        <h2>Нумерология имени <?php echo $model->name; ?></h2>
        <article class="shadowblock">
            <p><?php echo $model->numerology; ?></p>
        </article>
    </div>

    <div class="post">
        <h2>Знаки</h2>
        <article class="shadowblock">
            <p><?php echo $model->symbols; ?></p>
        </article>
    </div>
</div>
