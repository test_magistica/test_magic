<div class="registerpage">
    <h1>Забыли пароль?</h1>
    <article>  
        <p>Введите Ваш E-Mail (указанный при регистрации) и мы вышлем новый пароль на Ваш электронный адрес.</p>
    </article>

    <!-- Форма -->
    <div class="shadowblock registerform">
        <?php if (Yii::app()->user->hasFlash('forgot')): ?>

            <div class="flash-success">
                <?php echo Yii::app()->user->getFlash('forgot'); ?>
            </div>

        <?php else: ?>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <div class="socialminiicons">
                <em>Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</em>
            </div>
            <div class="clr"></div>
            <div style="margin-left: 95px; padding-right: 80px;">
                <?php echo $form->errorSummary($model); ?>
            </div>
            <div class="field" style="display: none;">
                <?php echo $form->error($model, 'username'); ?>            
            </div>
            <br />
            <div class="field">
                <span>Ваш E-Mail*</span>
                <?php echo $form->textField($model, 'username', array('class' => 'inp')); ?>
            </div>

            <div class="row buttons">
                <?php echo CHtml::submitButton('Выслать пароль', array('class' => 'submit', 'style' => 'margin-left:327px;',)); ?>
            </div>

            <?php $this->endWidget(); ?>
        <?php endif; ?>
    </div><!-- form -->

</div>