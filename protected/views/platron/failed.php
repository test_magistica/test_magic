<div class="usercab_page">
    <div class="head">
        <h1>Платеж не выполнен</h1>

    </div>

    <div class="balance-add">
        <?php if (Yii::app()->user->hasFlash('failed')): ?>

            <div class="flash-success">
                <?php echo Yii::app()->user->getFlash('failed'); ?>
            </div>

        <?php endif; ?>
    </div>

</div><!-- #page -->