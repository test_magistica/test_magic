<?php
$this->pageTitle = Yii::app()->name . ' - ' . ucfirst($page->title);
    if(!empty($page->metaKey))
        $this->pageKeywords = explode(",", $page->metaKey);
    if(!empty($page->metaDesc))
        $this->pageDescription = $page->metaDesc;
?>
<div class="sonnikpage">
    <div itemprop="bredcrumb" class="bread">
        <a style="color: #969696 !important;" href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a style="color: #444444 !important;"><?php echo Yii::t("app", "Карта сайта"); ?></a>       
    </div>
<h1><?php echo $page->title; ?></h1>
</div>

<?php echo CHtml::decode($page->description); ?>
