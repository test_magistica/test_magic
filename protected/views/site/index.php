<?php


    if($category = Category::model()->findByAttributes(array("url" => Yii::app()->request->requestUri))){
        if($category->metaKey!="")
            $this->pageKeywords = explode(',',  mb_strtolower($category->metaKey, 'UTF-8'));
		if($category->metaDesc!="")
	            $this->pageDescription = $category->metaDesc;
		if($category->metaTitle!="")
	            $this->pageTitle = $category->metaTitle;

    }

	$description = '';

	if(Yii::app()->controller->id == "site" && Yii::app()->controller->action->id == "index" ){
		if($page = Page::model()->findByAttributes(array("title" => 'Главная'))){
			$description = $page->description;

			if($page->metaKey != "")
				$this->pageKeywords = explode(',',  mb_strtolower($page->metaKey, 'UTF-8'));
			if($page->metaDesc != "")
				$this->pageDescription = $page->metaDesc;
			if($page->name != "")
				$this->pageTitle = $page->name;
		}
	}
?>
<?php //$this->widget('ext.Horoscope.Horoscope');        ?>
<?php $this->renderPartial('/site/_sliderbox'); ?>

<!-- START TAKE CODE FROME HERE -->
<div style="clear: both;"></div>
<div class="form2 bg-grey radius-5" style="margin-bottom: 10px;">
        <div class="question">
            <form class="new-question" style="">
                <a href="<?php echo Yii::app()->createUrl("/question/create", array('st' => 1)); ?>">
                    <input type="button" value="Задать вопрос экстрасенсу" class="button ask" />
                </a>
                <a href="<?php echo Yii::app()->createUrl("/question"); ?>">
                    <input type="button" value="Просмотреть все вопросы" class="button view" />
                </a>
            </form>
        </div>
    </div>
<div class="head2">
    <h1>Сайт Экстрасенсы по телефону онлайн</h1>
</div>

<?php if (Yii::app()->user->hasFlash('error')): ?>
    <div style="clear: both;"></div>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>
<!-- Эксперт -->
<?php foreach ($experts as $expert): ?>
    <div class="expertitem2" id="ex-<?php echo $expert['id']; ?>">
        <div class="leftbox">
            <div class="image_zoom">
            <?php $image = $this->getUserImage($expert['id']); ?>
            	<img class="ava" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $image; ?>" alt="" height="150" width="150">
            </div>
            <!--            <a href="#" class="ratio"></a>
                        <span class="ratio-num">Рейтинг 9,9</span>-->

            <?php echo CHtml::link($this->getExpertReviewsCount($expert['id']) == 0 ? 'Нет отзывов' : 'Читать отзывы (' . $this->getExpertReviewsCount($expert['id']) ./* '{'.$expert['cntrev'].'}'.*/')', array('/extrasens/view/', 'id' => $expert['id'], 'title' => $this->getCHPU($expert['name'] . '-' . $expert['profession'])), array('class' => 'review')); ?>

        <?php $url = $this->getUrlToExpertChat($expert);?>
        <?php if((Yii::app()->user->isGuest || Yii::app()->user->role == Users::ROLE_USER) /*&& $url !== FALSE*/):?>
            <?php if($expert->chat_available == 1 ):?>
                <?php if($expert->online == "online"):?>
                <a href="<?php echo $url; ?>" title="Онлайн чат с экстрасенсом" target="_blank">
                    <div class="chat-online-button">Онлайн чат</div>
                </a>
                <?php else:?>
<!--                    <a href="#" title="Онлайн чат с экстрасенсом" onclick="return false;">
                        <div class="chat-online-button offline">Офлайн чат</div>
                    </a>-->
                <?php endif; ?>
            <?php endif; ?>
        <?php endif;?>

        <?php if(Yii::app()->user->role == Users::ROLE_EXPERT && $expert->chat_available == 1):?>
            <?php if($expert->online == "online"):?>
            <a href="<?php echo "#"; ?>" title="Онлайн чат с экстрасенсом" onclick="return false;">
                <div class="chat-online-button">Онлайн чат</div>
            </a>
            <?php else:?>
<!--                <a href="#" title="Онлайн чат с экстрасенсом" onclick="return false;">
                    <div class="chat-online-button offline">Офлайн чат</div>
                </a>-->
            <?php endif; ?>
        <?php endif;?>

        </div>
        <div class="rightbox">
            <?php if ($expert['online'] == 'online'): ?>

                <?php echo CHtml::link('Позвонить с сайта', array('callBack/view/', 'id' => $expert['id']), array('class' => 'green')) ?>

            <?php else: ?>
                <?php echo CHtml::link('Заказать звонок', array('site/register', 'id' => $expert['id']), array('class' => 'orange')) ?>
            <?php endif; ?>


            <span class="tarif">
                <?php echo $expert->tariff; ?> руб/мин
            </span>
            <h5><?php echo CHtml::link($expert['name'], array('/extrasens/view/', 'id' => $expert['id'], 'title' => $this->getCHPU($expert['name'] . '-' . $expert['profession'])), array('style' => 'color:#333333;')); ?></h5><br>
            <span class="type">
                <?php echo $expert->profession; ?>
            </span>&nbsp;
            <span class="<?php echo $expert['online']; ?>">
                <?php echo Yii::app()->params['status'][$expert['online']]; ?>
            </span>
            <div class="clr"></div>
            <br>
            <span class="exp-info">
                <?php echo mb_substr(str_replace("\n", '<br>', $expert['about']), 0, 370, 'UTF-8'); ?>

				  <?php echo CHtml::link('Узнать больше...', array('/extrasens/view/', 'id' => $expert['id'], 'title' => $this->getCHPU($expert['name'] . '-' . $expert['profession']))); ?>



                <?php //echo CHtml::link('узнать больше...', array('/extrasens/view/', 'id' => $expert['id'])); ?>
            </span>
            <div class="clr"></div>
            <?php if($expert->mobile !== NULL && !empty($expert->mobile)): ?>
                <div class="call-cell">
                    <?php echo $expert['mobile']; ?>
                    <span class="help">
                        стоимость
                        <span><?php echo $expert['mobileText']; ?></span>
                    </span>
                </div>
            <?php endif; ?>
            <?php if((($expert->home) != NULL)):?>
            <div class="call-stat">
                <?php echo $expert['home']; ?>
                <?php if($expert->homeText != NULL):?>
                <span class="help">
                    стоимость
                    <span><?php echo $expert['homeText']; ?></span>
                </span>
                <?php endif;?>
            </div>
            <?php endif;?>
        </div>
        <div class="clr"></div>
        <div class="last-review" style="margin-top: 12px;">
            <div class="corner"></div>
            <?php echo $this->getExpertReviewsCount($expert['id']) == 0 ? 'Нет отзывов' : $this->getLastExpertReview($expert['id']); ?>


        </div>
    </div>
<?php endforeach; ?>
<!-- #Эксперт -->


<div class="clr"></div>


<!--
        STOP TAKING CODE AND PUT IT INTO SECTION
        !!! don't forget to take new-pagination which is before footer !!!
        add reveal.css and jquery.reveal.js for pop up modal window
-->



<div class="pager_box">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $pages,
        'header' => '',
        'cssFile' => Yii::app()->request->baseUrl . '/css/mypager.css',
    ));
    ?>
</div> <!-- pager -->

<script>
    function updateStatus() {
        $.get("<?php echo CController::createUrl('/site/checkExpertStatus') ?>",
                function(data) {
                    json = jQuery.parseJSON(data);
                    $.each(json, function(k, v) {
                        if (v == 'online') {
                            $('#ex-' + k + ' .rightbox .type').next('span').text('online');
                        }
                        if (v == 'busy') {
                            $('#ex-' + k + ' .rightbox .type').next('span').text('Консультирует');
                        }
                        if (v == 'autonomous') {
                            $('#ex-' + k + ' .rightbox .type').next('span').text('нет на линии');
                        }
                        $('#ex-' + k + ' .rightbox .type').next('span').removeClass();
                        $('#ex-' + k + ' .rightbox .type').next('span').addClass(v);
                        if (v == 'online') {
                            $('#ex-' + k + ' .rightbox a:first').removeClass();
                            $('#ex-' + k + ' .rightbox a:first').addClass('green');
                            $('#ex-' + k + ' .rightbox a:first').text('Позвонить с сайта');
                            $('#ex-' + k + ' .rightbox a:first').attr('href','<?php echo Yii::app()->request->baseUrl; ?>'+'/callBack/view/'+k);
                        }else{
                            $('#ex-' + k + ' .rightbox a:first').removeClass();
                            $('#ex-' + k + ' .rightbox a:first').addClass('orange');
                            $('#ex-' + k + ' .rightbox a:first').text('Заказать звонок');
                            $('#ex-' + k + ' .rightbox a:first').attr('href','<?php echo Yii::app()->request->baseUrl; ?>'+'/site/register/'+k);
                        }


                    });
                });
    }
    setInterval("updateStatus()", 5000);
</script>

<?=$description ?>