<?php
$this->pageTitle = Yii::app()->name . ' - Пользовательского соглашения';
$modelP = Page::model()->find("`name`='user_agreement'");
?>
<?php Yii::app()->clientScript->registerMetaTag($modelP->metaKey, 'keywords'); ?>
<?php Yii::app()->clientScript->registerMetaTag($modelP->metaDesc, 'description'); ?>
<h1 style="font-family:'Tahoma'">Пользовательское соглашение</h1>
<div style="color:#666; font-family:'Tahoma'">
    <?php echo $modelP->description; ?>
</div>