<!-- Sliderbox -->
<div class="slbodywrapper">
    <div class="coda-slider" id="sliderbox">
        <?php
        $c = new CDbCriteria();
        $c->order = 'id desc';
        $c->limit = 4;
        $slides = Slider::model()->findAll($c);
        ?>
        <?php foreach ($slides as $slide): ?>
            <div>
                <span class="title"><?php echo $slide['title']; ?></span>
                <a href="<?php echo $slide['date']; ?>">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/slider/<?php echo $slide['image']; ?>" title="<?php echo $slide['title']; ?>" alt="<?php echo $slide['title']; ?>" />
                </a>
                <!--div class="txt">
                    <em><?php echo date('m.d.Y', strtotime($slide['date'])); ?></em>
                    <p><?php echo $slide['description']; ?></p>
                </div-->
            </div>
        <?php endforeach; ?>
    </div>
    <div class="skl"></div>
    <div class="skl2"></div>
</div>
<!-- #Sliderbox -->  


