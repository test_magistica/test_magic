<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="en-gb" class="lightbg">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">


        <meta name="generator" content="Joomla! - Open Source Content Management">
        <title>Home</title>    


        <link rel="stylesheet" href="css/layout00.css" type="text/css">        
        <link rel="stylesheet" href="css/template.css" type="text/css">
        <link rel="stylesheet" href="css/menu0000.css" type="text/css">
        <link rel="stylesheet" href="css/gk000000.css" type="text/css">        


        <style type="text/css">
            .gkcol { width: 220px; }
            @media screen and (max-width: 675px) {
                #k2Container .itemsContainer { width: 100%!important; } 
                .cols-2 .column-1,
                .cols-2 .column-2,
                .cols-3 .column-1,
                .cols-3 .column-2,
                .cols-3 .column-3,
                .demo-typo-col2,
                .demo-typo-col3,
                .demo-typo-col4 {width: 100%; }
            }
            #system-message-container { margin: 0 -20px; }
            .box > div, #gkContent > div { padding: 24px; }
            .product-field-display a img:after {
                content:'';
            }
            th,
            button,
            .button,
            h1, h2, h3, h4, h5, h6,
            .gkMenu > ul > li,
            h3.header,
            p.gkInfo1, p.gkTips1, p.gkWarning1, p.numblocks span,
            .pagination ul li,
            .PricesalesPrice,
            .catProductDetails,
            .showcart,
            .continue,
            .addtocart-button,
            .vm-button-correct,
            input.default,
            .userfields_info,
            .gkShowCart,
            input[type="button"],
            input[type="submit"],
            .nspVmStore,
            .k2TagCloudBlock a,
            .product_price,
            .componentheading,
            .cart_subtotal td + td  { font-family: 'Oswald', Arial, sans-serif; }

            #gkLogo.text, article time, .itemComments h3, .catProductPrice .PricesalesPrice,
            .gkTotal { font-family: 'Russo One', Arial, sans-serif; }

            body, input { font-family: Arial, Helvetica, sans-serif; }

            .blank,p.numblocks span.highlight { font-family: Arial, Helvetica, sans-serif; }

            #gkContent { width: 100%; }

            #gkPageWrap { max-width: 1000px; }

            body { padding: 0 20px; }
            #menu102 > div,
            #menu102 > div > .childcontent-inner { width: 440px; }

            #menu103 > div,
            #menu103 > div > .childcontent-inner { width: 220px; }

            #menu414 > div,
            #menu414 > div > .childcontent-inner { width: 220px; }

            #menu415 > div,
            #menu415 > div > .childcontent-inner { width: 220px; }

            #menu426 > div,
            #menu426 > div > .childcontent-inner { width: 220px; }

            #menu431 > div,
            #menu431 > div > .childcontent-inner { width: 220px; }

            #menu436 > div,
            #menu436 > div > .childcontent-inner { width: 220px; }

            #menu439 > div,
            #menu439 > div > .childcontent-inner { width: 220px; }

            #menu443 > div,
            #menu443 > div > .childcontent-inner { width: 220px; }

            #menu234 > div,
            #menu234 > div > .childcontent-inner { width: 220px; }

            #menu548 > div,
            #menu548 > div > .childcontent-inner { width: 220px; }

            #gk-cookie-law { background: #E55E48; bottom: 0; color: #fff; font: 400 16px/52px Arial, sans-serif; height: 52px; left: 0;  margin: 0!important; position: fixed; text-align: center; width: 100%; z-index: 10001; }
            #gk-cookie-law span { display: inline-block; max-width: 90%; }
            #gk-cookie-law a { color: #fff; font-weight: 600; text-decoration: underline}
            #gk-cookie-law a:hover { color: #222}
            #gk-cookie-law a.gk-cookie-law-close { background: #c33c26; color: #fff; display: block; float: right; font-size: 28px; font-weight: bold; height: 52px; line-height: 52px; width: 52px;text-decoration: none}
            #gk-cookie-law a.gk-cookie-law-close:active,
            #gk-cookie-law a.gk-cookie-law-close:focus,
            #gk-cookie-law a.gk-cookie-law-close:hover { background: #282828; }
            @media (max-width: 1280px) { #gk-cookie-law { font-size: 13px!important; } }
            @media (max-width: 1050px) { #gk-cookie-law { font-size: 12px!important; line-height: 26px!important; } }
            @media (max-width: 620px) { #gk-cookie-law { font-size: 11px!important; line-height: 18px!important; } #gk-cookie-law span { max-width: 80%; } }
            @media (max-width: 400px) { #gk-cookie-law { font-size: 10px!important; line-height: 13px!important; } }

        </style>
        <script src="js/mootools.js" type="text/javascript"></script>
        <script src="js/core0000.js" type="text/javascript"></script>
        <script src="js/jquery00.js" type="text/javascript"></script>



        <script src="js/mootoolt.js" type="text/javascript"></script>

        <script src="js/engine00.js" type="text/javascript"></script>
        <script src="js/engine01.js" type="text/javascript"></script>
        <script src="js/engine02.js" type="text/javascript"></script>
        <script type="text/javascript">

            window.addEvent('domready', function() {

                SqueezeBox.initialize({});
                SqueezeBox.assign($$('a.modal'), {
                    parse: 'rel'
                });
            });
            var k2storeURL = 'http://demo.gavick.com/joomla25/bikestore/';

            if (typeof(K2Store) == 'undefined') {
                var K2Store = jQuery.noConflict();
            }

            K2Store(document).ready(function() {
                K2Store('.k2storeCartForm').each(function() {
                    K2Store(this).validate({
                        errorElement: 'em',
                        errorPlacement: function(error, element) {
                            error.appendTo(element.parent().parent().next('div'));
                        },
                        success: function(label) {
                            label.text('Ok').addClass('success');
                        },
                        submitHandler: function(form) {

                            k2storeAddToCart('addtocart', form);

                        }
                    });
                });
            });
            window.addEvent('load', function() {
                new JCaption('img.caption');
            });
            $GKMenu = {height: true, width: true, duration: 250};
            $GK_TMPL_URL = "http://demo.gavick.com/joomla25/bikestore/templates/gk_bikestore";

            $GK_URL = "http://demo.gavick.com/joomla25/bikestore/";

            function keepAlive() {
                var myAjax = new Request({method: "get", url: "index.php"}).send();
            }
            window.addEvent("domready", function() {
                keepAlive.periodical(3600000);
            });
            vmSiteurl = 'http://demo.gavick.com/joomla25/bikestore/';
            vmLang = '&amp;lang=en';
            vmCartText = ' was added to your cart.';
            vmCartError = 'There was an error while updating your cart.';
            loadingImage = '/joomla25/bikestore/components/com_virtuemart/assets/images/facebox/loading.gif';
            closeImage = '/joomla25/bikestore/components/com_virtuemart/assets/images/facebox/closelabel.png';
            Virtuemart.addtocart_popup = '1';
            faceboxHtml = '<div id="facebox" style="display:none;"><div class="popup"><div class="content"></div> <a href="#" class="close"></a></div></div>';

            try {
                $Gavick;
            } catch (e) {
                $Gavick = {};
            }
            ;
            $Gavick["gkIs-gk-is-433"] = {"anim_speed": 500, "anim_interval": 5000, "autoanim": 0, "slide_links": 1};
        </script>
        <script type="text/javascript">
            // Cookie Law
            window.addEvent('domready', function() {
                   if (document.getElement('#gk-cookie-law')) {
                           document.getElement('#gk-cookie-law').getElements('a').each(function(el) {
                                   el.addEvent('click', function(e) {
                                           // stop propagation and prevent default action
                                           e.stop();
                                           // for both links
                                           if (e.target.getProperty('href') == '#close') {    
                                                   Cookie.write('gk-demo-cookie-law', '1', {duration: 365});
                                                   document.getElement('#gk-cookie-law').dispose();
                                           } else {
                                                   Cookie.write('gk-demo-cookie-law', '1', {duration: 365});
                                                   window.location.href = e.target.getProperty('href')
                                           }
                                   });
                           });
                   }
            });
        </script>


        <!--[if IE 9]>
        <link rel="stylesheet" href="http://demo.gavick.com/joomla25/bikestore/templates/gk_bikestore/css/ie/ie9.css" type="text/css" />
        <![endif]-->

        <!--[if IE 8]>
        <link rel="stylesheet" href="http://demo.gavick.com/joomla25/bikestore/templates/gk_bikestore/css/ie/ie8.css" type="text/css" />
        <![endif]-->

        <!--[if lte IE 7]>
        <link rel="stylesheet" href="http://demo.gavick.com/joomla25/bikestore/templates/gk_bikestore/css/ie/ie7.css" type="text/css" />
        <![endif]-->

        <!--[if (gte IE 6)&(lte IE 8)]>
        <script type="text/javascript" src="http://demo.gavick.com/joomla25/bikestore/templates/gk_bikestore/js/respond.js"></script>
        <script type="text/javascript" src="http://demo.gavick.com/joomla25/bikestore/templates/gk_bikestore/js/selectivizr.js"></script>
        <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->	 </head>
    <body class="lightbg" data-tablet-width="940" data-mobile-width="580">	



        <div id="gkPageWrap">	 
            <section id="gkPageTop">                    	

                <a href="http://demo.gavick.com/joomla25/bikestore/" id="gkLogo" class="cssLogo">Bikestore</a>

                <div id="gkTopLinks">

                    <a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/shopping-cart" id="btnCart">My Cart<span>Loading</span></a>

                    <a href="http://demo.gavick.com/joomla25/bikestore/index.php?option=com_users&amp;view=registration" id="btnRegister">Register</a>

                    <a href="http://demo.gavick.com/joomla25/bikestore/index.php?option=com_users&amp;view=login" id="btnLogin">Login</a>
                </div>

                <div id="gkSearch">
                    <form action="http://demo.gavick.com/joomla25/bikestore/index.php" method="post">
                        <div class="search">
                            <input name="searchword" id="mod-search-searchword" maxlength="20" class="inputbox" type="text" size="20" value="Search..." onblur="if (this.value == '')
                    this.value = 'Search...';" onfocus="if (this.value == 'Search...')
                    this.value = '';">
                            <input type="submit" value="Search" class="button" onclick="this.form.searchword.focus();">	<input type="hidden" name="task" value="search">
                            <input type="hidden" name="option" value="com_search">
                            <input type="hidden" name="Itemid" value="9999">
                        </div>
                    </form>
                </div>

                <div id="gkMainMenu">
                    <nav id="gkExtraMenu" class="gkMenu">
                        <ul class="gkmenu level0"><li class="first active"><a href="http://demo.gavick.com/joomla25/bikestore/" class=" first active" id="menu101" title="Home">Home</a></li><li class="haschild"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template" class=" haschild" id="menu102" title="Template">Template</a><div class="childcontent">
                                    <div class="childcontent-inner">
                                        <div class="gkcol gkcol2  first">
                                            <ul class="gkmenu level1">
                                                <li class="first group">
                                                    <div class="group">
                                                        <header>
                                                            <a href="http://demo.gavick.com/joomla25/bikestore/#" class=" first group" id="menu123" title="Lorem ipsum">Lorem ipsum<small>First menu group</small></a></header><div class="gk-group-content"><ul class="gkmenu level1"><li class="first haschild"><a href="http://demo.gavick.com/joomla25/bikestore/#" class=" first haschild" id="menu103" title="Template articles">Template articles<small>Articles about template possibilities</small></a>
                                                                        <div class="childcontent">
                                                                        <div class="childcontent-inner">
                                                                            <div class="gkcol gkcol1  first"><ul class="gkmenu level2"><li class="first"><a href="http://demo.gavick.com/joomla25/bikestore/?option=non-existing-component" class=" first" id="menu105" title="Error page">Error page</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/?tmpl=offline" id="menu106" title="Offline page">Offline page</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/template-articles/module-positions" id="menu107" title="Module positions">Module positions</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/template-articles/module-variations" id="menu127" title="Module variations">Module variations</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/template-articles/page-breaks" id="menu272" title="Page breaks">Page breaks</a></li><li class="last"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/template-articles/iframe-wrapper" class=" last" id="menu435" title="Iframe wrapper">Iframe wrapper</a></li></ul></div>
                                                                        </div>
                                                                    </div>
                                                                    </li><li class="haschild"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/articles" class=" haschild" id="menu414" title="Articles">Articles<small>com_content menu items types</small></a><div class="childcontent">
                                                                        <div class="childcontent-inner">
                                                                            <div class="gkcol gkcol1  first"><ul class="gkmenu level2"><li class="first haschild"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/articles/archived-articles" class=" first haschild" id="menu415" title="Archived articles">Archived articles</a><div class="childcontent">
                                                                                            <div class="childcontent-inner">
                                                                                                <div class="gkcol gkcol1  first"><ul class="gkmenu level3"><li class="first"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/articles/archived-articles/archived-articles-by-title" class=" first" id="menu416" title="by Title">by Title<small>Archived articles sorted by title</small></a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/articles/archived-articles/archived-articles-by-author" id="menu417" title="by Author">by Author<small>Archived articles sorted by author</small></a></li><li class="last"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/articles/archived-articles/archived-articles-by-hits" class=" last" id="menu418" title="by Hits">by Hits<small>Archived articles sorted by hits</small></a></li></ul></div>
                                                                                            </div>
                                                                                        </div></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/articles/single-article" id="menu419" title="Single Article">Single Article<small>Example of the single article</small></a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/articles/list-all-categories" id="menu420" title="List All Categories">List All Categories<small>List of the all categories.</small></a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/articles/category-blog" id="menu421" title="Category blog">Category blog<small>Category with the blog layout.</small></a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/articles/category-list" id="menu422" title="Category list">Category list<small>List of the items in the specific category.</small></a></li><li class="last"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/articles/featured-articles" class=" last" id="menu423" title="Featured Articles">Featured Articles<small>List of the featured articles</small></a></li></ul></div>
                                                                        </div>
                                                                    </div></li><li class="haschild"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/contacts" class=" haschild" id="menu426" title="Contacts">Contacts<small>com_contact menu item types</small></a><div class="childcontent">
                                                                        <div class="childcontent-inner">
                                                                            <div class="gkcol gkcol1  first"><ul class="gkmenu level2"><li class="first"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/contacts/list-contacts-in-a-category" class=" first" id="menu428" title="List Contacts in a Category">List Contacts in a Category</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/contacts/list-all-contact-categories" id="menu427" title="List All Contact Categories">List All Contact Categories</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/contacts/featured-contacts" id="menu429" title="Featured Contacts">Featured Contacts</a></li><li class="last"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/contacts/single-contact" class=" last" id="menu430" title="Single Contact">Single Contact</a></li></ul></div>
                                                                        </div>
                                                                    </div></li><li class="last haschild"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/newsfeeds" class=" last haschild" id="menu431" title="Newsfeeds">Newsfeeds<small>com_newsfeeds menu item types</small></a><div class="childcontent">
                                                                        <div class="childcontent-inner">
                                                                            <div class="gkcol gkcol1  first"><ul class="gkmenu level2"><li class="first"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/newsfeeds/list-all-news-feed-categories" class=" first" id="menu432" title="List All News Feed Categories">List All News Feed Categories</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/newsfeeds/list-news-feeds-in-a-category" id="menu433" title="List News Feeds in a Category">List News Feeds in a Category</a></li><li class="last"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum/newsfeeds/single-news-feed" class=" last" id="menu434" title="Single News Feed">Single News Feed</a></li></ul></div>
                                                                        </div>
                                                                    </div>
                                                                    </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="gkcol gkcol2  last">
                                                <ul class="gkmenu level1"><li class="first group"><div class="group"><header><a href="http://demo.gavick.com/joomla25/bikestore/#" class=" first group" id="menu124" title="Lorem ipsum II">Lorem ipsum II<small>Second menu group</small></a></header><div class="gk-group-content"><ul class="gkmenu level1"><li class="first"><a href="http://www.gavick.com/documentation/joomla-templates/templates-for-joomla-1-6/responsive-layout-2/" class=" first" id="menu113" title="Layouts">Layouts<small>Template layouts</small></a></li><li class="haschild"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/search" class=" haschild" id="menu436" title="Search">Search<small>Joomla! search engines</small></a><div class="childcontent">
                                                                        <div class="childcontent-inner">
                                                                            <div class="gkcol gkcol1  first"><ul class="gkmenu level2"><li class="first"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/search/smart-search" class=" first" id="menu438" title="Smart search">Smart search<small>com_finder component</small></a></li><li class="last"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/search/com-search" class=" last" id="menu437" title="Search">Search<small>com_search component</small></a></li></ul></div>
                                                                        </div>
                                                                    </div></li><li class="haschild"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/weblinks" class=" haschild" id="menu439" title="Weblinks">Weblinks<small>com_weblinks menu item types</small></a><div class="childcontent">
                                                                        <div class="childcontent-inner">
                                                                            <div class="gkcol gkcol1  first"><ul class="gkmenu level2"><li class="first"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/weblinks/list-all-web-link-categories" class=" first" id="menu440" title="List All Web Link Categories">List All Web Link Categories</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/weblinks/list-web-links-in-a-category" id="menu441" title="List Web Links in a Category">List Web Links in a Category</a></li><li class="last"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/weblinks/submit-a-web-link" class=" last" id="menu442" title="Submit a Web Link">Submit a Web Link</a></li></ul></div>
                                                                        </div>
                                                                    </div></li><li class="last haschild"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users" class=" last haschild" id="menu443" title="Users">Users<small>com_users menu item types</small></a><div class="childcontent">
                                                                        <div class="childcontent-inner">
                                                                            <div class="gkcol gkcol1  first"><ul class="gkmenu level2"><li class="first"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/login-form" class=" first" id="menu444" title="Login form">Login form</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/user-profile" id="menu445" title="User profile">User profile</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/edit-user-profile" id="menu446" title="Edit User Profile">Edit User Profile</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/registration-form" id="menu447" title="Registration form">Registration form</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/password-reset" id="menu449" title="Password Reset">Password Reset</a></li><li class="last"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/username-reminder-request" class=" last" id="menu448" title="Username Reminder Request">Username Reminder Request</a></li></ul></div>
                                                                        </div>
                                                                    </div></li></ul></div></div></li></ul></div>
                                    </div>
                                </div></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/typography" id="menu263" title="Typography">Typography</a></li><li class="haschild"><a href="http://demo.gavick.com/joomla25/bikestore/#" class=" haschild" id="menu234" title="K2">K2</a><div class="childcontent">
                                    <div class="childcontent-inner">
                                        <div class="gkcol gkcol1  first"><ul class="gkmenu level1"><li class="first"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/2011-08-04-23-47-02/item" class=" first" id="menu424" title="Item">Item</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/2011-08-04-23-47-02/categories" id="menu425" title="Categories">Categories</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/2011-08-04-23-47-02/tag" id="menu450" title="Tag">Tag</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/2011-08-04-23-47-02/user-page" id="menu451" title="User page">User page</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/2011-08-04-23-47-02/latest-items-one-column" id="menu452" title="Latest items (one column)">Latest items (one column)</a></li><li class="last"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/2011-08-04-23-47-02/latest-items-two-columns" class=" last" id="menu453" title="Latest items (two columns)">Latest items (two columns)</a></li></ul></div>
                                    </div>
                                </div></li><li class="haschild"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart" class=" haschild" id="menu548" title="VirtueMart">VirtueMart</a><div class="childcontent">
                                    <div class="childcontent-inner">
                                        <div class="gkcol gkcol1  first"><ul class="gkmenu level1"><li class="first"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout" class=" first" id="menu535" title="Default layout">Default layout</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/category-layout" id="menu537" title="Category layout">Category layout</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/product-details" id="menu541" title="Product details">Product details</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/shopping-cart" id="menu536" title="Shopping cart">Shopping cart</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/list-orders" id="menu540" title="List Orders">List Orders</a></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/account-maintenance" id="menu542" title="Account maintenance">Account maintenance</a></li><li class="last"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/user-edit-address" class=" last" id="menu543" title="User edit address">User edit address</a></li></ul></div>
                                    </div>
                                </div></li><li><a href="http://demo.gavick.com/joomla25/bikestore/index.php/blog" id="menu555" title="Blog">Blog</a></li><li class="last"><a href="http://www.gavick.com/products/joomla-templates.html" class=" last" id="menu474" title="Joomla Templates">Joomla Templates</a></li></ul>
                    </nav>   

                    <div id="gkMobileMenu">
                        Menu		    		<select onchange="window.location.href = this.value;">
                            <option selected="selected" value="http://demo.gavick.com/joomla25/bikestore/">Home</option><option value="/joomla25/bikestore/index.php/template">Template</option><option value="#">&nbsp;&nbsp;»Lorem ipsum</option><option value="#">&nbsp;&nbsp;&nbsp;&nbsp;»Template articles</option><option value="?option=non-existing-component">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Error page</option><option value="?tmpl=offline">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Offline page</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/template-articles/module-positions">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Module positions</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/template-articles/module-variations">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Module variations</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/template-articles/page-breaks">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Page breaks</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/template-articles/iframe-wrapper">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Iframe wrapper</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/articles">&nbsp;&nbsp;&nbsp;&nbsp;»Articles</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/articles/archived-articles">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Archived articles</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/articles/archived-articles/archived-articles-by-title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»by Title</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/articles/archived-articles/archived-articles-by-author">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»by Author</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/articles/archived-articles/archived-articles-by-hits">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»by Hits</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/articles/single-article">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Single Article</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/articles/list-all-categories">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»List All Categories</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/articles/category-blog">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Category blog</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/articles/category-list">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Category list</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/articles/featured-articles">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Featured Articles</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/contacts">&nbsp;&nbsp;&nbsp;&nbsp;»Contacts</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/contacts/list-contacts-in-a-category">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»List Contacts in a Category</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/contacts/list-all-contact-categories">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»List All Contact Categories</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/contacts/featured-contacts">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Featured Contacts</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/contacts/single-contact">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Single Contact</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/newsfeeds">&nbsp;&nbsp;&nbsp;&nbsp;»Newsfeeds</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/newsfeeds/list-all-news-feed-categories">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»List All News Feed Categories</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/newsfeeds/list-news-feeds-in-a-category">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»List News Feeds in a Category</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum/newsfeeds/single-news-feed">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Single News Feed</option><option value="#">&nbsp;&nbsp;»Lorem ipsum II</option><option value="http://www.gavick.com/documentation/joomla-templates/templates-for-joomla-1-6/responsive-layout-2/">&nbsp;&nbsp;&nbsp;&nbsp;»Layouts</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/search">&nbsp;&nbsp;&nbsp;&nbsp;»Search</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/search/smart-search">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Smart search</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/search/com-search">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Search</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/weblinks">&nbsp;&nbsp;&nbsp;&nbsp;»Weblinks</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/weblinks/list-all-web-link-categories">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»List All Web Link Categories</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/weblinks/list-web-links-in-a-category">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»List Web Links in a Category</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/weblinks/submit-a-web-link">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Submit a Web Link</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users">&nbsp;&nbsp;&nbsp;&nbsp;»Users</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/login-form">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Login form</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/user-profile">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»User profile</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/edit-user-profile">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Edit User Profile</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/registration-form">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Registration form</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/password-reset">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Password Reset</option><option value="/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/username-reminder-request">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;»Username Reminder Request</option><option value="/joomla25/bikestore/index.php/typography">Typography</option><option value="#">K2</option><option value="/joomla25/bikestore/index.php/2011-08-04-23-47-02/item">&nbsp;&nbsp;»Item</option><option value="/joomla25/bikestore/index.php/2011-08-04-23-47-02/categories">&nbsp;&nbsp;»Categories</option><option value="/joomla25/bikestore/index.php/2011-08-04-23-47-02/tag">&nbsp;&nbsp;»Tag</option><option value="/joomla25/bikestore/index.php/2011-08-04-23-47-02/user-page">&nbsp;&nbsp;»User page</option><option value="/joomla25/bikestore/index.php/2011-08-04-23-47-02/latest-items-one-column">&nbsp;&nbsp;»Latest items (one column)</option><option value="/joomla25/bikestore/index.php/2011-08-04-23-47-02/latest-items-two-columns">&nbsp;&nbsp;»Latest items (two columns)</option><option value="/joomla25/bikestore/index.php/virtuemart">VirtueMart</option><option value="/joomla25/bikestore/index.php/virtuemart/default-layout">&nbsp;&nbsp;»Default layout</option><option value="/joomla25/bikestore/index.php/virtuemart/category-layout">&nbsp;&nbsp;»Category layout</option><option value="/joomla25/bikestore/index.php/virtuemart/product-details">&nbsp;&nbsp;»Product details</option><option value="/joomla25/bikestore/index.php/virtuemart/shopping-cart">&nbsp;&nbsp;»Shopping cart</option><option value="/joomla25/bikestore/index.php/virtuemart/list-orders">&nbsp;&nbsp;»List Orders</option><option value="/joomla25/bikestore/index.php/virtuemart/account-maintenance">&nbsp;&nbsp;»Account maintenance</option><option value="/joomla25/bikestore/index.php/virtuemart/user-edit-address">&nbsp;&nbsp;»User edit address</option><option value="/joomla25/bikestore/index.php/blog">Blog</option><option value="http://www.gavick.com/products/joomla-templates.html">Joomla Templates</option>		    	</select>
                    </div>
                </div>
            </section>

            <section id="gkHeader">

                <div id="gkIs-gk-is-433" class="gkIsWrapper-gk_bikestore">
                    <div class="gkIsPreloader"></div>

                    <figure>
                        <div class="gkIsSlide" style="z-index: 1;" title="Cervelo P3 Ultegra Carbon Triathlon Bike">http://demo.gavick.com/joomla25/bikestore/modules/mod_image_show_gk4/cache/demo.is7gk-is-433.jpg<a href="http://demo.gavick.com/joomla25/bikestore/index.php/2011-08-04-23-47-02/categories/item/107-cervelo-p3-ultegra-carbon-triathlon-bike">link</a></div>


                        <figcaption class="bottom left">
                            <h3><a href="http://demo.gavick.com/joomla25/bikestore/index.php/2011-08-04-23-47-02/categories/item/107-cervelo-p3-ultegra-carbon-triathlon-bike">Cervelo P3 Ultegra Carbon Triathlon</a></h3>
                        </figcaption>
                    </figure>
                    <figure>
                        <div class="gkIsSlide" style="z-index: 2;" title="Pinarello unveil new Dogma 65.1 Think 2 frame">http://demo.gavick.com/joomla25/bikestore/modules/mod_image_show_gk4/cache/demo.is21gk-is-433.jpg<a href="http://demo.gavick.com/joomla25/bikestore/index.php/2011-08-04-23-47-02/categories/item/104-pinarello-unveil-new-dogma-651-think-2-frame">link</a></div>


                        <figcaption class="bottom left">
                            <h3><a href="http://demo.gavick.com/joomla25/bikestore/index.php/2011-08-04-23-47-02/categories/item/104-pinarello-unveil-new-dogma-651-think-2-frame">Pinarello unveil new Dogma 65.1 Thi</a></h3>
                        </figcaption>
                    </figure>

                    <div class="gkIsButtons">
                        <div class="prevSlide">«</div>
                        <div class="nextSlide">»</div>
                    </div>
                </div>
            </section>

            <section id="gkTop1">
                <div>
                    <div class="box alpha gkmod-1"><div><div class="content">
                                <div class="nspMainPortalMode5 nspFs100" id="nsp-nsp_485" data-direction="ltr">
                                    <div class="nspImages">
                                        <div class="nspArts">
                                            <div class="nspArtsScroll">
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/ultimatte-vision-detail"><img class="nspImage tleft fleft" src="images/stories0.jpg" alt="Ultimatte Vision" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/ultimatte-vision-detail" title="Ultimatte Vision">Ultimatte Vision</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/downhill-bikes/kellys-master-detail"><img class="nspImage tleft fleft" src="images/stories1.jpg" alt="KELLYS Master" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/downhill-bikes/kellys-master-detail" title="KELLYS Master">KELLYS Master</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/vivelo-fly-aero-detail"><img class="nspImage tleft fleft" src="images/stories2.jpg" alt="Vivelo Fly Aero" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/vivelo-fly-aero-detail" title="Vivelo Fly Aero">Vivelo Fly Aero</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/category-layout/gt-force-carbon-expert-detail"><img class="nspImage tleft fleft" src="images/stories3.jpg" alt="GT FORCE Carbon Expert" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/category-layout/gt-force-carbon-expert-detail" title="GT FORCE Carbon Expert">GT FORCE Carbon …</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/downhill-bikes/scott-gamble-detail"><img class="nspImage tleft fleft" src="images/stories4.jpg" alt="SCOTT Gamble" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/downhill-bikes/scott-gamble-detail" title="SCOTT Gamble">SCOTT Gamble</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/zaffiro-pro-slick-detail"><img class="nspImage tleft fleft" src="images/stories5.jpg" alt="Zaffiro Pro Slick" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/zaffiro-pro-slick-detail" title="Zaffiro Pro Slick">Zaffiro Pro Slic…</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/rubino-pro-zc-detail"><img class="nspImage tleft fleft" src="images/stories6.jpg" alt="Rubino Pro ZC" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/rubino-pro-zc-detail" title="Rubino Pro ZC">Rubino Pro ZC</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/category-layout/kolo-dhs-climber-2642-detail"><img class="nspImage tleft fleft" src="images/stories7.jpg" alt="KOLO DHS Climber 2642" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/category-layout/kolo-dhs-climber-2642-detail" title="KOLO DHS Climber 2642">KOLO DHS Climber…</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/downhill-bikes/marina-attack-track-detail"><img class="nspImage tleft fleft" src="images/stories8.jpg" alt="MARINA Attack Track" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/downhill-bikes/marina-attack-track-detail" title="MARINA Attack Track">MARINA Attack Tr…</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/category-layout/kolo-dhs-climber-2442-detail"><img class="nspImage tleft fleft" src="images/stories9.jpg" alt="KOLO DHS Climber 2442" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/category-layout/kolo-dhs-climber-2442-detail" title="KOLO DHS Climber 2442">KOLO DHS Climber…</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/category-layout/marina-rock-shock-detail"><img class="nspImage tleft fleft" src="images/storiesA.jpg" alt="MARINA Rock Shock" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/category-layout/marina-rock-shock-detail" title="MARINA Rock Shock">MARINA Rock Shoc…</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/fuji-oval-carbon-detail"><img class="nspImage tleft fleft" src="images/storiesB.jpg" alt="Fuji Oval Carbon" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/fuji-oval-carbon-detail" title="Fuji Oval Carbon">Fuji Oval Carbon</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/downhill-bikes/derbi-dh-20-detail"><img class="nspImage tleft fleft" src="images/storiesC.jpg" alt="Derbi DH 20" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/downhill-bikes/derbi-dh-20-detail" title="Derbi DH 20">Derbi DH 20</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/product-details"><img class="nspImage tleft fleft" src="images/storiesD.jpg" alt="NOX StarTrak DL" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/product-details" title="NOX StarTrak DL">NOX StarTrak DL</a></h4></div>				</div>
                                                <div class="nspArt" style="padding: 0 10px;width:165px;">
                                                    <div style="width:165px;">
                                                        <div class="nspImageGallery"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/ar-tt-9-8-detail"><img class="nspImage tleft fleft" src="images/storiesE.jpg" alt="AR TT 9.8" style="width:163px;height:90px;margin:0 10px;"></a></div>					</div>

                                                    <div class="nspHeadline"><h4 class="nspHeader tcenter fnone"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/ar-tt-9-8-detail" title="AR TT 9.8">AR TT 9.8</a></h4></div>				</div>
                                            </div>	
                                        </div>
                                    </div>

                                    <a class="nspPrev">Prev</a>
                                    <a class="nspNext">Next</a>
                                </div>


                                <script type="text/javascript" src="images/engine01.js"></script>

                                <script type="text/javascript">
            //<![CDATA[
            try {
                $Gavick;
            } catch (e) {
                $Gavick = {};
            }
            ;
            $Gavick["nsp-nsp_485"] = {
                "animation_speed": 400};
            //]]>
                                </script>	</div></div></div>
                </div>
            </section>


            <div id="gkPageContent">

                <section id="gkContent">
                    <div>					



                        <section id="gkMainbody">
                            <div class="box colortitle clear nsp zoom"><div><h3 class="header"><span id="module487">Latest Offers</span></h3><div class="content">	<div class="nspMain nspFs100" id="nsp-nsp_487" style="width:100%;" data-direction="ltr">
                                            <div class="nspArts bottom" style="width:100%;">
                                                <div class="nspTopInterface">
                                                    <div>
                                                        <ul class="nspPagination">
                                                            <li>1</li>
                                                            <li>2</li>
                                                        </ul>


                                                    </div>
                                                </div>

                                                <div class="nspArtScroll1">
                                                    <div class="nspArtScroll2 nspPages2">
                                                        <div class="nspArtPage nspCol2">
                                                            <div class="nspArt nspCol4" style="padding:0 14px;clear:both;">
                                                                <div class="center tcenter fnull gkResponsive"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/zaffiro-pro-slick-detail"><img class="nspImage" src="js/storiesF.jpg" alt="Zaffiro Pro Slick" style="margin:20px 0 0 0;"></a></div><h4 class="nspHeader tcenter fnull"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/zaffiro-pro-slick-detail" title="Zaffiro Pro Slick">Zaffiro Pro Slick</a></h4><div class="nspVmStore"><div class="PricesalesPrice" style="display : block;"><span class="PricesalesPrice">526,59 €</span></div>
                                                                    
                                                                    <div class="PricetaxAmount" style="display : block;">inc. tax: 44,59 €</div></div>							</div>
                                                            <div class="nspArt nspCol4" style="padding:0 14px;">
                                                                <div class="center tcenter fnull gkResponsive"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/ordu-gdi2-detail"><img class="nspImage" src="images/storiesG.jpg" alt="Ordu GDi2" style="margin:20px 0 0 0;"></a></div><h4 class="nspHeader tcenter fnull"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/ordu-gdi2-detail" title="Ordu GDi2">Ordu GDi2</a></h4><div class="nspVmStore"><div class="PricesalesPrice" style="display : block;"><span class="PricesalesPrice">898,04 €</span></div><form method="post" class="product" action="http://demo.gavick.com/joomla25/bikestore/index.php"><div class="addtocart-bar"><span class="quantity-box" style="display: none">
                                                                                <input type="text" class="quantity-input" name="quantity[]" value="1">
                                                                            </span><span class="addtocart-button">
                                                                                <input type="submit" name="addtocart" class="addtocart-button" value="Add to Cart" title="Add to Cart"></span><div class="clear"></div></div>
                                                                        <input type="hidden" class="pname" value="Ordu GDi2">
                                                                        <input type="hidden" name="option" value="com_virtuemart">
                                                                        <input type="hidden" name="view" value="cart">
                                                                        <noscript><input type="hidden" name="task" value="add"></noscript>
                                                                        <input type="hidden" name="virtuemart_product_id[]" value="64">
                                                                        <input type="hidden" name="virtuemart_category_id[]" value="8">
                                                                    </form><div class="PricetaxAmount" style="display : block;">inc. tax: 76,04 €</div></div>							</div>
                                                            <div class="nspArt nspCol4" style="padding:0 14px;">
                                                                <div class="center tcenter fnull gkResponsive"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/ar-tt-9-8-detail"><img class="nspImage" src="images/storiesH.jpg" alt="AR TT 9.8" style="margin:20px 0 0 0;"></a></div><h4 class="nspHeader tcenter fnull"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/ar-tt-9-8-detail" title="AR TT 9.8">AR TT 9.8</a></h4><div class="nspVmStore"><div class="PricesalesPrice" style="display : block;"><span class="PricesalesPrice">857,61 €</span></div><form method="post" class="product" action="http://demo.gavick.com/joomla25/bikestore/index.php"><div class="addtocart-bar"><span class="quantity-box" style="display: none">
                                                                                <input type="text" class="quantity-input" name="quantity[]" value="1">
                                                                            </span><span class="addtocart-button">
                                                                                <input type="submit" name="addtocart" class="addtocart-button" value="Add to Cart" title="Add to Cart"></span><div class="clear"></div></div>
                                                                        <input type="hidden" class="pname" value="AR TT 9.8">
                                                                        <input type="hidden" name="option" value="com_virtuemart">
                                                                        <input type="hidden" name="view" value="cart">
                                                                        <noscript><input type="hidden" name="task" value="add"></noscript>
                                                                        <input type="hidden" name="virtuemart_product_id[]" value="65">
                                                                        <input type="hidden" name="virtuemart_category_id[]" value="8">
                                                                    </form><div class="PricetaxAmount" style="display : block;">inc. tax: 72,61 €</div></div>							</div>
                                                            <div class="nspArt nspCol4" style="padding:0 14px;">
                                                                <div class="center tcenter fnull gkResponsive"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/fuji-oval-carbon-detail"><img class="nspImage" src="images/storiesI.jpg" alt="Fuji Oval Carbon" style="margin:20px 0 0 0;"></a></div><h4 class="nspHeader tcenter fnull"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/fuji-oval-carbon-detail" title="Fuji Oval Carbon">Fuji Oval Carbon</a></h4><div class="nspVmStore"><div class="PricesalesPrice" style="display : block;"><span class="PricesalesPrice">853,24 €</span></div><form method="post" class="product" action="http://demo.gavick.com/joomla25/bikestore/index.php"><div class="addtocart-bar"><span class="quantity-box" style="display: none">
                                                                                <input type="text" class="quantity-input" name="quantity[]" value="1">
                                                                            </span><span class="addtocart-button">
                                                                                <input type="submit" name="addtocart" class="addtocart-button" value="Add to Cart" title="Add to Cart"></span><div class="clear"></div></div>
                                                                        <input type="hidden" class="pname" value="Fuji Oval Carbon">
                                                                        <input type="hidden" name="option" value="com_virtuemart">
                                                                        <input type="hidden" name="view" value="cart">
                                                                        <noscript><input type="hidden" name="task" value="add"></noscript>
                                                                        <input type="hidden" name="virtuemart_product_id[]" value="66">
                                                                        <input type="hidden" name="virtuemart_category_id[]" value="8">
                                                                    </form><div class="PricetaxAmount" style="display : block;">inc. tax: 72,24 €</div></div>							</div>
                                                        </div>
                                                        <div class="nspArtPage nspCol2">
                                                            <div class="nspArt nspCol4" style="padding:0 14px;clear:both;">
                                                                <div class="center tcenter fnull gkResponsive"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/rubino-pro-zc-detail"><img class="nspImage" src="images/storiesJ.jpg" alt="Rubino Pro ZC" style="margin:20px 0 0 0;"></a></div><h4 class="nspHeader tcenter fnull"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/rubino-pro-zc-detail" title="Rubino Pro ZC">Rubino Pro ZC</a></h4><div class="nspVmStore"><div class="PricesalesPrice" style="display : block;"><span class="PricesalesPrice">973,42 €</span></div><form method="post" class="product" action="http://demo.gavick.com/joomla25/bikestore/index.php"><div class="addtocart-bar"><span class="quantity-box" style="display: none">
                                                                                <input type="text" class="quantity-input" name="quantity[]" value="1">
                                                                            </span><span class="addtocart-button">
                                                                                <input type="submit" name="addtocart" class="addtocart-button" value="Add to Cart" title="Add to Cart"></span><div class="clear"></div></div>
                                                                        <input type="hidden" class="pname" value="Rubino Pro ZC">
                                                                        <input type="hidden" name="option" value="com_virtuemart">
                                                                        <input type="hidden" name="view" value="cart">
                                                                        <noscript><input type="hidden" name="task" value="add"></noscript>
                                                                        <input type="hidden" name="virtuemart_product_id[]" value="67">
                                                                        <input type="hidden" name="virtuemart_category_id[]" value="8">
                                                                    </form><div class="PricetaxAmount" style="display : block;">inc. tax: 82,42 €</div></div>							</div>
                                                            <div class="nspArt nspCol4" style="padding:0 14px;">
                                                                <div class="center tcenter fnull gkResponsive"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/clv-carbon-detail"><img class="nspImage" src="images/storiesK.jpg" alt="CLV Carbon" style="margin:20px 0 0 0;"></a></div><h4 class="nspHeader tcenter fnull"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/clv-carbon-detail" title="CLV Carbon">CLV Carbon</a></h4><div class="nspVmStore"><div class="PricesalesPrice" style="display : block;"><span class="PricesalesPrice">672,00 €</span></div><form method="post" class="product" action="http://demo.gavick.com/joomla25/bikestore/index.php"><div class="addtocart-bar"><span class="quantity-box" style="display: none">
                                                                                <input type="text" class="quantity-input" name="quantity[]" value="1">
                                                                            </span><span class="addtocart-button">
                                                                                <input type="submit" name="addtocart" class="addtocart-button" value="Add to Cart" title="Add to Cart"></span><div class="clear"></div></div>
                                                                        <input type="hidden" class="pname" value="CLV Carbon">
                                                                        <input type="hidden" name="option" value="com_virtuemart">
                                                                        <input type="hidden" name="view" value="cart">
                                                                        <noscript><input type="hidden" name="task" value="add"></noscript>
                                                                        <input type="hidden" name="virtuemart_product_id[]" value="68">
                                                                        <input type="hidden" name="virtuemart_category_id[]" value="8">
                                                                    </form><div class="PricetaxAmount" style="display : block;">inc. tax: 62,16 €</div></div>							</div>
                                                            <div class="nspArt nspCol4" style="padding:0 14px;">
                                                                <div class="center tcenter fnull gkResponsive"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/vivelo-fly-aero-detail"><img class="nspImage" src="images/storiesL.jpg" alt="Vivelo Fly Aero" style="margin:20px 0 0 0;"></a></div><h4 class="nspHeader tcenter fnull"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/vivelo-fly-aero-detail" title="Vivelo Fly Aero">Vivelo Fly Aero</a></h4><div class="nspVmStore"><div class="PricesalesPrice" style="display : block;"><span class="PricesalesPrice">1100,00 €</span></div><form method="post" class="product" action="http://demo.gavick.com/joomla25/bikestore/index.php"><div class="addtocart-bar"><span class="quantity-box" style="display: none">
                                                                                <input type="text" class="quantity-input" name="quantity[]" value="1">
                                                                            </span><span class="addtocart-button">
                                                                                <input type="submit" name="addtocart" class="addtocart-button" value="Add to Cart" title="Add to Cart"></span><div class="clear"></div></div>
                                                                        <input type="hidden" class="pname" value="Vivelo Fly Aero">
                                                                        <input type="hidden" name="option" value="com_virtuemart">
                                                                        <input type="hidden" name="view" value="cart">
                                                                        <noscript><input type="hidden" name="task" value="add"></noscript>
                                                                        <input type="hidden" name="virtuemart_product_id[]" value="69">
                                                                        <input type="hidden" name="virtuemart_category_id[]" value="8">
                                                                    </form><div class="PricetaxAmount" style="display : block;">inc. tax: 101,75 €</div></div>							</div>
                                                            <div class="nspArt nspCol4" style="padding:0 14px;">
                                                                <div class="center tcenter fnull gkResponsive"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/ultimatte-vision-detail"><img class="nspImage" src="images/storiesM.jpg" alt="Ultimatte Vision" style="margin:20px 0 0 0;"></a></div><h4 class="nspHeader tcenter fnull"><a href="http://demo.gavick.com/joomla25/bikestore/index.php/virtuemart/default-layout/triathlon-tt/ultimatte-vision-detail" title="Ultimatte Vision">Ultimatte Vision</a></h4><div class="nspVmStore"><div class="PricesalesPrice" style="display : block;"><span class="PricesalesPrice">1110,00 €</span></div><form method="post" class="product" action="http://demo.gavick.com/joomla25/bikestore/index.php"><div class="addtocart-bar"><span class="quantity-box" style="display: none">
                                                                                <input type="text" class="quantity-input" name="quantity[]" value="1">
                                                                            </span><span class="addtocart-button">
                                                                                <input type="submit" name="addtocart" class="addtocart-button" value="Add to Cart" title="Add to Cart"></span><div class="clear"></div></div>
                                                                        <input type="hidden" class="pname" value="Ultimatte Vision">
                                                                        <input type="hidden" name="option" value="com_virtuemart">
                                                                        <input type="hidden" name="view" value="cart">
                                                                        <noscript><input type="hidden" name="task" value="add"></noscript>
                                                                        <input type="hidden" name="virtuemart_product_id[]" value="70">
                                                                        <input type="hidden" name="virtuemart_category_id[]" value="8">
                                                                    </form><div class="PricetaxAmount" style="display : block;">inc. tax: 102,68 €</div></div>							</div>
                                                        </div>
                                                    </div>
                                                </div>	
                                            </div>
                                        </div>

                                        <script type="text/javascript">
                                            //<![CDATA[
                                            try {
                                                $Gavick;
                                            } catch (e) {
                                                $Gavick = {};
                                            }
                                            ;
                                            $Gavick["nsp-nsp_487"] = {
                                                "animation_speed": 400,
                                                "animation_interval": 5000,
                                                "animation_function": Fx.Transitions.Expo.easeIn,
                                                "news_column": 4,
                                                "news_rows": 1,
                                                "links_columns_amount": 1,
                                                "links_amount": 1,
                                                "counter_text": '<strong>Page:</strong>'
                                            };
                                            //]]>
                                        </script>	</div></div></div>
                        </section>

                    </div>
                </section>

            </div>


            <section id="gkBottom2">
                <div>
                    <div class="box banner gkmod-3"><div><div class="content">

                                <div class="custom banner">
                                    <p><img src="images/bottom_b.png" border="0" alt="Banner 1"></p></div>
                            </div></div></div><div class="box banner gkmod-3"><div><div class="content">

                                <div class="custom banner">
                                    <p><img src="images/bottom_c.png" border="0" alt="Banner 2"></p></div>
                            </div></div></div><div class="box banner gkmod-3"><div><div class="content">

                                <div class="custom banner">
                                    <p><img src="images/bottom_d.png" border="0" alt="Banner 3"></p></div>
                            </div></div></div>
                </div>
            </section>

            <section id="gkBottom3">
                <div>
                    <div class="box gkmod-4"><div><h3 class="header"><span id="module467">About Us</span></h3><div class="content">

                                <div class="custom">
                                    <ul>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Lorem ipsum dolor</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Sit amet</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Donec sed odio dui</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Etiam porta sem malesuada</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Magna mollis euismod.</a></li>
                                    </ul></div>
                            </div></div></div><div class="box gkmod-4"><div><h3 class="header"><span id="module468">Guest Services </span></h3><div class="content">

                                <div class="custom">
                                    <ul>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Lorem ipsum dolor</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Sit amet</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Donec sed odio dui</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Etiam porta sem malesuada</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Magna mollis euismod.</a></li>
                                    </ul></div>
                            </div></div></div><div class="box gkmod-4"><div><h3 class="header"><span id="module469">Catalog</span></h3><div class="content">

                                <div class="custom">
                                    <ul>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Lorem ipsum dolor</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Sit amet</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Donec sed odio dui</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Etiam porta sem malesuada</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Magna mollis euismod.</a></li>
                                    </ul></div>
                            </div></div></div><div class="box gkmod-4"><div><h3 class="header"><span id="module470">Find Us</span></h3><div class="content">

                                <div class="custom">
                                    <ul>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Lorem ipsum dolor</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Sit amet</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Donec sed odio dui</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Etiam porta sem malesuada</a></li>
                                        <li><a href="http://demo.gavick.com/joomla25/bikestore/#">Magna mollis euismod.</a></li>
                                    </ul></div>
                            </div></div></div>
                </div>
            </section>


            <footer class="gkFooter">

                <ul class="menu">
                    <li class="item-152"><a href="http://demo.gavick.com/joomla25/bikestore/#">Owners Manuals</a></li><li class="item-153"><a href="http://demo.gavick.com/joomla25/bikestore/#">Job Openings </a></li><li class="item-466"><a href="http://demo.gavick.com/joomla25/bikestore/#">FAQs</a></li><li class="item-468"><a href="http://demo.gavick.com/joomla25/bikestore/#">Contact Us</a></li><li class="item-469"><a href="http://demo.gavick.com/joomla25/bikestore/#">Site Map</a></li></ul>


                <p>Template Design © <a href="http://www.gavick.com/" title="Joomla Templates">Joomla Templates</a> GavickPro. All rights reserved.</p>
            </footer> 

            <div id="gkStyleArea">
                <a href="http://demo.gavick.com/joomla25/bikestore/#" id="gkColor1">Orange</a>
                <a href="http://demo.gavick.com/joomla25/bikestore/#" id="gkColor2">Blue</a>
                <a href="http://demo.gavick.com/joomla25/bikestore/#" id="gkColor3">Green</a>
            </div>

            <a href="http://www.gavick.com/" id="gkFrameworkLogo" title="Gavern Framework">Gavern Framework</a>
        </div>


        <div id="gkPopupLogin">	
            <div class="gkPopupWrap">
                <div id="loginForm">
                    <h3>Login <small> or <a href="http://demo.gavick.com/joomla25/bikestore/index.php?option=com_users&amp;view=registration">Register</a></small></h3>

                    <div class="clear overflow">
                        <form action="http://demo.gavick.com/joomla25/bikestore/index.php" method="post" id="login-form">
                            <fieldset class="userdata">
                                <p id="form-login-username">
                                    <label for="modlgn-username">User Name</label>
                                    <input id="modlgn-username" type="text" name="username" class="inputbox" size="24">
                                </p>
                                <p id="form-login-password">
                                    <label for="modlgn-passwd">Password</label>
                                    <input id="modlgn-passwd" type="password" name="password" class="inputbox" size="24">
                                </p>
                                <div id="form-login-remember">
                                    <input id="modlgn-remember" type="checkbox" name="remember" class="inputbox" value="yes">
                                    <label for="modlgn-remember">Remember Me</label>
                                </div>
                                <div id="form-login-buttons">
                                    <input type="submit" name="Submit" class="button" value="Log in">
                                </div>
                                <input type="hidden" name="option" value="com_users">
                                <input type="hidden" name="task" value="user.login">
                                <input type="hidden" name="return" value="aW5kZXgucGhwP0l0ZW1pZD0xMDE=">
                                <input type="hidden" name="f258230d1d15268b53e7e8c95bfddd26" value="1">		</fieldset>
                            <ul>
                                <li>

                                </li>
                                <li> <a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/password-reset"> Forgot your password?</a> </li>
                                <li> <a href="http://demo.gavick.com/joomla25/bikestore/index.php/template/lorem-ipsum-ii/users/username-reminder-request"> Forgot your username?</a> </li>
                            </ul>
                            <div class="posttext">  </div>
                        </form>


                    </div>
                </div>	     
            </div>
        </div>


        <div id="gkPopupCart">	
            <div class="gkPopupWrap">	
                <div id="gkAjaxCart"></div>
            </div>
        </div>
        <div id="gkPopupOverlay"></div>

        <div id="gkfb-root"></div>

        <script type="text/javascript">
            //<![CDATA[

            window.fbAsyncInit = function() {

                FB.init({appId: '171342606239806',
                    status: true,
                    cookie: true,
                    xfbml: true,
                    oauth: true

                });




            };

            //      

            window.addEvent('load', function() {

                (function() {

                    if (!document.getElementById('fb-root')) {

                        var root = document.createElement('div');

                        root.id = 'fb-root';

                        document.getElementById('gkfb-root').appendChild(root);

                        var e = document.createElement('script');

                        e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';

                        e.async = true;

                        document.getElementById('fb-root').appendChild(e);

                    }

                }());

            });

            //]]>

        </script>
        <!-- +1 button -->
        <!-- twitter -->
        <!-- Pinterest script --> 
    </body>
</html>