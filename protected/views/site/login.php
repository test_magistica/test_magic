﻿<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login'; 
$this->breadcrumbs = array(
    'Login',
);
?>
<div class="registerpage">
    <?php if (Yii::app()->user->hasFlash('error')): ?>
    <div class="flash-error">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
    <?php endif; ?>
    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>        
        <a><?php echo ucfirst(Yii::t("app", "Вход")); ?></a> 
    </div>
    <h1><?php echo Yii::t("app", "Вход на сайт") ?></h1>
    <article>  
        <p>Пожалуйста, заполните следующую форму с учетные данные для входа:</p>
    </article>

    <!-- Форма -->
    <div class="shadowblock registerform">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
                ));
        ?>
        <div class="socialminiicons">
            <em>Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</em>
        </div>
        <div class="clr"></div>
        <div style="margin-left: 95px; padding-right: 80px;">
            <?php echo $form->errorSummary($model); ?>
        </div>
        <div class="field" style="display: none;">
            <?php echo $form->error($model, 'username'); ?>
            <?php echo $form->error($model, 'password'); ?>
            <?php echo $form->error($model, 'rememberMe'); ?>
        </div>
        <br />
        <div class="field">
            <span>Логин*</span>
            <?php echo $form->textField($model, 'username', array('class' => 'inp')); ?>
        </div>

        <div class="field">
            <span>Пароль*</span>          
            <?php echo $form->passwordField($model, 'password', array('class' => 'inp')); ?>
        </div>

        <div class="rules" style="margin: 0 0 10px 240px;">
            <?php echo $form->checkBox($model, 'rememberMe', array('class' => 'check', 'uncheckValue' => null)); ?>
            <?php echo $form->label($model, 'rememberMe'); ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::submitButton('Войти', array('class' => 'submit')); ?>
            <span class="submit_or">ИЛИ</span>
            <?php echo CHtml::link("Регистрация", Yii::app()->createAbsoluteUrl("user/create"))?>
            <?php // echo CHtml::submitButton('Войти', array('class' => 'submit','style' => 'margin-right:110px;')); ?>
        </div>
        

        <?php $this->endWidget(); ?>
    </div><!-- form -->
</div>
<h2>Заказать звонок эксперта по предоплате 8-800-555-3317</h2>

