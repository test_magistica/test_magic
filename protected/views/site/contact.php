<?php
$modelP = Page::model()->find("`name`='contact'");
$category = Category::model()->findByAttributes(array("url" => Yii::app()->request->requestUri));
if ($category !== NULL) {
    if ($category->metaKey != "")
        $this->pageKeywords = explode(',', mb_strtolower($category->metaKey, 'UTF-8'));
    if ($category->metaDesc != "")
        $this->pageDescription = $category->metaDesc;
    if ($category->metaTitle != "")
        $this->pageTitle = $category->metaTitle;
}else {
    $this->pageTitle = Yii::app()->name . ' - Контакты';
    $this->pageKeywords = explode(',', mb_strtolower($modelP->metaKey, 'UTF-8'));
    $this->pageDescription = $modelP->metaDesc;
}
?>
    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a href="<?php echo Yii::app()->createUrl("site/contact"); ?>">Контакты</a>        
    </div>
<h1>Контакты экстрасенсов по телефону</h1>

<?php if (Yii::app()->user->hasFlash('contact')): ?>

    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('contact'); ?>
    </div>

<?php else: ?>

    <?php echo $modelP->description; ?>

    <div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'contact-form',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
        ?>

        <p class="note">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</p>

        <?php echo $form->errorSummary($model); ?>
        <div class="row" style="color:#AE4EC9;">
            <?php echo $form->error($model, 'name'); ?>
            <?php echo $form->error($model, 'email'); ?>
            <?php echo $form->error($model, 'subject'); ?>
            <?php echo $form->error($model, 'body'); ?>
        </div>
        <table class="contact-form">
            <tr>
                <td><?php echo $form->labelEx($model, 'name'); ?></td>
                <td><?php echo $form->textField($model, 'name'); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model, 'email'); ?></td>
                <td><?php echo $form->textField($model, 'email'); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model, 'subject'); ?></td>
                <td><?php echo $form->textField($model, 'subject', array('size' => 60, 'maxlength' => 128)); ?></td>
            </tr>
            <tr>
                <td><?php echo $form->labelEx($model, 'body'); ?></td>
                <td><?php echo $form->textArea($model, 'body', array('rows' => 6, 'cols' => 50)); ?></td>
            </tr>
            <tr>
                <td>
                    <?php if (CCaptcha::checkRequirements()): ?>
                        <?php echo $form->labelEx($model, 'verifyCode'); ?>
                        <?php $this->widget('CCaptcha'); ?>
                    </td>
                    <td>
                        <?php echo $form->textField($model, 'verifyCode'); ?>
                        <span style="color:#AE4EC9;"><?php echo $form->error($model, 'verifyCode'); ?></span>
                    <?php endif; ?>
                </td>
            </tr>
        </table>


        <div class="row buttons-contact">
            <?php echo CHtml::submitButton('Отправить'); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->

<?php endif; ?>