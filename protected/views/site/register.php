<section>
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/lightbox.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/lightbox.js" type="text/javascript"></script>

<div class="zakaz_page">

<h1>Заказ телефонной консультации с экспертом</h1>

<?php if (Yii::app()->user->hasFlash('сonsultation')) { ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('сonsultation'); ?>
</div>

<? }
else {?>
	<? if (Yii::app()->user->hasFlash('error')) { ?>
	<div class="flash-error">
		<?php echo Yii::app()->user->getFlash('error'); ?>
	</div>
	<? } ?>

<h4>
	Консультация экстрасенса поможет вам разобраться в любой сложной и запутанной жизненой ситуации, и получить
	ответы, на самые сложные вопросы, вставшие на вашем жизненном пути.
</h4>
<img class="steps" src="<?=Yii::app()->createUrl("images/steps.png")?>" />
<div class="shadowblock zakaz_page_block">
	<div class="imgbox_exp"><?=$infoBlock;?></div>
	<div class="expts">
		<div class="selectbox selectbox2">
		<?=CHtml::dropDownList('expts', 'expts', CHtml::listData($expertList, 'id', 'name'),
				array('options' => array($_GET['id']=>array('selected'=>true)),'class' => 'selectBlock2'));
			?>
		</div> <div class="clear"></div>
		<p class="pr15px">Вы заказываете обратный звонок от эксперта <b class="exp_name"><?php echo $expert['name']; ?></b></p>
	</div>

<div class="clear"></div>
<br>
<hr>



<h2>Оформление заказа:</h2>
	<?
	$this->widget('xupload.XUpload', array(
		'url'           => Yii::app()->createUrl("site/upload"),
		'model'         => new XUploadForm,
		'attribute'     => 'file',
		'showForm'      => true,
		'multiple'      => false,
		'formView'      => 'form_consult',
		'downloadView'  => 'download_consult',
		'autoUpload'    => true,
		'options'       => array(
			'maxFileSize' => 5000000,
			'acceptFileTypes' => "js:/(\.|\/)(jpe?g|png)$/i",
			'complete' => 'js:function (result) {
				a = jQuery.parseJSON(result.responseText.slice(1, -1));
				$(".upload_image").val(a.realName);
			}',
		)
	));
	?>
	<?php
	$form = $this->beginWidget('CActiveForm', array(
		'id' => 'site-form',
		'enableAjaxValidation' => true,
		'clientOptions'=> array(
			'validateOnSubmit'=>true
		),
	));
	?>

<!-- Форма -->
<div class="formblock">
	<div style="margin-left: 95px;">
		<?php echo $form->errorSummary($model); ?>
	</div>
	<div class="field" style="display: none;">
		<?php echo $form->error($model, 'duration'); ?>
		<?php echo $form->error($model, 'date_call'); ?>
		<?php echo $form->error($model, 'time_call'); ?>
		<?php echo $form->error($model, 'phone'); ?>
		<?php echo $form->error($model, 'guest_name'); ?>
		<?php echo $form->error($model, 'email'); ?>
	</div>

	<div class="clr"></div>
	<div class="select-patch">

		<label>Длительность разговора:</label>
		<input id="tariff" type="hidden" value="<?php echo $expert['tariff']; ?>" name="tariff">
		<div class="selectbox selectbox2">
			<?php
			echo $form->dropDownList($model, 'duration', array(5 => 5, 8 => 8, 10 => 10, 15 => 15, 20 => 20, 30 => 30, 45 => 45), array(
				'class' => 'selectBlock2',
				'empty' => '--',
			));
			?>
		</div>
		<script>
			$(document).ready(function() {
				$('#Consultation_duration').on('change', function() {
					if (!$(this).val()){
						$('.itog input[type=submit]').val('Оплатить заказ');
					}
					else{
						$('.itog input[type=submit]').val('Оплатить заказ '+parseInt($(this).val()) * parseInt($('#tariff').val()) + ' руб.');
					}
				});
				$.ajax({
					type: "GET",
					url: "/extrasens/view?id=<?=$_GET['id']?>&tab=otzyvy&all=1&nonform=1",
					dataType: "html",
					success: function(data) {
						$('.review_res').html(data);
					}
				});
				$('#Consultation_date_call').inputmask("99.99.9999").val('<?=date('d.m.Y', time())?>');
				$('#Consultation_time_call').inputmask("99:99");

				$('#schedule').on('click', function(){
					vvv = $('.schedule_res').html() == '';
					if(vvv){
						$('.schedule_res').html(vvv ? '<img src="/images/facybox/loading.gif">' : '');
						$('p #schedule').text('скрыть расписание');
						vid = $('#expts').val();
						$.ajax({
							type: "GET",
							url: "/extrasens/view?id="+vid+"&tab=raspisanie",
							dataType: "html",
							success: function(data) {
								$('.schedule_res').html(data);;
							}
						});
					}
					else{
						$('p #schedule').text('смотреть расписание');
						$('.schedule_res').html('');
					}
				});

				$('#expts').on('change', function(){
					vvv = $('.schedule_res').html() == '';
					$('#Consultation_expertID').val(this.value);
					$('.exp_name').html($('#expts option:selected').text());
					$('.imgbox_exp').html('<img src="/images/facybox/loading.gif">');
					$('.review_res').html('<img src="/images/facybox/loading.gif">');
					if( ! vvv)
						$('.schedule_res').html('<img src="/images/facybox/loading.gif">');

					$.ajax({
						type: "GET",
						url: "/site/expertInfo?idx="+this.value+"&nonform=1&all=1",
						dataType: "json",
						success: function(data) {
							$('.imgbox_exp').html(data.info);
							$('.review_res').html(data.reviews);
							if( ! vvv)
								$('.schedule_res').html(data.schedule);
						}
					});
				})
			});
			function changeCountry(elem){
				$("#Consultation_phone").val($(elem).val());
				$("#inp2_hd").val($(elem).val());
			}
			function changeTel(elem, e){
				var currPref = $("#inp2_hd").val();
				if($(elem).val() == currPref && e.keyCode == 8){
					$(elem).val(currPref);
				}

			}
			function changeTelUp(elem, e){
				var currPref = $("#inp2_hd").val();
				if($(elem).val().length < currPref.length && e.keyCode == 8){
					$(elem).val(currPref);
				}
				else{
					var t = $(elem).val().split(' ');
					if(t.length > 1){
						$(elem).val(t.join(''));
					}
				}

			}
		</script>
		<span>минут</span>
		<div class="clr"></div>


		<!-- Номер телефона -->
		<label>Номер телефона</label>

		<div class="selectbox">
			<?php
			echo $form->dropDownList(
				$model,
				'country', Controller::getCountry(),
				array(
					'class' => 'selectBlock',
					'onchange' => 'changeCountry(this);',
				)
			);
			?>
		</div>

		<?php
		//$phone = isset($model->country)?(isset($model->errors['phone']) ? $model->country : $model->phone): "+7";
		$phone="+7";
		if ( ! Yii::app()->user->isGuest)
			$phone = $this->getUser(Yii::app()->user->id, 'tele');

		echo $form->textField(
			$model,
			'phone',
			array(
				'class' => 'date_inp border3',
				'size'=>'20',
				//    'style'=> 'width : 20px;',
				'maxlength'=> "14",
				"value" => $phone,
				'onkeydown' => 'changeTel(this,event);',
				'onkeyup' => 'changeTelUp(this,event);',
			)
		);
		echo CHtml::tag("input", array('type' => 'hidden', 'id'=>'inp2_hd', 'value' => $phone));
		?>

		<div class="clr"></div>
		<label>Дата и время звонка:</label>
		<script type="text/javascript" src="/js/jquery.inputmask.js"></script>

		<?php
		echo '<div style="display:none">'.
			$form->labelEx($model,'date_call').'</div>';
		Yii::import('zii.widgets.jui.CJuiDatePicker');
		$this->widget('CJuiDatePicker',array(
				'name'=>'Consultation[date_call]',
				'value'=>$model->date_call,
				'options'=>array('showAnim'=>'slideDown',
					'showOn'=> 'both',
					'buttonImage' => '/images/calendar.png',
					'dateFormat' => 'dd.mm.yy',
					'navigationAsDateFormat' => true,
					'minDate' => 'new Date()',
					'monthNames' => array("Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"),
					'dayNames' => array("Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"),
					'dayNamesMin'=>array("Вс","Пн","Вт","Ср","Чт","Пт","Сб")
				),
				'htmlOptions'=> array('size' => '10','class' => 'date_inp border3','style' => 'z-index:10;')
			)
		);

		?>
		<span>в </span> <?=$form->textField($model, 'time_call', array('class' => 'time_inp border3', 'value' => date('H:i', time() + 7200)));?>
		<span class="mosc_time">время<br>Москвы</span>
		<div class="clr"></div>
		<br>
		<p>(Рекомендуем вам бронировать звонок за 1-2 часа) <b>></b> <a href="javascript: void(0);" id="schedule">смотреть расписание</a></p>
		<br>
		<br>
		<div class="schedule_res"></div>

		<!-- #Имя -->
		<?php
		if ( Yii::app()->user->isGuest ) {
			echo $form->hiddenField($model,'userID', array('value'=> '0'));

			echo '<label>Имя</label>'.
				$form->textField($model, 'guest_name', array('class' => 'border3 inp')).
				'<div class="clr"></div>';
		}
		else {
			echo $form->hiddenField($model,'userID', array('value'=> Yii::app()->user->id));
			echo $form->hiddenField($model,'guest_name', array('value'=> ''));
		}
		echo $form->hiddenField($model, 'expertID', array('value'=> $_GET['id']));
		echo $form->hiddenField($model, 'image', array('class' => 'upload_image'));
		?>

		<!-- #Номер телефона -->
		<div class="clr"></div>
		<? if (Yii::app()->user->isGuest) { ?>
			<label>E-mail</label>
			<?php echo $form->textField($model, 'email', array('class' => 'inp border3')); ?>
		<? } else {
			echo $form->hiddenField($model, 'email', array('value'=> Users::model()->findByPk(Yii::app()->user->id)->email));
		} ?>
		<div class="clr"></div>
		<br>
		<hr><br>
		<div class="itog">
			<?=CHtml::submitButton('Оплатить заказ', array('class' => 'submit')); ?>
		</div>
	</div>
</div>




	<?php $this->endWidget(); ?>
</div>
<div class="clr"></div>

<h1>отзывы</h1>
<div class="review_res"></div>
	<? } ?>


</div><!-- #page -->

</section>