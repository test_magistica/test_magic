<?php
$this->breadcrumbs=array(
	'Answers'=>array('index'),
	'Update Answer #'.$model->id,
);
?>

<h1>Update Answer #<?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>