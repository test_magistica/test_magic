<div class="user-cab">

    <h1>История звонков</h1>

    <p>
        Здесь представлена статистика по всем вашим консультациям помесячно. Просим обратить внимание, что статистика за текущий день обновлятся раз в три часа.
    </p>
    <p>
        Вы можете посмотреть статистику за последние четыре месяца выбрав соответствующиий месяц из списка в выпадающем меню.
    </p>
    <?php $s = isset($_GET['date']) ? $_GET['date'] : ''; ?>
    <?php echo CHtml::dropDownList('filter', $s, $this->getMonthYear(), array('onchange' => 'javascript:window.location = "/statistic/index.html?date="+this.value;', 'empty' => '--')); ?>
    <div class="summary">  
        Итого предоплатных звонков: 212 минут<br>
        Итого бесплатных звонков: 323 минут<br>
        Всего потрачено: 1221 рублей
    </div>

    <table class="user_stat">
        <tbody>
            <tr>
        	<th>Тип</th>
                <th>Эксперт</th>
                <th>Дата/время</th>
                <th>Длительность(минут)</th>
                <th>Стоимость</th>
                <th>Статус</th>
            </tr>
	    <?php 
		//$expert = Users::model()->findByPk(
		$user_id = Yii::app()->user->id; 
		//$cj = CallJournal::model()->findAllBySql('SELECT calljournal.*,sec_to_time(duration) as duration  FROM `calljournal` WHERE `caller_id`='.$user_id.
		//    ' OR `called_id`='.$user_id);
		foreach ($models as $k=>$model)
		{
	    ?>
                <tr>
                    <?php
                        if($model['caller_id'] != $user_id)//входящий
                        {
                    	    echo "<td>Входящий</td>";
                    	    $expert = Users::model()->findByPk($model['caller_id']);
                    	}
                    	else 
                    	{
                    	    echo "<td>Исходящий</td>";
                    	    $expert = Users::model()->findByPk($model['called_id']);
                    	} 
                     ?>
                    <td><?php echo $expert['name'];?></td>
                    <td><?php echo $model['call_date'];?></td>
                    <td><?php echo ceil($model['duration']/60);?></td>
                    <td><?php echo $model['cost'];?></td>
                    <td><?php Controller::getNameStatus($model['status']);?></td>
                </tr>
    	    <?php };?>
        </tbody>
    </table>
    <div class="pager">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $pages,
            'header' => '',
        ));
        ?>
    </div>
</div>