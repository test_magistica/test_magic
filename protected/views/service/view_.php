<?php
$image = !empty($model['image']) ? $model['image'] : 'nophoto.jpg';
?>
 <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a href="<?php echo Yii::app()->createUrl("service"); ?>">Наши услуги</a>        
            <a><?php echo $model['notes']; ?></a>        
    </div>  
<section class="usluga" style="margin: 0px 0 30px 18px !important;">
    <div class="usluga">
        <h1><?php echo $model['notes']; ?></h1>
        <div class="order-box">
            <div class="wrap-img">
                <div class="clip"></div>
                <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/service/<?php echo $image; ?>">
            </div>
            <div class="price">
                <span><?php echo $model['honorarium']; ?>&nbsp;</span>руб.
                <?php echo CHtml::link('Заказать', array('service/booking', 'id' => $model['id'])); ?>                
            </div>
            <span>Срок выполнения: <?php echo $model['period']; ?></span>
        </div>
        <div class="usluga-text" style="float:left; text-align:justify; width:430px;">
            
                <?php echo $model['description']; ?>    
            
            <span>
                <p><span>Обьем услуги:</span>
                    <?php echo $model['volume']; ?>
                </p>
                <p><span>Аннотация:</span>
                    <?php echo $model['annotation']; ?>
                </p>
                <p><span>Требования:</span>
                    <?php echo $model['requirements']; ?>
                </p>	
            </span>
            <?php $image = $this->getUserImage($model['expertID']); ?>
            <div class="exp-avatar">
                <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $image; ?>">

                <a href="<?php echo Yii::app()->request->baseUrl; ?>/extrasens/view/<?php echo $model['expertID']; ?>"><?php echo Controller::getUser($model['expertID'], 'name'); ?> <?php echo Controller::getUser($model['expertID'], 'surname'); ?></a><br>
                <span><?php echo Controller::getUser($model['expertID'], 'profession'); ?></span>
            </div>
        </div>
        <?php foreach ($reviews as $review): ?>
            <div class="review" style="margin-left:125px; width:540px;">
                <?php $y = explode('.', Controller::getUser($review['userID'], 'birthday')); ?>
                <a>
                    <?php echo Controller::getUser($review['userID'], 'name'); ?>, <?php echo date('Y') - $y[2]; ?> лет
                </a>	
                <span>
                    <?php echo $review['description']; ?>
                </span>
            </div>
        <?php endforeach; ?>                
    </div>
    <div class="chatformbox">
        <div id="show-review">
            <div class="ttl" style="float: left;">Написать сообщение:</div>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'register-form',
                //'enableClientValidation' => TRUE,
                //'htmlOptions' => array('class' => 'searchbox'),
                'enableAjaxValidation' => FALSE,
                'clientOptions' => array(
                    'validateOnSubmit' => TRUE,
                ),
            ));
            ?>
            <?php echo $form->textArea($r, 'description', array('class' => 'border5')); ?>
            <?php echo $form->error($r, 'description'); ?>
        </div>
        <span class="write_review-button" id="fake-submit">ОСТАВИТЬ ОТЗЫВ</span>
        <?php echo CHtml::submitButton('ОСТАВИТЬ ОТЗЫВ', array('class' => 'write_review-button', 'id' => 'true-submit')); ?>                
        <?php $this->endWidget(); ?>
    </div>
</section>