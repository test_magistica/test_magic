<?php
$image = !empty($model['image']) ? $model['image'] : 'nophoto.jpg';
?>
<?php $pageKeywords = new HKeywords(); 
      $pageKeywords = array_merge(
                                explode(',', 'гадание онлайн, ясновидящие, экстрасенсы'),
                                explode(',', $pageKeywords->get_keywords($model->description))); 
      $pageTitle = 'Название услуги: '.Controller::getUser($model['expertID'], 'profession').' | Гадание онлайн по телефону';
?>

<?php $this->pageKeywords =  trim($model->metaKey)<>'' ? explode(',',  mb_strtolower($model->metaKey, 'UTF-8')) : $pageKeywords ; ?>
<?php $this->pageDescription = trim(CHtml::encode($model->metaDesc))<>'' ? CHtml::encode($model->metaDesc) : CHtml::encode($model->notes); ?>
<?php $this->pageTitle = trim(CHtml::encode($model->metaTitle))<>'' ? $model->metaTitle : $pageTitle; ?>

<?//php if($model->metaKey!="")Yii::app()->clientScript->registerMetaTag($model->metaKey, 'keywords'); ?>
<?//php if($model->metaDesc!="")Yii::app()->clientScript->registerMetaTag($model->metaDesc, 'description'); ?>
<?//php if($model->metaTitle!="") $this->pageTitle = $model->metaTitle; ?>

<section class="all-services">
	<div itemprop="bredcrumb" class="bread">
	    <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
	   <a href="<?php echo Yii::app()->createUrl("service"); ?>">Наши услуги</a>
	    <a><?php echo $model['notes']; ?></a>
	</div>
	<div class="clr"></div>
	<div class="service-wrap"><!-- Service wrap -->
		<div class="service complete"><!-- Блок сервис -->
                    <div class="image_zoom"><img class="poster" alt="" src="/uploads/service/<?php echo $image; ?>"></div><!-- главное фото -->
			<div class="clr"></div>
			<div class="center">
				<div class="period_complete">&nbsp;&nbsp;Срок выполнения: <b><?php echo $model['period']; ?></b></span>
			</div>
				<!--div class="service-guru">
		                    <div class="border-bottom"></div>
		                    <h3>Эксперты:</h3>
		                    <?php $image = $this->getUserImage($model['expertID']); ?>
		                    <?php //echo CHtml::link($this->getUser($model['expertID'], 'name') . ' ' . $this->getUser($model['expertID'], 'surname'), array('extrasens/view/', 'id' => $model['expertID'])); ?>
		                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/extrasens/view/<?php echo $model['expertID']; ?>">
		                        <img width="50" height="50" alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $image; ?>">
		                        <i>
		                            <?php echo $this->getUser($model['expertID'], 'name') . ' ' . $this->getUser($model['expertID'], 'surname'); ?>
		                            <b></b>
		                        </i>
		                    </a>
		                </div-->
				<div class="clr"></div>
				<div class="center">
					<? $params = array($model->paymentPath ? $model->paymentPath : 'service/booking');
					if( ! $model->paymentPath) $params['id'] = $model->id;?>
					<?php echo CHtml::link('Заказать услугу', $params, array('class'=>'get-service')); ?>
				</div>
				<!-- #Блок сервис -->
				<div class="price-corner"><?php echo $model['honorarium']; ?></div>
			</div>
		</div>
	<?php
		if($model['expertID']){?>
		<div class="clr"></div>
		<div class="ex_block">
			<?php
			$image = $this->getUserImage($model['expertID']); ?>
			<div class="exp-avatar">
				<img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $image; ?>">

				<span>Предоставляет услугу:</span>
                                <a href="<?php echo Yii::app()->createUrl("/extrasens/view", array('id' => $model->expertID, 'title' => $this->getCHPU($this->getUser($model->expertID, 'name')."-".$this->getUser($model->expertID, 'profession')))); ?>"><?php echo Controller::getUser($model['expertID'], 'name'); ?> <?php echo Controller::getUser($model['expertID'], 'surname'); ?></a><br>
				<span class="profession"><?php echo Controller::getUser($model['expertID'], 'profession'); ?></span>
			</div>
		</div>
	<? } ?>
		<div class="clr"></div>
	</div>
	<!-- #Service wrap ----------------------------------------------------------------------------------------------------------------->
	<div class="usluga">
		<h1><?php echo $model['notes']; ?></h1>
		<div class="usluga-text">
			<?php echo $model['description']; ?>

			<!--span>
                <b>Обьем услуги:</b>
                <?php echo $model['volume']; ?>
			</span-->
			<div class="clr"></div>
			<span>
                <b>Аннотация:</b>
				<div class="clr"><hr></div>
                <?php echo $model['annotation']; ?>
			</span>




			<div class="clr"></div>
			<span>
                <b>Требования:</b>
				<div class="clr"><hr></div>
                <?php echo $model['requirements']; ?>
            </span>
		</div>

		<script type="text/javascript" src="//yandex.st/share/share.js"
charset="utf-8"></script>
<div class="yashare-auto-init" data-yashareL10n="ru"
 data-yashareType="button" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,gplus"

></div>

		<div class="clr"></div>
		<?php if (Yii::app()->controller->id . '/' . Yii::app()->controller->action->id == 'service/view'): ?>
		<?php $service = Service::model()->findByPk((int) $_GET['id']); ?>
		<aside class="usluga">
			<h2>Все услуги эксперта</h2>
			<section>
				<?php $services = Service::model()->findAll("expertID = '" . $service['expertID'] . "' and status = 1"); ?>
				<?php
				foreach ($services as $service):
					echo CHtml::link($service['notes'], array('service/view', 'id' => $service['id'],'title'=>$this->getCHPU($service['notes'])));
				endforeach;
				?>
				<div class="clr"></div>
			</section>
		</aside>
		<div class="clr"></div>
		<?php endif; ?>
	</div>
	<div class="clr"></div>
	<!--div class="chatformbox">
		<div class="rev_head"><h3>Отзывы о услуге</h3></div>
	<div id="show-review">
		<div class="ttl" style="float: left;">Написать сообщение:</div>
		<div class="clr"></div>
		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'register-form',
			//'enableClientValidation' => TRUE,
			//'htmlOptions' => array('class' => 'searchbox'),
			'enableAjaxValidation' => FALSE,
			'clientOptions' => array(
				'validateOnSubmit' => TRUE,
			),
		));
		?>
		<?php echo $form->textArea($r, 'description', array('class' => 'border5')); ?>
		<?php echo $form->error($r, 'description'); ?>
	</div>
	<span class="write_review-button" id="fake-submit">Оставить отзыв</span>
	<?php echo CHtml::submitButton('Оставить отзыв', array('class' => 'write_review-button', 'id' => 'true-submit')); ?>
	<?php $this->endWidget(); ?>
</div-->

	<div class="clr"></div>
	<?php foreach ($reviews as $review): ?>
	<?php $y = explode('.', Controller::getUser($review['userID'], 'birthday')); ?>
	<!--div class="comment_user">
		<b><?=date("d.m.Y", strtotime($review['date']))?></b>
		<a href="#">
			<?php echo Controller::getUser($review['userID'], 'name'); ?>, <?php echo date('Y') - $y[2]; ?> лет
		</a>
		<div class="clr"></div><br>
        <span>
            <?php echo $review['description']; ?>
        </span>
	</div-->
	<?php endforeach; ?>



</section>