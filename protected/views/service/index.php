<?php
if (isset($_GET['categoryID']))
{
	$category = Category::model()->findByPk($_GET['categoryID']);
	if($category->metaKey!="")
        $this->pageKeywords = explode(',',  mb_strtolower($category->metaKey, 'UTF-8'));
	if($category->metaDesc!="")
        $this->pageDescription = $category->metaDesc;
	if($category->metaTitle!="")
	$this->pageTitle = $category->metaTitle;
}else{
    $category = Category::model()->findByAttributes(array('url' => Yii::app()->request->requestUri));
    if($category !== NULL){
        if($category->metaKey!="")
            $this->pageKeywords = explode(',',  mb_strtolower($category->metaKey, 'UTF-8'));
	if($category->metaDesc!="")
            $this->pageDescription = $category->metaDesc;
	if($category->metaTitle!="")
            $this->pageTitle = $category->metaTitle;
}
}
 ?>
<section class="all-services">
    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a href="<?php echo Yii::app()->createUrl("service"); ?>">Наши услуги</a>
        <?php if (isset($_GET['categoryID'])): ?>
            <a><?php echo $category->name; ?></a>
        <?php endif; ?>
    </div>
    <?php if (isset($_GET['categoryID'])): ?>
            <h1><?php echo $category->name; ?></h1>
    <?php else: ?>
            <h1><?php echo Yii::t("app", "Наши услуги"); ?></h1>
    <?php endif; ?>
    <div class="clr"></div>
    <div class="left">
<!--        <div class="glamour-nav">
            <?php // $cats = Category::model()->findAll(); ?>
            <?php // foreach ($cats as $cat): ?>
                <a class="<?php // echo isset($_GET['categoryID']) && $_GET['categoryID'] == $cat['id'] ? 'active' : ''; ?>" href="?categoryID=<?php // echo $cat['id']; ?>"><?php // echo $cat['name']; ?></a>
            <?php // endforeach; ?>
        </div>-->
    </div>
    <?php foreach ($models as $model): ?>
        <?php $image = !empty($model['image']) ? $model['image'] : 'nophoto.jpg'; ?>
        <div class="service-wrap"><!-- Service wrap -->
            <div class="service"><!-- Блок сервис -->
                <div class="image_zoom">
	            <?php echo CHtml::link('<img class="poster" alt="" src="'.Yii::app()->request->baseUrl.'/uploads/service/'.$image.'">', array('service/view', 'id' => $model['id'],'title'=>$this->getCHPU($model['notes']))); ?>
                </div>
	            <div class="clr"></div>
                <div class="center h30px">
		                <?php echo CHtml::link($model['notes'], array('service/view', 'id' => $model['id'],'title'=>$this->getCHPU($model['notes']))); ?>
                </div>
	            <span><!-- описание -->
                    <?php $description = strip_tags($model['annotation']);?>
		            <?php echo CHtml::link((strlen($description) > 200 ? mb_substr($description, 0, 200, 'UTF-8') . '...' : $description), array('service/view', 'id' => $model['id'],'title'=>$this->getCHPU($model['notes']))); ?>
                    <b></b><!-- нижний градиент -->
                </span>
                <!--div class="service-guru">
                    <div class="border-bottom"></div>
                    <h3>Эксперты:</h3>
                    <?php $image = $this->getUserImage($model['expertID']); ?>
                    <?php //echo CHtml::link($this->getUser($model['expertID'], 'name') . ' ' . $this->getUser($model['expertID'], 'surname'), array('extrasens/view/', 'id' => $model['expertID'])); ?>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/extrasens/view/<?php echo $model['expertID']; ?>">
                        <img width="50" height="50" alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $image; ?>">
                        <i>
                            <?php echo $this->getUser($model['expertID'], 'name') . ' ' . $this->getUser($model['expertID'], 'surname'); ?>
                            <b></b>
                        </i>
                    </a>
                </div-->
	            <div class="clr"></div>
	            <div class="center">
		            <? $params = array($model->paymentPath ? $model->paymentPath : 'service/booking');
		            if( ! $model->paymentPath) $params['id'] = $model->id;?>
		            <?php echo CHtml::link('Заказать услугу', $params, array('class'=>'get-service')); ?>
	            </div>
	            <!-- #Блок сервис -->
	            <div class="price-corner"><?php echo $model['honorarium']; ?></div>
	        </div><!-- #Service wrap -->
	    </div><!-- #Service wrap -->
    <?php endforeach; ?>

</section>

<div class="clr"></div>
<br>
<div class="clr"></div>
<br>
<!-- Постраничная навигация -->
<div class="pagination">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $pages,
        'header' => '',
    ));
    ?>
</div>
<!-- #Постраничная навигация -->
