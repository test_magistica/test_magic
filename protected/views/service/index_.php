<section class="all-services">
    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a href="<?php echo Yii::app()->createUrl("service"); ?>">Наши услуги</a>
        <?php if (isset($_GET['categoryID'])): ?>
            <a><?php echo Category::model()->findByPk($_GET['categoryID'])->name; ?></a>
        <?php endif; ?>
    </div>
    <div class="clr"></div>
    <div class="center">
        <div class="glamour-nav">
            <?php $cats = Category::model()->findAll(); ?>
            <?php foreach ($cats as $cat): ?>
                <a class="<?php echo isset($_GET['categoryID']) && $_GET['categoryID'] == $cat['id'] ? 'active' : ''; ?>" href="?categoryID=<?php echo $cat['id']; ?>"><?php echo $cat['name']; ?></a>
            <?php endforeach; ?>            
        </div>
    </div>
    <?php foreach ($models as $model): ?>
        <?php $image = !empty($model['image']) ? $model['image'] : 'nophoto.jpg'; ?>
        <div class="service-wrap"><!-- Service wrap -->
            <div class="service"><!-- Блок сервис -->
                <div class="img-grad"></div><!-- Градиент внизу главной фотки -->
                <div class="label"><!-- Надпись на застежке -->
                    <?php echo $model['notes']; ?>
                    <div class="label-corner"></div>
                    <div class="left-shadow"></div>
                </div>
                <img class="poster" alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/service/<?php echo $image; ?>"><!-- главное фото -->
                <span><!-- описание -->
                    <?php
                    $description = strip_tags($model['annotation']);
                    echo strlen($description) > 200 ? mb_substr($description, 0, 200, 'UTF-8') . '...' : $description;
                    ?> 
                    <b></b><!-- нижний градиент -->
                </span>
                <?php echo CHtml::link('подробнее...', array('service/view', 'id' => $model['id'],'title'=>$this->getCHPU($model['notes'])), array('class' => 'more')); ?>                
                <div class="service-guru"><!-- Фото экспертов -->
                    <div class="border-bottom"></div><!-- нижняя граница -->
                    <h3>Эксперты:</h3>
                    <?php $image = $this->getUserImage($model['expertID']); ?>
                    <?php //echo CHtml::link($this->getUser($model['expertID'], 'name') . ' ' . $this->getUser($model['expertID'], 'surname'), array('extrasens/view/', 'id' => $model['expertID'])); ?>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/extrasens/view/<?php echo $model['expertID']; ?>">
                        <img width="50" height="50" alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $image; ?>">
                        <i><!-- Всплывающее имя в одно слово максимум 13 символов-->
                            <?php echo $this->getUser($model['expertID'], 'name') . ' ' . $this->getUser($model['expertID'], 'surname'); ?>
                            <b></b><!-- нижняя стрелка -->
                        </i>
                    </a>
                </div><!-- #Фото экспертов -->
                <?php echo CHtml::link('Получить предсказание', array('service/booking', 'id' => $model['id']), array('class' => 'get-service')); ?>

            </div><!-- #Блок сервис -->
            <div class="price-corner">цена: <?php echo $model['honorarium']; ?> руб.</div>
        </div><!-- #Service wrap -->
    <?php endforeach; ?>

</section>

<!-- Постраничная навигация -->
<div class="pagination">
    <?php
    $this->widget('CLinkPager', array(
        'pages' => $pages,
        'header' => '',
    ));
    ?>    
</div>
<!-- #Постраничная навигация -->	
