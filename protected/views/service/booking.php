<div class="zakaz_page">
    <h1>Заказ услуги</h1>

<?php if (Yii::app()->user->hasFlash('booking')): ?>

	<div class="flash-success">
		<?php echo Yii::app()->user->getFlash('booking'); ?>
	</div>
	<?php else: ?>
	<div class="shadowblock zakaz_page_block pr15px pl15px">
		<h2>Ваш заказ</h2>
		<p>Название услуги:<?php echo $servive['notes']; ?></p>
		<p>Стоимость:<?php echo $servive['honorarium']; ?> руб.</p>
		<p>Аннотация:<?php echo $servive['annotation']; ?></p>
		<p>Требования:<?php echo $servive['requirements']; ?></p>

		<h2>Основная информация</h2>
		<?php $servive['expertID'] = Yii::app()->user->id; ?>
		<p>Имя и фамилия: <?php echo Controller::getUser($servive['expertID'], 'name'); ?> <?php echo Controller::getUser($servive['expertID'], 'surname'); ?></p>
		<p>Пол:<?php $sex = array('2'=>'Женщина', '1'=>'Мужчина');
			$key = Controller::getUser($servive['expertID'], 'sex');
			if ( in_array($key,array(1,2)) ) echo $sex[strval($key)];
			else echo $sex['2'];//$key='Не определен';
			?></p>
		<p>Дата рождения: <?php echo Controller::getUser($servive['expertID'], 'birthday'); ?></p>
		<p>Номер телефона:<?php echo Controller::getUser($servive['expertID'], 'tele'); ?></p>
		<p>Ваш E-Mail:<?php echo Controller::getUser($servive['expertID'], 'email'); ?></p>

		<h2>Дополнительная информация</h2>

		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'register-form',
			//'enableClientValidation' => TRUE,
			'enableAjaxValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,
			),
		));
		?>
		<div class="graphicbox tarifbox" id="list8">
			<div style="margin-left: 95px;">
				<?php echo $form->errorSummary($message); ?>
			</div>
			<div class="field" style="display: none;">
				<?php echo $form->error($message, 'message'); ?>
			</div>
			<label>Ваше сообщение эксперту:</label>
			<?php echo $form->textarea($message, 'message', array('class' => 'border5 text470')); ?>
			<?php echo $form->error($message, 'message'); ?>
			<div class="clr"></div>

			<div class="order-this fRight">
				<p>
					<?php echo CHtml::submitButton('Продолжить', array('class' => 'submit')); ?>
				</p>
			</div>
			<?php $this->endWidget(); ?>
			<div class="filebox2">
				<?php
				$this->widget('ext.xupload_1.XUpload', array(
					'url' => Yii::app()->createUrl("service/upload?serviceID=" . $id),
					'model' => $model,
					'attribute' => 'file',
					'multiple' => true,
					'autoUpload'    => true,
					'options' => array(
						'maxFileSize' => 5000000,
						'acceptFileTypes' => "js:/(\.|\/)(jpe?g|png)$/i",
					)
				));
				?>
			</div>
		</div>

	</div>
	<div class="clr"></div>
	<!-- #shadowbox -->


    </div><!-- #page -->
<?php endif; ?>