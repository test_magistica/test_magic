<div class="usercab_page">
    <div class="head">
        <h1>Пополнение счета</h1>

    </div>
    <!-- Пополнение счета -->
    <div class="balance-add">    
        <?php if (Yii::app()->user->hasFlash('fail')): ?>

            <div class="flash-success">
                <?php echo Yii::app()->user->getFlash('fail'); ?>
            </div>
	<?php else: ?>
	    <div class="flash-success">
        	Нет переданных даных. Обратитесь к администратору.
            </div>
        <?php endif; ?>
    </div>
    <!-- #пополнение счета -->
</div><!-- #page -->