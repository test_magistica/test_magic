<?php 
    $category = Category::model()->findByAttributes(array("url" => Yii::app()->request->requestUri));
    if($category !== NULL){
        if($category->metaKey!="")
            $this->pageKeywords = explode(',',  mb_strtolower($category->metaKey, 'UTF-8'));
	if($category->metaDesc!="")
            $this->pageDescription = $category->metaDesc;
	if($category->metaTitle!="")
            $this->pageTitle = $category->metaTitle;
    }
?>
<div class="sonnikpage">

    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a href="<?php echo Yii::app()->createUrl("divinations"); ?>">Онлайн-гадания</a>       
    </div>

    <div class="post">
        <h1>Онлайн-гадания</h1>
        <article class="my_posts">
            <?php echo Yii::t("app", "People have long sought to reveal the secret veil of time , and look into the future . At first this was only public affairs : shamans and sorcerers danced around the campfire and went into the woods to listen to the spirits in order to know when the rains come , there will be the next successful hunt . Passion for divination became increasingly popular , and later every person has wished himself to understand whether he chose the second half, not whether it zagryzet jaguar in the jungle, if the snake did not bite the child. Why do people want to understand the future? This is an opportunity to prepare , and thus completely avoid any problems in life , it is an opportunity in advance to make the right choice. It is a way to adjust itself under upcoming events . We hope that our online divination will help you sort out your thoughts and calculate possible scenarios . Predictions of this section, you can use every day. As a rule, they give a small forecast night , at least - for a longer period . You are granted a great way to learn what to expect today that the fate in store for you and will come true if what you want in the near future . But do not apply to these predictions lightly , they are as useful as the more complex divination."); ?>
        </article>
        <article class="shadowblock">
            <ul>  
                <?php foreach ($models as $model): ?>
                    <li><?php echo CHtml::link($model['title'], array('divinations/view', 'id' => $model['id'], 'title' => $this->getCHPU($model['title']))); ?></li>
                <?php endforeach; ?>
            </ul>
        </article>
    </div>

    <div class="pager_box">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $pages,
            'header' => '',
            'cssFile' => Yii::app()->request->baseUrl . '/css/mypager.css',
        ));
        ?>
    </div> <!-- pager -->

</div>
