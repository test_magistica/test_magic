<?php 
    $category = Category::model()->findByAttributes(array("url" => Yii::app()->request->requestUri));
    if($category !== NULL){
        if($category->metaKey!="")
            $this->pageKeywords = explode(',',  mb_strtolower($category->metaKey, 'UTF-8'));
	if($category->metaDesc!="")
            $this->pageDescription = $category->metaDesc;
	if($category->metaTitle!="")
            $this->pageTitle = $category->metaTitle;
    }
?>
<div class="sonnikpage">
    
    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a href="<?php echo Yii::app()->createUrl("dreams"); ?>">Сонник</a>                
    </div>
    
    <h1>Сонник онлайн. Что значит Ваш сон</h1>		

    <div class="searchbox">
        <p>Расшифровать сон, узнать значения сна вы можете онлайн. Зная толкование вашего сна можно получить информацию что ждет вас в будущем, тем самым вы поймете как поступить лучше, чтобы избежать неприятностей
		</p>
        <h3>Поиск по соннику</h3>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'dreams-form',
            'method' => 'get',
        ));
        ?>
        <div class="searchboxRow">
            <span style="color: red;"><?php echo $form->error($model, 'name'); ?> </span>
            <?php echo $form->textField($model, 'name', array('class' => 'inp', 'id' => 'q')); ?>
            <div class="clr"></div>
            <span>(введите ключевое вашего сна, чтобы узнать значение)</span>
            <div class="clr"></div>
            <div id="result" class="sonnik-result"></div>
                       
            <div class="clr"></div>
        </div>
        <div class="searchboxRow">
            <?php echo $form->dropDownList($model, 'category', CHtml::listData(DreamsCategory::model()->findAll(), 'id', 'name'), array('class' => 'selectBlock', 'empty' => 'Все сонники')); ?>                  
            <input type="submit" value="найти" class="submit">
        </div>
        <script>
            $(document).ready(function() {
                $('#q').keyup(function() {
                    $.get("<?php echo CController::createUrl('/dreams/search') ?>",
                            $('#dreams-form').serialize(),
                            function(data) {
                                $('#result').html(data);
                            });
                });
            });
        </script>
        <?php $this->endWidget(); ?>
    </div>
    <?php if (isset($_GET['DreamsForm']) && !$model->hasErrors("name")): ?>
        <div class="post">
            <h2>Результаты поискапо вашем сну</h2>
            <article class="shadowblock">
                <p>

                </p>
                <ul class="search-sonnic">
                    <?php foreach ($models as $model2): ?>
                        <li><?php echo CHtml::link($model2['name'] . ' - ' . DreamsCategory::model()->findByPk($model2['category_id'])->name, array('dreams/view/', 'id' => $model2['id'], 'title'=>$this->getCHPU($model2['name'] . '-' . DreamsCategory::model()->findByPk($model2['category_id'])->name))); ?></li>
                    <?php endforeach; ?>
                </ul>
                <p>

                </p>

            </article>
        </div>
        <div class="clr"></div>
        <div class="pager_box">
            <?php
            $this->widget('CLinkPager', array(
                'pages' => $pages,
                'header' => '',
                'cssFile' => Yii::app()->request->baseUrl . '/css/mypager.css',
            ));
            ?>
        </div> <!-- pager -->
    <?php endif; ?>
</div>