<div class="sonnikpage">
    
    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a href="<?php echo Yii::app()->createUrl("dreams"); ?>">Сонник</a>       
        <a><?php echo $model['name'] . ' - ' . DreamsCategory::model()->findByPk($model['category_id'])->name; ?></a>       
    </div>
    
    <h1>Сонник онлайн. Что значит Ваш сон</h1>	

    <div class="searchbox">
        <p>Расшифровать сон, узнать значения сна вы можете онлайн. Зная толкование вашего сна можно получить информацию что ждет вас в будущем, тем самым вы поймете как поступить лучше, чтобы избежать неприятностей
		</p>
        <h3>Поиск по соннику</h3>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'dreams-form',
            'method' => 'get',
'action' => Yii::app()->createUrl('//dreams/index'),
        ));
        ?>
        <div class="searchboxRow">
            <?php echo $form->textField($model2, 'name', array('class' => 'inp', 'id' => 'q')); ?>
            <div class="clr"></div>
            <span>(введите ключевое вашего сна, чтобы узнать значение)</span>
            <div class="clr"></div>
            <div id="result" class="sonnik-result"></div>
            <?php echo $form->error($model2, 'name'); ?>            
            <div class="clr"></div>
        </div>
        <div class="searchboxRow">
            <?php echo $form->dropDownList($model2, 'category', CHtml::listData(DreamsCategory::model()->findAll(), 'id', 'name'), array('class' => 'selectBlock', 'empty' => 'Все сонники')); ?>                  
            <input type="submit" value="найти" class="submit">
        </div>
        <script>
            $(document).ready(function() {
                $('#q').keyup(function() {
                    $.get("<?php echo CController::createUrl('/dreams/search') ?>",
                            $('#dreams-form').serialize(),
                            function(data) {
                                $('#result').html(data);
                            });
                });
            });
        </script>
        <?php $this->endWidget(); ?>
    </div>
<br /><br />
    <div class="post">
        <h2><?php echo $model['name'] . ' - ' . DreamsCategory::model()->findByPk($model['category_id'])->name; ?></h2>
        <article class="shadowblock">
            <?php echo $model['data']; ?>
        </article>
    </div>

</div>
