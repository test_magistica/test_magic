<?php
/* @var $this AreaExpertiseController */
/* @var $model AreaExpertise */

$this->breadcrumbs=array(
	'Area Expertises'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List AreaExpertise', 'url'=>array('index')),
	array('label'=>'Create AreaExpertise', 'url'=>array('create')),
	array('label'=>'Update AreaExpertise', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AreaExpertise', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AreaExpertise', 'url'=>array('admin')),
);
?>

<h1>View AreaExpertise #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'parentID',
		'name',
		'metaKey',
		'metaDesc',
		'metaTitle',
		'status',
		'sort',
	),
)); ?>
