<?php
/* @var $this AreaExpertiseController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Area Expertises',
);

$this->menu=array(
	array('label'=>'Create AreaExpertise', 'url'=>array('create')),
	array('label'=>'Manage AreaExpertise', 'url'=>array('admin')),
);
?>

<h1>Area Expertises</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
