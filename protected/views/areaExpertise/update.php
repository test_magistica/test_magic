<?php
/* @var $this AreaExpertiseController */
/* @var $model AreaExpertise */

$this->breadcrumbs=array(
	'Area Expertises'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AreaExpertise', 'url'=>array('index')),
	array('label'=>'Create AreaExpertise', 'url'=>array('create')),
	array('label'=>'View AreaExpertise', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AreaExpertise', 'url'=>array('admin')),
);
?>

<h1>Update AreaExpertise <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>