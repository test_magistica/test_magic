<?php
/* @var $this AreaExpertiseController */
/* @var $model AreaExpertise */

$this->breadcrumbs=array(
	'Area Expertises'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AreaExpertise', 'url'=>array('index')),
	array('label'=>'Manage AreaExpertise', 'url'=>array('admin')),
);
?>

<h1>Create AreaExpertise</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>