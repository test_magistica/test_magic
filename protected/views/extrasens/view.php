<?php
// $_GET['a'] = "4";
    // Стили для окна подтверждения обратного звонка
if (isset($_GET['a']) && $_GET['a'] == "4") {
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . "/../css/extrasens/view.css");
    Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . "/../css/facybox/facybox.css");
    Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . "/../js/facybox.js");
    }
?>
<?php 
$this->pageTitle = CHtml::encode(ucfirst($model['profession']) . ' онлайн ' . Controller::getUser($model['id'], 'name')
	. ' — консультации по телефону, гадание');
        $this->pageDescription = mb_substr($model['about'], 0, 450, 'UTF-8');
        $pageKeywords = new HKeywords();
        $this->pageKeywords = array_merge(
        explode(',', mb_strtolower($model['profession']) . ', гадание онлайн, ясновидящие, экстрасенсы'),
	        explode(',', $pageKeywords->get_keywords($model['about'])));
if (!empty($model->metaTitle)) {
    $this->pageTitle = trim($model->metaTitle);
}
if (!empty($model->metaKey)) {
    $this->pageKeywords = explode(",", trim($model->metaKey));
}
else{
	$this->pageKeywords = array(Controller::getUser($model['id'], 'name') ." - ". $model['profession'] . ' онлайн');
}
if (!empty($model->metaDesc)) {
    $this->pageDescription = trim($model->metaDesc);
}
?>
<!-- START TAKE CODE FROME HERE -->

<!--		<div class="exp-bread">
                        <a href="#">Главная</a>
                        <a href="#">Экстрасенс</a>
                        <a href="#">Диана Лианова</a>
                </div>-->

<!-- Эксперт -->
<div class="expertitem2">
    
    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>        
        <a><?php echo Controller::getUser($model['id'], 'name'); ?></a> 
    </div>
    <h1><?php echo ucfirst($model->profession)."&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;".ucfirst($model->name); ?></h1>
    <?php if (Yii::app()->user->hasFlash('callK')): ?>

    <div class="flash-success" style="clear:both">
        <?php echo Yii::app()->user->getFlash('callK'); ?>
    </div>
<?php endif; ?>
    <?php if (Yii::app()->user->hasFlash('error')): ?>
            <div class="flash-error">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
    <?php endif; ?>
    <div class="exp-quick">
        <?php if ($model['online'] == 'online'): ?>     

            <?php echo CHtml::button('Позвонить с сайта', array('class' => 'green', 'data-reveal-id' => 'bookModal', 'data-animation' => 'fade', 'submit' => array('callBack/view/', 'id' => $model['id']))) ?>

        <?php else: ?>
            <?php echo CHtml::button('Заказать звонок', array('class' => 'orange', 'data-reveal-id' => 'bookModal', 'data-animation' => 'fade', 'submit' => array('site/register', 'id' => $model['id']))) ?>
        <?php endif; ?>


        <span class="tarif"><?php echo $model['tariff']; ?> руб/мин</span>
        <h5><?php echo Controller::getUser($model['id'], 'name'); ?> </h5>
        <div style="margin: -22px 0 0 168px; position: absolute;">
            <span class="type"><?php echo $model['profession']; ?></span>&nbsp;
            <span class="not-available"><?php echo Yii::app()->params['status'][Controller::getUser($model['id'], 'online')]; ?></span>
        </div>
        <div class="clr"></div>
    </div>
    <?php if (Yii::app()->user->hasFlash('call')/* || isset($_GET['a']) */): ?>
        <?php
        if (isset($_GET['a']) && $_GET['a'] != "4") {
                echo '<script> alert("' . $this->getAlerts($_GET['a']) . '");</script>';
        } else {
            ?>
                <div id="callbackform" style="display: none;">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                               'id' => 'register-form-facybox',
                               //'enableClientValidation' => TRUE,
                               //'htmlOptions' => array('class' => 'searchbox'),
                               'enableAjaxValidation' => FALSE,
                               'clientOptions' => array(
                                   'validateOnSubmit' => TRUE,
                               ),
                           ));
                ?>

                    <div class="cbf-header">
                        <span>Ожидайте пожалуйста звонка от эксперта через 1-2 мин ...</span>
                        <br />
                        <span>* звонок поступит на телефон который вы указали при регистрации</span>
                    </div>
                    <div class="cbf-textcontainer">
                        <div class="cbf-text-form">
                        <?php
                        echo $form->textArea(
                                $review, 'description', array(
                                         'class' => 'cbf-text',
                                         'placeholder' => "Пожалйста напишите свой отзыв",
                                         )
                                     );
                             ?>
<!--                            <textarea placeholder="Пожалуйста напишите свой отзыв" class="cbf-text" >

                            </textarea>-->
                        </div>
                        <div id="cbf-btn" class="cbf-btn">
                            <a href="javascript:void(0);">ОТПРАВИТЬ ОТЗЫВ</a>
                        </div>
                    </div>

                <?php $this->endWidget(); ?>
                </div>
                <a rel="facybox" href="#callbackform">&nbsp;</a>
                <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        $('a[rel*=facybox]').facybox({
//                            noAutoload: false,
                            modal: true
                        });
                        $('a[rel*=facybox]').click();
                    $('#cbf-btn a').click(function() {
                            $($(this).parents('form').get(0)).submit();
                        });
    });
    </script>
	    <?php }
	    ?>
    <?php endif; ?>
    <div class="leftbox">
        <?php $photo = Userphoto::model()->find("`userID` = '" . $model['id'] . "' AND `main`=1"); ?>
        <img class="ava" src="<?php echo Yii::app()->request->baseUrl . '/uploads/user/Thumb/' . $photo['image']; ?>" alt="" height="150" width="150">      
        <!--        <a href="#" class="ratio"></a>
                <span class="ratio-num">Рейтинг 9,9</span>-->
        <?/*/set:/<a href="#" class=review>Отзывы (<?php echo $count; ?>)</a>*/?>
        <?php $url = $this->getUrlToExpertChat($model); ?>
        <?php if ((Yii::app()->user->isGuest || Yii::app()->user->role == Users::ROLE_USER)/* && $url !== FALSE */): ?>
            <?php if ($model->chat_available == 1): ?>
                <?php if ($model->online == "online"): ?>
            <a href="<?php echo $url; ?>" title="Онлайн чат с экстрасенсом" target="_blank">
                <div class="chat-online-button">Онлайн чат</div>
            </a>
                <?php else: ?>
<!--                    <a href="#" title="Онлайн чат с экстрасенсом" onclick="return false;">
                    <div class="chat-online-button offline">Офлайн чат</div>
                    </a>-->
            <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>

        <?php if (Yii::app()->user->role == Users::ROLE_EXPERT && $model->chat_available == 1): ?>
            <?php if ($model->online == "online"): ?>
                <a href="<?php echo "#"; ?>" title="Онлайн чат с экстрасенсом" onclick="return false;">
                    <div class="chat-online-button">Онлайн чат</div>
                </a>
            <?php else: ?>
<!--                <a href="#" title="Онлайн чат с экстрасенсом" onclick="return false;">
                    <div class="chat-online-button offline">Офлайн чат</div>
                </a>-->
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <div class="rightbox" style="margin-top:15px;">

        <span class="exp-info">
            <?php echo mb_substr(str_replace("\n", '<br>', $model['about']), 0, 1000, 'UTF-8'); ?><br>

        </span>
        <div class="clr"></div>
        <?php if($model->mobile != NULL && !empty($model->mobile)): ?>
        <div class="call-cell">
                <?php echo $model['mobile']; ?> 
                <span class="help">
                    платно
                    <span><?php echo $model['mobileText']; ?></span>
                </span>
            </div>
        <?php endif; ?>
        <?php if ((($model->home) != NULL)): ?>
            <div class="call-stat">
                <?php echo $model['home']; ?>
                <?php if ($model->homeText != NULL): ?>
                <span class="help">
                    стоимость
                    <span><?php echo $model['homeText']; ?></span>
                </span>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="clr"></div>
</div>
<!-- #Эксперт -->
<div class="clr"></div>

<!-- 
        STOP TAKING CODE AND PUT IT INTO SECTION
-->


<div class="extrasens_page">
    <!-- Вкладки -->
    <div id="tabsbox">
        <ul class="tabhead">
            <li class="<?= ($tab == 'review' || $tab === null ? 'ui-tabs-active' : '') ?>">
	            <a href="javascript:void(0);" >Отзывы <span style="color:#934fb9 !important;" >(<?php echo $count; ?>)</span></a>
            </li>
            <li class="<?= ($tab == 'service' ? 'ui-tabs-active' : '') ?>" >
	            <a href="javascript:void(0);">Услуги</a>
            </li>
            <li class="<?= ($tab == 'article' ? 'ui-tabs-active' : '') ?>" >
	            <a href="javascript:void(0);">Статьи</a>
            </li>
            <li class="<?= ($tab == 'schedule' ? 'ui-tabs-active' : '') ?>">
	            <a href="javascript:void(0);">Расписание</a>
            </li>
        </ul>
	    <? $title_a = $this->getCHPU($model->name . '-' . $model->profession);?>
        <script type="text/javascript">
            var extrasensTabs = {
                1 : "<?='/extrasens/view?id='.$expertID.'&tab='.$routabs['review'] ; ?>",
                2 : "<?='/extrasens/view?id='.$expertID.'&tab='.$routabs['service'] ; ?>",
                3 : "<?='/extrasens/view?id='.$expertID.'&tab='.$routabs['article'] ; ?>",
                4 : "<?='/extrasens/view?id='.$expertID.'&tab='.$routabs['schedule'] ; ?>"
            };
        </script>

        <div class="shadowblock tabcontent">
           <div id="ui-tabs-1" class="tabitem reviewsbox">
                <?php
                if ($tab == 'review' || $tab === null) {
                    $this->renderPartial('_review', array(
                            'model' => $model, 'review' => $review, 'count' => $count
                          ));
                     }
                ?>
            <!-- #Отзывы -->
           </div>

            <div id="ui-tabs-2" class="tabitem">
            <!-- Мои услуги -->
                <?php
                //if ($tab == 'service' /* || $tab==null */) {
                    $this->renderPartial('_service', array(
                            'model' => $model, 'review' => $review, 'count' => $count
                          ));
               // }
                ?>
            <!-- #Мои услуги -->
            </div>
            <div id="ui-tabs-3" class="tabitem">
            <!-- Мои статьи -->
                <?php
                //if ($tab == 'article' /* || $tab==null */) {
                    $this->renderPartial('_article', array(
                            'model' => $model, 'review' => $review, 'count' => $count
                          ));
                //     }
                ?>
            <!-- #Мои статьи -->
            </div>
            <!-- Чат со мной -->
            <?php
           // if ($tab == 'chat') { /* || $tab==null */
           //     $this->renderPartial('_chat', array(
           //                 'model' => $model, 'review' => $review, 'count' => $count
             //             ));
             //          }
                 ?>
            <!-- #Чат со мной -->


            <div id="ui-tabs-4" class="tabitem">
            <!-- График работы -->
                <?php
               // if ($tab == 'schedule' /* || $tab==null */) {
                    $this->renderPartial('_schedule', array(
                            'model' => $model, 'review' => $review, 'count' => $count
                          ));
                  //   }
                ?>
            </div>
            <!-- #График работы -->
        </div><!-- #tabcontent -->
    </div>
    <!-- #Вкладки -->

</div>
<?php if (isset(Yii::app()->session['cbk_id'])) { ?>
    <script>
        function checkCall() {
            $.post("<?php echo CController::createUrl('/callBack/check') ?>",
                    {check: '<?php echo Yii::app()->session['cbk_id']; ?>'},
            function(data) {
                if (data != "") {
                    $('.balance b').text(data + ' руб.');
                }
            });
        }
        setInterval("checkCall()", 30000);

    </script>
<?php } ?>
