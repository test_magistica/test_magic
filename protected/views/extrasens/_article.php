            <!-- Мои статьи -->          
                <?php $articles = Article::model()->findAll("author = '" . $model['id'] . "' and `status`=1"); ?>
                <?php foreach ($articles as $article): ?>
                    <!-- Анонс -->
                    <div class="shadowblock anonse2">
                        <?php $image = !empty($article['image']) ? $article['image'] : 'nophoto.jpg'; ?>
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/article/<?php echo $image; ?>"/>
                        <span class="title"><?php echo CHtml::link($article['title'], array('article/view', 'id'=>$article['id'], 'title' => $article['url'])); ?></span>
                        <p><?php echo $article['anons']; ?></p>
                        <?php echo CHtml::link('читать далее', array('article/view', 'id'=>$article['id'], 'title' => $article['url']), array('class' => 'more')); ?>                        
                    </div>
                    <!-- #Анонс -->
                <?php endforeach; ?>      
            <!-- #Мои статьи -->
