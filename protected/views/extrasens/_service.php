<?
$criteria = new CDbCriteria();
$criteria->compare('expertID', $model['id']);
$staticServices = ExpertStaticServices::model()->findAll($criteria);
$arrIdSS = array();
foreach($staticServices as $ss)
	$arrIdSS[] = $ss->serviceID;

if(count($arrIdSS)){
	$crit = new CDbCriteria();
	$crit->addInCondition('id', $arrIdSS);
	$services[] = Service::model()->findAll($crit);
}

?>

<?php $services[] = Service::model()->findAll("expertID = '" . $model['id'] . "' and `status`=1"); ?>
<?for($i = 0; $i < count($services); $i++) {?>
<?php foreach ($services[$i] as $servive): ?>
<?php $image = !empty($servive['image']) ? $servive['image'] : 'nophoto.jpg'; ?>
<!-- Анонс -->
<div class="shadowblock anonse1">
	<article>
		<h3><?php echo CHtml::link($servive['notes'], array('service/view', 'id' => $servive['id'],'title'=>$this->getCHPU($servive['notes']))); ?></h3>
		<p>
			<?php
			$description = strip_tags($servive['annotation']);
			echo strlen($description) > 400 ? mb_substr($description, 0, 400, 'UTF-8') . '...' : $description;
			?>
		</p>
		<div class="shadowblock cost">
			<em><b><?php echo $servive['honorarium']; ?></b> руб.</em>
                                <span class="buttons">
	                                <? $params = array($servive->paymentPath ? $servive->paymentPath : 'service/view');
	                                if( ! $servive->paymentPath) $params['id'] = $servive->id;?>
                                    <?php echo CHtml::link('ЗАКАЗАТЬ', $params, array('class' => 'zakaz')); ?>
                                </span>
		</div>
	</article>
	<div class="img">
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/service/<?php echo $image; ?>" alt="" /> <b></b>
	</div>
</div>
<!-- #Анонс -->
<?php endforeach; ?>
<? } ?>
<!-- #Мои услуги -->
