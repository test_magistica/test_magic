<div class="usercab_page">
    <div class="head">
        <h1>Пополнить счет</h1>

    </div>
    <!-- Пополнение счета -->
    <div class="balance-add">    
        <?php if (Yii::app()->user->hasFlash('success')): ?>

            <div class="flash-success">
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
	<?php else:?>
	    <div class="flash-success">
                Нет переданных данных. <br/> Обратитесь к администратру.
            </div>
        <?php endif; ?>
    </div>
    <!-- #пополнение счета -->
</div><!-- #page -->