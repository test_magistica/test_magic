<?php
$status = array('Не Принят', 'В ожидании', 'Выполнен');
$type = array('Услуга', 'Звонок');
?>
<script>
    function getUrl(key, val) {
        var url;
        var len = location.search;
        if (len == "")
            url = '?' + key + '=' + val;
        else
            url = len + '&' + key + '=' + val;
        return url;
    }
</script>
<div class="usercab_page">
    <div class="head">
        <h1>Мои заказы</h1>
        <!--        <a href="#" class="border10 all">Сменить пароль</a>-->
    </div>
    <div class="tab-container">
        <?php if (Yii::app()->user->hasFlash('info-message')): ?>
`        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('info-message'); ?>
        </div>

        <?php else: ?>
        <ul class="tabs">
            <li class="current">Услуги (<?php echo $count; ?>)</li>
            <li>Консультации (<?php echo $count2; ?>)</li>
        </ul>
        <div class="box visible">
            <table class="z-order">
                <tr>
                    <th>Эксперт</th>
                    <th>Услуга</th>
                    <th>Состояние</th>
                    <th>Создан</th>
                </tr>
                <?php foreach ($models as $model): ?>
                    <tr>
                        
                        <td class="name">

                           <?php echo $this->getUser($model->expertID, 'name') . '&nbsp;' . $this->getUser($model->expertID, 'surname') ?>

                        </td>
                        <td class="call"><?php echo CHtml::link(Service::model()->findByPk($model->serviceID)->notes, array('myBooking/view', 'id' => $model['id'])); ?></td>
                        <?php $b = Controller::checkBooking($model['id']) > -1 ? '(' . Controller::checkBooking($model['id']) . ')' : ''; ?>
                        <td class="sost"><?php echo $status[$model['status']]."$b"; ?> </td>
                        <td class="date"><?php echo date('m.d.y H:i:s', strtotime($model['createTime'])); ?></td>
                    </tr>    
                <?php endforeach; ?>                    
            </table>
            <div class="pager">
                <?php
                $this->widget('CLinkPager', array(
                    'pages' => $pages,
                    'header' => '',
                ));
                ?>
            </div>
        </div>

        <div class="box" style="display:none;">
            <table class="z-order">
                <tr>
                    <th>Эксперт</th>
                    <th>Длительность разговора(минут)</th>                    
                    <th>Дата звонка</th>
                    <th>Стоимость</th>
                    <th>Статус</th>
                </tr>
                <?php foreach ($consulations as $consulation): ?>
                    <tr>
                        
                        <td class="name">
                            <span>
                                <?php echo $this->getUser($consulation->expertID, 'name') . '&nbsp;' . $this->getUser($consulation->expertID, 'surname') ?>

                            </span>
                        </td>      
                        <td><?php echo ceil($consulation['duration']/60); ?></td>
                        <td><?php echo date('d.m.Y H:i:s', strtotime($consulation['date'])); ?></td>
                        <td><?php echo $consulation['cost'];?></td>
                        <td>
                        <?php
                    	    Controller::getNameStatus($consulation['status']);
                    	    if ($consulation['status'] == '1')
                    		echo CHtml::link('Отменить', array('/site/cancel/', 'id' => $consulation['id']),
                    		    array('onclick'=>'return confirm(\'Вы действительно хотите удалить консультацию?\') '.
                    			'? true : false;')); 
                    	?>
                        </td>
                    </tr>    
                <?php endforeach; ?>

            </table>
        </div>
        <?php endif; ?> 
    </div><!-- #tab-container -->

    <!-- #Таблица -->
</div><!-- #page -->
