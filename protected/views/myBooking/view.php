<div class="usercab_page">
    <div class="head">
        <h1>Мои заказы</h1>
    </div>


    <!-- Мои сообщения -->
    <div class="my_msg_list block my_user_msg">

        <div class="commentform">
            <div class="commentstat"><?php echo $count; ?> комментариев</div>
            <div class="chatformbox chatformbox2">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'register-form',
                    //'enableClientValidation' => TRUE,
                    'htmlOptions' => array('class' => 'searchbox','enctype' => 'multipart/form-data'),
                    'enableAjaxValidation' => false,
                    'clientOptions' => array(
                        'validateOnSubmit' => TRUE,
                    ),
                ));
                ?> 
                <div style="margin-left: 95px;">
                    <?php echo $form->errorSummary($model); ?>
                </div>
                <br />
                <div class="field" style="display: none;">
                    <?php echo $form->error($model, 'toID'); ?> 
                    <?php echo $form->error($model, 'topic'); ?>
                    <?php echo $form->error($model, 'message'); ?>
                </div>
                <?php echo $form->textArea($model, 'message', array('class' => 'border5')); ?>      	<br /><br />

	            <div style="text-align: left !important;">
	            <label>Файл:</label>
	            <p>
		            <?php
		            $this->widget('CMultiFileUpload', array(
			            'name' => 'files',
			            'accept' => 'jpg|png|gif|doc|docx|rar|zip|txt',
			            //'max' => 3,
			            'remove' => 'удалить',
			            //'denied'=>'', message that is displayed when a file type is not allowed
			            //'duplicate'=>'', message that is displayed when a file appears twice
			            'htmlOptions' => array('size' => 25),
		            ));
		            ?>
	            </p>
	            </div>
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Отправить' : 'Сохранить', array('class' => 'submit')); ?>
                <?php $this->endWidget(); ?>
            </div>
        </div>
        <?php foreach ($models as $model): ?>
            <?php $image = $this->getUserImage($model['userID']); ?>
            <div class="shadowblock mymess">
                <!--change class new message class: "name new_msg" -->
                <p class="lnk-name"><a><?php echo $model['userID'] == Yii::app()->user->id ? 'Вы' : $this->getUser($model->userID, 'name') . '&nbsp;' . $this->getUser($model->userID, 'surname') ?></a></p>

                <span class="user_info">
                    <img width="82" height="75" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $image; ?>" alt="" />
                </span>	

                <div class="user_msg">
                    <p>
                        <?php echo $model['message']; ?>                        
                    </p>
                    <?php $files = Bookingfiles::model()->findAll("messageID = '" . $model['id'] . "'"); ?>
                    <?php foreach ($files as $file) : ?>
                        <p><?php echo CHtml::link($file['file'], array('myBooking/download', 'id' => $file['id'])); ?></p>                  
                    <?php endforeach; ?>
                </div>
                <span class="date"><?php echo date('d.m.Y', strtotime($model->createTime)); ?></span>
            </div>       
        <?php endforeach; ?>
    </div>
<div class="pager_box">
            <?php
            $this->widget('CLinkPager', array(
                'pages' => $pages,
                'header' => '',
                'cssFile'=>Yii::app()->request->baseUrl.'/css/mypager.css',
            ));
            ?>
        </div>
    <!-- #Мои сообщения -->
</div><!-- #page -->
