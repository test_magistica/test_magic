<?php 
    $category = Category::model()->findByAttributes(array("url" => Yii::app()->request->requestUri));
    if($category !== NULL){
        if($category->metaKey!="")
            $this->pageKeywords = explode(',',  mb_strtolower($category->metaKey, 'UTF-8'));
	if($category->metaDesc!="")
            $this->pageDescription = $category->metaDesc;
	if($category->metaTitle!="")
            $this->pageTitle = $category->metaTitle;
    }
?>
 <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a>Гороскоп</a>        
    </div>
<div class="clr"></div>
<!-- HOROSCOPE -->
<div class="post bottom-shadow">
    <div class="horoscope-wrap">
        <div class="head-wrap"><h1>Гороскоп онлайн на сегодня и завтра, для всех знаков зодиака</h1></div>
        <div class="content-wrap">
            <h5>Выберете знак зодиака:</h5>
            <ul id="horo-items">
                <li id="aries"><a href="/horoscope/index.html?zodiac=aries"><img alt="photo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/hrcp-Aries.jpg">Овен</a></li>
                <li id="taurus"><a href="/horoscope/index.html?zodiac=taurus"><img alt="photo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/hrcp-Taurus.jpg">Телец</a></li>
                <li id="gemini"><a href="/horoscope/index.html?zodiac=gemini"><img alt="photo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/hrcp-Gemini.jpg">Близнецы</a></li>
                <li id="cancer"><a href="/horoscope/index.html?zodiac=cancer"><img alt="photo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/hrcp-Cancer.jpg">Рак</a></li>
                <li id="leo"><a href="/horoscope/index.html?zodiac=leo"><img alt="photo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/hrcp-Leo.jpg">Лев</a></li>
                <li id="virgo"><a href="/horoscope/index.html?zodiac=virgo"><img alt="photo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/hrcp-Virgo.jpg">Дева</a></li>
                <li id="libra"><a href="/horoscope/index.html?zodiac=libra"><img alt="photo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/hrcp-Libra.jpg">Весы</a></li>
                <li id="scorpio"><a href="/horoscope/index.html?zodiac=scorpio"><img alt="photo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/hrcp-Scorpio.jpg">Скорпион</a></li>
                <li id="sagittarius"><a href="/horoscope/index.html?zodiac=sagittarius"><img alt="photo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/hrcp-Sagittarius.jpg">Стрелец</a></li>
                <li id="capricorn"><a href="/horoscope/index.html?zodiac=capricorn"><img alt="photo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/hrcp-Capricorn.jpg">Козерог</a></li>
                <li id="aquarius"><a href="/horoscope/index.html?zodiac=aquarius"><img alt="photo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/hrcp-Aquarius.jpg">Водолей</a></li>
                <li id="pisces"><a href="/horoscope/index.html?zodiac=pisces"><img alt="photo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/hrcp-Pisces.jpg">Рыбы</a></li>
            </ul>
        </div>
        <div class="foot-wrap">
            <p><a href="#">астрологический прогноз</a></p>
        </div>
        <div class="skl"></div>
        <div class="skl2"></div>
    </div>
</div>
<!-- #HOROSCOPE -->

<div class="sonnikpage forecast">
    <div class="head1">
        <h2>ОБЩИЙ ПРОГНОЗ</h2>
    </div>
    <?php $this->widget('ext.Horoscope.Horoscope'); ?>
</div>