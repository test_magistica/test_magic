<div itemprop="bredcrumb" class="bread">
	<a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
	<a href="<?php echo Yii::app()->createUrl("horoscope"); ?>">Гороскоп</a>
	<a>Рассылка гороскопов</a>
</div>
<div class="clr"></div>
<div class="sonnikpage forecast br_">
	<h1>Подписка на ежедневную рассылку гороскопов</h1>
	<div class="post bottom-shadow">
		<article class="shadowblock">
		<? if (Yii::app()->user->hasFlash('subscription')) { ?>

			<div class="flash-success">
				<?php echo Yii::app()->user->getFlash('subscription'); ?>
			</div>

		<? } else {

			$form = $this->beginWidget('CActiveForm', array(
				'id'                    => 'subscription-form',
				'action'                => array('horoscope/subscription'),
				'enableClientValidation'=> true,
				'enableAjaxValidation'  => true,
				'clientOptions'         => array(
							'validateOnSubmit' => false,
							'validateOnChange' => true,
				)
			));?>

			<?=$form->error($model,'zodiak', array('class'=>'errFld')); ?>
			<strong class="number">1.</strong> <strong>Выберите один или несколько знаков зодиака:</strong>
			<br class="_br">
			<?=$form->checkBoxList($model, 'zodiak', $zodiaks); ?>
			<br><br><br>

			<?=$form->error($model,'category', array('class'=>'errFld')); ?>
			<strong class="number">2.</strong> <strong>Выберите вид прогноза:</strong>
			<br class="_br">
			<?=$form->checkBoxList($model, 'category', array('day' => 'Ежедневный','week' => 'Еженедельный'), array('separator'=>'&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;')); ?>
			<br><br><br>

			<?=$form->error($model,'email', array('class'=>'errFld')); ?>
			<strong class="number">3.</strong> <strong>Введите адрес, по которому хотите получать прогноз:</strong>
			<br class="_br">
			<div class="input-mask">
				<span class="f_left mt10px w100px"> &nbsp;E-mail:</span> <?php echo $form->textField($model, 'email', array('class')); ?>
			</div>
			<br><br>

			<input class="subscribe" name="subscribe" type="submit" value="Подписаться">
			<?php $this->endWidget(); ?>
		<? } ?>
		</article>
	</div>
</div>
