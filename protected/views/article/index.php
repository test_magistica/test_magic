<div class="usercab_page">
    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a href="<?php echo Yii::app()->createUrl("article");?>">Статьи</a>        
    </div>
    
    <div class="head">
        <h1>СТАТЬИ</h1>
    </div>
    <div class="all_posts">
        <?php foreach ($models as $model): ?>
            <article class="my_posts">
                <div class="post_header article_title">
	                <div class="article_auth">
		                <i>Автор:</i>
		                <?php echo CHtml::link($model->authort->name, Yii::app()->createUrl('extrasens/view',
		                array(  'id' => $model['author'],
			                'title' => Controller::getCHPU($model->authort->name . '-' . $model->authort->profession)))); ?>
		                <i class="lefte_D">,</i>
		                <span><?php echo $this->getDate($model['createTime']); ?></span>
	                </div>
                    <?php echo CHtml::link($model['title'] , array('article/view', 'id' => $model['id'], 'title' => $model['url'])); ?>


                </div>
                <?php $image = !empty($model['image']) ? $model['image'] : 'nophoto.jpg'; ?>
                <img src="/<?php echo Article::PATH_TO_IMAGE.$image; ?>" alt="photo" class="fLeft"/>
                <p><?php echo $model['anons']; ?></p>
                <?php echo CHtml::link('Читать полностью', array('article/view', 'id' => $model['id'], 'title' => $model['url']), array('class' => 'lnk-more')); ?>                
                <div class="clr"></div>
            </article>
        <?php endforeach; ?>
    </div>

    <div class="pager">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $pages,
            'header' => '',
        ));
        ?>
    </div>

</div><!-- #page -->
