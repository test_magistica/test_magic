<?php
$pageKeywords = new HKeywords();
$pageKeywords = array_merge(
        explode(',', 'гадание онлайн, ясновидящие, экстрасенсы'), 
        explode(',', $pageKeywords->get_keywords($model['description']))
        );
?>
<?php $this->pageKeywords = ( trim($model->metaKey) <> '' ? explode(',', mb_strtolower($model->metaKey, 'UTF-8')) : $pageKeywords ); ?>
<?php $this->pageDescription = ( trim(CHtml::encode($model->metaDesc)) <> '' ? CHtml::encode($model->metaDesc) : CHtml::encode($model->anons) ); ?>
<?php $this->pageTitle = CHtml::encode($model->title) . ' | Гадание онлайн Экстрасенсы позвонить'; ?>
<div class="usercab_page">

    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a href="<?php echo Yii::app()->createUrl("article");?>">Статьи</a>  
        <a><?php print($model->title); ?></a> 
    </div>

    <div class="head">
        
    </div>

    <article class="my_posts">
        <div class="post_header">
            <h1 style="margin: 0;"><b><?php print($model->title); ?></b></h1>  <i>Автор:</i> <?php echo CHtml::link(
                    Controller::getUser($model['author'], 'name'), 
                    array('extrasens/view', 'id' => $model->authort->id, 'title' => $this->getCHPU($model->authort->name)."-".$this->getCHPU($model->authort->profession))); ?>, <span><?php echo $this->getDate($model['createTime']); ?></span>
        </div>
        <?php $image = !empty($model['image']) ? $model['image'] : 'nophoto.jpg'; ?>
        <img src="/<?php echo Article::PATH_TO_IMAGE.$image; ?>" alt="photo" class="fLeft"/>
        <?php print($model->description); ?>
    </article>

    <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
    <div class="yashare-auto-init" 
         data-yashareL10n="ru"
         data-yashareType="button" 
        data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,gplus">
    </div>
</div><!-- #page -->