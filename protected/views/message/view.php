<div class="usercab_page">
    <div class="head">
        <h1>МОИ СООБЩЕНИЯ</h1>
    </div>


    <!-- Мои сообщения -->
    <div class="my_msg_list block my_user_msg">
        <p class="cmnt_lnk">
            <?php echo CHtml::link('Написать письмо другому эксперту', array('message/create')); ?>
        </p>
        <div class="commentform">
            <div class="commentstat"><?php echo $count; ?> комментариев</div>
            <div class="chatformbox chatformbox2">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'register-form',
                    //'enableClientValidation' => TRUE,
                    'htmlOptions' => array('class' => 'searchbox'),
                    'enableAjaxValidation' => TRUE,
                    'clientOptions' => array(
                        'validateOnSubmit' => TRUE,
                    ),
                        ));
                ?> 
                <div style="margin-left: 95px;">
                    <?php echo $form->errorSummary($model); ?>
                </div>
                <br />
                <div class="field" style="display: none;">
                    <?php echo $form->error($model, 'toID'); ?> 
                    <?php echo $form->error($model, 'topic'); ?>
                    <?php echo $form->error($model, 'message'); ?>
                </div>
                <?php echo $form->textArea($model, 'message', array('class' => 'border5')); ?>                    
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Отправить' : 'Сохранить', array('class' => 'submit')); ?>
                <?php $this->endWidget(); ?>
            </div>
        </div>
        <?php foreach ($models as $model): ?>
            <?php $image = $this->getUserImage($model['userID']); ?>
            <div class="shadowblock mymess">
                <!--change class new message class: "name new_msg" -->
                <p class="lnk-name"><a><?php echo $model['userID'] == Yii::app()->user->id ? 'Вы' : $this->getUser($model->userID, 'name') . '&nbsp;' . $this->getUser($model->userID, 'surname') ?></a></p>
                <p class="lnk-name"><?php echo $model['topic']; ?></p>
                <span class="user_info">
                    <img width="82" height="75" src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $image; ?>" alt="" />
                </span>	

                <div class="user_msg">
                    <p>
                        <?php echo $model['message']; ?>
                    </p>
                </div>
                <span class="date"><?php echo date('d.m.Y', strtotime($model->createTime)); ?></span>
            </div>       
        <?php endforeach; ?>
        <div class="pager">
            <?php
            $this->widget('CLinkPager', array(
                'pages' => $pages,
                'header' => '',
            ));
            ?>
        </div>


    </div>
    <!-- #Мои сообщения -->
</div><!-- #page -->