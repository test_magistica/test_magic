<div class="usercab_page">
    <div class="head">
        <h1>МОИ СООБЩЕНИЯ</h1>
    </div>
    <!-- Мои сообщения -->
    <div class="my_msg_list block my_user_msg sonnikpage ">
        <div class="cmnt_lnk">            
            <p><?php echo CHtml::link('мой сообщение', array('/message/')); ?></p>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'register-form',
                //'enableClientValidation' => TRUE,
                'htmlOptions' => array('class' => 'searchbox'),
                'enableAjaxValidation' => TRUE,
                'clientOptions' => array(
                    'validateOnSubmit' => TRUE,
                ),
            ));
            ?> 
            <div style="margin-left: 95px;">
                <?php echo $form->errorSummary($model); ?>
            </div>
            <br />
            <div class="field" style="display: none;">
                <?php echo $form->error($model, 'toID'); ?> 
                <?php echo $form->error($model, 'topic'); ?>
                <?php echo $form->error($model, 'message'); ?>
            </div>
            <div class="searchboxRow">
                <label class="fLeft">Эксперт: &nbsp; </label> 
                <div class="selectbox smallselect">
                    <?php
                    echo $form->dropDownList($model, 'toID', CHtml::listData(Expert::model()->findAll("role = 'expert'"), 'id', 'name'), array(
                        'class' => 'selectBlock',
                        'empty' => '--',
                    ));
                    ?>
                </div>
                <div class="clr"></div>
            </div>
            <div class="searchboxRow">
                <label class="fLeft">Тема:  &nbsp;</label> <?php echo $form->textField($model, 'topic', array('class' => 'inp')); ?>
                <div class="clr"></div>
            </div>
            <p>Текс собшения</p>
            <div class="commentform">
                <div class="commentstat">&nbsp;</div>
                <div class="chatformbox chatformbox2">
                    <?php echo $form->textArea($model, 'message', array('class' => 'border5')); ?>                    
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Отправить' : 'Сохранить', array('class' => 'submit')); ?>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>

    </div>
    <!-- #Мои сообщения -->
</div><!-- #page -->