<div class="usercab_page">
    <div class="head">
        <h1>Мои сообщения</h1>
        <!--        <a href="#" class="border10 all">Сменить пароль</a>-->
    </div>


    <!-- Мои сообщения -->
    <div class="my_msg_list block">
        <p class="cmnt_lnk"><?php echo CHtml::link('написать сообщение', array('message/create')); ?></p>
        <?php foreach ($models as $model): ?>
            <?php
            $criteria = new CDbCriteria();
            $criteria->order = 'id desc';
            $criteria->compare('fromID', (int) Yii::app()->user->id);
            $criteria->compare('toID', (int) $model->toID);
            $criteria->compare('userID', (int) $model->toID);
            $criteria->compare('status', 0);
            $cnt = Message::model()->count($criteria);            
            ?>
            <div class="shadowblock mymess">
                <!--change class new message class: "name new_msg" -->
                <span class="name">
                    <?php if($cnt>0): ?>
                    <span class="user_name"><span id="message<?php echo $model['id']; ?>">(<?php echo $cnt; ?>)</span> <?php echo CHtml::link($this->getUser($model->toID, 'name') . '&nbsp;' . $this->getUser($model->toID, 'surname'), array('/message/view/', 'id' => $model->toID)); ?></span>
                    <?php else: ?>
                    <span class="user_name"><span id="message<?php echo $model['id']; ?>"></span><?php echo CHtml::link($this->getUser($model->toID, 'name') . '&nbsp;' . $this->getUser($model->toID, 'surname'), array('/message/view/', 'id' => $model->toID)); ?></span>
                    <?php endif; ?>
                    <i><?php echo strlen($this->getLastTopic($model->toID)) >= 50 ? mb_substr($this->getLastTopic($model->toID), 0, 50, 'UTF-8') . '...' : $this->getLastTopic($model->toID); ?></i> 
                </span>	
                <span class="date"><?php echo date('d.m.Y', strtotime($model->createTime)); ?></span>
            </div>
        <?php endforeach; ?>
    </div>
    <!-- #Мои сообщения -->
    <div class="pagination">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $pages,
            'header' => '',
        ));
        ?>
    </div>
</div><!-- #page -->