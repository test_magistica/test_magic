<div id="questions" class="rtvs">
    <?php if(false):?>
        <div class="info radius-5 bg-light-blue c-light-grey" >
            <ul class="list" >
                <li>- Вы можете задать <b>вопрос онлайн бесплатно</b> сразу всем экпертам.</li>
                <li>- Ваши личныйе даные и фотографии доступны для просмотра, только эксертам</li>
                <li>- Вы можете <b>задать вопрос ясновидящим</b>, оставаясь инкогнито без публикации на сайте</li>
            </ul>    
            
            <div class="form question">
            <?php if (Yii::app()->user->hasFlash('error')): ?>
            <div class="flash-error">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
            <?php endif; ?>
                <?php 
                    $form = $this->beginWidget('CActiveForm', array(
                        'action' => Yii::app()->createUrl('question/create', array('st' => Question::STATUS_DRAFT)),
                        'enableClientValidation'=>FALSE,
                    )); 
                ?>
                    <div class="row">
                        <?php 
                            echo $form->textField(
                                $model, 
                                'title', 
                                array(
                                    'class' => 'quest-text radius-5',
                                    'placeholder' => "Введите ваш вопрос",
                                    'maxlength' => 78,
                                    )
                                );
                        ?>
                        <?php echo $form->error($model,'title'); ?>
                        <?php echo CHtml::submitButton("Отправить", array('class' => 'quest-sub bg-fiolet radius-5'));?>
                    </div>
                <?php $this->endWidget();?>
                <!-- form -->
            </div>    
        </div>  
    <?php endif;?>
        <div id="tabs-question" class="list-view question bg-light-ellow radius-5" >
            <ul class="tabs-menu c-light-grey tr-upper" >Вопросы пользователя
                    <li class="fr tr-none"><a href="javascript:void(0);">Все вопросы</a></li> 
            </ul>
            <div id="ui-tabs-2" class="tab-nopay-quest">
                <?php
                $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$dataProvider,
                'itemView'=>'index_item_view',   // refers to the partial view named '_post'
    //                'sortableAttributes'=>array(
    //                    'title',
    //                    'create_time'=>'Post Time',
    //                ),
                ));
                ?>
            </div> 
        </div>
        <script>
            $("#Question_title").keyup(function(){
                $(this).val($(this).val().charAt(0).toUpperCase() + $(this).val().slice(1).toLowerCase());
             });
        </script>
</div>