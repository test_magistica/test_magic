<?php
$expert = $data->a_author;
if($expert !== NULL){
$is_question_owner = Yii::app()->user->id == $data->question->author_id;
$is_expert = Yii::app()->user->role == Users::ROLE_EXPERT;
//var_dump($is_expert)       ;die();
// Достиг ли уровень рейтинга эксперта максимума
$expertRatingOverUp = ((int) $expert->expert_raiting >= (int) Yii::app()->params['maxExpertRaiting']);
}
?>
<?php if($expert !== NULL): ?>
<div id="answer-<?php echo $data->id; ?>" class="item answer ">
    <div class="pic expert fl" >
        <a href="#" title="Переход на страницу пользователя" class="link expert" >
            <?php
            $expPhoto = $this->getUserImage($expert->id);
            ?>
            <?php // echo CHtml::link( array('/extrasens/view/', 'id' => $expert['id'], 'title' => $this->getCHPU($expert['name'] . '-' . $expert['profession'])), array('class' => 'review')); ?>
            <a href="<?php echo Yii::app()->createUrl('/extrasens/view/', array('id' => $expert->id, 'title' => $this->getCHPU($expert['name'] . '-' . $expert['profession']),'class' => 'review'))?>">
            <img 
                src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $expPhoto; ?>" 
                title="Картинка експерта" 
                valign='top' 
                width="70px"
                height="66px"
                />
            </a>
        </a> 
    </div>
    <div class="info expert ff-tahoma" >
        <div class="name">
            <a href="<?php echo Yii::app()->createUrl('/extrasens/view/', array('id' => $expert->id, 'title' => $this->getCHPU($expert['name'] . '-' . $expert['profession']),'class' => 'review'))?>" class="link expert fs-16 c-fiolet" >
                <?php echo $expert->name . "&nbsp;" . $expert->surname; ?>
            </a>
        </div>
        <div class="about">
            <div class="status">
                <div class="short">
                    <span class="prof"><?php echo $expert->profession; ?></span>
                    <?php $url = $this->getUrlToExpertChat($expert);?>
                    <?php if((Yii::app()->user->isGuest || Yii::app()->user->role == Users::ROLE_USER)/* && $url !== FALSE*/):?>
                        <?php if($expert->chat_available == 1):?>
                            <?php if($expert->online == "online"):?>
                        <a href="<?php echo $url; ?>" title="Онлайн чат с экстрасенсом" target="_blank">
                            <div class="chat-online-button">Онлайн чат</div>
                        </a>
                        <?php else:?>
                        <?php endif; ?>
                        <?php endif; ?>
                    <?php endif;?>

                    <?php if(Yii::app()->user->role == Users::ROLE_EXPERT && $expert->chat_available == 1):?>
                        <?php if($expert->online == "online"):?>
                        <a href="<?php echo "#"; ?>" title="Онлайн чат с экстрасенсом" onclick="return false;">
                            <div class="chat-online-button">Онлайн чат</div>
                        </a>
                        <?php else:?>
                        <?php endif; ?>
                    <?php endif;?>
                    
                </div>
                <div class="raiting_star">
                    <div id="raiting-<?php echo $expert->id; ?>" 
                        class="raiting vote-static">
                        <div id="raiting_blank"></div> <!--блок пустых звезд-->
                        <div id="raiting_hover"></div> <!--блок  звезд при наведении мышью-->
                        <div id="raiting_votes" class="raiting_votes-<?php echo $expert->id; ?>"></div> <!--блок с итогами голосов -->
                    </div>
                    <div class="raiting_info"><h5><?php echo $expert->expert_raiting; ?></h5></div>
                    <input type="hidden" name="totalMaxRaiting" value="<?php echo Yii::app()->params['maxExpertRaiting']; ?>"/>
                    <input type="hidden" name="displayRaiting" value="<?php echo $expert->expert_raiting; ?>"/>
                    <input type="hidden" name="r_id" value="<?php echo $expert->id; ?>"/>
                    <input type="hidden" name="countVotes" value="<?php echo $expert->countVotes;?>"/>
                </div>
            </div>
            <div class="sub form">
            </div>
            <div class="clear"></div>
        </div>
        <p class="desc answer c-green-grey fs-13" >
            <?php echo $data->content; ?>
        </p>
        <?php // echo CHtml::link("Онлайн чат", Yii::app()->createUrl("/chat/Chat/startChat", array('id' => $expert->id)),array('target' => '_blank'))?>
    </div> 
    <div class="right">
        <div class="vote_control">
            <?php if (Yii::app()->user->id == $data->question->author_id): ?>
                <?php if ($data->is_voted == "0"): ?>
                    <div class="raiting_star_vote_scale">
                        <div class="vote_scale_raiting_info">
                            Ваша оценка
                        </div>
                        <div id="raiting2-<?php echo $expert->id; ?>" 
                             class="voting_scale">
                            <div id="raiting_blank"></div> <!--блок пустых звезд-->
                            <div id="raiting_hover"></div> <!--блок  звезд при наведении мышью-->
                            <div id="raiting_votes" class="raiting_votes-<?php echo $expert->id; ?>"></div> <!--блок с итогами голосов -->
                        </div>
                        <input type="hidden" name="totalMaxRaiting" value="<?php echo Yii::app()->params['maxExpertRaiting']; ?>"/>
                        <input type="hidden" name="displayRaiting" value="<?php echo $expert->expert_raiting; ?>"/>
                        <input type="hidden" name="r_id" value="<?php echo $expert->id; ?>"/>
                        <input type="hidden" name="countVotes" value="<?php echo $expert->countVotes;?>"/>
                    </div>
                    <div class="clear"></div>
                <?php else: ?>
                    <div class="raiting_star_vote_scale">
                        <div class="vote_scale_raiting_info">
                            Ваша оценка
                        </div>
                        <div id="raiting2-<?php echo $expert->id; ?>" 
                             class="voting_scale_static">
                            <div id="raiting_blank"></div> <!--блок пустых звезд-->
                            <div id="raiting_hover"></div> <!--блок  звезд при наведении мышью-->
                            <div id="raiting_votes" class="raiting_votes-<?php echo $expert->id; ?>"></div> <!--блок с итогами голосов -->
                        </div>
                        
                        <input type="hidden" name="r_id" value="<?php echo $expert->id; ?>"/>
                        <input type="hidden" name="voteScale" value="<?php echo $data->is_voted; ?>"/>
                    </div>
                    <div class="clear"></div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
</div>  

<?php endif;?>




