<?php 
    Yii::app()->clientScript->registerCssFile(
            "/css/question/myprofile.css"
            );
?>
<?php if(Yii::app()->user->hasFlash('error')): ?>
    <div class="flash-error-question">
            <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php  endif;?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
    <div class="flash-success-question">
            <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php  endif;?>
<div class="usercab_page">
    <div class="head">
        <h1>Платные вопросы</h1>
    </div>
    <br />
    <br />
    <br />
        <!-- Вопросы -->
    
    <table class="extra-mail-table">
        <tr>
            <th>Отправитель</th>
            <th class="second">Тема сообщения</th>
            <th class="last">Дата</th>
        </tr>
        <?php
                $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$paidQuestions,
                'itemView'=>'admin_item_view_paid',   // refers to the partial view named '_post'
                'pagerCssClass' => 'pagination',
                'template' => 
                    '{items}'
                    . '<tr>'
                    . '<td colspan=3>{pager}</td>'
                    . '</tr>'
    //                'sortableAttributes'=>array(
    //                    'title',
    //                    'create_time'=>'Post Time',
    //                ),
                ));
                ?>

    </table>
</div>
