<div>
    Поступил новый вопрос:   <span><?php echo $question->title; ?></span>
</div>
<div>
    <p>
        <?php echo $question->content; ?>
    </p>
</div>
<div>
    Просим Вас отреагировать и дать подробный ответ в ближайшее время.<br />
    <a href="<?php echo Yii::app()->createAbsoluteUrl("/question/view", array('id' => $question->id, "title" => $question->url)); ?>">Ссылка на публикацию вопроса.</a>
        
</div>