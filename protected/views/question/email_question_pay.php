<div>
    Здравствуйте, Вы создали платный (анонимный) вопрос для экспертов:   <span><?php echo $question->title; ?></span>
</div>
<div>
    <p>
        <?php echo $question->content; ?>
    </p>
</div>
<?php if($question->is_paid == 0): ?>
<div>
    Для отправки вопроса <a href="<?php echo Yii::app()->createAbsoluteUrl("/payment"); ?>"> пополните Ваш баланс</a> и активируйте ваш вопрос.<br />
    После оплаты Все эксперты увидят Ваш вопрос и смогут ответить на него. Актвировать свой вопрос вы можете из <a href="<?php echo Yii::app()->createAbsoluteUrl("/question/myProfile"); ?>">личного кабинета</a><br />
    <a href="<?php echo Yii::app()->createAbsoluteUrl("/question/view", array('id' => $question->id,"title" => $question->url)); ?>">Ссылка на публикацию Вашего вопроса.</a>
</div>
<?php else:?>
<div>
    <a href="<?php echo Yii::app()->createAbsoluteUrl("/question/view", array('id' => $question->id,"title" => $question->url)); ?>">Ссылка на публикацию Вашего вопроса.</a>
</div>
<?php endif;?>
<div>
    Всех Вам благ, <br />
    Эзотерическая служба "Магистика"
	<a href="http://magistika.com">magistika.com</a>
</div>