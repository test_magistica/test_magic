<div>
    Здравствуйте, <span><?php echo $answer->question->author->name; ?></span>
</div>
<div>
    <p>
        Эксперт <?php echo $answer->a_author->name; ?> ответил на Ваш вопрос «<?php echo $answer->question->title; ?>»
    </p>
    <p>
        <a href="<?php echo Yii::app()->createAbsoluteUrl("/question/view", array('id' => $answer->question->id)); ?>">
            <?php 
                if($answer->question->pay == "1"){
                    echo $answer->question->content; 
                }
                else{
                   echo strlen($answer->content) >= 50 ? mb_substr($answer->content, 0, 50, 'UTF-8') . '...' : $answer->content;
                }
            ?>
        </a>
    </p>
</div>
<div>
    <p>
         Если у Вас осталить  вопросы эксперту, вы всегда можете задать их позвонив по платному телефону:<br />
         из России <?php echo  $answer->a_author->mobile. " " . $answer->a_author->mobileText;?><br />
         Или совершить звонок с сайта или заказать обратный звонок(<a href="<?php echo Yii::app()->createAbsoluteUrl("/payment"); ?>">для звонка необходимо пополнить счет</a>)<br />
		 При звонке на платный телефон убедитесь, что эксперт свободен в данный момент</a>)<br />
    </p>
</div>
<div>
    Всех Вам благ, 
    Эзотерическая  служба "Магистика"
    http://magistika.com
</div>