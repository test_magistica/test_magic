<?php $user = $data->author; ?>
<?php if(!is_null($user)):?>
<div id="quest-<?php echo $data->id; ?>" class="item-quest">
    <div class="pic-quest fl" >
        <a href="#" title="Переход на страницу пользователя" class="info-user" >
            <?php 
//            var_dump($user);die();
                $userPhoto = $this->getUserImage($user->id, "question");
            ?>
            <img 
                src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $userPhoto; ?>" 
                title="Картинка пользователя" 
                valign='top' 
                width="58px"
                height="58px"
                />
        </a> 
    </div>
    <div class="info-quest ff-tahoma" >
        <h3>
            <?php if($data->is_paid == Question::QUESTION_PAID && $data->pay == Question::QUESTION_PAY):  ?>
                <a style="color: red !important;" href="<?php echo Yii::app()->createUrl("question/view", array('id' => $data->id, 'title' => $data->url)); ?>" class="title fs-16 c-fiolet" ><?php echo $data->title; ?></a>
            <?php else: ?>
                <a href="<?php echo Yii::app()->createUrl("question/view", array('id' => $data->id, 'title' => $data->url)); ?>" class="title fs-16 c-fiolet" ><?php echo $data->title; ?></a>
            <?php endif; ?>
        </h3>
        <p class="desc-quest c-green-grey fs-13" >
            <?php 
                if(strlen($data->content) >= 130)
                    echo mb_substr($data->content, 0, 130, 'UTF-8') . '...';
                else {
                    echo $data->content;
                }
            ?>
        </p>
        <div class="icons-quest fs ff-tahoma fs-12" >
            <div class="date-quest fl" >
                <?php echo Yii::app()->dateFormatter->format("d MMMM yyyy | HH:mm", $data->create_time); ?>
            </div>
            <div class="maps-taro-quest fl" >
                <a href="<?php echo Yii::app()->createUrl("/question/viewArea", array('id' => $data->area_expertise->id, 'title' => $data->area_expertise->seo_url))?>">
                <?php 
                    echo $data->area_expertise->name; 
                ?>
                </a>
            </div>
            <div class="answers-quest fl" >Ответов(<?php echo count($data->answers); ?>)</div>
            <div class="clear"></div>
        </div>
    </div>                    
</div>
<?php endif;?>
