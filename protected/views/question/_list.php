<?php foreach($answers as $answers): ?>
<div class="question" id="c<?php echo $question->id; ?>">

	<?php echo CHtml::link("#{$answers->id}", $answers->getUrl($question), array(
		'class'=>'cid',
		'title'=>'Permalink to this answer',
	)); ?>

	<div class="author">
		<?php echo $answer->authorLink; ?> says:
	</div>

	<div class="time">
		<?php echo date('F j, Y \a\t h:i a',$answer->create_time); ?>
	</div>

	<div class="content">
		<?php echo nl2br(CHtml::encode($answer->content)); ?>
	</div>

</div><!-- answer -->
<?php endforeach; ?>