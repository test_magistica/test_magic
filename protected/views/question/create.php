<div id="ask-expert">
    <h1 class="title" >Задать свой вопрос эксперту</h1>
    <?php if($step == "1"): ?>
    <p class="info" >
        Чтобы задать вопрос экспертам онлайн бесплатно правильно заполните поля на этой странице.  
        Пожалуйста оформляйте вопрос правильно, без грамматических ошибок.
		После публикации вашего вопроса эксперты начнут отвечать, ответы будут приходить на email.
		Внимание! Платные вопросы не публикуются в открытом доступе и могут быть удалены по первому требованию.        
    </p>
    <div class="form question-answer" style="margin-top:62px;">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
                'id' => 'ask-question',
                'enableClientValidation' => FALSE,
                'enableAjaxValidation' => FALSE,
                'clientOptions' => array(
                    'validateOnSubmit' => FALSE,
                ),
                'action' => Yii::app()->createUrl('question/create', array('st' => 2, "#" => 'ask-expert')),
                    
                'htmlOptions' => array('enctype' => 'multipart/form-data'),
                 )                 
            );
      ?>
       <div class="head"> 
           <?php echo $form->errorSummary($model); ?>
           <div class="title inline left w-248">ШАГ 1. Задать вопрос</div>
             <div class="area inline fr right">
             <?php echo $form->labelEx($model,'area_expertise_id',array('label'=>'Область знаний')); ?>
             <?php echo $form->dropDownList($model,'area_expertise_id',                           
                       CHtml::listData( 
                               AreaExpertise::model()->findAll(
                                       "(status=1)",
                                       array("order"=>"sort")
                                       ),
                               'id',
                               'name'),
                     array('prompt'=>Yii::t('all','Выбор')."...")
                     ); 
             ?>             

             <?php echo $form->error($model,'area_​​expertise_id'); ?>             
              </div>
          </div>    
          <div class="row">            
              <?php echo $form->labelEx($model,'title',array('label'=>'Заголовок:')); ?>
              <?php echo $form->textField($model, 'title', array('class' => 'title', 'maxlength' => 78)); ?>              
              <?php echo $form->error($model,'area_​​expertise_id'); ?>  
          </div>
          <div class="row">    
              <?php echo $form->labelEx($model,'content',array('label'=>'Вопрос:')); ?>
              <?php echo $form->textArea($model, 'content', array('class' => 'text', 'maxlength' => 2000)); ?>              
              <?php echo $form->error($model,'content'); ?>               
          </div>            
          <div class="row">    
              <?php echo $form->labelEx($model,'image',array('label'=>'Фотография:')); ?>		              
              <?php echo CHtml::activeFileField($model,'image',array('class'=>'upload-file')); ?>                
              <?php echo $form->error($model,'image'); ?>
          </div> 
          <div class="row button center">
              <?php echo CHtml::submitButton('ДАЛЕЕ  >>', array('class' => 'ask')); ?>              
          </div> 
        <?php $this->endWidget();?>
        <?php endif;?>
        <?php if($step == "2"): ?>
            <div style="margin-top:52px;"></div>
            <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'terms-placement',
                                //'enableClientValidation' => TRUE,
                                'enableAjaxValidation' => FALSE,
                                'clientOptions' => array(
                                    'validateOnSubmit' => FALSE,
                                ),
                        'action' => Yii::app()->createAbsoluteUrl("question/create", array('st'=>2)),
                          'htmlOptions'=>array(/*'class'=>'hidden'*/)
                         )                 
                     );
             ?>
                <?php if(Yii::app()->user->hasFlash('not_enough_money')): ?>
                    <div class="flash-notice">
                            <?php echo Yii::app()->user->getFlash('not_enough_money'); ?>
                    </div>
                <?php  endif;?>
                <?php if(Yii::app()->user->hasFlash('notice')): ?>
                    <div class="flash-notice">
                            <?php echo Yii::app()->user->getFlash('notice'); ?>
                    </div>
                <?php  endif;?>
                  <div class="head"> 
                      <div class="title inline left w-248">ШАГ 2. Условия размещения</div>
                  </div>    
                  <div class="row">
                      <div class="column pay">
                          <div class="title">       
                            <?php echo $form->radioButton($model, 'pay', array('value' => 0, 'class' => 'radio')); ?> 
                            <?php echo $form->labelEx($model,'pay',array('label'=>'<span class="name">Платный</span><span class="price">'.Question::PAY_AMOUNT.' руб.</span>')); ?>  
                          </div>
                          <div class="info">
                              <div class="item <?php echo $model->pay ? '' : 'active';?>">
                                  <div class="title">Приватный вопрос</div>
                                  <p class="desc">
                                     Вопрос не публикуется в открытом доступе. Его видят только эксперты
                                  </p>
                              </div>
                              <div class="item <? echo $model->pay ? '' : 'active';?>">
                                  <p class="desc">
                                      Вы получете ответ на Ваш вопрос в течение 30 минут
                                  </p>    
                              </div>   
                          </div>
                      </div>
                      <div class="column nopay">
                          <div class="title">
                            <?php echo $form->radioButton($model, 'pay', array('value' => 1, 'class' => 'radio')); ?>                     
                            <?php echo $form->labelEx($model,'pay',array('label'=>'<span class="name">Бесплатный</span>')); ?>                        
                          </div>
                          <div class="info">
                            <div class="item <? echo $model->pay ? 'active' : '';?>">
                                <div class="title">Модерация вопроса</div>
                                <p class="desc">
                                   Ваш вопрос будет опубликован в порядке очереди
                                </p>
                            </div>
                            <div class="item <? echo $model->pay ? 'active' : '';?>">
                                <p class="desc" >
                                Ответы экспертов могут даны в течение 24 часов часов 
                                </p>
                            </div>        
                          </div>                   
                      </div>
                      <div class="clear"></div>
                  </div>
                  <div class="row button center">
                      <?php echo CHtml::submitButton('Задать вопрос', array('class' => 'ask')); ?>     
                  </div>  

            <?php $this->endWidget(); ?>
        <?php endif; ?>
            <div id="t_moptip" style="display: none;">
                    Четко сформулируйте Ваш вопрос.<br>
					<br>
                    например: Когда я выйду замуж?<br>
            </div>
            <div id="q_moptip" style="display: none;">
			    * Требования к заполнению:<br>
				                                    <br>
                - Опишите как можно подробнее о вашей ситуации.<br>
                - Не пишите свою дату рождения и имя<br>
                - Фотография поможет ответить экспертам более точно.</i>
            </div>
        <script>             
           $('input:radio').live('change',function(){  
                    $('.column.pay .info .item').toggleClass('active');
                    $('.column.nopay .info .item').toggleClass('active');
           }); 
            $(document).ready(function(){
                $("#Question_title").on('focus', function(){
                   var w = $("#Question_title").width();
                    $("#t_moptip").show();
                    $("#t_moptip").offset({top:$(this).offset().top + 10, left:$(this).offset().left + w + 25})
                });
                $("#Question_content").on('focus', function(){
                   var w = $("#Question_title").width();
                    $("#q_moptip").show();
                    $("#q_moptip").offset({top:$(this).offset().top + 10, left:$(this).offset().left + w + 25})
                });
                $("#Question_title").on('focusout', function(){
                    $("#t_moptip").hide();
                });
                $("#Question_content").on('focusout', function(){
                    $("#q_moptip").hide();
                });
            });
        </script>
    </div>
</div>
