<?php $user = $data->author; ?>
<tr>
    <?php if (0): ?>
        <td>  
    <!--                        <img src="images/avatar.jpg" alt="" class="extra-avatar">-->
            (<?php // echo $cnt;  ?>) 
            <?php
//                            echo CHtml::link(
//                                    $question->author->name . '&nbsp;' . $question->author->surname,
//                                    array('question/view/', 'id' => $question->id)
//                                    ); 
            ?>
        </td>
    <?php else: ?>
        <td>  
            <?php if($data->is_paid == 0 && $data->pay == 1):?>
                <a 
                    href="<?php echo Yii::app()->createUrl("question/confirmQuestion", array('id' => $data->id)) ?>" 
                    title="Отправить экспертам">Отправить
                </a>
                <a 
                    href="<?php echo Yii::app()->createUrl("question/refuseFromQuestion", array('id' => $data->id)) ?>" 
                    title="Отказать от вопроса">Отказаться
                </a>
            <?php else: ?>
            <span class="" title="Количество ответов">Ответы (<?php echo $data->answerCount; ?>)</span>
            <?php endif;?>
            <!--<span class="" title="Количество ответов"><?php echo $data->answerCount; ?></span>-->
        </td>
    <?php endif; ?>
    <td>
        <?php
        echo CHtml::link(
                (strlen($data->title) >= 50 ? mb_substr($data->title, 0, 50, 'UTF-8') . '...' : $data->title), Yii::app()->createAbsoluteUrl("question/view", array('id' => $data->id, '#' =>"quest-".$data->id ))
        );
        ?>
    </td>
    <td class="last">
        <?php echo Yii::app()->dateFormatter->formatDateTime($data->create_time, 'long', null); ?>
    </td>
</tr>