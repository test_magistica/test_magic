<?php
$category = Category::model()->findByAttributes(array("url" => Yii::app()->request->requestUri));
if ($category !== NULL) {
    if ($category->metaKey != "")
        $this->pageKeywords = explode(',', mb_strtolower($category->metaKey, 'UTF-8'));
    if ($category->metaDesc != "")
        $this->pageDescription = $category->metaDesc;
    if ($category->metaTitle != "")
        $this->pageTitle = $category->metaTitle;
}else {
    if (isset($area)) {
        if ($area->metaKey != "")
            $this->pageKeywords = explode(',', mb_strtolower($area->metaKey, 'UTF-8'));
        if ($area->metaDesc != "")
            $this->pageDescription = $area->metaDesc;
        if ($area->metaTitle != "")
            $this->pageTitle = $area->metaTitle;
    }
}
?>

<div id="questions" class="rtvs">
    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a>Задать вопрос</a>        
    </div>
    <h1>вопросы экстрасенсам</h1>
    <div class="info radius-5 bg-light-blue c-light-grey" >
        <ul class="list" >
            <li>- Вы можете задать <b>вопрос онлайн бесплатно</b> и узнать мнение сразу нескольких экспертов.</li>
            <li>- Четко заданный вопрос и описание вашей ситуации с фото гарантирует точность ответа.</li>
            <li>- Платный вопрос не публикуется в открытом доступе, увидеть вопрос и фото могут только эксперты.</li>
            <li>- Платный вопрос вы можете удалить в любое время.</li>
            <li>.				</li>
            <li>- Чтобы получить консультацию экстрасенса бесплатно, Вам необходимо зарегистрироваться.</li>
        </ul>    

        <div class="form question">
<!--            --><?php //if (Yii::app()->user->hasFlash('error')): ?>
<!--                <div class="flash-error">-->
<!--                    --><?php //echo Yii::app()->user->getFlash('error'); ?>
<!--                </div>-->
<!--            --><?php //endif; ?>
<!--            --><?php
//            $form = $this->beginWidget('CActiveForm', array(
//                'action' => Yii::app()->createUrl('question/create', array('st' => Question::STATUS_DRAFT)),
//                'enableClientValidation' => FALSE,
//            ));
//            ?>
<!--            <div class="row">-->
<!--                --><?php
//                echo $form->textField(
//                        $model, 'title', array(
//                    'class' => 'quest-text radius-5',
//                    'placeholder' => "Введите заголовок вопроса",
//                    'maxlength' => 78,
//                        )
//                );
//                ?>
<!--                --><?php //echo $form->error($model, 'title'); ?>
<!--                --><?php //echo CHtml::submitButton("Отправить", array('class' => 'quest-sub bg-fiolet radius-5')); ?>
<!--            </div>-->
<!--            --><?php //$this->endWidget(); ?>
            <!-- form -->
	        <div class="add_question_bl">
		        <?=CHtml::link("задать бесплатно вопрос экстрасенсам", $this->createUrl('/question/zadat-vopros-jekstrasensu'));?>
	        </div>
        </div>    
    </div>                      
    <div id="tabs-question" class="list-view question bg-light-ellow radius-5" >
        <ul class="tabs-menu c-light-grey tr-upper" >Последние вопросы экстрасенсам
            <li class="fr tr-none"><a href="javascript:void(0);">Бесплатно</a></li> 
        </ul>
        <div id="ui-tabs-2" class="tab-nopay-quest">
            <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => 'index_item_view', // refers to the partial view named '_post'
                'pager' => array(
                    'class' => 'CLinkPager',
                    'previousPageCssClass' => "prev",
                    'prevPageLabel' => "< назад",
                    'maxButtonCount' => 6,
                    'header' => "",
                    'firstPageLabel' => "<",
                    'lastPageLabel' => ">",
                ),
                    //                'sortableAttributes'=>array(
                    //                    'title',
                    //                    'create_time'=>'Post Time',
                    //                ),
            ));
            ?>
        </div> 
    </div>
    <script>
        $("#Question_title").keyup(function() {
            $(this).val($(this).val().charAt(0).toUpperCase() + $(this).val().slice(1).toLowerCase());
        });
    </script>
</div>
