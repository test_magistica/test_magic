<?php
    // Стили для окна подтверждения обратного звонка
   if(/*$question->image !== NULL && */(Yii::app()->user->role == Users::ROLE_EXPERT || Yii::app()->user->role == Users::ROLE_ADMIN)){
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/../css/extrasens/view.css"); 
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/../css/facybox/facybox.css"); 
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/../js/facybox.js");
    }
?>

<div class="slider-container">
<?php // $this->renderPartial('/site/_sliderbox'); ?>
</div>
<div id="question" class="m-t380">
    <div itemprop="bredcrumb" class="bread">
        <a href="<?php echo Yii::app()->homeUrl; ?>"><?php echo Yii::app()->params['bredcrumbHomeText']; ?></a>
        <a href="<?php echo Yii::app()->createUrl("question"); ?>">Задать вопрос</a>        
        <a><?php echo CHtml::encode($question->title);?></a>        
    </div>
    <div class="clr"></div>
    <div class="form bg-grey radius-5" style="">
        <div class="form question">
        <form class="new-question" style=""> 
            <a href="<?php echo Yii::app()->createUrl("/question/create", array('st' => 1)); ?>">
                <input type="button" value="Задать вопрос онлайн" class="button ask" />
            </a>
            <a href="<?php echo Yii::app()->createUrl("/question"); ?>">
                <input type="button" value="Просмотреть все вопросы" class="button view" />
            </a>
        </form>
        </div>
    </div>
    <?php if (Yii::app()->user->hasFlash('error')): ?>
            <div class="flash-error">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
    <?php endif; ?>
    <?php if (Yii::app()->user->hasFlash('question_placed')): ?>
            <div class="flash-success">
                <?php echo Yii::app()->user->getFlash('question_placed'); ?>
            </div>
    <?php endif; ?>
    <div class="about bg-light-ellow radius-10" >
        <div id="quest-<?php echo $question->id;?>" class="item m-0">
            <div class="pic fl" >
               <a href="#customer-card" rel="facybox" title="Переход на страницу пользователя" class="info-user" >
                  <?php $img = $this->getUserImage($question->author->id, "question");?>
                   <img 
                       src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $img; ?>" 
                       title="Картинка пользователя" 
                       valign='top' 
                       width="58px"
                       height="58px"
                       />
               </a> 
            </div>
            <div class="info ff-tahoma" >
                <?php if($question->is_paid == Question::QUESTION_PAID && $question->pay == Question::QUESTION_PAY):  ?>
                <h1 style="color: red !important;" class="title fs-16 c-fiolet"><?php echo CHtml::encode($question->title);?></h1>
                <?php else: ?>
                <h1 class="title fs-16 c-fiolet"><?php echo CHtml::encode($question->title);?></h1>
                <?php endif; ?>
                <p class="desc c-green-grey fs-13" >
                    <?php echo CHtml::encode($question->content);?>
                </p>
                <?php if($question->image !== NULL): ?>
                    <p class="photo desc c-green-grey fs-13" >
                         <a href="javascript:noid(0);">
                             <img width="536px" src="<?php echo "/".Yii::app()->baseUrl.Yii::app()->params['uploadDir']."question_img/".$question->image; ?>" />
                        </a>
                    </p>
                <?php endif;?>
                    <?php if(Yii::app()->user->role == Users::ROLE_EXPERT || Yii::app()->user->role == Users::ROLE_ADMIN): ?>
                    <div style="display: none;" id="customer-card">
                        <div class="customer_card_container">
                            <div class="cc-photo">
                                <img 
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $img; ?>" 
                                    title="Картинка пользователя" 
                                    valign='top' 
                                    width="58px"
                                    height="58px"
                                    />
                            </div>
                            <div class="cc-data-container">
                                <div class="cc-data-title">карточка клиента</div>
                                <div class="cc-data-body">
                                    <div class="cc-name">
                                        <span>имя:</span><span><?php echo $question->author->name." ".$question->author->surname; ?></span>
                                    </div>
                                    <div class="cc-age">&nbsp;
                                        <!--<span>возраст:</span><span>18</span>-->
                                    </div>
                                    <div class="cc-country">
                                        <span>страна:</span><span><?php echo $question->author->country; ?></span>
                                    </div>
                                    <div class="cc-date-of-birth">
                                        <span>дата рожд.:</span><span><?php echo $question->author->birthday;?></span>
                                    </div>
                                    <div class="cc-questions">
                                        <span>вопросы:</span><span><a href="<?php echo Yii::app()->createAbsoluteUrl("question/userQuestions", array('id' => $question->author->id)); ?>">(<?php echo $question->author->countQuestions;?>)</a></span>
                                    </div>
                                    <div class="cc-reg-date">
                                        <span>рег:</span><span><?php 
                                        echo Yii::app()->dateFormatter->formatDateTime($question->author->reg_date, 'long', null)
                                        ?></span>
                                    </div>
                                    <div class="cc-balance">
                                        <span>баланс:</span><span><?php echo $question->author->bill;?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        jQuery(document).ready(function($) {
                            $('a[rel*=facybox]').facybox({
                                noAutoload: false,
                                modal: true
                            });
                        }); 
                    </script>
                    <?php endif;?>
                <div class="user ff-tahoma fs-12" >
                    <div class="name p-0 fl" >Автор вопроса:&nbsp;<span><?php echo $question->author->name; ?></span><span>&nbsp;|&nbsp;</span></div>
                   <div class="date p-0 fl" >
                       <?php echo Yii::app()->dateFormatter->format("d MMMM yyyy | HH:mm", $question->create_time); ?>
                       <span>&nbsp;|&nbsp;</span>
                   </div> 
                   <div class="clear"></div>
                 </div>
                <?php if(Yii::app()->user->role == Users::ROLE_EXPERT):?>
                <div class="exp-answer-container">
                    <div>
                        <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id' => "exp-answer",
                                'action' => Yii::app()->createUrl('question/NewAnswer'),
                            ));
                            $answer = Answer::model();
                            $uemail = Users::model()->findByPk(Yii::app()->user->id)->email;
//                          echo '<pre>';  var_dump();die();
                        ?>
                        <?php // echo $form->labelEx($answer,'content',array('label'=>'Ответ:')); ?>
                        <?php echo $form->textArea($answer, 'content', array("class" => "exp-answer-content")); ?>
                        <?php echo $form->error($answer,'content'); ?>
                        
                        <?php echo CHtml::hiddenField("Answer[author]", Yii::app()->user->id); ?>
                        <?php echo CHtml::hiddenField("Answer[status]", Answer::STATUS_PENDING); ?>
                        <?php echo CHtml::hiddenField("Answer[post_id]", $question->id); ?>
                        <?php echo CHtml::hiddenField("Answer[email]", $uemail); ?>
                        <?php echo CHtml::submitButton('Ответить', array('class' => 'exp-ans')); ?>
                        <?php $this->endWidget();?>
                    </div>
                </div>
                <?php endif;?>
             </div>                    
        </div>        
    </div>
    <?php if($a_dataprovider->itemCount > 0):?>
    <div id="quest-answers">
        <h3><span class="title">Ответы экспертов</span> <span class="section">Категория: <?php echo $question->area_expertise->name; ?></span></h3>
        <?php
            $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$a_dataprovider,
                'itemView'=>'answer_item_view',   // refers to the partial view named '_post'
               'afterAjaxUpdate' => 'function(id, data){$.voteManager.initVotes()}', 
//                'sortableAttributes'=>array(
//                    'title',
//                    'create_time'=>'Post Time',
//                ),
            ));
        ?>
        
    </div>
    <?php endif;?>
    <!-- questions -->
</div>