<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main'); ?>
<?php $c = Controller::getMessageCount() > 0 ? '(' . Controller::getMessageCount() . ')' : ''; ?>
<?php $b = Controller::getBookingCount() > 0 ? '(' . Controller::getBookingCount() . ')' : ''; ?>


<!-- Sidebar здесь начинаеться column2 -->
<section class="sRight">
    <?php echo $content; ?>
</section>



<aside>
    <div class="profile_wrap">

        <div class="ava"> <b>    
                <?php $photo = Userphoto::model()->find("`userID` = '" . Yii::app()->user->id . "' AND `main`=1"); ?>
                <img src="<?php echo $photo['fullPath']; ?>" alt="" width="50px" height="50px" /></b></div>
        <div class="user_status">
            <h1><?php echo Controller::getUser(Yii::app()->user->id, 'name'); ?> <?php echo Controller::getUser(Yii::app()->user->id, 'surname'); ?></h1>
            <span class="online">онлайн</span> 
            <!--   you may change status class: online, busy, autonomous   -->
        </div>
        <?php
        $this->widget('zii.widgets.CMenu', array(
            'htmlOptions' => array('class' => 'profile_list'),
            'items' => array(               
                array('label' => 'Мои статьи<i class="i_post"></i>', 'url' => array('article/admin'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Мои сообщения<i class="i_msg"></i> <span id="shoutbox"></span></span>', 'url' => array('message/admin'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Мои услуги<i class="i_service"></i> ', 'url' => array('service/admin'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Мои заказы<i class="i_order"></i> ' . $b . '', 'url' => array('booking/admin'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Мой профайль<i class="i_profile"></i>', 'url' => array('//user/update/', 'id' => Yii::app()->user->id), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Сменить пароль <i class="i_lock"></i>', 'url' => array('//user/password/'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Мой фотография<i class="i_gallery"></i>', 'url' => array('//user/photo/', 'id' => Yii::app()->user->id), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Выход (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
            ), 'encodeLabel' => false,
                )
        );
        ?>      

        <div class="clr"></div>
    </div>
</aside>
<!-- #Sidebar -->


<!--<div id="sidebar">
<?php
//    $this->beginWidget('zii.widgets.CPortlet', array(
//        'title' => 'Operations',
//    ));
//    $this->widget('zii.widgets.CMenu', array(
//        'items' => $this->menu,
//        'htmlOptions' => array('class' => 'operations'),
//    ));
//    $this->endWidget();
?>
</div>-->


<!-- sidebar -->


<?php $this->endContent(); ?>