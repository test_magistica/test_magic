<!DOCTYPE html>
<html>
<head>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?php  Yii::app()->clientScript->registerMetaTag(null,'text/html; charset=utf-8','Content-Type');
	Yii::app()->clientScript->registerMetaTag(Yii::t('all',$this->pageDescription), 'description');
	Yii::app()->clientScript->registerMetaTag(implode(', ',$this->pageKeywords), 'keywords');
	Yii::app()->clientScript->registerMetaTag("ru", 'language'); //!@TODO Lang

	if(!preg_match('/test.|.lc/', $_SERVER['HTTP_HOST'])) {
		Yii::app()->clientScript->registerMetaTag("44897d7014ba123e", 'yandex-verification');
		Yii::app()->clientScript->registerMetaTag("google-site-verification", 'Q57X7l9UefDppWjHJRayvt0xe47uj8PVgB3joRVMa-w');
	}
	?>
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/magistika_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/magistika_select.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/magistika_slider.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/magistika_checked.css" media="all" />
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap_expert.css">

	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<?php Yii::app()->clientScript->registerCoreScript('yiiactiveform'); ?>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.coda-slider-3.0.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-ui-1.10.2.custom.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/select.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.ezmark.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.coda-slider-3.0.min.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/horoscope.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/radio.js" type="text/javascript"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.placeholder.js" type="text/javascript"></script>
	<link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" />
</head>

<body itemscope itemtype="http://schema.org/WebPage">
<?php if (Yii::app()->user->role == Users::ROLE_EXPERT): ?>
	<?php $this->widget('application.modules.chat.components.ChatExpertNotification'); ?>
	<?php $this->widget('ext.ExpertNotification.ExpertNotification'); ?>
	<?php endif; ?>
<?php if(Yii::app()->user->role == Users::ROLE_USER || Yii::app()->user->isGuest): ?>
	<?php $this->widget('ext.messageNotification.MessageNotification'); ?>
	<?php endif; ?>
<div id="wrapper">
<div class="middlewrap">

<!-- Шапка сайта -->
<header>
	<a href="<?php echo Yii::app()->homeUrl; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png" alt="" class="logo" /></a>
	<a href="#" class="consult">Консультации экстрасенсов онлайн<br />Прогнозы на ближайшее будущее</a>

	<?php $this->widget('ext.loginWidget.loginWidget'); ?>


	<div class="helpbox">
		<div class="ttl">&nbsp;</div>
		<?php if(Yii::app()->user->role == Users::ROLE_USER): ?>
		<a href="<?php echo Yii::app()->createUrl("/payment"); ?>" class="call pay-btn">Пополнить счет</a>
		<div class="icon"></div>
		<?php else:?>
		<div class="ttl">ПОДДЕРЖКА КЛИЕНТОВ:</div>
		<?php endif; ?>
		<div class="ttl">8 800 555 3317</div>
		<p>E-mail: <a href="mailto:help@magistika.ru">help@magistika.ru</a><br />
			Skype: <a href="skype:magistika-com?call">magistika-com</a></p>
	</div>
	<?php $this->widget('ext.topMenu.topMenu'); ?>
</header>
<section>

	<?php echo $content; ?>

</section>

<aside>
	<?
	$this->widget('ext.staticServices.StaticServicesWidget');

	if (count($this->clips['newAnswers']) > 0) {?>
	<div id="new-answers">
		<h3>Новые ответы</h3>
		<? foreach ($this->clips['newAnswers'] as $answer){ ?>
		<div id="answer-<?php echo $answer->id; ?>" class="item answer ">
			<div class="title answer" >
				<a href="<?php echo Yii::app()->createAbsoluteUrl("question/view", array('id' => $answer->question->id, 'title' => $answer->question->url, "#" => "answer-" . $answer->id)); ?>" title="Переход на страницу ответа" class="link answer" >
					<?php echo $answer->question->title; ?>
				</a>
			</div>
			<p class="anons answer ff-tahoma" >
				<?php
				if (strlen($answer->content) >= 130) {
					echo mb_substr($answer->content, 0, 130, 'UTF-8') . '...';
				} else {
					echo $answer->content;
				}
				?>
			</p>
			<div class="date">
				<span><?php echo Yii::app()->dateFormatter->formatDateTime($answer->create_time, 'long', null); ?></span>
			</div>
		</div>
		<? } ?>
	</div>
	<? } ?>
</aside>

<footer>
	<div class="fmenu">
		<?php
		$this->widget("ext.footer.Footer");
		?>
	</div>
	<div class="flogo" >
		<a href="#" style="display:inline-block;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo2.png" alt="" class="<?php //echo "logo2"; ?>" /></a>
	</div>
	<div style="display: inline-block;width: 860px;">
		<div class="copyright" >
                            <span class="copyright-text"  >
                                &copy; 2007-<?php echo Yii::app()->dateFormatter->format("yyyy", time()); ?> Экстрасенсы онлайн помощь и консультации.<br />Сайт гадания онлайн по телефону. Карты ТАРО и предсказание будущего
                            </span>
		</div>
		<noindex>
			<div style="float:right;" >
				<? if(!preg_match('/test.|.lc/', $_SERVER['HTTP_HOST'])) { ?>
				<!-- Yandex.Metrika informer -->
				<a href="http://metrika.yandex.ru/stat/?id=21394309&amp;from=informer"
				   target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/21394309/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
				                                       style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try {
                                                                            Ya.Metrika.informer({i: this, id: 21394309, lang: 'ru'});
                                                                            return false
                                                                        } catch (e) {
                                                                        }"/></a>
				<!-- /Yandex.Metrika informer -->

				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function(d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter21394309 = new Ya.Metrika({id: 21394309,
									webvisor: true,
									clickmap: true,
									trackLinks: true,
									accurateTrackBounce: true});
							} catch (e) {
							}
						});

						var n = d.getElementsByTagName("script")[0],
							s = d.createElement("script"),
							f = function() {
								n.parentNode.insertBefore(s, n);
							};
						s.type = "text/javascript";
						s.async = true;
						s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else {
							f();
						}
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="//mc.yandex.ru/watch/21394309" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
				<?}?>
			</div>
		</noindex>
		<div class="phone" >
			<span>наш телефон:</span>
			<div class="clr"></div>
			8 800 555 33 17
		</div>
	</div>
</footer>
<!-- #Подвал -->

</div><!-- #middlewrap -->
</div>
<?php
if (intval(Yii::app()->user->id) > 0) {
	?>
<script>
	function updateShouts() {

		$.post("<?php echo CController::createUrl('/message/check') ?>",
			{check: '1'},
			function(data) {
				if (data != 0) {
					$('#shoutbox').text('(' + data + ')');
					$('.loginbox .icon_mess').css("display", "block");
				}
				else
					$('.loginbox .icon_mess').css("display", "none");
			});
	}
	setInterval("updateShouts()", 5000);
</script>
	<?php
}
?>

<script>

	<?php
	if (in_array(Yii::app()->controller->id, Yii::app()->params['controllers']) && Yii::app()->controller->id . '/' . Yii::app()->controller->action->id !== 'user/create') {
		echo '$(".articlebox").hide();$("aside").css("float","left");$("section").css("float","right");$(".action1").hide();$(".goroskopbox").hide();$(".subscribebox").hide();$(".socialsidebox").hide();';
	} else {
		echo '$(".profile_wrap").hide();';
	}

	if (Yii::app()->controller->id . '/' . Yii::app()->controller->action->id == 'service/index') {
		echo '$(".articlebox").hide();$(".goroskopbox").hide();$(".subscribebox").hide();$(".socialsidebox").hide();$(".action1").hide();$(".action2").hide();';
	}

	if (Yii::app()->controller->id . '/' . Yii::app()->controller->action->id == 'service/view') {
		echo '$(".articlebox").hide();$(".goroskopbox").hide();$(".subscribebox").hide();$(".socialsidebox").hide();$(".action1").hide();$(".action2").hide();';
	}
	?>
</script>
<? if(!preg_match('/test.|.lc/', $_SERVER['HTTP_HOST'])) { ?>
<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-45223925-1', 'magistika.com');
	ga('send', 'pageview');

</script>
<script type="text/javascript" src="https://lcab.sms-uslugi.ru/support/support.js?h=5cd93fa7b638a0eaf83c0bcfe003520c" id="supportScript"></script>
	<?}?>
</body>
</html>
