<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main_quest'); ?>
<div id="content" >
    <!-- Центральный блок -->
    <section>
        <?php echo $content; ?>
    </section>
    <!-- #Центральный блок -->
    <!-- Sidebar -->
    <aside>        
        <!-- Beg:Block -->  
        <?php if(count($this->clips['areas']) > 0): ?>
        <div id="block" class="rtvs-block radius-5 bg-fiolet" >
        <h2 class="title-block" >Каталог</h2>        
        <div id="seticons" class="section" >
            <?php foreach ($this->clips['areas'] as $areaExpertise): ?>
            <div id="item-<?php echo $areaExpertise->id; ?>" class="item">
                <a href="<?php echo Yii::app()->createAbsoluteUrl("question/viewArea", array('id' => $areaExpertise->id, 'title' => $areaExpertise->seo_url )) ; ?>" class="link"><?php echo $areaExpertise->name ?> ( <?php echo $areaExpertise->countQuestion;?> )</a>
            </div>
            <?php endforeach; ?>
<!--            <div class="item soc">
                <a href="#" class="link">Рассказать друзьям: Одноклассники</a>
            </div>-->
        </div>
        <!-- Block -->
        <?php
        /* $this->widget('ext.stock.StockWidget'); $this->widget('ext.profile.Profile'); ?>
        <!-- Гороскоп -->
        <?php// $this->widget('ext.horoscopeWidget.horoscopeWidget'); ?>
           <!-- #Гороскоп -->

               <!-- Подписка на гороскоп -->
               <div class="subscribebox">
                    <div class="ttl">Гороскоп на сегодня</div>
                        <div class="in">
                            <form action="#" method="get">

                                <?php echo CHtml::link('ПОДП�?САТЬСЯ', array('horoscope/subscription'), array('class'=>'horo-subscribe')); ?>

                                <div class="clr"></div>
                            </form>
                        </div>
                    </div>
                    <!-- #Подписка на гороскоп -->


                    <!-- Поделиться с друзьями -->
                    <div class="socialsidebox">
                        <span>Магистика в интернете:</span>
						<a href="http://vk.com/magistika_com" class="vk"></a>
                        <a href="https://twitter.com/Magistika" class="tw"></a>
						<a href="#" class="ok"></a>
                        <a href="#" class="fb"></a>
                        <a href="https://plus.google.com/u/0/101924654359254468361" class="gl"></a>
                    </div>
                    <!-- #Поделиться с друзьями -->*/?>
        </div>
        <?php endif;?>
        </aside>
        <!-- #Sidebar -->
</div><!-- content -->
<?php $this->endContent(); ?>
<script>
    <?php
        if (in_array(Yii::app()->controller->id, Yii::app()->params['controllers']) && Yii::app()->controller->id . '/' . Yii::app()->controller->action->id !== 'user/create') {
           echo '$("aside").css("float","left");$("section").css("float","right");$(".action1").hide();$(".goroskopbox").hide();$(".subscribebox").hide();$(".socialsidebox").hide();';
        } else {
           echo '$(".profile_wrap").hide();';
        }

        if (Yii::app()->controller->id . '/' . Yii::app()->controller->action->id == 'service/index') {
            echo '$(".goroskopbox").hide();$(".subscribebox").hide();$(".socialsidebox").hide();$(".action1").hide();$(".action2").hide();';
        }

        if (Yii::app()->controller->id . '/' . Yii::app()->controller->action->id == 'service/view') {
            echo '$(".goroskopbox").hide();$(".subscribebox").hide();$(".socialsidebox").hide();$(".action1").hide();$(".action2").hide();';
        }
    ?>
</script>
