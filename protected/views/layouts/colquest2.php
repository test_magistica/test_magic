<?php /* @var $this Controller */ ?>
<?php $this->beginContent('/layouts/main_quest'); ?>
<div id="content" >
    <!-- Центральный блок -->
    <section>
        <?php echo $content; ?>
    </section>
    <!-- #Центральный блок -->
    <!-- Sidebar -->
    <aside> 

        <?php if (Yii::app()->controller->id == 'question' && Yii::app()->controller->action->id == 'view') : ?>
            <!-- Accija -->
            <?php // for ($t = 1; $t < 3; $t++) : ?>
<!--                <div id="block-action" class="action bg-action">
                    <img src="/images/pic-action-<?php // echo $t; ?>.png" >
                    <div class="title">Пример акции</div>
                </div>-->
            <?php // endfor; ?>
            <?php if(count($this->clips['areas']) > 0): ?>
            <div id="block" class="rtvs-block radius-5 bg-fiolet" >
                <h2 class="title-block" >Темы вопросов</h2>        
                <div id="seticons" class="section" >
                    <?php foreach ($this->clips['areas'] as $areaExpertise): ?>
                    <div id="item-<?php echo $areaExpertise->id; ?>" class="item">
                        <a href="<?php echo Yii::app()->createAbsoluteUrl("question/viewArea", array('id' => $areaExpertise->id, 'title' => $areaExpertise->seo_url )) ; ?>" class="link"><?php echo $areaExpertise->name ?> ( <?php echo $areaExpertise->countQuestion;?> )</a>
                    </div>
                    <?php endforeach; ?>
        <!--            <div class="item soc">
                        <a href="#" class="link">Рассказать друзьям: Одноклассники</a>
                    </div>-->
                </div>
            </div>
            <?php endif;?>
            <!-- #Accija -->   
            <!-- The best expert for section -->
            <div id="best-experts">
                <h3>Лучшие экстрасенсы в этой категории</h3>
                <?php foreach ($this->clips['bestExperts'] as $expert): ?>
                    <?php $expPhoto = $this->getUserImage($expert->id); ?>
                    <div id="expert-<?php echo $expert->id; ?>" class="item answer ">
                        <div class="pic expert fl" >
                            <a href="<?php echo Yii::app()->createUrl('/extrasens/view/', array('id' => $expert->id, 'title' => $this->getCHPU($expert->name . '-' . $expert->profession),'class' => 'review'))?>" 
                               title="Переход на страницу эксперта" 
                               class="link expert" >
                                <img 
                                    src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/user/<?php echo $expPhoto; ?>" 
                                    title="Картинка эксперта" 
                                    valign='top' 
                                    width="70px"
                                    height="66px"
                                    />
                                    <!--<img src="/images/avatar-expert.png" title="Картинка експерта" valign='top' />-->
                            </a> 
                        </div>
                        <div class="info expert ff-tahoma" >
                            <div class="name">
                                <a href="<?php echo Yii::app()->createUrl('/extrasens/view/', array('id' => $expert->id, 'title' => $this->getCHPU($expert->name . '-' . $expert->profession),'class' => 'review'))?>" class="link expert fs-16 c-fiolet" >
                                    <?php echo $expert->name; ?>
                                </a>
                            </div>
                            <div class="about">
                                <div class="status">
                                    <div class="short">
                                        <span class="prof"><?php echo $expert->profession; ?></span>
                                        <!--<span class="stat">online2</span>-->
                                        <?php $url = $this->getUrlToExpertChat($expert);?>
                                        <?php if((Yii::app()->user->isGuest || Yii::app()->user->role == Users::ROLE_USER) /*&& $url !== FALSE*/):?>
                                            <?php if($expert->chat_available == 1):?>
                                                <?php if($expert->online == "online"):?>
                                            <a href="<?php echo $url; ?>" title="Онлайн чат с экстрасенсом" target="_blank">
                                                <div class="chat-online-button-best-experts">Онлайн чат</div>
                                            </a>
                                            <?php else:?>
<!--                                                <a href="#" title="Онлайн чат с экстрасенсом" onclick="return false;">
                                                    <div class="chat-online-button-best-experts offline">Офлайн чат</div>
                                                </a>-->
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endif;?>

                                        <?php if(Yii::app()->user->role == Users::ROLE_EXPERT && $expert->chat_available == 1):?>
                                            <?php if($expert->online == "online"):?>
                                            <a href="<?php echo "#"; ?>" title="Онлайн чат с экстрасенсом" onclick="return false;">
                                                <div class="chat-online-button-best-experts">Онлайн чат</div>
                                            </a>
                                            <?php else:?>
<!--                                                <a href="#" title="Онлайн чат с экстрасенсом" onclick="return false;">
                                                    <div class="chat-online-button-best-experts offline">Офлайн чат</div>
                                                </a>-->
                                            <?php endif; ?>
                                        <?php endif;?>
                                    </div>
                                    <div class="raiting_star">
                                        <div id="raiting-<?php echo $expert->id; ?>" class="raiting vote-static">
                                            <div id="raiting_blank"></div> <!--блок пустых звезд-->
                                            <div id="raiting_hover"></div> <!--блок  звезд при наведении мышью-->
                                            <div id="raiting_votes" class="raiting_votes-<?php echo $expert->id; ?>"></div> <!--блок с итогами голосов -->
                                        </div>
                                        <div class="raiting_info"><h5><?php echo $expert->expert_raiting; ?></h5></div>
                                        <input type="hidden" name="totalMaxRaiting" value="<?php echo Yii::app()->params['maxExpertRaiting']; ?>"/>
                                        <input type="hidden" name="displayRaiting" value="<?php echo $expert->expert_raiting; ?>"/>
                                        <input type="hidden" name="r_id" value="<?php echo $expert->id; ?>"/>
                                        <input type="hidden" name="countVotes" value="<?php echo $expert->countVotes; ?>"/>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>                    
                    </div>  
                <?php endforeach; ?>
                <script type="text/javascript">

                </script>
            </div>        
            <!-- #The new answer section -->  
            <!-- The best expert for section -->
            <?php if(count($this->clips['newAnswers']) > 0):?>
            <div id="new-answers">
                <h3>Новые ответы</h3>
                <?php foreach ($this->clips['newAnswers'] as $answer): ?>
                    <div id="answer-<?php echo $answer->id; ?>" class="item answer ">
                        <div class="title answer" >
                            <a href="<?php echo Yii::app()->createAbsoluteUrl("question/view", array('id' => $answer->question->id, 'title' => $answer->question->url, "#" => "answer-" . $answer->id)); ?>" title="Переход на страницу ответа" class="link answer" >
                                <?php echo $answer->question->title; ?>
                            </a> 
                        </div>
                        <p class="anons answer ff-tahoma" >
                            <?php
                            if (strlen($answer->content) >= 130){
                                echo mb_substr($answer->content, 0, 130, 'UTF-8') . '...';
                            }
                            else {
                                echo $answer->content;
                            }
                            ?>
                            <!--Самые просты правила. Как сделать так, чтоб не дойти до критической ситуации в семье?--> 
                        </p>
                        <div class="date">
                            <span><?php echo Yii::app()->dateFormatter->formatDateTime($answer->create_time, 'long', null); ?></span>
                        </div>
                    </div>                    
                <?php endforeach; ?>
            </div>   
                <?php endif;?>
            <!-- #The best expert for section -->      
        
        <?php endif; ?> 
        <?php if (Yii::app()->controller->id == 'question' && Yii::app()->controller->action->id == 'index') : ?>
            <!-- Block -->        
            <div id="block-catalog" class="rtvs-block radius-5 bg-fiolet" >
                <h2 class="title-block" >Каталог</h2>        
                <div id="seticons" class="section" >
                    <?php for ($i = 0; $i < 22; $i++): ?>
                        <div i="item-1" class="item">
                            <a href="#" class="link">Гадание на картах #<?php echo $i; ?></a>
                        </div>
                    <?php endfor; ?>
                    <div class="item soc">
                        <a href="#" class="link">Рассказать друзьям: Одноклассники</a>
                    </div>
                </div>
            </div>
                <!-- #Block -->
        <?php endif; ?>        
        <?php if (Yii::app()->controller->id == 'question' && (Yii::app()->controller->action->id == 'create')) :

                //$this->widget('ext.stock.StockWidget'); $this->widget('ext.profile.Profile'); 
                ?>            
                <!-- Гороскоп -->
                <?php // $this->widget('ext.horoscopeWidget.horoscopeWidget', array('is_gortoday' => false)); ?>
                <!-- #Гороскоп -->

                <!-- Подписка на гороскоп -->
<!--               <div class="subscribebox">
                <div class="ttl">Гороскоп на сегодня</div>
                <div class="in">
                <form action="#" method="get">
                <?php // echo CHtml::link('ПОДП�?САТЬСЯ', array('horoscope/subscription'), array('class' => 'horo-subscribe')); ?>
                <div class="clr"></div>
                </form>
                </div>
                </div>-->
               
                <!-- #Подписка на гороскоп -->
        <?php endif; ?> 
            
            <!-- Поделиться с друзьями -->
<!--            <div class="socialsidebox">
            <span>Магистика в интернете:</span>
            <a href="http://vk.com/magistika_com" class="vk"></a>
            <a href="https://twitter.com/Magistika" class="tw"></a>
            <a href="#" class="ok"></a>
            <a href="#" class="fb"></a>
            <a href="https://plus.google.com/u/0/101924654359254468361" class="gl"></a>
            </div>-->
            <!-- #Поделиться с друзьями -->
        <!--</div>-->
    </aside>
    <!-- #Sidebar -->
<!-- content-->
</div> 


<script>
<?php
if (in_array(Yii::app()->controller->id, Yii::app()->params['controllers']) && Yii::app()->controller->id . '/' . Yii::app()->controller->action->id !== 'user/create') {
    echo '$("aside").css("float","left");$("section").css("float","right");$(".action1").hide();$(".goroskopbox").hide();$(".subscribebox").hide();$(".socialsidebox").hide();';
} else {
    echo '$(".profile_wrap").hide();';
}

if (Yii::app()->controller->id . '/' . Yii::app()->controller->action->id == 'service/index') {
    echo '$(".goroskopbox").hide();$(".subscribebox").hide();$(".socialsidebox").hide();$(".action1").hide();$(".action2").hide();';
}

if (Yii::app()->controller->id . '/' . Yii::app()->controller->action->id == 'service/view') {
    echo '$(".goroskopbox").hide();$(".subscribebox").hide();$(".socialsidebox").hide();$(".action1").hide();$(".action2").hide();';
}
?>
</script><?php $this->endContent(); ?>
