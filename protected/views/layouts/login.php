<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>    
<title><?php echo CHtml::encode($this->pageTitle); ?></title>           
<?php
    Yii::app()->clientScript->registerMetaTag(null,'text/html; charset=utf-8','Content-Type');
    Yii::app()->clientScript->registerMetaTag(Yii::t('all',$this->pageDescription), 'description'); 
    Yii::app()->clientScript->registerMetaTag(implode(', ',$this->pageKeywords), 'keywords');
    Yii::app()->clientScript->registerMetaTag("ru", 'language');  
?>
<?php 
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/backend/reset.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/backend/screen.css','screen, projection');
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/backend/print.css','print');
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/backend/ie.css', 'screen, projection:lt IE 8');
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/backend/main.css');             
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/backend/form.css'); 
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/backend/button.css'); 
?>
</head>
    
<body itemscope itemtype="http://schema.org/WebPage">
    <div class="login-wrapper">
        <div class="login-content">
            <div class="block wrapper radius-40-10 bg-ellow" >
                <?php echo $content; ?>
            </div>
        </div>
    </div>
</body>
</html>
