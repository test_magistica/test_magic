<?php

Yii::setPathOfAlias('bootstrap', dirname(__FILE__) . '/../extensions/bootstrap');

$actions = array(
    '', //no configuration specified, this will only work if site/index has default argument values
    'site/contact',
    'names',
    //echo CHtml::link($model2['name'], array('names/view/', 'id' => $model2['id'], 'title' => $this->getCHPU($model2['name']), 't' => Yii::app()->controller->action->id));
    /* array(
      'route' => 'names/view',
      'params' => array( //specify action parameters
      'model' => array(
      'class' => 'ManNames',
      //'criteria' => array('condition' => 'status=1'),
      'map' => array(
      'title' => 'name',
      'id' => 'id',
      't' => 'safe'
      ),
      ),
      ),
      ), */
    /* array(
      'route' => 'names/index',
      'params' => array( //specify action parameters
      'array' => array( //parameters provided in an array
      ),
      'model' => array(
      'class' => 'WomanNames',
      //'criteria' => array('condition' => 'status=1'),
      'map' => array(
      'id' => 'id',
      'url' => 'url', //@TODO create tr name
      ),
      ),
      ),
      ), */
    /* array(
      'route' => 'names/index',
      'params' => array( //specify action parameters
      'array' => array( //parameters provided in an array
      ),
      'model' => array(
      'class' => 'ManNames',
      //'criteria' => array('condition' => 'status=1'),
      'map' => array(
      'name' => 'name',
      'url' => 'url', //@TODO create tr name
      ),
      ),
      ),
      ), */
    'horoscope',
    array(
        'route' => 'horoscope/index',
        'params' => array(//specify action parameters
            'array' => array(//parameters provided in an array
                array('zodiac' => 'aries'),
                array('zodiac' => 'taurus'),
                array('zodiac' => 'gemini'),
                array('zodiac' => 'cancer'),
                array('zodiac' => 'leo'),
                array('zodiac' => 'virgo'),
                array('zodiac' => 'libra'),
                array('zodiac' => 'scorpio'),
                array('zodiac' => 'sagittarius'),
                array('zodiac' => 'capricorn'),
                array('zodiac' => 'aquarius'),
                array('zodiac' => 'pisces'),
            ),
            'model' => array(
                //'class' => 'Horoscope',
                //'criteria' => array('condition' => 'status=1'),
                'map' => array(
                    'zodiac' => 'zodiac',
                ),
            ),
        ),
    ),
    'service/index',
    array(
        'route' => 'service/index',
        'params' => array(//specify action parameters
            'model' => array(
                'class' => 'Category',
                //'criteria' => array('condition' => 'status=1'),
                'map' => array(
                    'categoryID' => 'id',
                ),
            ),
        ),
    ),
    'service/view',
    array(
        'route' => 'service/view',
        //'condition' => 'return array("service/view", "id" => $model["id"],"title"=>$this->getCHPU($model["notes"]));', //only if user is not guest
        'params' => array(//specify action parameters
            'model' => array(
                'class' => 'Service',
                'criteria' => array('condition' => 'status=1'),
                'map' => array(
                    'title' => 'notes',
                    'id' => 'id',
                ),
            ),
        ),
    ),
    'dreams',
    /* array(
      'route' => 'dreams/view',
      //'condition' => 'return array("service/view", "id" => $model["id"],"title"=>$this->getCHPU($model["notes"]));', //only if user is not guest
      'params' => array( //specify action parameters
      'model' => array(
      'class' => 'Dreams',
      //'criteria' => array('condition' => 'status=1'),
      'map' => array(
      'title' => 'name',
      'id' => 'id',
      ),
      ),
      ),
      ), */
    'article',
    array(
        'route' => 'article/view',
        //'condition' => 'return array("service/view", "id" => $model["id"],"title"=>$this->getCHPU($model["notes"]));', //only if user is not guest
        'params' => array(//specify action parameters
            'model' => array(
                'class' => 'Article',
                'criteria' => array('condition' => 'status=1'),
                'map' => array(
                    'title' => 'title',
                    'id' => 'id',
                ),
            ),
        ),
    ),
    'divinations',
    array(
        'route' => 'divinations/view',
        //'condition' => 'return array("service/view", "id" => $model["id"],"title"=>$this->getCHPU($model["notes"]));', //only if user is not guest
        'params' => array(//specify action parameters
            'model' => array(
                'class' => 'Divination',
                //'criteria' => array('condition' => 'status=1'),
                'map' => array(
                    'title' => 'title',
                    'id' => 'id',
                ),
            ),
        ),
    ),
    'extrasens/view',
    array(
        'route' => 'extrasens/view',
        //'condition' => 'return array("service/view", "id" => $model["id"],"title"=>$this->getCHPU($model["notes"]));', //only if user is not guest
        'params' => array(//specify action parameters
            'model' => array(
                'class' => 'Users',
                'criteria' => array('select' => '*,CONCAT(name," ",profession) as name', 'condition' => "role='expert' and status=1"),
                'map' => array(
                    'title' => 'name',
                    'id' => 'id',
                ),
            ),
        ),
    ),
    'question',
    array(
        'route' => 'question/view',
        'params' => array(//specify action parameters
            'model' => array(
                'class' => 'Question',
                'criteria' => array('condition' => 'status=2 AND pay=0'),
                'map' => array(
                    'title' => 'title',
                    'id' => 'id',
                ),
            ),
        ),
    ),
);



// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
            'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
            'name' => 'Magistika.com',
            'language' => 'ru',
            // preloading 'log' component
            'preload' => array('log'),
            // autoloading model and component classes
            'import' => array(
                'application.models.*',
                'application.components.*',
                'application.extensions.eoauth.*',
                'application.extensions.eoauth.lib.*',
                'application.extensions.lightopenid.*',
                'application.extensions.eauth.services.*',
                'application.extensions.minify.*',
                'application.web.helpers.*',
            //'application.web.widgets.*',
            ),
            'aliases' => array(
                //If you used composer your path should be
                //'xupload' => 'ext.vendor.Asgaroth.xupload',
                //If you manually installed it
                'xupload' => 'ext.xupload'
            ),
            'modules' => array(
                'admin',
                'expert',
                'chat' => array(
                    'defaultController' => "Chat"
                ),
                // uncomment the following to enable the Gii tool
                'gii' => array(
                    'class' => 'system.gii.GiiModule',
                    'password' => 'delphi77',
                    // If removed, Gii defaults to localhost only. Edit carefully to taste.
                    'ipFilters' => array('127.0.0.1', '::1'),
                    //'ipFilters' => array('127.0.0.1', '::1', '37.55.90.11'),
                ),
                'sitemap' => array(
                    'class' => 'ext.sitemap.SitemapModule', //or whatever the correct path is
                    'actions' => $actions, //optional
                    'absoluteUrls' => true, //optional
                    'protectedControllers' => array('admin'), //optional
                    'protectedActions' => array('site/error'), //optional
                    'priority' => '0.5', //optional
                    'changefreq' => 'always', //optional
                    'lastmod' => date('Y-m-d'), //optional
                    'cacheId' => 'cache', //optional
                    'cachingDuration' => 0, //optional
                ),
            ),
			// robokassa request result URL  - http://magistika.com/result
            // application components
            'components' => array(
                'bootstrap' => array(
                    'class' => 'bootstrap.components.Bootstrap',
                ),
                'platron' => array(
                    'class' => 'application.extensions.platron.PlatronPayment',
                    'merchant_id' => '1365',
                    'secret_key' => 'tymynyrowyfixoto',
                    'site_url' => 'http://magistika.com',
                    'result_url' => 'http://magistika.com/platron',
                    'success_url' => 'http://magistika.com/platron/success',
                    'failure_url' => 'http://magistika.com/platron/failure',
                    'test_mode' => '0',
                    'request_method' => 'POST'
                ),
                'curl' => array(
                    'class' => 'ext.Curl',
                //'options' => array()
                ),
                'cache' => array(
                    'class' => 'system.caching.CFileCache',
//            'class'=>'system.caching.CRedisCache',
//            'hostname'=>'localhost',
//            'port'=>6379,
//            'database'=>0,
                ),
                'loid' => array(
                    'class' => 'ext.lightopenid.loid',
                ),
                'eauth' => array(
                    'class' => 'ext.eauth.EAuth',
                    'popup' => true, // Use the popup window instead of redirecting.
                    'services' => array(// You can change the providers and their classes.
                        'google' => array(
                            'class' => 'GoogleOpenIDService',
                        ),
                        'yandex' => array(
                            'class' => 'YandexOpenIDService',
                        ),
                        'twitter' => array(
                            'class' => 'TwitterOAuthService',
                            'key' => '...',
                            'secret' => '...',
                        ),
                        'facebook' => array(
                            'class' => 'FacebookOAuthService',
                            'client_id' => '429461760439684',
                            'client_secret' => 'fce7235366e068cbd981c319a4164f69',
                        ),
                        'vkontakte' => array(
                            'class' => 'VKontakteOAuthService',
                            'client_id' => '...',
                            'client_secret' => '...',
                        ),
                        'mailru' => array(
                            'class' => 'MailruOAuthService',
                            'client_id' => '...',
                            'client_secret' => '...',
                        ),
                        'odnoklassniki' => array(
                            'class' => 'OdnoklassnikiOAuthService',
                            'client_id' => '...',
                            'client_secret' => '...',
                        ),
                    ),
                ),
                'session' => array(
                    'class' => 'system.web.CDbHttpSession',
                    'connectionID' => 'db',
                    'sessionTableName' => 'usersonline',
                    'autoCreateSessionTable' => FALSE,
                ),
                'simpleImage' => array(
                    'class' => 'application.extensions.CSimpleImage',
                ),
                'Check' => array(
                    'class' => 'application.extensions.Check',
                ),
                'authManager' => array(
                    // Будем использовать свой менеджер авторизации
                    'class' => 'PhpAuthManager',
                    // Роль по умолчанию. Все, кто не админы, модераторы и юзеры — гости.
                    'defaultRoles' => array('guest'),
                ),
                'user' => array(
                    'class' => 'WebUser',
                    // enable cookie-based authentication
                    'allowAutoLogin' => true,
                ),
                'clientScript' => array(
                    'class' => 'RsClientScript',
                    'combineScriptFiles' => false, //!YII_DEBUG, // By default this is set to true, set this to true if you'd like to combine the script files
                    'combineCssFiles' => false, //!YII_DEBU, // By default this is set to true, set this to true if you'd like to combine the css files
                    'optimizeScriptFiles' => false, //!YII_DEBU,	// @since: 1.1
                    'optimizeCssFiles' => false, //!YII_DEBU, // @since: 1.1
                ),
                // uncomment the following to enable URLs in path-format
                'urlManager' => array(
//'class' => 'ext.yii-multilanguage.MLUrlManager',
	                'urlFormat' => 'path',
//'urlSuffix' => '.html',
//'useStrictParsing'=>true,
	                'showScriptName' => false,
	                'rules' => array(
		                '' => 'site/index',
//'index.php' => array('site/index', 'parsingOnly'=>TRUE, 'caseSensitive'=>false),
//'sitemap.xml'=>'site/sitemapxml',
/* '<tab:otzyvy>-extrasens/<title:[\w\-]+>-<id:\d+>'=>'extrasens/view',
'<tab:uslugi>-extrasens/<title:[\w\-]+>-<id:\d+>'=>'extrasens/view',
"<tab:stat'i>-extrasens/<title:[\w\-]+>-<id:\d+>"=>'extrasens/view',
'<tab:chat>-extrasens/<title:[\w\-]+>-<id:\d+>'=>'extrasens/view',
'<tab:raspisanie>-extrasens/<title:[\w\-]+>-<id:\d+>'=>'extrasens/view',
		                '<tab:[\w\'\-]+>-extrasens/<title:[\w\-]+>-<id:\d+>' => 'extrasens/view',*/
		                'sp/<curl:[\w\'\-]+>' => 'site/showStaticPage',
		                'gadanie-online' => 'divinations/index',
		                'gadanie-po-fotografii' => 'photoGuess/register',
		                'gadanie-po-foto-primer' => 'photoGuess/example',
		                'vopros-ekstrasensu' => 'question/index',
//'service/<title:[\w\-]+>-<id:\d+>'=>'service/view',
//'names/<title:[\w\-]+>-<id:\d+>'=>'names/view',
//'dreams/<title:[\w\-]+>-<id:\d+>'=>'dreams/view',
//'article/<title:[\w\-]+>-<id:\d+>'=>'article/view',
//'divinations/<title:[\w\-]+>-<id:\d+>'=>'divinations/view',
//'extrasens/<title:[\w\-]+>-<id:\d+>'=>'extrasens/view',
//'question/<title:[\w\-]+>-<id:\d+>'=>'question/view',
		                'chat/<action:\w+>/<id:\d+>' => "chat/Chat/<action>",
		                'chat/<action:\w+>' => "chat/Chat/<action>",
//'question' => 'question/index',
		                'question/zadat-vopros-jekstrasensu' => array('question/create', 'defaultParams' => array('st' => 1)),
		                'question/viewArea/<title:[\w\-]+>-<id:\d+>' => 'question/viewArea',
		                '<_c:(service|names|dreams|article|divinations|extrasens|question)>/<title:[\w\-]+>-<id:\d+>' => '<_c>/view',
		                'service/<categoryID:\d+>' => 'service/index',
//'<tab:otzyvy>-extrasens/<id:\d+>'=>'extrasens/view',
//'<tab:uslugi>-extrasens/<id:\d+>'=>'extrasens/view',
//"<tab:stat'i>-extrasens/<id:\d+>"=>'extrasens/view',
//'<tab:chat>-extrasens/<id:\d+>'=>'extrasens/view',
//'<tab:raspisanie>-extrasens/<id:\d+>'=>'extrasens/view',
//'<tab:[\w\-]+>-extrasens/<id:\d+>' => 'extrasens/view',
//'<controller:w+>/<action:view>/<url:[\w]+>' => '<controller>/<action>',
//'<controller:\w+>/<id:\d+>' => '<controller>/view',
//'<controller:\w+>/<id:\d+>/<tab:\w+>' => '<controller>/view',
//'<controller:\w+>/<title:[\w\-]+>-<id:\d+>/<tab:\w+>' => '<controller>/view',
//'<controller:\w+>/<tab:\w+>/<id:\d+>/' => '<controller>/view',
//'<controller:\w+>/<tab:\w+>/<title:[\w\-]+>-<id:\d+>/' => '<controller>/view',
		                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
		                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
		                'sitemap.xml' => 'sitemap/default/index',
//'sitemap.html' 	=> 'sitemap/default/index/format/html',
                    ),
                ),
                'db' => include_once(dirname(__FILE__) . '/db.php'),
                'errorHandler' => array(
                    // use 'site/error' action to display errors
                    'errorAction' => 'site/error',
                ),
                'log' => array(
                    'class' => 'CLogRouter',
                    'routes' => array(
                        array(
                            'class' => 'CFileLogRoute',
                            'levels' => 'error, warning',
                        ),
                    // uncomment the following to show log messages on web pages
                    /*
                      array(
                      'class'=>'CWebLogRoute',
                      ),
                     */
                    ),
                ),
            ),
            // application-level parameters that can be accessed
            // using Yii::app()->params['paramName']
            'params' => array(
	            'mainAdminEmail' => 'parissema@ya.ru',
                'status' => array('online' => 'Свободен', 'busy' => 'Занят', 'autonomous' => 'Оффлайн'),
                'controllers' => array('user', 'message', 'myBooking', 'payment', 'statistic'),
                // this is used in contact page
                'adminEmail' => 'parissema@yandex.ru',
                'uploadDir' => 'upload2/',
                'maxExpertRaiting' => 100,
                'questionsPerPage' => 10,
                'bookingPerPage' => 20,
                'expertMP3notification' => "test.mp3",
                'bredcrumbHomeText' => "Экстрасенсы онлайн",
                // Бесплатные минуты чата. Значения от 0 (включительно) до много
                'freeChatMinutes' => 2,
	            //услуги которые фактически привязаны к платежам, и их основные ключи
	            'paymentServices' => array(
		            'consultation'  => array(
			            'table'         => 'consultation',
			            'keyRemind'     => 'paymentRemind',
			            'keyPayment'    => 'paymentSuccess',
			            'keyComplete'   => '',
		            ),
		            'photoGuess'    => array(
			            'table'         => 'photo_guess',
			            'keyRemind'     => 'reminderPhotoGuess',
			            'keyPayment'    => 'paymentSuccessService',
			            'keyComplete'   => 'answerePhotoGuess',
		            ),
	            ),
	            'whatList' => array(0 => 'На любовь', 1 => 'На будущее', 2 => 'На благополучие'),
            ),
);
