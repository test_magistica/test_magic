<?php

/**
 * This is the model class for table "page".
 *
 * The followings are the available columns in table 'page':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $name
 * @property string $metaKey
 * @property string $metaDesc
 */
class Page extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Page the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'page';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, description, name, clean_url', 'required'),
            array('metaKey, metaDesc', 'default'),
            array('title, name, clean_url', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, description, name, metaKey, metaDesc', 'safe', 'on' => 'search'),
            array('description', 'safe', 'on' => 'create'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'name' => 'Name',
            'metaKey' => 'Meta Key',
            'metaDesc' => 'Meta Desc',
            'clean_url' => 'Clean URL',
        );
    }

    public function beforeSave() {
        if(parent::beforeSave()){
            if($this->isNewRecord){
                $this->clean_url = Yii::app()->controller->translit(trim($this->clean_url));
                $url_exists = (int)Yii::app()->db->createCommand("select count(1) from page where clean_url='".$this->clean_url."'")->queryScalar();
                if($url_exists > 0){
                    $this->addError("clean_url", Yii::t("app", "Page duplicate. Rename the page."));
                    $this->addError("title", Yii::t("app", "Page duplicate. Rename the page."));
                    return FALSE;
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('metaKey', $this->metaKey, true);
        $criteria->compare('metaDesc', $this->metaDesc, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}