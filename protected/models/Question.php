<?php

class Question extends CActiveRecord {
    /**
     * The followings are the available columns in table 'tbl_question':
     * @var integer $id
     * @var string $title
     * @var string $content
     * @var string $tags
     * @var integer $status
     * @var integer $create_time
     * @var integer $update_time
     * @var integer $author_id
     */

    /**
     *  Вопрос сохранен в базе, но не выбран способ оплаты
     */
    const STATUS_DRAFT = 1;

    /**
     * Вопрос опубликован (можно просмотреть на сайте)
     */
    const STATUS_PUBLISHED = 2;

    /**
     * ХЗ
     */
    const STATUS_ARCHIVED = 3;
    const DATE_TIME_FORMAT = "Y-m-d H:i:s";
    const PAY_AMOUNT = 500;
    /**
     * Question is not free.
     */
    const QUESTION_PAY = 1;
    /**
     * Question is a free.
     */
    const QUESTION_FREE = 0;
    /**
     * Question is not free and paid.
     */
    const QUESTION_PAID = 1;
    /**
     * Question is not free and not paid yet.
     */
    const QUESTION_NOT_PAID = 0;
    /**
     * Интервал в днях между возможностью задать бесплатный вопрос
     */
    const DELAY_FOR_FREE_QUESTION = 7;

    private $_oldTags;

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'question';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, content, status, author_id, area_expertise_id', 'required'),
            array('status', 'in', 'range' => array(1, 2, 3)),
            array('title', 'length', 'max' => 128),
            array('metaKey, metaDesc, url', 'length', 'max' => 255),
//                        array('image', 'allowEmpty' => true),
            array('image', 'file', 'types' => 'jpg, gif, png', 'allowEmpty' => true, 'maxSize' => 8388608),
            array('tags', 'match', 'pattern' => '/^[\w\s,]+$/', 'message' => 'Tags can only contain word characters.'),
//			array('tags', 'normalizeTags'),
            array('author_id, area_expertise_id', 'numerical', 'integerOnly' => true),
            array('title, status, author_id, area_expertise_id, image', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'author' => array(self::BELONGS_TO, 'Users', 'author_id'),
            'area_expertise' => array(
                self::HAS_ONE,
                'AreaExpertise',
                array('id' => 'area_expertise_id'),
//                            'condition' => "t.area_expertise_id=аrea_expertise.id"
            ),
            'answers' => array(self::HAS_MANY, 'Answer', array('post_id' => 'id'), 'with' => 'a_author', 'condition' => 'answers.status=' . Answer::STATUS_APPROVED, 'order' => 'answers.create_time DESC'),
            'answerCount' => array(self::STAT, 'Answer', 'post_id', 'condition' => 'status=' . Answer::STATUS_APPROVED),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Id',
            'title' => 'Заголовок',
            'content' => 'Вопрос',
            'tags' => 'Tags',
            'image' => 'Фотография',
            'status' => 'Status',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
            'author_id' => 'Author',
            'area_expertise_id' => 'Область знаний'
        );
    }

    /**
     * @return string the URL that shows the detail of the question
     */
    public function getUrl() {
        return Yii::app()->createUrl('question/view', array(
                    'id' => $this->id,
                    'title' => $this->url,
        ));
    }

    /**
     * @return array a list of links that point to the question list filtered by every tag of this question
     */
    public function getTagLinks() {
        $links = array();
        foreach (Tag::string2array($this->tags) as $tag)
            $links[] = CHtml::link(CHtml::encode($tag), array('question/index', 'tag' => $tag));
        return $links;
    }

    /**
     * Normalizes the user-entered tags.
     */
    public function normalizeTags($attribute, $params) {
        $this->tags = Tag::array2string(array_unique(Tag::string2array($this->tags)));
    }

    /**
     * Adds a new question to this question.
     * This method will set status and question_id of the answer accordingly.
     * @param Answer the question to be added
     * @return boolean whether the question is saved successfully
     */
    public function addAnswer($answer) {
        if (Yii::app()->params['answerNeedApproval'])
            $answer->status = Answer::STATUS_PENDING;
        else
            $answer->status = Answer::STATUS_APPROVED;
        $answer->post_id = $this->id;
        return $answer->save();
    }

    protected function beforeValidate() {
        if ($this->isNewRecord && isset($_POST['Question']['area_expertise_id'])) {
//                $this->аrea_expertise = AreaExpertise::model()->findByPk($_POST['Question']['аrea_expertise_id']);
            $this->title = trim($this->title);
            $this->area_expertise_id = $_POST['Question']['area_expertise_id'];
            $this->status = Question::STATUS_DRAFT;
            $this->author_id = Yii::app()->user->id;

//                return TRUE;
        }
        return TRUE;
    }

    /**
     * This is invoked when a record is populated with data from a find() call.
     */
    protected function afterFind() {
        parent::afterFind();
        $this->_oldTags = $this->tags;
    }

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
//				$this->create_time=$this->update_time=time();
                $dt = new DateTime();
                $this->create_time = $this->update_time = $dt->format(Question::DATE_TIME_FORMAT);
                $this->author_id = Yii::app()->user->id;
                $symbols = array(
                    '/\?/', '/\!/', '/\(/', '/\)/', '/:/', '/;/', '/-/',
                    '/\./', '/\,/', '/%/', '/\\\/', '/\//', '/\+/', '/\*/',
                    '/=/', '/@/', '/#/', '/\$/', '/\&/', '/\^/', '/_/',
                    '/\|/', '/\{/', '/\}/', '/\[/', '/\]/', '/\</', '/\>/',
                    '/\"/', '/\'/', '/\~/', '/\`/', '/№/',
                );
                $tmp_title = preg_replace($symbols, "", $this->title);
                $tmp_title = trim($tmp_title, " ><|%&?!.\t\n\r\0\x0B");
                AreaExpertise::model()->findByPk($this->area_expertise_id)->name;
                $this->url = $this->translit($tmp_title);
                
                $words = explode(" ", $this->content);
                $sum = 0;
                $descArr = array();
                foreach ($words as $word) {
                    $sum += mb_strlen($word);
                    $descArr[] = $word;
                    if ($sum > 255)
                        break;
                }
                $this->metaDesc = implode(" ", $descArr);

                $del_symbols = array(",", ".", ";", ":", "\"", "#", "\$", "%", "^",
                    "!", "@", "`", "~", "*", "-", "=", "+", "\\",
                    "|", "/", ">", "<", "(", ")", "&", "?", "¹", "\t",
                    "\r", "\n", "{", "}", "[", "]", "'", "“", "”", "•",
                    "не", "на", "то", "вы", "по", "бы", "потому", "но", "и", "в", "с", "из", "за", "из-за",
                    "как", "для", "что", "или", "это", "этих", "он", "она", "я", "ты", "вы",
                    "всех", "вас", "они", "оно", "еще", "когда", "а",
                    "где", "эта", "лишь", "уже", "вам", "нет",
                    "если", "надо", "все", "так", "его", "чем",
                    "при", "даже", "мне", "есть", "раз", "два",
                    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                );
                $words = explode(' ', trim($this->title));
                $words = array_diff($words, $del_symbols);
                $tmp = array();
                if (count($words) > 15) {
                    $keys = array_rand($words, 15);
                } else {
                    $keys = array_rand($words, count($words));
                }
                if (is_array($keys)) {
                    foreach ($keys as $key) {
                        $tmp[] = $words[$key];
                    }
                    $words = $tmp;
                } else {
                    $words = array($words[0]);
                }
                $this->metaKey = implode(',', $words);
                
                $uploadedFile = CUploadedFile::getInstance($this, 'image');
                if ($uploadedFile !== NULL) {
//                    if (!$this->isNewRecord) {
//                        if (file_exists("/uploads/article/" . $this->image)) {
//                            unlink("/uploads/article/" . $this->image);
//                        }
//                    }
                    $image = Yii::app()->simpleImage->load($uploadedFile->tempName);
//                    $image->resizeToWidth(410);
//                    $image->resizeToHeight(300);
                    $newFileName = uniqid() . '.' . pathinfo($uploadedFile->name, PATHINFO_EXTENSION);
                    $image->save(Yii::app()->params['uploadDir'] . 'question_img/' . $newFileName);
                    $this->image = $newFileName;
                }
            } else
                $this->update_time = time();
            return true;
        } else
            return false;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave() {
        parent::afterSave();
//		Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete() {
        parent::afterDelete();
        Answer::model()->deleteAll('post_id=' . $this->id);
//		Tag::model()->updateFrequency($this->tags, '');
    }

    /**
     * Retrieves the list of questions based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed questions.
     */
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('title', $this->title, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('author_id', $this->author_id);
        $criteria->compare('аrea_expertise_id', $this->аrea_expertise_id);
        $criteria->compare('image', $this->image, true);

        return new CActiveDataProvider('Question', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'status, update_time DESC',
            ),
        ));
    }

    /**
     * Возвращает вопросы пользователя.
     * @param int $uid Идентификатор пользователя
     * @param int $type Платные (1) или бесплатные (0)
     * @return array
     */
    public static function getUserQuestions($uid, $type) {
        $criteria = new CDbCriteria();
        $criteria->order = "create_time DESC";
        $criteria->with = array(
            'answerCount'
        );
        $criteria->condition = "author_id=" . $uid . " AND pay=" . $type;
        return Question::model()->findAll($criteria);
    }

    public function translit($text) {
        preg_match_all('/./u', $text, $text);
        $text = $text[0];
        $simplePairs = array('а' => 'a', 'л' => 'l', 'у' => 'u', 'б' => 'b', 'м' => 'm', 'т' => 't', 'в' => 'v', 'н' => 'n', 'ы' => 'y', 'г' => 'g', 'о' => 'o', 'ф' => 'f', 'д' => 'd', 'п' => 'p', 'и' => 'i', 'р' => 'r', 'А' => 'A', 'Л' => 'L', 'У' => 'U', 'Б' => 'B', 'М' => 'M', 'Т' => 'T', 'В' => 'V', 'Н' => 'N', 'Ы' => 'Y', 'Г' => 'G', 'О' => 'O', 'Ф' => 'F', 'Д' => 'D', 'П' => 'P', 'И' => 'I', 'Р' => 'R',);
        $complexPairs = array('з' => 'z', 'ц' => 'c', 'к' => 'k', 'ж' => 'zh', 'ч' => 'ch', 'х' => 'kh', 'е' => 'e', 'с' => 's', 'ё' => 'jo', 'э' => 'eh', 'ш' => 'sh', 'й' => 'jj', 'щ' => 'shh', 'ю' => 'ju', 'я' => 'ja', 'З' => 'Z', 'Ц' => 'C', 'К' => 'K', 'Ж' => 'ZH', 'Ч' => 'CH', 'Х' => 'KH', 'Е' => 'E', 'С' => 'S', 'Ё' => 'JO', 'Э' => 'EH', 'Ш' => 'SH', 'Й' => 'JJ', 'Щ' => 'SHH', 'Ю' => 'JU', 'Я' => 'JA', 'Ь' => "", 'Ъ' => "", 'ъ' => "", 'ь' => "",);
        $specialSymbols = array("–" => "-", "-" => "-", "'" => "", "`" => "", "^" => "", " " => "-", '.' => '', '?' => '', ',' => '', ':' => '', '"' => '', "'" => '', '<' => '', '>' => '', '«' => '', '»' => '', ' ' => '-',);
        $translitLatSymbols = array('a', 'l', 'u', 'b', 'm', 't', 'v', 'n', 'y', 'g', 'o', 'f', 'd', 'p', 'i', 'r', 'z', 'c', 'k', 'e', 's', 'A', 'L', 'U', 'B', 'M', 'T', 'V', 'N', 'Y', 'G', 'O', 'F', 'D', 'P', 'I', 'R', 'Z', 'C', 'K', 'E', 'S',);
        $simplePairsFlip = array_flip($simplePairs);
        $complexPairsFlip = array_flip($complexPairs);
        $specialSymbolsFlip = array_flip($specialSymbols);
        $charsToTranslit = array_merge(array_keys($simplePairs), array_keys($complexPairs));
        $translitTable = array();
        foreach ($simplePairs as $key => $val)
            $translitTable[$key] = $simplePairs[$key]; foreach ($complexPairs as $key => $val)
            $translitTable[$key] = $complexPairs[$key]; foreach ($specialSymbols as $key => $val)
            $translitTable[$key] = $specialSymbols[$key]; $result = "";
        $nonTranslitArea = false;
        foreach ($text as $char) {
            if (in_array($char, array_keys($specialSymbols))) {
                $result.= $translitTable[$char];
            } elseif (in_array($char, $charsToTranslit)) {
                if ($nonTranslitArea) {
                    $result.= "";
                    $nonTranslitArea = false;
                } $result.= $translitTable[$char];
            } else {
                if (!$nonTranslitArea && in_array($char, $translitLatSymbols)) {
                    $result.= "";
                    $nonTranslitArea = true;
                } $result.= $char;
            }
        } return strtolower(preg_replace("/[-]{2,}/", '-', $result));
    }

}
