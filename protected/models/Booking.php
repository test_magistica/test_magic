<?php

/**
 * This is the model class for table "booking".
 *
 * The followings are the available columns in table 'booking':
 * @property integer $id
 * @property integer $userID
 * @property integer $serviceID
 */
class Booking extends CActiveRecord {

    /**
     *  Performance of service declined by expert
     */
    const STATUS_NOT_ACCEPTED = 0;
    /**
     * Waiting for performance
     * Set by default when creates new booking of service
     */
    const STATUS_WAIT = 1;
    /**
     * Service is done by expert
     */
    const STATUS_DONE = 2;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Booking the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'booking';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('message', 'required'),
            array('userID, serviceID', 'default'),
            array('userID, serviceID', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, userID, serviceID', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
	        'service' => array(self::HAS_ONE, 'Service', array('id' => 'serviceID')),
	        'user' => array(self::HAS_ONE, 'Users', array('id' => 'userID')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'        => '№ заказа',
            'userID'    => 'Пользователь',
            'serviceID' => 'Услуга',
            'message'   => 'Сообщение',
            'createTime'=> 'Время создания',
            'status'    => 'Состояние',
            'type'      => 'Тип',
            'date_birth'=> 'Дата рождения',
            'time_birth'=> 'Время рождения',
            'what'      => 'На что хочу погадать',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->order = 'id desc';
        $criteria->compare('id', $this->id);
        $criteria->compare('expertID', Yii::app()->user->id);
        $criteria->compare('userID', $this->userID);
        $criteria->compare('serviceID', $this->serviceID);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
	        'pagination'=>array(
		        'pageSize'=>Yii::app()->params['bookingPerPage'],
	        ),
        ));
    }

}