<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $id
 * @property string $name
 * @property string $second_name
 * @property string $surname
 * @property string $email
 * @property string $pwd
 * @property string $tele
 * @property integer $sex
 * @property string $birthday
 * @property string $country
 * @property string $reg_date
 * @property string $role
 * @property string $bill
 * @property string $image
 * @property string $contact_email
 * @property string $skype
 * @property string $profession
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Comments[] $comments
 */
class Expert extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Expert the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, surname, email, pwd, tele, sex, birthday, country', 'required'),
            array('about, profession, tariff, mobile, mobileText, home, homeText, metaTitle, metaKey, metaDesc', 'default'),
            array('sex, status', 'numerical', 'integerOnly' => true),
            array('name, second_name, surname, email, tele, birthday, country, bill, image, contact_email, skype, profession', 'length', 'max' => 255),
            array('pwd', 'length', 'max' => 32),
            array('role', 'length', 'max' => 30),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, second_name, surname, email, pwd, tele, sex, birthday, country, reg_date, role, bill, image, contact_email, skype, profession, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'comments' => array(self::HAS_MANY, 'Comments', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Имя',
            'second_name' => 'Отчество',
            'surname' => 'Фамилия',
            'email' => 'Email',
            'pwd' => 'Пароль',
            'tele' => 'Телефон знак "+" ставить обязательно',
            'sex' => 'Пол',
            'birthday' => 'Дата рождения',
            'country' => 'Страна',
            'reg_date' => 'Reg Date',
            'role' => 'Role',
            'bill' => 'Баланс',
            'image' => 'Аватар',
            'contact_email' => 'контактный Email',
            'skype' => 'Skype',
            'profession' => 'Спецификация',
            'status' => 'Статус',
            'about' => 'Обо мне',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('role', 'expert',TRUE,'OR');
        $criteria->compare('role', 'user',TRUE,'OR');
        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('second_name', $this->second_name, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('pwd', $this->pwd, true);
        $criteria->compare('tele', $this->tele, true);
        $criteria->compare('sex', $this->sex);
        $criteria->compare('birthday', $this->birthday, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('reg_date', $this->reg_date, true);
        $criteria->compare('role', $this->role, true);
        $criteria->compare('bill', $this->bill, true);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('contact_email', $this->contact_email, true);
        $criteria->compare('skype', $this->skype, true);
        $criteria->compare('profession', $this->profession, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave() {
        if ($this->isNewRecord) {
            $this->pwd = md5($this->pwd);
            $this->reg_date = date('Y-m-d H:i:s');
        }
        $this->role = 'expert';
        $this->online = 'autonomous';
        return parent::beforeSave();
    }

}
