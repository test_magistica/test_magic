<?php

/**
 * This is the model class for table "faq".
 *
 * The followings are the available columns in table 'faq':
 * @property integer $id
 * @property string $fquestion
 * @property string $fanswer
 * @property integer $fgroup
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property FaqGroups $fgroup0
 */
class Faq extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'faq';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('fquestion, fanswer, fgroup', 'required'),
            array('status', 'numerical', 'integerOnly' => true),
            array('fgroup', 'numerical', 'integerOnly' => true, 'min' => 1, 'message' => 'Выберите другую категорию.'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, fquestion, fanswer, fgroup, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'fgroup0' => array(self::BELONGS_TO, 'FaqGroups', 'fgroup'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'fquestion' => 'Частый вопрос',
            'fanswer' => 'Ответ на вопрос',
            'fgroup' => 'Группа вопросов.',
            'status' => 'Показывать:',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('fquestion', $this->fquestion, true);
        $criteria->compare('fanswer', $this->fanswer, true);
        $criteria->compare('fgroup', $this->fgroup);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Faq the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getDropdownItems($parentId = 0, $level = 0) {
        $itemsFormatted = array();

        $items = FaqGroups::model()->findAllByAttributes(array(
            'parent_group' => $parentId,
        ));
        foreach ($items as $item) {
            $itemsFormatted[$item->id] = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $level) . $item->gname;
            $itemsFormatted = $itemsFormatted + $this->getDropdownItems($item->id, $level + 1);
        }

        return $itemsFormatted;
    }

}
