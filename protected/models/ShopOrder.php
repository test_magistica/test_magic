<?php

/**
 * This is the model class for table "ShopOrder".
 *
 * The followings are the available columns in table 'ShopOrder':
 * @property integer $id
 * @property string $amount
 * @property string $description
 * @property integer $userID
 */
class ShopOrder extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ShopOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ShopOrder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('amount, entity, userID', 'required'),
			array('userID', 'numerical', 'integerOnly'=>true),
			array('amount, entity', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, amount, entity, entity_id, userID', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'amount' => 'Amount',
			'entity' => 'entity',
			'entity_id' => 'entity_id',
			'userID' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('entity',$this->entity,true);
		$criteria->compare('entity_id',$this->entity,true);
		$criteria->compare('userID',$this->userID);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function sendMailSuccessPaid($entity, $entity_id) {
		if($entity != 'bill'){

			$mod = ucfirst($entity);
			if ($model = $mod::model()->findByPk($entity_id)) {

				$serviceParams = Yii::app()->params['paymentServices'];
				$serviceParams = $serviceParams[$entity];

				if($entity != 'consultation'){
					$booking = new Booking;
					$message = new Servicemessage();

					$booking->userID    = $model->userID;
					$booking->expertID  = $model->expertID;
					$booking->serviceID = 35;
					$booking->entity    = $entity;
					$booking->entity_id = $entity_id;
					$booking->type      = 'Статическая услуга';
					$booking->createTime = new CDbExpression('NOW()');

					if ($booking->save(false)) {
						$message->message   = $model->question;
						$message->fromID    = $booking->userID;
						$message->toID      = $booking->expertID;
						$message->userID    = $booking->userID;
						$message->createTime = new CDbExpression('NOW()');
						$message->status    = 0;
						$message->bookingID = $booking->id;
						$message->save(false);
					}
				}


				$img_name = trim($model->image) != '' ? $model->image : 'nofoto.gif';
				$userName = $userSurname = '';
				$expImg = Controller::getUserImage($model->expertID);
				if($entity == 'photoGuess'){
					$userName = $model->username;
				}
				else{
					if($model->guest_name)
						$userName = $model->guest_name;
					else{
						$user = Users::model()->findByPk($model->userID);
						$userName = $user->name . " " . $user->surname;
						$userSurname = $user->surname;
					}
				}

				// доработать
				$service = Service::model()->findByPk(35);

				$params = array(
					'userName'          => $userName,
					'service'           => $service->notes,
					'expertName'        => Controller::getUser($model->expertID, 'name'),
					'expertSurname'     => Controller::getUser($model->expertID, 'surname'),
					'expertProfession'  => Controller::getUser($model->expertID, 'profession'),
					'expertHref'        => "http://{$_SERVER['HTTP_HOST']}/extrasens/view/{$model->expertID}",
					'expertImage'       => "http://{$_SERVER['HTTP_HOST']}/uploads/user/{$expImg}",
					'orderNum'          => $model->id,
					'countMin'          => isset($model->duration) ? $model->duration / 60 : '',
					'reservedDateTime'  => isset($model->date) ? $model->date : '',
					'contactPhone'      => isset($model->phone) ? $model->phone : '',
					'imgSrc'           => "http://{$_SERVER['HTTP_HOST']}/uploads/consultation/{$img_name}",
					'reservedDateTime'  => isset($model->date) ? date('d.m.Y h:i', strtotime($model->date)) :'',
					'userSurname'       => isset($userSurname) ? $userSurname : '',
				);

				$tp = new TemplateParams();
				$tp->setBodyParams($params);
				$tp->setSubjectParams($params);
				$mt = EmailTemplates::parseTemplate('haveCreatedService', $tp);
				$ptxt = ($entity == 'consultation') ? 'звонка' : 'услуги';
				Controller::sendMailUser(Yii::app()->params['mainAdminEmail'], "Проконтролируйте выполнение {$ptxt}", $mt->parsedBody);



				$tp = new TemplateParams();
				$tp->setBodyParams($params);
				$tp->setSubjectParams($params);
				$mt = EmailTemplates::parseTemplate($serviceParams['keyPayment'], $tp);
				Controller::sendMailUser($model->email, $mt->parsedSubject, $mt->parsedBody);
			}
		}
	}
}