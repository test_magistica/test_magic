<?php

/**
 * This is the model class for table "payments".
 *
 * The followings are the available columns in table 'payments':
 * @property integer $id
 * @property integer $InvId
 * @property string $OutSum
 * @property string $SignatureValue
 * @property string $Culture
 * @property string $shpItem
 * @property string $status expectations - при направке,
 */
class Payments extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Payments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('InvId, OutSum, SignatureValue, Culture, shpItem, status', 'required'),
			array('InvId', 'numerical', 'integerOnly'=>true),
			array('OutSum, SignatureValue, Culture, shpItem', 'length', 'max'=>255),
			array('status', 'length', 'max'=>110),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, InvId, OutSum, SignatureValue, Culture, shpItem, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'InvId' => 'Inv',
			'OutSum' => 'Out Sum',
			'SignatureValue' => 'Signature Value',
			'Culture' => 'Culture',
			'shpItem' => 'Shp Item',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('InvId',$this->InvId);
		$criteria->compare('OutSum',$this->OutSum,true);
		$criteria->compare('SignatureValue',$this->SignatureValue,true);
		$criteria->compare('Culture',$this->Culture,true);
		$criteria->compare('shpItem',$this->shpItem,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}