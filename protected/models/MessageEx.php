<?php

/**
 * This is the model class for table "message".
 *
 * The followings are the available columns in table 'message':
 * @property integer $id
 * @property integer $fromID
 * @property integer $toID
 * @property string $topic
 * @property string $message
 * @property double $status
 */
class MessageEx extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Message the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'message';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('message', 'required'),
            array('topic', 'default'),
            array('fromID, toID', 'numerical', 'integerOnly' => true),
            array('status', 'numerical'),
            array('topic', 'length', 'max' => 255),            
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, fromID, toID, topic, message, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'fromID' => 'Пользователь',
            'toID' => 'Эксперт',
            'topic' => 'Тема',
            'message' => 'Текс собшения',
            'status' => 'Статус',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->group = 'fromID';
        $criteria->compare('toID', Yii::app()->user->id);
        $criteria->compare('id', $this->id);
        $criteria->compare('fromID', $this->fromID);
        $criteria->compare('toID', $this->toID);
        $criteria->compare('topic', $this->topic, true);
        $criteria->compare('message', $this->message, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave() {
        $this->toID = Yii::app()->user->id;
        $this->userID = Yii::app()->user->id;
        $this->fromID = (int) $_GET['id'];
        $this->status = 0;
        $this->createTime = new CDbExpression('NOW()');
        $this->message = str_replace("\r\n",'<br>', $this->message);
        //$this->message = str_replace(" ",'&nbsp;', $this->message);
        return parent::beforeSave();
    }


}
