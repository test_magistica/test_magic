<?php

/**
 * This is the model class for table "photo_guess".
 *
 * The followings are the available columns in table 'photo_guess':
 * @property integer $id
 * @property integer $userID
 * @property integer $expertID
 * @property string $image
 * @property string $date_birth
 * @property string $time_birth
 * @property string $city_birth
 * @property string $status
 * @property string $question
 * @property double $cost
 * @property string $email
 * @property string $name
 * @property string $created
 * @property integer $remind
 */
class PhotoGuess extends CActiveRecord
{
	public $therms='';
	public $forget='';
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PhotoGuess the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'photo_guess';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('expertID, image, date_birth, city_birth, email, question, what, username, cost', 'required'),
			array('cost, what', 'numerical'),
			array('image', 'length', 'max'=>50),
			array('date_birth', 'length', 'max'=>10),
			array('date_birth','dateValidate'),
			array('therms','thermsValidate'),
			array('time_birth', 'length', 'max'=>5),
			array('time_birth','timeValidate'),
			array('question', 'length', 'max'=>2000),
			array('city_birth, username', 'length', 'max'=>64),
			array('status', 'length', 'max'=>32),
			array('email', 'length', 'max'=>70),
			array('email','email'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, userID, expertID, image, date_birth, city_birth, question, cost, email, username', 'safe', 'on'=>'search'),
		);
	}

	public function dateValidate() {
		if (! empty ($this->date_birth)) {
			$date =explode('-', $this->date_birth);

		$check = checkdate((int)$date[1],(int)$date[2],(int)$date[0]);
		if (! $check )
			$this->addError('date_birth','Дата рождения указана некорректно.');
		}
	}

	public function timeValidate() {
		if (! empty ($this->time_birth)) {
			$time = preg_match("/^([0-9]{2}):([0-9]{2})$/", $this->time_birth);

			if (!$time)
				$this->addError('time_birth','Время рождения указано некорректно.');
		}
		else
			$this->addError('time_birth','Укажите Время рождения.');
	}

	public function thermsValidate() {
		if ($this->therms != 1) {
			$this->addError('therms','Необходимо подтвердить условия предоставления сервиса.');
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'        => 'ID',
			'userID'    => 'User',
			'expertID'  => 'Expert',
			'image'     => 'Прикрепить фотографию',
			'date_birth' => 'Дата рождения',
			'time_birth' => 'Время рождения',
			'city_birth' => 'Город рождения',
			'status'    => 'Status',
			'question'  => 'Вопрос',
			'cost'      => 'Cost',
			'email'     => 'Электронная почта',
			'username'  => 'Ваше имя',
			'what'      => 'На что хотите погадать',
			'created'   => 'Created',
			'remind'    => 'Remind',
			'therms'    => 'Пользовательское соглашение',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('userID',$this->userID);
		$criteria->compare('expertID',$this->expertID);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('date_birth',$this->date_birth,true);
		$criteria->compare('time_birth',$this->time_birth,true);
		$criteria->compare('city_birth',$this->city_birth,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('question',$this->question,true);
		$criteria->compare('cost',$this->cost);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('remind',$this->remind);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	protected function beforeSave() {
		$this->question = str_replace("\r\n",'<br>', $this->question);
		//$this->message = str_replace(" ",'&nbsp;', $this->message);
		return parent::beforeSave();
	}
}
