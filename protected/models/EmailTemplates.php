<?php

/**
 * This is the model class for table "email_templates".
 *
 * The followings are the available columns in table 'email_templates':
 * @property integer $id
 * @property string $name
 * @property string $template
 * @property string $subject
 * @property string $code
 */
class EmailTemplates extends CActiveRecord
{
	public $parsedSubject;
	public $parsedBody;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'email_templates';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, template, subject, code', 'required'),
			array('name, code', 'length', 'max'=>128),
			array('subject', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, template, subject, code', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'        => 'ID',
			'name'      => 'Заголовок',
			'template'  => 'Тело письма',
			'subject'   => 'Тема письма',
			'code'      => 'Ключ',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('template',$this->template,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('code',$this->code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EmailTemplates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function parseTemplate($code, $templateParams)
	{
		if (!($templateParams instanceof TemplateParams)) return array();

		$pModel = EmailTemplates::model()->find('code=:code', array('code'=>$code));

		$pModel->parsedSubject = $pModel->subject;
			foreach ($templateParams->getSubjectParams() as $paramKey => $paramName)
				$pModel->parsedSubject = str_replace('{' . $paramKey . '}', $paramName, $pModel->parsedSubject);

		$pModel->parsedBody = $pModel->template;
			foreach ($templateParams->getBodyParams() as $paramKey => $paramName)
				$pModel->parsedBody = str_replace('{' . $paramKey . '}', $paramName, $pModel->parsedBody);

		return $pModel;
	}
}
