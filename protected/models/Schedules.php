<?php

/**
 * This is the model class for table "schedules".
 *
 * The followings are the available columns in table 'schedules':
 * @property string $id
 * @property string $expert_id
 * @property string $mon
 * @property string $tues
 * @property string $wednes
 * @property string $thurs
 * @property string $fri
 * @property string $satur
 * @property string $sun
 */
class Schedules extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Schedules the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'schedules';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('mon, tues, wednes, thurs, fri, satur, sun', 'required'),
            array('mon, tues, wednes, thurs, fri, satur, sun', 'match', 'pattern' => '/[0-9-]{2}:[0-9-]{2} - [0-9-]{2}:[0-9-]{2}/'),
            //array('mon, tues, wednes, thurs, fri, satur, sun', 'date', 'format'=>'HH:mm', 'allowEmpty'=>false),
            array('expert_id', 'length', 'max' => 11),
            array('mon, tues, wednes, thurs, fri, satur, sun', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, expert_id, mon, tues, wednes, thurs, fri, satur, sun', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'expert_id' => 'Expert',
            'mon' => 'Пн (чч:мм - чч:мм)',
            'tues' => 'Вт',
            'wednes' => 'Ср',
            'thurs' => 'Чт',
            'fri' => 'Пт',
            'satur' => 'Сб',
            'sun' => 'Вс',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        //$criteria->compare('expert_id', Yii::app()->user->id, true);
        $criteria->compare('expert_id', $this->expert_id, true);
        $criteria->compare('mon', $this->mon, true);
        $criteria->compare('tues', $this->tues, true);
        $criteria->compare('wednes', $this->wednes, true);
        $criteria->compare('thurs', $this->thurs, true);
        $criteria->compare('fri', $this->fri, true);
        $criteria->compare('satur', $this->satur, true);
        $criteria->compare('sun', $this->sun, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave() {
        $this->expert_id = Yii::app()->user->id;
        return parent::beforeSave();
    }

}
