<?php

/**
 * This is the model class for table "article".
 *
 * The followings are the available columns in table 'article':
 * @property integer $id
 * @property string $title
 * @property string $anons
 * @property string $description
 * @property string $metaKey
 * @property string $metaDesc
 * @property string $createTime
 * @property string $url
 */
class Article extends CActiveRecord {
    
    const STATUS_ONLINE = 1;
    const STATUS_OFFLINE = 0;
    const PATH_TO_IMAGE = "uploads/article/";

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Article the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'article';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, anons, description, createTime', 'required'),
            array('image', 'file', 'allowEmpty' => true, 'types' => 'jpg,png,bmp,jpeg'),
            array('metaKey, metaDesc, url, author', 'default'),
            array('title, url', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, anons, description, metaKey, metaDesc, createTime, url, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'authort' => array(self::HAS_ONE, "Users", array('id' => 'author')),
        );
    }
        
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Название',
            'anons' => 'Анонс',
            'description' => 'Описание',
            'metaKey' => 'Мета тег - Ключевые слова',
            'metaDesc' => 'Мета тег - Описание',
            'createTime' => 'Время создания',
            'url' => 'Url',
            'image' => 'Рисунок 165px X 125px'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('anons', $this->anons, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('metaKey', $this->metaKey, true);
        $criteria->compare('metaDesc', $this->metaDesc, true);
        $criteria->compare('createTime', $this->createTime, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('status', $this->status, true);
        if (Yii::app()->user->role == 'expert')
            $criteria->compare('author', Yii::app()->user->id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    function translit($text) {
        preg_match_all('/./u', $text, $text);
        $text = $text[0];
        $simplePairs = array('а' => 'a', 'л' => 'l', 'у' => 'u', 'б' => 'b', 'м' => 'm', 'т' => 't', 'в' => 'v', 'н' => 'n', 'ы' => 'y', 'г' => 'g', 'о' => 'o', 'ф' => 'f', 'д' => 'd', 'п' => 'p', 'и' => 'i', 'р' => 'r', 'А' => 'A', 'Л' => 'L', 'У' => 'U', 'Б' => 'B', 'М' => 'M', 'Т' => 'T', 'В' => 'V', 'Н' => 'N', 'Ы' => 'Y', 'Г' => 'G', 'О' => 'O', 'Ф' => 'F', 'Д' => 'D', 'П' => 'P', 'И' => 'I', 'Р' => 'R',);
        $complexPairs = array('з' => 'z', 'ц' => 'c', 'к' => 'k', 'ж' => 'zh', 'ч' => 'ch', 'х' => 'kh', 'е' => 'e', 'с' => 's', 'ё' => 'jo', 'э' => 'eh', 'ш' => 'sh', 'й' => 'jj', 'щ' => 'shh', 'ю' => 'ju', 'я' => 'ja', 'З' => 'Z', 'Ц' => 'C', 'К' => 'K', 'Ж' => 'ZH', 'Ч' => 'CH', 'Х' => 'KH', 'Е' => 'E', 'С' => 'S', 'Ё' => 'JO', 'Э' => 'EH', 'Ш' => 'SH', 'Й' => 'JJ', 'Щ' => 'SHH', 'Ю' => 'JU', 'Я' => 'JA', 'Ь' => "", 'Ъ' => "", 'ъ' => "", 'ь' => "",);
        $specialSymbols = array("–" => "-", "-" => "-", "'" => "", "`" => "", "^" => "", " " => "-", '.' => '', '?'=>'', ',' => '', ':' => '', '"' => '', "'" => '', '<' => '', '>' => '', '«' => '', '»' => '', ' ' => '-',);
        $translitLatSymbols = array('a', 'l', 'u', 'b', 'm', 't', 'v', 'n', 'y', 'g', 'o', 'f', 'd', 'p', 'i', 'r', 'z', 'c', 'k', 'e', 's', 'A', 'L', 'U', 'B', 'M', 'T', 'V', 'N', 'Y', 'G', 'O', 'F', 'D', 'P', 'I', 'R', 'Z', 'C', 'K', 'E', 'S',);
        $simplePairsFlip = array_flip($simplePairs);
        $complexPairsFlip = array_flip($complexPairs);
        $specialSymbolsFlip = array_flip($specialSymbols);
        $charsToTranslit = array_merge(array_keys($simplePairs), array_keys($complexPairs));
        $translitTable = array();
        foreach ($simplePairs as $key => $val)
            $translitTable[$key] = $simplePairs[$key]; foreach ($complexPairs as $key => $val)
            $translitTable[$key] = $complexPairs[$key]; foreach ($specialSymbols as $key => $val)
            $translitTable[$key] = $specialSymbols[$key]; $result = "";
        $nonTranslitArea = false;
        foreach ($text as $char) {
            if (in_array($char, array_keys($specialSymbols))) {
                $result.= $translitTable[$char];
            } elseif (in_array($char, $charsToTranslit)) {
                if ($nonTranslitArea) {
                    $result.= "";
                    $nonTranslitArea = false;
                } $result.= $translitTable[$char];
            } else {
                if (!$nonTranslitArea && in_array($char, $translitLatSymbols)) {
                    $result.= "";
                    $nonTranslitArea = true;
                } $result.= $char;
            }
        } return strtolower(preg_replace("/[-]{2,}/", '-', $result));
    }

    public function beforeSave() {
        $this->url = $this->translit($this->title);
        if($this->isNewRecord){
            $this->author = Yii::app()->user->id;
        }
        if (Yii::app()->user->role == 'administrator')
            $this->status = 1;

        $uploadedFile = CUploadedFile::getInstance($this, 'image');
        if ($uploadedFile !== NULL) {
            if (!$this->isNewRecord) {
                if (file_exists(DIRECTORY_SEPARATOR.self::PATH_TO_IMAGE . $this->image)) {
                    unlink(DIRECTORY_SEPARATOR.self::PATH_TO_IMAGE . $this->image);
                }
            }
            $image = Yii::app()->simpleImage->load($uploadedFile->tempName);
            $image->resizeToWidth(205);
            $image->resizeToHeight(151);
            $newFileName = uniqid() . '.' . pathinfo($uploadedFile->name, PATHINFO_EXTENSION);
            $image->save(self::PATH_TO_IMAGE . $newFileName);
            $this->image = $newFileName;
        }

        return parent::beforeSave();
    }

    public static function newArticles($expertId = 0, $count = 5, $exqlude = array()){
        $criteria = new CDbCriteria();
        $criteria->limit = $count;
        $criteria->with = "authort";
        $exq = implode(",", $exqlude);
        if(!empty($exq)){
            $exq = " AND t.id not in (".$exq.")";
        }
        if($expertId > 0){
            $criteria->condition = "t.status=".Article::STATUS_ONLINE." AND authort.id=".$expertId.$exq;
        }else{
            $criteria->condition = "t.status=".Article::STATUS_ONLINE.$exq;
        }
        $criteria->order = "createTime DESC";
        $articles = Article::model()->findAll($criteria);
        $needAdd = $count - count($articles);
        if($expertId > 0 && count($articles) < $count){
            // ID всех статей
            $ids = Yii::app()->db->createCommand("select id from article where author<>".$expertId)->queryAll();
            
            $keys = array_rand($ids, $needAdd);
            $tmp = array();
            if(is_array($keys)){
                foreach ($keys as $key){
                  $tmp[] = $ids[$key]["id"];
                }
            }else if(is_int($keys)){
                $tmp[] = $ids[$keys]["id"];
            }
            $criteria = new CDbCriteria();
            $criteria->with = "authort";
            $criteria->limit = $count - count($articles);
            $criteria->condition = "t.status=".Article::STATUS_ONLINE." AND t.id IN (".  implode(",", $tmp).")";
            $addArticles = Article::model()->findAll($criteria);
            $articles = array_merge($articles, $addArticles);
        }
        return $articles;
    }
    
    protected function afterDelete() {
        parent::afterDelete();
        if (file_exists(DIRECTORY_SEPARATOR.self::PATH_TO_IMAGE . $this->image))
            unlink(self::PATH_TO_IMAGE . $this->image);
    }

}