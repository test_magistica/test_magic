<?php

/**
 * This is the model class for table "guest".
 */
class Guest extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Payments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'guest';
	}
	
	public function beforeSave() {
	
	$criter = new CDbCriteria;
	$criter->compare('id_guest',$this->id_guest);
        if (Guest::model()->count($criter)>0)
    	    $this->id_guest = 'error';
        return parent::beforeSave();
	}
}