<?php

/**
 * This is the model class for table "payments_log".
 *
 * The followings are the available columns in table 'payments':
 */
class Payments_log extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Payments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payments_log';
	}

}