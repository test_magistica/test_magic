<?php

/**
 * This is the model class for table "call_back".
 *
 * The followings are the available columns in table 'call_back':
 * @property string $id
 * @property string $user_id
 * @property string $cbk_id
 * @property string $from_num
 * @property string $to_num
 * @property string $maxdur
 * @property string $date
 * @property string $check
 */
class CallBack extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CallBack the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'call_back';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	/*public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, cbk_id, from_num, to_num, maxdur, date', 'required'),
			array('user_id, maxdur', 'length', 'max'=>11),
			array('cbk_id, from_num, to_num', 'length', 'max'=>255),
			array('check', 'length', 'max'=>1),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, cbk_id, from_num, to_num, maxdur, date, check', 'safe', 'on'=>'search'),
		);
	}*/

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	/*public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'cbk_id' => 'Cbk',
			'from_num' => 'From Num',
			'to_num' => 'To Num',
			'maxdur' => 'Maxdur',
			'date' => 'Date',
			'check' => 'Check',
		);
	}*/

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	/*public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('cbk_id',$this->cbk_id,true);
		$criteria->compare('from_num',$this->from_num,true);
		$criteria->compare('to_num',$this->to_num,true);
		$criteria->compare('maxdur',$this->maxdur,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('check',$this->check,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}*/
	/*public function beforeSave() 
	{
	    $expert = Users::model()->findByPk($this->user_id);
	    if ($expert->sip_tel == 'sip')
	        if (!empty($expert->sip))
	    	    $this->from_num = $expert->sip;
		else 
		    $this->from_num = $expert->tele;
	    else 
		$this->from_num = $expert->tele; 
    	    return parent::beforeSave();
	}*/
}