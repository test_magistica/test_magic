<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $id
 * @property string $name
 * @property string $second_name
 * @property string $surname
 * @property string $email
 * @property string $pwd
 * @property string $tele
 * @property integer $sex
 * @property string $birthday
 * @property string $country
 * @property string $reg_date
 * @property integer $role
 * @property string $bill
 * @property string $image
 * @property string $contact_email
 * @property string $skype
 * @property string $profession
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Comments[] $comments
 */
class Users extends CActiveRecord {

    const ROLE_ADMIN = 'administrator';
    const ROLE_EXPERT = 'expert';
    const ROLE_USER = 'user';
    const ROLE_BANNED = 'banned';

    public $month;
    public $day;
    public $confirmPassword;
    public $verifyCode;
    public $rul;
    public $title;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, surname, email, pwd, confirmPassword, tele, sex, birthday, month, day, country', 'required', 'on' => 'create'),
            array('name, surname, sex, birthday, month, day, country', 'required', 'on' => 'update'),
            array('confirmPassword', 'compare', 'compareAttribute' => 'pwd', 'on' => 'create'),
            array('tele', 'phoneValidate'),
            array('email', 'unique', 'on' => 'create'),
            array('tele', 'unique', 'on' => 'create'),
            array('about, profession, tariff, sip, sip_tel, mobile, mobileText, home, homeText', 'default'),
            array('email', 'email'),
            array('rul', 'required', 'message' => 'Надо принять правила сайта и публичной оферты', 'on' => 'create'),
            //  array('sex, role, status', 'numerical', 'integerOnly' => true),
            array('name, second_name, surname, email, tele, birthday, country, bill, image, contact_email, skype, profession', 'length', 'max' => 255),
            array('pwd', 'length', 'min' => 6, 'max' => 32),
            //array('name, surname, email, pwd, confirmPassword, tele, sex, birthday, month, day, country', 'filter', 'filter' => array('CHtml', 'encode')),
            array('name, second_name, surname, email, pwd, tele, sex, birthday, country, reg_date, role, bill, image, contact_email, skype, profession, status, about', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify')),
            //array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements(), 'on' => 'create'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, second_name, surname, email, pwd, tele, sex, birthday, country, reg_date, role, bill, image, contact_email, skype, profession, status', 'safe', 'on' => 'search'),
        );
    }

    public function phoneValidate() {
        if (!preg_match("/^\+[0-9]{1,}$/", $this->tele)) {
            $this->addError('tele', 'Необходимо заполнить поле Номер телефона.');
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'comments' => array(self::HAS_MANY, 'Comments', 'user_id'),
	        'booking' => array(self::BELONGS_TO, 'Booking', 'id'),
            'answers' => array(self::HAS_MANY, 'Answer', 'author'),
            'countVotes' => array(self::STAT, 'Answer', 'author', 'condition' => 'status='.Answer::STATUS_APPROVED.' and is_voted<>0'),
            'countQuestions' => array(self::STAT, 'Question', 'author_id', 'condition' => 'status='.Question::STATUS_PUBLISHED),
//            'profilePhoto' => array(self::HAS_ONE, 'Userphoto', 'userID', 'condition' => 'main=1'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'rul' => 'rul',
            'name' => 'Имя',
            'month' => 'Месяц',
            'day' => 'День',
            'confirmPassword' => 'Подтверждение пароля',
            'second_name' => 'Second Name',
            'surname' => 'Фамилия',
            'email' => 'E-mail',
            'pwd' => 'Пароль',
            'tele' => 'Номер телефона',
            'sex' => 'Пол',
            'birthday' => 'Год',
            'country' => 'Страна',
            'reg_date' => 'Reg Date',
            'role' => 'Role',
            'bill' => 'Bill',
            'image' => 'Image',
            'contact_email' => 'Contact Email',
            'skype' => 'Skype',
            'profession' => 'Profession',
            'status' => 'Status',
            'about' => 'О себе',
            'mobile' => 'Мобильный',
            'mobileText' => 'Мобильный нижный текст',
            'home' => 'Домашный',
            'homeText' => 'Домашный нижный текст',
            'title' => 'Заголовок',
            'chat_available' => 'Онлайн чат',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('second_name', $this->second_name, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('pwd', $this->pwd, true);
        $criteria->compare('tele', $this->tele, true);
        $criteria->compare('sex', $this->sex);
        $criteria->compare('birthday', $this->birthday, true);
        $criteria->compare('country', $this->country, true);
        $criteria->compare('reg_date', $this->reg_date, true);
        $criteria->compare('role', $this->role);
        $criteria->compare('bill', $this->bill, true);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('contact_email', $this->contact_email, true);
        $criteria->compare('skype', $this->skype, true);
        $criteria->compare('profession', $this->profession, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function bestExperts($idArea = 0) {
        $criteria = new CDbCriteria();
        $criteria->limit = 5;
//        $idArea = 2;
        $bestExperts = array();
        if ($idArea == 0) {
            // Упорядывачиваем всех подряд
            $criteria->order = "expert_raiting DESC";
            $criteria->condition = "role='".Users::ROLE_EXPERT."' AND status=1";
            $bestExperts = Users::model()->findAll($criteria);
        } else {
            // Упорядывачиваем експертов в указанной области
//            $criteria->select = "author.id";
//            $criteria->order = "author.expert_raiting DESC";
//            $criteria->with = 'answers';
//            $criteria->condition = "t.area_expertise_id=".$idArea
//                    ." AND t.status=1";
//            $a = Answer::model()->findAll($criteria);
            
            $data = Question::model()->with(
                    array(
                        'answers' => array(
                            'condition' => "answers.status=".Answer::STATUS_APPROVED,
                        ), 
                        'answers.a_author' => array(
                            'order' => "a_author.expert_raiting DESC",
                            'condition' => "a_author.status = 1",
                            'group' => "a_author.id",
                            'item' => 'id',
                            'limit' => 5,
                            )
                        ))->findAll("t.area_expertise_id=".$idArea." AND t.status=".Question::STATUS_PUBLISHED." AND pay=0");
//            var_dump(count($data));die();
            $expIds = array();
            foreach ($data as $question) {
                foreach ($question->answers as $answer) {
                   $expIds[] = $answer->a_author->id;
                }
            }
                if(count($expIds) > 0){
                $criteria = new CDbCriteria();
                $criteria->with = array('countVotes');
                $criteria->order = "expert_raiting DESC";
                $criteria->condition = "id IN (".implode(",", array_unique($expIds)).")";
                $criteria->limit = 5;
                $bestExperts = Users::model()->findAll($criteria);
//                var_dump(array_unique($expIds));
//                var_dump(count($bestExperts));
//                die();
            }
        }

        return $bestExperts;
    }
   

    public function beforeSave() {
        if ($this->isNewRecord) {
            $userIp = $_SERVER["REMOTE_ADDR"];
            $alredyReg = Yii::app()->db->createCommand("select count(1) from `users` where remoteAddr='".$userIp."'")->queryScalar();
            if($alredyReg > 0){
                $msgNotif = new MessageNotificationComponent();
                $msgNotif->addMessage(__CLASS__.__LINE__, "Повторная регистрация не возможна, воспользуйтесь платными услугами");
//                Yii::app()->user->setFlash("error", "Повторная регистрация не возможна, воспользуйтесь платными услугами");
                Yii::app()->controller->redirect("/site/login");
                        
            }
            $this->free_chat_minutes = isset(Yii::app()->params['freeChatMinutes']) ? (int)  Yii::app()->params['freeChatMinutes'] : 0;
            $this->tele = $this->tele;
            $this->pwd = md5($this->pwd);
            $this->reg_date = date('Y-m-d H:i:s');
            $this->remoteAddr = $userIp;
        };
        //$this->tele = str_replace($this->country, '', $this->tele);
        //$this->tele = $this->country.$this->tele;
        if (!empty($this->day) && !empty($this->month)) {
            $this->birthday = $this->day . '.' . $this->month . '.' . $this->birthday;
        }
        return parent::beforeSave();
    }

}
