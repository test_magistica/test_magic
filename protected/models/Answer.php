<?php

class Answer extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tbl_answer':
	 * @var integer $id
	 * @var string $content
	 * @var integer $status
	 * @var integer $create_time
	 * @var string $author
	 * @var string $email
	 * @var string $url
	 * @var integer $question_id
	 */
    
        // Не рассмотренные я  так понимаю что ответ должен модерироваться
	const STATUS_PENDING=1;
        
        // Утвержден ну и может быть показан на сайте и т.д.
	const STATUS_APPROVED=2;

        const DATE_TIME_FORMAT = "Y-m-d H:i:s";

        /**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'answer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content, author, email', 'required'),
			array('email, url', 'length', 'max'=>128),
                        array('is_voted', 'length', 'max' =>1),
                        array('is_voted, author', 'numerical', 'integerOnly'=>true),
			array('email','email'),
			array('url','url'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'question' => array(self::BELONGS_TO, 'Question', array('post_id' => 'id')),
                    'a_author' => array(self::BELONGS_TO, 'Users', array('author' => 'id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'content' => 'Answer',
			'status' => 'Status',
			'create_time' => 'Create Time',
			'author' => 'Name',
			'email' => 'Email',
			'url' => 'Website',
			'question_id' => 'Question',
		);
	}

	/**
	 * Approves a answer.
	 */
	public function approve()
	{
		$this->status=Answer::STATUS_APPROVED;
		$this->update(array('status'));
	}

	/**
	 * @param Question the question that this answer belongs to. If null, the method
	 * will query for the question.
	 * @return string the permalink URL for this answer
	 */
	public function getUrl($question=null)
	{
		if($question===null)
			$question=$this->question;
		return $question->url.'#c'.$this->id;
	}

	/**
	 * @return string the hyperlink display for the current answer's author
	 */
	public function getAuthorLink()
	{
		if(!empty($this->url))
			return CHtml::link(CHtml::encode($this->author),$this->url);
		else
			return CHtml::encode($this->author);
	}

	/**
	 * @return integer the number of answers that are pending approval
	 */
	public function getPendingAnswerCount()
	{
		return $this->count('status='.self::STATUS_PENDING);
	}

	/**
	 * @param integer the maximum number of answers that should be returned
	 * @return array the most recently added answers
	 */
	public function findRecentAnswers($limit=10)
	{
		return $this->with('question')->findAll(array(
			'condition'=>'t.status='.self::STATUS_APPROVED,
			'order'=>'t.create_time DESC',
			'limit'=>$limit,
		));
	}

        public static function newAnswers($id = 0, $limit = 5){
            $criteria = new CDbCriteria();
            $criteria->limit = $limit;
            $freshAnswers = array();
            if($id == 0){
                // Выбираем свежие ответы подряд
                $criteria->order = "t.create_time DESC";
                $criteria->with = array("question");
                $criteria->condition = "t.status=".Answer::STATUS_APPROVED." and pay=0";
                $freshAnswers = Answer::model()->findAll($criteria);
            }
            else {
                $criteria->order = "t.create_time DESC";
                $criteria->with = array("question");
                $criteria->condition = "t.status=".Answer::STATUS_APPROVED." AND post_id<>".$id." and pay=0";
                $freshAnswers = Answer::model()->findAll($criteria);
                // Выбираем свежие ответы из указанной ареи
            }
            return $freshAnswers;
        }

                /**
         * Return last 5 (by default) answers of expert which ID will pass as a parameter.
         * @param int $expertId ID of expert
         * @param int $limit Count of answers need to return
         * @return array
         * @throws CException
         */
        public static function lastExpertAnswers($expertId, $limit = 5){
            if(isset($expertId) && is_numeric($expertId)){
                $criteria = new CDbCriteria();
                $criteria->limit = $limit;
                $criteria->order = "t.create_time DESC";
                $criteria->with = array("a_author", "question");
                $criteria->condition = "a_author.id=".$expertId." and t.status=".Answer::STATUS_APPROVED." and pay=0";
                $lastAnswers = Answer::model()->findAll($criteria);
                if($lastAnswers != NULL){
                    return $lastAnswers;
                }
                return array();
            }
            throw new CException(Yii::t('app','Expert ID = "{id}" is invalid. Please make sure that you pass correct ID.',
					array('{id}'=>$expertId)));
        }

        /**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord){
//                            $this->create_time=time();
                            $dt = new DateTime();
                            $this->create_time = $dt->format(Answer::DATE_TIME_FORMAT);
                        }
			return true;
		}
		else
			return false;
	}
        
        
}
