<?php

/**
 * This is the model class for table "stock".
 *
 * The followings are the available columns in table 'stock':
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $image
 */
class Stock extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Stock the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'stock';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, url', 'required'),
            array('url', 'url', 'defaultScheme' => 'http'),
            array('image', 'file', 'on' => 'create', 'allowEmpty' => false, 'types' => 'jpg, gif, png'),
            array('image', 'file', 'on' => 'update', 'allowEmpty' => true, 'types' => 'jpg, gif, png'),
            array('title, url', 'length', 'max' => 255),
            array('image', 'length', 'max' => 50),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, title, url, image', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'image' => 'Image',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('image', $this->image, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    protected function beforeSave() {

        $uploadedFile = CUploadedFile::getInstance($this, 'image');

        if ($uploadedFile !== NULL) {

            if (!$this->isNewRecord) {
                if (file_exists('/uploads/stock/' . $this->image)) {
                    unlink('/uploads/stock/' . $this->image);
                }
            }

            $newFileName = uniqid() . '.' . pathinfo($uploadedFile->name, PATHINFO_EXTENSION);

            $image = Yii::app()->simpleImage->load($uploadedFile->tempName);
            //$image->resizeToWidth(294);
            //$image->resizeToHeight(169);
            //$image->save('uploads/stock/' . $newFileName);
	    copy($uploadedFile->tempName,'uploads/stock/' . $newFileName);

            $this->image = $newFileName;
        }
        
        return parent::beforeSave();
    }

    protected function afterDelete() {
        parent::afterDelete();
        if (file_exists('/uploads/stock/' . $this->image)) {
            unlink('/uploads/stock/' . $this->image);
        }
    }

}
