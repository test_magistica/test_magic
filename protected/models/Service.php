<?php

/**
 * This is the model class for table "service".
 *
 * The followings are the available columns in table 'service':
 * @property integer $id
 * @property string $notes
 * @property string $honorarium
 * @property string $period
 * @property string $volume
 * @property string $annotation
 * @property string $description
 * @property string $requirements
 * @property integer $expertID
 */
class Service extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Service the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'service';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('notes, honorarium, period, volume, annotation, description, requirements, categoryID', 'required'),
            array('image', 'file', 'allowEmpty' => true, 'types' => 'jpg,png,bmp,jpeg'),
            array('expertID, honorarium', 'numerical', 'integerOnly' => true),
            array('status, expertID, metaTitle, metaKey, metaDesc', 'default'),
            array('notes, honorarium, period, volume', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, notes, honorarium, period, volume, annotation, description, requirements, expertID, metaTitle, metaKey, metaDesc', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
	        'booking' => array(self::BELONGS_TO, 'Booking', 'id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'categoryID' => 'Категория',
            'notes' => 'Названия услуги',
            'honorarium' => 'Цена',
            'period' => 'Срок выполнения',
            'volume' => 'Обьем услуги',
            'annotation' => 'Краткое описание услуги',
            'description' => 'Полное описание услуги',
            'requirements' => 'Требования',
            'expertID' => 'Эксперт',
            'image' => 'Рисунок: 300px X 250px',
            'metaTitle' => 'Meta Title',
            'metaKey' => 'Meta Key',
            'metaDesc' => 'Meta Desc',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('notes', $this->notes, true);
        $criteria->compare('honorarium', $this->honorarium, true);
        $criteria->compare('period', $this->period, true);
        $criteria->compare('volume', $this->volume, true);
        $criteria->compare('annotation', $this->annotation, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('requirements', $this->requirements, true);
        $criteria->compare('expertID', $this->expertID);
        $criteria->compare('metaTitle', $this->metaTitle, true);
        $criteria->compare('metaKey', $this->metaKey, true);
        $criteria->compare('metaDesc', $this->metaDesc, true);
        //$criteria->compare('expertID',  Yii::app()->user->id);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function expert() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('notes', $this->notes, true);
        $criteria->compare('honorarium', $this->honorarium, true);
        $criteria->compare('period', $this->period, true);
        $criteria->compare('volume', $this->volume, true);
        $criteria->compare('annotation', $this->annotation, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('requirements', $this->requirements, true);
        //$criteria->compare('expertID',$this->expertID);
        $criteria->compare('status', $this->status);
        $criteria->compare('expertID', Yii::app()->user->id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function staticService() {
	    $crit = new CDbCriteria;
	    $crit->compare('expertID', Yii::app()->user->id);
	    $expertSS = ExpertStaticServices::model()->findAll($crit);
	    $ua = array();
	    foreach($expertSS as $row)
		    $ua[] = $row->serviceID;

        $criteria = new CDbCriteria;
        $criteria->addInCondition('id', $ua,true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    protected function afterDelete() {
        parent::afterDelete();
        if (file_exists("/uploads/service/" . $this->image))
            unlink("uploads/service/" . $this->image);
    }

    public function beforeSave() {
        if (Yii::app()->user->name == 'administrator')
            $this->status = 1;
        else
            $this->status = 0;
        if ($this->isNewRecord)
            $this->expertID = Yii::app()->user->id;

        $uploadedFile = CUploadedFile::getInstance($this, 'image');
        if ($uploadedFile !== NULL) {
            if (!$this->isNewRecord) {
                if (file_exists("/uploads/service/" . $this->image)) {
                    unlink("/uploads/service/" . $this->image);
                }
            }
            $image = Yii::app()->simpleImage->load($uploadedFile->tempName);
            $image->resizeToWidth(298);
            $image->resizeToHeight(249);
            $newFileName = uniqid() . '.' . pathinfo($uploadedFile->name, PATHINFO_EXTENSION);
            $image->save('uploads/service/' . $newFileName);
            $this->image = $newFileName;
        }
        return parent::beforeSave();
    }

}