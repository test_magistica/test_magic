<?php

/**
 * This is the model class for table "man_names".
 *
 * The followings are the available columns in table 'man_names':
 * @property string $id
 * @property string $name
 * @property string $history
 * @property string $description
 * @property string $numerology
 * @property string $symbols
 */
class ManNames extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ManNames the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'man_names';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>255),
			array('history, description, numerology, symbols', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, history, description, numerology, symbols', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'history' => 'History',
			'description' => 'Description',
			'numerology' => 'Numerology',
			'symbols' => 'Symbols',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('history',$this->history,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('numerology',$this->numerology,true);
		$criteria->compare('symbols',$this->symbols,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}