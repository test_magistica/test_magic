<?php

/**
 * This is the model class for table "faq_groups".
 *
 * The followings are the available columns in table 'faq_groups':
 * @property integer $id
 * @property string $gname
 * @property integer $parent_group
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Faq[] $faqs
 */
class FaqGroups extends CActiveRecord
{
    const STATUS_SHOW = 1;
    const STATUS_HIDE = 0;
    // Группа вопросов верхнего уровня
    const ROOT_GROUP = 0;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'faq_groups';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gname', 'required'),
			array('parent_group, status', 'numerical', 'integerOnly'=>true),
			array('gname', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, gname, parent_group, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'faqs' => array(self::HAS_MANY, 'Faq', array('fgroup' => 'id')),
//			'parent' => array(self::HAS_ONE, 'FaqGroups', array('parent_group'=>'id')),
			'nested' => array(
                            self::HAS_MANY, 
                            'FaqGroups',
                            array('parent_group'=>'id'),
//                            'with' => array("nested", "faqs")
                            ),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'gname' => 'Название группы вопросов',
			'parent_group' => 'Родительская группа',
			'status' => 'Показывать:',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('gname',$this->gname,true);
		$criteria->compare('parent_group',$this->parent_group);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FaqGroups the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
