<?php

/**
 * This is the model class for table "consultation".
 *
 * The followings are the available columns in table 'consultation':
 * @property integer $id
 * @property integer $userID
 * @property integer $expertID
 * @property string $image
 * @property integer $duration
 * @property string $phone
 * @property string $date
 * @property integer $total
 status 3 - не оплачена
 */
class Consultation extends CActiveRecord {

    //public $year;
    //public $month;
    //public $day;
    //public $hour;
    //public $minute;
    public $date_call;
    public $time_call;
    public $email;
    public $country;
    public $tele;
    public $guest_name;
    /**
     * Звонок отклонен пользователем или хз кем ещё.
     */
    const STATUS_CANCELED_BY_USER = 0;
    /**
     * Звонок забронирован.
     */
    const STATUS_WAIT = 1;
    /**
     * Звонок прошел успешно. ПРоверено через http://api.comtube.com/scripts/api/callback.php
     */
    const STATUS_CONSULTATION_SUCCEEDED = 2;
    /**
     * Забронированный звонок отменил эксперт.
     */
    const STATUS_CANCELED_BY_EXPERT = 3;
    const STATUS_ANSWER = "ANSWER";
    /**
     * Устанавливается когда эксперт начинает дозвон пользователю.
     */
    const STATUS_BEGIN = "BEGIN";
    /**
     * Занят/Отклонен
     */
    const STATUS_BUSY = "BUSY";
    /**
     * "Отменен"
     */
    const STATUS_CANCEL = "CANCEL";
    /**
     * Звонок сорвался. Неполадки на линии.
     */
    const STATUS_BREAK = "BREAK";
    /**
     * Не выяснил.
     */
    const STATUS_CONGESTION = "CONGESTION";
    /**
     * Звонок забронировал гость и оплатил его через платежные системы.
     */
    const STATUS_GUEST_PAID = "paid";
    /**
     * Звонок забронировал гость. Время звонка не оплачено.
     */
    const STATUS_GUEST_NO_PAID = "nopaid";

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Consultation the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'consultation';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            // array('duration, phone, month, day, hour, minute', 'required'),
            array('duration, phone, date_call, time_call, email', 'required'),
            array('duration','numerical'),
            array('phone', 'length', 'min'=> 3 , 'max' => 30),
            array('date_call','ValidatorDate'),
            array('phone','phoneValidate'),
            array('time_call','ValidatorTime'),
            array('guest_name','ValidatorGuest'),
            array('email','email'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, userID, expertID, image, duration, phone, date', 'safe', 'on' => 'search'),
        );
    }


	public function phoneValidate() {
		if (!preg_match("/^\+[0-9]{1,}$/", $this->phone)) {
			$this->addError('phone', 'Неверно задан номер телефона');
		}
	}
    
    public function ValidatorGuest() {
		if (Yii::app()->user->isGuest){
			if (empty($this->guest_name))
				$this->addError('guest_name','Не заполнено поле Имя.');
	    }
    }

	public function ValidatorDate($attribute, $params) {
		if (! empty ($this->date_call)) {
			$date =@explode('.',$this->date_call);

			$my_date = date('Y-m-d',mktime(0,0,0,(int)$date[1],(int)$date[0],(int)$date[2]));
			$now_date = date('Y-m-d');

			if(	$my_date < $now_date)
				$this->addError('date_call','Выбираемая дата не может быть меньше текущей.');

			$check = checkdate((int)$date[1],(int)$date[0],(int)$date[2]);
			if (! $check )
				$this->addError('date_call','Такой даты не существует.');
		}
	}

	public function ValidatorTime($attribute,$params){
		if (! empty($this->time_call)){
			$time = @explode(':',$this->time_call);
			$hour = 23 - (int)$time[0];
			$min = 59 - (int)$time[1];
			if (($hour < 0) || ($min < 0))
				$this->addError('time_call','Не верно задано время');
		}
	}

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id'        => 'ID',
            'userID'    => 'Пользователь',
            'expertID'  => 'Expert',
            'image'     => 'Изображение',
            'duration'  => 'Длительность разговора',
            'phone'     => 'Номер телефона',
            'date'      => 'Дата и время вызова',
            'total'     => 'Total',
            'status'    => 'Статус звонка',
            'cost'      => 'Стоимость',
            'date_call' => 'Дата звонка',
            'time_call' => 'Время звонка',
            'email'     =>'E-mail',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('userID', $this->userID);
        
        $criteria->addCondition('userID > 0 AND status<>"nopaid"');
        
        $criteria->compare('expertID', Yii::app()->user->id);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('duration', $this->duration);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('date', $this->date, true);
         
        //$criteria->order = '`date` desc';
        $criteria->order = "Field(`status`,'1','BREAK','BEGIN') desc,`date` desc"; 
        
        //$criteria->select = array('*','DATE_FORMAT(`date`,\'%d.%m.%Y %H:%i:%s\') as date');

        //$criteria->compare('total', $this->total);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
	        'pagination'=>array(
		        'pageSize'=>Yii::app()->params['bookingPerPage'],
	        ),
        ));
    }
    
    public function search_guest() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('guest_name', $this->guest_name);
        
        $criteria->compare('userID',0);

	    $criteria->addCondition('status<>"nopaid"');
        $criteria->compare('expertID', Yii::app()->user->id);
        $criteria->compare('image', $this->image, true);
        $criteria->compare('duration', $this->duration);
        $criteria->compare('phone', $this->phone, true);
        $criteria->compare('date', $this->date, true);
         
        $criteria->order = "Field(`status`,'paid','BREAK','BEGIN') desc,`date` desc"; 
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
	        'pagination'=>array(
		        'pageSize'=>Yii::app()->params['bookingPerPage'],
	        ),
        ));
    }

    public function beforeSave() {

        $date_call = @explode('.',$this->date_call);
        $time_call = @explode(':',$this->time_call);
        
        $this->date = $date_call[2]. '-' .$date_call[1]. '-' . $date_call[0] . ' ' . $time_call[0] . ':' .$time_call[1]. ':00';

        return parent::beforeSave();
    }

    protected function afterDelete() {
        parent::afterDelete();
        if (!empty($this->image) && file_exists("uploads/consultation/" . $this->image))
          unlink("uploads/consultation/" . $this->image);
    }

}