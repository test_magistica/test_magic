<?php

/**
 * This is the model class for table "calljournal".
 *
 * The followings are the available columns in table 'calljournal':
    tel_expert
    tel_client
    status
    duration
    cost
    
 */
class CallJournal extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Article the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'calljournal';
    }
    public function primaryKey()
    {
	return 'id';
    }

    
}