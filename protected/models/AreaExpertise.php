<?php

/**
 * This is the model class for table "AreaExpertise".
 *
 * The followings are the available columns in table 'AreaExpertise':
 * @property integer $id
 * @property integer $parentID
 * @property string $name
 * @property string $metaKey
 * @property string $metaDesc
 * @property string $metaTitle
 * @property integer $status
 * @property integer $sort
 */
class AreaExpertise extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AreaExpertise the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'аrea_expertise';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('parentID, name, metaKey, metaDesc, metaTitle', 'required'),
            array('parentID, status, sort', 'numerical', 'integerOnly' => true),
            array('name, metaKey, metaDesc, metaTitle', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, parentID, name, metaKey, metaDesc, metaTitle, status, sort', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'questions' => array(
                self::HAS_MANY,
                "Question",
                'аrea_expertise_id',
//                        'condition' => 't.id=questions.аrea_expertise_id'
            ),
            'countQuestion' => array(
                self::STAT,
                'Question',
                'area_expertise_id',
//                array(
//                    'condition' => 'pay=0'
//                )
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'parentID' => 'Parent',
            'name' => 'Name',
            'metaKey' => 'Meta Key',
            'metaDesc' => 'Meta Desc',
            'metaTitle' => 'Meta Title',
            'status' => 'Status',
            'sort' => 'Sort',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('parentID', $this->parentID);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('metaKey', $this->metaKey, true);
        $criteria->compare('metaDesc', $this->metaDesc, true);
        $criteria->compare('metaTitle', $this->metaTitle, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('sort', $this->sort);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    
    
    function translit($text) {
        preg_match_all('/./u', $text, $text);
        $text = $text[0];
        $simplePairs = array('а' => 'a', 'л' => 'l', 'у' => 'u', 'б' => 'b', 'м' => 'm', 'т' => 't', 'в' => 'v', 'н' => 'n', 'ы' => 'y', 'г' => 'g', 'о' => 'o', 'ф' => 'f', 'д' => 'd', 'п' => 'p', 'и' => 'i', 'р' => 'r', 'А' => 'A', 'Л' => 'L', 'У' => 'U', 'Б' => 'B', 'М' => 'M', 'Т' => 'T', 'В' => 'V', 'Н' => 'N', 'Ы' => 'Y', 'Г' => 'G', 'О' => 'O', 'Ф' => 'F', 'Д' => 'D', 'П' => 'P', 'И' => 'I', 'Р' => 'R',);
        $complexPairs = array('з' => 'z', 'ц' => 'c', 'к' => 'k', 'ж' => 'zh', 'ч' => 'ch', 'х' => 'kh', 'е' => 'e', 'с' => 's', 'ё' => 'jo', 'э' => 'eh', 'ш' => 'sh', 'й' => 'jj', 'щ' => 'shh', 'ю' => 'ju', 'я' => 'ja', 'З' => 'Z', 'Ц' => 'C', 'К' => 'K', 'Ж' => 'ZH', 'Ч' => 'CH', 'Х' => 'KH', 'Е' => 'E', 'С' => 'S', 'Ё' => 'JO', 'Э' => 'EH', 'Ш' => 'SH', 'Й' => 'JJ', 'Щ' => 'SHH', 'Ю' => 'JU', 'Я' => 'JA', 'Ь' => "", 'Ъ' => "", 'ъ' => "", 'ь' => "",);
        $specialSymbols = array("–" => "-", "-" => "-", "'" => "", "`" => "", "^" => "", " " => "-", '.' => '', '?'=>'', ',' => '', ':' => '', '"' => '', "'" => '', '<' => '', '>' => '', '«' => '', '»' => '', ' ' => '-',);
        $translitLatSymbols = array('a', 'l', 'u', 'b', 'm', 't', 'v', 'n', 'y', 'g', 'o', 'f', 'd', 'p', 'i', 'r', 'z', 'c', 'k', 'e', 's', 'A', 'L', 'U', 'B', 'M', 'T', 'V', 'N', 'Y', 'G', 'O', 'F', 'D', 'P', 'I', 'R', 'Z', 'C', 'K', 'E', 'S',);
        $simplePairsFlip = array_flip($simplePairs);
        $complexPairsFlip = array_flip($complexPairs);
        $specialSymbolsFlip = array_flip($specialSymbols);
        $charsToTranslit = array_merge(array_keys($simplePairs), array_keys($complexPairs));
        $translitTable = array();
        foreach ($simplePairs as $key => $val)
            $translitTable[$key] = $simplePairs[$key]; foreach ($complexPairs as $key => $val)
            $translitTable[$key] = $complexPairs[$key]; foreach ($specialSymbols as $key => $val)
            $translitTable[$key] = $specialSymbols[$key]; $result = "";
        $nonTranslitArea = false;
        foreach ($text as $char) {
            if (in_array($char, array_keys($specialSymbols))) {
                $result.= $translitTable[$char];
            } elseif (in_array($char, $charsToTranslit)) {
                if ($nonTranslitArea) {
                    $result.= "";
                    $nonTranslitArea = false;
                } $result.= $translitTable[$char];
            } else {
                if (!$nonTranslitArea && in_array($char, $translitLatSymbols)) {
                    $result.= "";
                    $nonTranslitArea = true;
                } $result.= $char;
            }
        } return strtolower(preg_replace("/[-]{2,}/", '-', $result));
    }
}
