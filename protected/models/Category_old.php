<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property integer $parentID
 * @property string $name
 * @property string $nameTitle
 * @property string $metaKey
 * @property string $metaDesc
 */
class Category extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Category the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'category';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('parentID, metaTitle, metaKey, metaDesc', 'default'),
            array('parentID', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, parentID, name, metaTitle, metaKey, metaDesc', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function getDropdownItems($parentId = 0, $level = 0) {
        $itemsFormatted = array();

        $items = Category::model()->findAllByAttributes(array(
            'parentID' => $parentId,
        ));

        foreach ($items as $item) {
            $itemsFormatted[$item->id] = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $level) . $item->name;
            $itemsFormatted = $itemsFormatted + $this->getDropdownItems($item->id, $level + 1);
        }

        return $itemsFormatted;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'parentID' => 'Родительская категория',
            'name' => 'Имя',
            'metaTitle' => 'Meta Title',
            'metaKey' => 'Meta Key',
            'metaDesc' => 'Meta Desc',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('parentID', $this->parentID);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('metaTitle', $this->metaTitle, true);
        $criteria->compare('metaKey', $this->metaKey, true);
        $criteria->compare('metaDesc', $this->metaDesc, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}