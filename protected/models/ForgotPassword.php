<?php

class ForgotPassword extends CFormModel {

    public $username;

    public function rules() {
        return array(
            // username are required
            array('username', 'required'),
            array('username', 'email')
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'username' => 'E-Mail',
        );
    }

}
