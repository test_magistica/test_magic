User-agent: *
Disallow: /site/contact
Disallow: /dreams*
Disallow: /forgot/password
Disallow: /horoscope/subscription
Disallow: /statistic.html
Disallow: /site/agreement.html
Disallow: /names*
Disallow: /service/booking*


Sitemap: http://magistika.com/sitemap.xml 
Host: magistika.com