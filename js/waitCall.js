$(document).ready(function () {
	$('#waitCall').wrap('<div style="z-index:500;position:absolute; left:0; top:0; width:100%; height:100%; background:#000; opacity:0.8;">');
	$('#waitCall').css({
		'z-index':'502',
		'position':'absolute',
		'top':'50%',
		'left':'40%',
		'font-size':'18px',
		'width':'405px',
		'text-align':'center'
	});
	$('#waitCall').append('<div class="close-waitCall"></div>');
	$('.close-waitCall').css({
		'display':'block',
		'position':'absolute',
		'top':'0',
		'right':'0',
		'cursor':'pointer',
		'width':'17px',
		'height':'17px',
		'background':'url(/images/close-pop.jpg) 0 0 no-repeat'
	});
	$('.close-waitCall').click(function(){
		$('#waitCall').unwrap();
		$('#waitCall').remove();
	});
	
});
		