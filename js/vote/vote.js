$(document).ready(function() {

    function VoteManager() {
        var maxR; // Максимальный рейтинг которого можно достичь
        var e_reiting; // текущий рейтинг
        var raiting_votes_id;
        this.calculateVote;
        var widthWidthOneStar = 17;
        this.initVotes = function() {
            $(".raiting").each(function() {
                // Выставляем текущий рейтинг пользователю
                initVote(
                        $(this).parent().find("input[name='totalMaxRaiting']").val(),
                        $(this).parent().find("input[name='displayRaiting']").val(),
                        $(this).parent().find("input[name='r_id']").val(),
                        $(this).parent().find("input[name='countVotes']").val(),
                        this
                        );
                if (!$(this).hasClass("vote-static")) {
                    bindHoverHandler(this);
                    bindMousemoveHandler(this);
                    bindClickHandler(this);
                }
            });
            $(".voting_scale, .voting_scale_static").each(function() {
                // Выставляем текущий рейтинг пользователю
                initVoteScale(
                        $(this).parent().find("input[name='voteScale']").val(),
                        $(this).parent().find("input[name='r_id']").val(),
                        this
                        );
                if (!$(this).hasClass("voting_scale_static")) {
                    bindHoverHandler(this);
                    bindMousemoveHandler(this);
                    bindClickHandler(this);
                }
            });
        };
        var bindHoverHandler = function(elem) {
            $(elem).on('hover',
                    function() {
                        $(this).find('#raiting_votes, #raiting_hover').toggle();
                    },
                    function() {
                        $(this).find('#raiting_votes, #raiting_hover').toggle();
                    }
            );
        };
        var bindMousemoveHandler = function(elem) {
            $(elem).on('mousemove', function(e) {
                // Определяем сколько баллов выбрал пользователь
                var user_votes = getUserVote(this, e);
                // обратите внимание переменная user_votes должна задаваться без var, т.к. в этом случае она будет глобальной и мы сможем к ней обратиться из другой ф-ции (нужна будет при клике на оценке.
                $(this).find('#raiting_hover').width(user_votes * 17);
            });
        };
        var bindClickHandler = function(elem) {
            $(elem).one('click', function(e) {
                $(this).off("hover");
                $(this).off("mousemove");
//                $(this).find('#raiting_votes, #raiting_hover').toggle();
                // Определяем сколько баллов выбрал пользователь
                var user_votes = getUserVote(this, e);
                var aId = jQuery(this).parents("div[id^='answer-']").attr('id').split('-')[1];
                jQuery.ajax({
                    method: "POST",
                    url: "/answer/addRaiting",
                    dataType: "json",
                    data: "id=" + aId + "&reit=" + user_votes, 
                    success: function(data, textStatus, jqXHR) {
                        var maxWidthOfStars = 83; // Ширина взята из css #raiting_votes
                        $("div[id^='answer-'], div[id^='expert-']").each(function() {
                            var author = $(this).find("div[id='raiting-" + data.author + "']");
                            var widthOneStar = 17;
                            if (author.length == 1) {
                                //Изменяем рейтинг эксперта
                                $(this).find(".raiting_info h5").text(data.exp_reit);
                                // Перерисовываем звездочки
                                var currWidth = parseFloat($(author).find("#raiting_votes").width());
                                var needToAddWidth = parseFloat((maxWidthOfStars * data.usermark) / data.maxRating);
                                if(currWidth + needToAddWidth <= maxWidthOfStars){
                                    $(author).find(".status #raiting_votes").width(currWidth + needToAddWidth);
                                }
                                $(author).find("#raiting_votes").width((data.exp_reit / data.countVotes) * widthOneStar);
                            }

                        });
//                        $("#answer-" + data.answerid + " .vote_control").html($("<span>").addClass("voted").append(data.result + '&nbsp;<span class="vote_rate">' + data.usermark + '</span><div class="clear"></div>'));
                    },
                    error: function(data, textStatus, jqXHR) {
                        console.log(data);
                    }
                });
            });
        };

        var getUserVote = function(elem, e) {
            var widht_votes = e.pageX - $(elem).offset().left;
            if (widht_votes <= 0)
                widht_votes = 1;
            return Math.ceil(widht_votes / 17);
        };



        var initVote = function(maxReiting, currentReiting, id, countVotes, elem) {
//                console.log($(this));
                maxR = countVotes; // Максимальный рейтинг которого может достичь эксперт
                e_reiting = parseFloat(currentReiting / maxR); // рейтинг эксперта
                raiting_votes_id = id;
    //            var maxWidthOfStars = 83; // Ширина взята из css #raiting_votes
    //            var widthToDisplay = parseFloat((maxWidthOfStars / maxR));
                $(elem).find('.raiting_votes-' + raiting_votes_id).width(e_reiting * widthWidthOneStar);
            
        };
        var initVoteScale = function(vote, id, elem) {
             $(elem).find('.raiting_votes-' + id).width(vote * widthWidthOneStar);
        };
    }

    $(document).ready(function() {
        $.voteManager = new VoteManager();
        $.voteManager.initVotes();
    });
});