$(function(){		
	// ����������� � ��������
	$('input.check, input.radio').ezMark();
	
	// check all functionality
	$('#checkAllBtn').click(function(e) {
		$('input[name^="item"]').each(function() {
			$(this).attr({"checked":"checked"});
			$(this).trigger('change');
		});
		return false;
	});

	// uncheck all functionality
	$('#uncheckAllBtn').click(function(e) {
		$('input[name^="item"]').each(function() {
			$(this).removeAttr('checked');
			$(this).trigger('change');
		});
		return false;
	});
});