$(function() {
    // Selects
    $('.selectBlock').sSelect();
    $('.selectBlock2').sSelect();


    // Slider
    $('#sliderbox').codaSlider({
        autoSlide: true,
        autoHeight: false,
        slideEaseDuration: 400,
        dynamicTabsPosition: "bottom",
        panelTitleSelector: "span.title",
        slideEaseFunction: "easeInOutExpo"
    });

    // Tabs
    $("#tabsbox").tabs({hide: {effect: "fadeOut", duration: 100},collapsible: true});
    /*$("#tabsbox > ul > li a").on("click", function(){
        var tabKey = $(this).attr("id").split("-")[2];
        //alert(tabKey);
        var tabUrl = window.extrasensTabs[tabKey];
	    //alert(tabUrl)
        $.ajax({
            url : tabUrl,
            type : 'GET',
            dataType : "html",
            success : function(data, textStatus, jqXHR){
                $("#ui-tabs-" + tabKey).html(data);
            }
		});
    });**/
});

(function($) {
$(function() {

    $('ul.tabs').on('click', 'li:not(.current)', function() {
        $(this).addClass('current').siblings().removeClass('current')
            .parents('div.tab-container').find('div.box').eq($(this).index()).fadeIn(150).siblings('div.box').hide();
    })

    var tabIndex = window.location.hash.replace('#tab','')-1;
    if (tabIndex != -1) $('ul.tabs li').eq(tabIndex).click();

    $('a[href*=#tab]').click(function() {
        var tabIndex = $(this).attr('href').replace(/(.*)#tab/, '')-1;
        $('ul.tabs li').eq(tabIndex).click();
    });

})
})(jQuery);


function TeleField(str_tel) {
	//$.mask.definitions['~']='[+-]';

	$("#tele_field").inputmask( str_tel+"9999999999999");


}